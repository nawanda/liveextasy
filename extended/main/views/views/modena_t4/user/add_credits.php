    
<html>
	<head>
	<style type="text/css">
<!--
@charset "utf-8";
#cp_add_credits{

	font-family:Verdana !important;
}
.bt {
	display: block;
font-family: "Nunito",sans-serif;
text-align: center;
width: 110px;
height: 30px;
background: #F7F7F7 none repeat scroll 0% 0%;
color: #000;
border-radius: 4px;
transition: all 0.2s ease-in 0s;
}

#cp_add_credits .add_credits_top{
	margin-left:2px;
	background:#3b3e3e;
	border-radius:0px 4px 4px 0px;
}

#cp_add_credits .add_credits_top .steps_top{
	position:relative;
}

#cp_add_credits .add_credits_top .steps_top a{
	display:block;
	float:left;
	width:180px;
	margin-left:10px;
	height:44px;
	padding:10px 10px 10px 14px;
	background:#393c3c;
	border-radius:4px;
}

#cp_add_credits .add_credits_top .steps_top a:hover{
	text-decoration:none;
}

#cp_add_credits .add_credits_top .steps_top a.no_cursor{
	cursor:default!important;
}

#cp_add_credits .add_credits_top .steps_top a.no_cursor:hover{
	cursor:default!important;
}

#cp_add_credits .add_credits_top .steps_top a h2{
	font-size:13px;
	color:#6d6d6d;
	text-transform:uppercase;
	line-height:44px;
	font-family:'Nunito',sans-serif;
}

#cp_add_credits .add_credits_top .steps_top a p{
	font-size:12px;
	color:#6d6d6d;
}

#cp_add_credits .add_credits_top .steps_top a.goto1{
	margin-left:0px;
}

#cp_add_credits .add_credits_top .steps_top span.arrow{
	display:block;
	width:25px;
	height:68px;
	background:url(../../images/users/add-credits/add_credits_arrow.png) no-repeat 1px -3px;
}

#cp_add_credits .add_credits_top .steps_top span.arrow_active{
	background:url(../../images/users/add-credits/add_credits_arrow.png) no-repeat -30px -3px;
}

#cp_add_credits .add_credits_top .steps_top span.arrow_active2{
	background:url(../../images/users/add-credits/add_credits_arrow.png) no-repeat -57px -3px;
}

#cp_add_credits .add_credits_top .steps_top span.arrow_active3{
	background:url(../../images/users/add-credits/add_credits_arrow.png) no-repeat -57px -3px;
}

#cp_add_credits .add_credits_top .steps_top span.arrow_active4{
	background:url(../../images/users/add-credits/add_credits_arrow.png) no-repeat -57px -3px;
}

#cp_add_credits .add_credits_top .steps_top span.arrow_1{
	position:absolute;
	left:190px;
}

#cp_add_credits .add_credits_top .steps_top span.arrow_2{
	position:absolute;
	left:403px;
}

#cp_add_credits .add_credits_top .steps_top span.arrow_3{
	position:absolute;
	left:618px;
}

#cp_add_credits .add_credits_top .steps_top span.arrow_4{
	position:absolute;
	left:833px;
}

#cp_add_credits .add_credits_top .steps_top a.active{
	background:#4b4b4b;
}

#cp_add_credits .add_credits_top .steps_top a.active h2{
	color:#fff;
}

#cp_add_credits .add_credits_top .steps_top a.active p{
	color:#fff;
}

#cp_add_credits .add_credits_content .status{
	margin-top:10px;
	margin-bottom:10px;
}

#cp_add_credits .add_credits_content .status p{
	font-size:12px;
	line-height:18px;
}

#cp_add_credits .add_credits_content{
	margin-top:2px;
	position:relative;
}

#cp_add_credits .add_credits_content .area_desc{
	padding:15px;
	margin-bottom:18px;
	font-size:13px;
	text-align:center;
	font-family:verdana;
	line-height:17px;
}

#cp_add_credits .add_credits_content .area_desc h1{
	line-height:30px;
	font-family:'Nunito',sans-serif;
}

#cp_add_credits .add_credits_content .area_desc p{
	padding-bottom:0px;
	font-family:'Nunito',sans-serif;
	color:#A3A3A3;
}

#cp_add_credits .add_credits_content .loading{
	display:none;
	position:absolute;
	top:20px;
	z-index:999;
	left:50%;
	margin-left:-100px;
	padding-bottom:20px;
	width:200px;
	background:rgba(0,0,0,0.6);
}

#cp_add_credits .add_credits_content .loading span{
	display:block;
	text-align:center;
	color:#fff;
	margin-top:20px;
	margin-bottom:20px;
}

#cp_add_credits .add_credits_content .loading img{
	display:block;
	margin:0 auto;
}

#cp_add_credits .add_credits_content .steps_bottom{
	width:auto;
	border-radius:4px;
	margin-top:18px;
	margin-left:2px;
	margin-bottom:6px;
	position:relative;
}

#cp_add_credits .add_credits_content .steps_bottom span.title{
	color:#6d6d6d;
}

#cp_add_credits .add_credits_content .steps_bottom p.geo_location{
	margin-top:10px;

	color:#64CF7E;
}

#cp_add_credits .add_credits_content .steps_bottom p.geo_location a{
	text-decoration:underline;
	color:#fff;
}

#cp_add_credits .add_credits_content .steps_bottom p.geo_location a:hover{
	color:#999;
}

#cp_add_credits .add_credits_content .steps_bottom p{
	line-height:20px;
	margin-top:6px;
	font-size:16px;
}

#cp_add_credits .add_credits_content .steps_bottom .package_wrapper{
	background:#4e504f;
	padding:8px;
	border-radius:4px;
display: inline-block;
	margin-right:10px;
	margin-bottom:8px;
	position:relative;
}

#cp_add_credits .add_credits_content .steps_bottom .packages{
	width:163px;
	border-radius:4px;
	background:#fff;
	cursor:pointer;
	-webkit-box-shadow:2px 3px 14px 1px rgba(0,0,0,0.75);
	-moz-box-shadow:2px 3px 14px 1px rgba(0,0,0,0.75);
	box-shadow:2px 3px 14px 1px rgba(0,0,0,0.75);
}

#cp_add_credits .add_credits_content .steps_bottom .package_7{
	margin-right:0px;
}

#cp_add_credits .add_credits_content .steps_bottom .packages .package_bottom{
	padding:0px 10px 10px 10px;
	border-radius:0px 0px 4px 4px;
}

#cp_add_credits .add_credits_content .steps_bottom .packages .package_top{
	padding:10px 10px 0px 10px;
}

#cp_add_credits .add_credits_content .steps_bottom .packages .package_top .package_text{
	margin-top:10px;
}

#cp_add_credits .add_credits_content .steps_bottom .packages .package_top .package_text p{
	text-align:center;
	color:#000;
	font-size:12px;
	font-family:'Lato',sans-serif;
}

#cp_add_credits .add_credits_content .steps_bottom .packages .package_top hr{
	margin-top:12px;
	border-top:1px solid #dae8e8;
	border-bottom:0px;
}

#cp_add_credits .add_credits_content .steps_bottom .packages .package_bottom hr{
	border-top:1px solid #51c1c5;
	border-bottom:0px;
}

#cp_add_credits .add_credits_content .packages_inner{
	width:942px;
	margin:0 auto;
}

#cp_add_credits .add_credits_content .steps_bottom .packages a.buy{
	display:block;
	font-family:'Nunito',sans-serif;
	margin:10px auto 0px;
	text-align:center;
	width:110px;
	height:30px;
	line-height:30px;
	background:#f7f7f7;
	color:#000;
	border-radius:4px;
	-webkit-transition:all .2s ease-in;
	-moz-transition:all .2s ease-in;
	-o-transition:all .2s ease-in;
	transition:all .2s ease-in;
}

#cp_add_credits .add_credits_content .steps_bottom .packages a.buy:hover{
	text-decoration:none;
	background:#dddbdb;
	-webkit-transition:all .2s ease-in;
	-moz-transition:all .2s ease-in;
	-o-transition:all .2s ease-in;
	transition:all .2s ease-in;
}

#cp_add_credits .add_credits_content .steps_bottom .packages span.credits{
	font-size:18px;
	letter-spacing:0px;
	font-weight:normal;
	display:block;
	text-align:center;
	color:#01a3b0;
}

#cp_add_credits .add_credits_content .steps_bottom .packages span.credits span{
	font-weight:bold;
	letter-spacing:-6px;
	font-size:45px;
	display:block;
	line-height:54px;
	color:#01a3b0;
}

#cp_add_credits .add_credits_content .steps_bottom .packages span.value{
	font-size:30px;
	font-family:'Nunito', sans-serif;
	font-weight:bold;
	color:#fff;
	display:block;
	text-align:center;
}

#cp_add_credits .add_credits_content .steps_bottom .packages span.value span{
	font-size:18px;
	font-family:'Nunito',sans-serif;
	font-weight:normal;
}

#cp_add_credits .add_credits_content .steps_bottom .packages input[type="radio"]{
	margin-top:9px;
	cursor:pointer;
}

#cp_add_credits .add_credits_content .steps_bottom .package_1 .package_bottom{
	background:#01a3b0;
}

#cp_add_credits .add_credits_content .steps_bottom .package_2 .package_bottom{
	background:#028996;
}

#cp_add_credits .add_credits_content .steps_bottom .package_3 .package_bottom{
	background:#027782;
}

#cp_add_credits .add_credits_content .steps_bottom .package_4 .package_bottom{
	background:#016c76;
}

#cp_add_credits .add_credits_content .steps_bottom .package_5 .package_bottom{
	background:#015a62;
}

#cp_add_credits .add_credits_content .steps_bottom .package_6 .package_bottom{
	background:#01a3b0;
}

#cp_add_credits .add_credits_content .steps_bottom .package_7 .package_bottom{
	background:#01a3b0;
}

#cp_add_credits .add_credits_content .steps_bottom .package_8 .package_bottom{
	background:#01a3b0;
}

#cp_add_credits .add_credits_content .steps_bottom .package_1 .strip{
	height:17px;
	background:url(<?php echo main_url()?>assets/modena_t4/images/buy/packages.png) no-repeat 0px -102px;
}

#cp_add_credits .add_credits_content .steps_bottom .package_2 .strip{
	height:17px;
	background:url(<?php echo main_url()?>assets/modena_t4/images/buy/packages.png) no-repeat 0px -75px;
}

#cp_add_credits .add_credits_content .steps_bottom .package_3 .strip{
	height:17px;
	background:url(<?php echo main_url()?>assets/modena_t4/images/buy/packages.png) no-repeat 0px -49px;
}

#cp_add_credits .add_credits_content .steps_bottom .package_4 .strip{
	height:17px;
	background:url(<?php echo main_url()?>assets/modena_t4/images/buy/packages.png) no-repeat 0px -24px;
}

#cp_add_credits .add_credits_content .steps_bottom .package_5 .strip{
	height:17px;
	background:url(<?php echo main_url()?>assets/modena_t4/images/buy/packages.png) no-repeat 0px 1px;
}

#cp_add_credits .add_credits_content .steps_bottom .package_active span.credits{
	color:#cf457e;
	letter-spacing:0px;
}

#cp_add_credits .add_credits_content .steps_bottom .package_active span.credits span{
	color:#cf457e;
	letter-spacing:-6px;
}

#cp_add_credits .add_credits_content .steps_bottom .package_active .package_bottom{
	background:#cf457e;
}

#cp_add_credits .add_credits_content .steps_bottom .package_active .strip{
	background:url(<?php echo main_url()?>assets/modena_t4/images/buy/packages.png) no-repeat 0px -130px;
}

#cp_add_credits .add_credits_content .steps_bottom .packages:hover span.credits{
	color:#cf457e;
	letter-spacing:0px;
}

#cp_add_credits .add_credits_content .steps_bottom .packages:hover span.credits span{
	color:#cf457e;
	letter-spacing:-6px;
}

#cp_add_credits .add_credits_content .steps_bottom .packages:hover .package_bottom{
	background:#cf457e;
}

#cp_add_credits .add_credits_content .steps_bottom .packages:hover .strip{
	background:url(<?php echo main_url()?>assets/modena_t4/images/buy/packages.png) no-repeat 0px -130px;
}

#cp_add_credits .add_credits_content .steps_bottom .payment_button_wrapper{
	background:#4e504f;
	position:relative;
	width:215px;
	padding:8px;
	border-radius:4px;
	float:left;
	margin-right:10px;
	margin-bottom:8px;
}

#cp_add_credits .add_credits_content .steps_bottom .payment_button_wrapper .payment_box{
	width:215px;
	min-height:170px;
	border-radius:4px;
	background:#fff;
	-webkit-box-shadow:2px 3px 14px 1px rgba(0,0,0,0.75);
	-moz-box-shadow:2px 3px 14px 1px rgba(0,0,0,0.75);
	box-shadow:2px 3px 14px 1px rgba(0,0,0,0.75);
}

#cp_add_credits .add_credits_content .steps_bottom .payment_button_wrapper .payment_box .top{
	padding-top:24px;
}

#cp_add_credits .add_credits_content .steps_bottom .payment_button_wrapper .payment_box .top .payment_image{
	margin-bottom:22px;
	margin-top:20px;
}

#cp_add_credits .add_credits_content .steps_bottom .payment_button_wrapper .payment_box .top h4{
	text-align:center;
	margin-bottom:3px;
	color:#3D3D3D;
	font-size:16px;
	font-family:'Nunito',sans-serif;
}

#cp_add_credits .add_credits_content .steps_bottom .payment_button_wrapper .payment_box .top .subtitle{
	color:#999;
	font-size:10px;
	text-align:center;
}

#cp_add_credits .add_credits_content .steps_bottom .payment_button_wrapper .payment_box .top hr{
	margin:0 auto 8px;
	width:210px;
	border-top:1px solid rgb(219,219,219);
}

#cp_add_credits .add_credits_content .steps_bottom .payment_button_wrapper .payment_box .top img{
	display:block;
	margin:0 auto 12px;
}

#cp_add_credits .add_credits_content .steps_bottom .payment_button_wrapper .payment_box .bottom{
	padding:2px;
	margin-top:18px;
	height:68px;
}

#cp_add_credits .add_credits_content .steps_bottom .payment_button_wrapper .payment_box .bottom a{
	display:block;
	font-family:'Nunito',sans-serif;
	margin:0px auto 0px;
	text-align:center;
	width:211px;
	height:68px;
	line-height:68px;
	background:#01a3b1;
	font-weight:bold;
	font-size:15px;
	color:#fff;
	-webkit-transition:all .2s ease-in;
	-moz-transition:all .2s ease-in;
	-o-transition:all .2s ease-in;
	transition:all .2s ease-in;
}

#cp_add_credits .add_credits_content .steps_bottom .payment_button_wrapper .payment_box:hover a{
	text-decoration:none;
	background:#b4477b;
	-webkit-transition:all .2s ease-in;
	-moz-transition:all .2s ease-in;
	-o-transition:all .2s ease-in;
	transition:all .2s ease-in;
}

#cp_add_credits .add_credits_content .steps_bottom .payment_button_wrapper .payment_box:hover{
	cursor:pointer;
}

#cp_add_credits .add_credits_content .steps_bottom .payment_button_wrapper .payment_box .input_radio .right .brand_mini{
	display:block;
	margin-top:1px;
	width:20px;
	height:14px;
	float:left;
	margin-right:5px;
}

#cp_add_credits .add_credits_content .steps_bottom .overlay{
	position:absolute;
	z-index:999;
	top:0px;
	left:0px;
	width:100%;
	height:100%;
	background:rgba(80,80,80,0.5);
}

#cp_add_credits .add_credits_content .step_3 .step3_wrapper{
	width:688px;
	margin:0 auto;
}

#cp_add_credits .add_credits_content .step_3 p{
	padding-bottom:8px;
}

#cp_add_credits .add_credits_content .step_3 span.title{
	padding-bottom:8px;
	display:block;
}

#cp_add_credits .add_credits_content .step_3 .pagseguro{
	display:none;
}

#cp_add_credits .add_credits_content .step_3 .billet{
	display:none;
}

#cp_add_credits .add_credits_content .step_3 .debit{
	display:none;
}

#cp_add_credits .add_credits_content .step_3 .creditcard{
	display:none;
}

#cp_add_credits .add_credits_content .step_3 h2.conclusion_text{
	font-size:16px;
	color:#ACACAC;
	text-align:center;
	padding:20px 0px 20px 0px;
}

#cp_add_credits .add_credits_content .step_3 a.edit_button{
	display:block;
	width:62px;
	padding-left:4px;
	position:absolute;
	top:6px;
	right:6px;
	height:18px;
	line-height:18px;
	color:#c0c0c0;
	background:#4c4d4d;
	border-top:1px solid #646464;
	border-left:1px solid #5C5C5C;
	border-bottom:1px solid #494949;
	font-size:8px;
	font-weight:bold;
}

#cp_add_credits .add_credits_content .step_3 a.edit_button:hover{
	text-decoration:none;
}

#cp_add_credits .add_credits_content .step_3 a.edit_button span{
	display:block;
	float:left;
	width:16px;
	height:16px;
	margin-top:1px;
	margin-left:4px;
	background:url(../../images/common/model-profile/icons_profile.png) no-repeat -59px -37px;
}

#cp_add_credits .add_credits_content .step_3 .creditcard_brands .radio_image{
	margin-left:8px;
	margin-bottom:2px;
}

#cp_add_credits .add_credits_content .step_3 .creditcard_brands .radio_image.first{
	margin-left:0px;
}

#cp_add_credits .add_credits_content .step_3 .cofre_text{
	float:left;
	display:block;
	font-size:10px;
	height:48px;
	line-height:48px;
	margin-left:10px;
}

#cp_add_credits .add_credits_content .cofre_list .form_content.left{
	width:677px;
}

#cp_add_credits .add_credits_content .cofre_list .form_content.right{
	display:none;
}

#cp_add_credits .add_credits_content .cofre_list.cofre_active .form_content.left{
	width:447px;
}

#cp_add_credits .add_credits_content .cofre_list.cofre_active .form_content.right{
	display:block;
}

#cp_add_credits .add_credits_content #finalize_creditcard #parcelas{
	width:200px;
	height:48px;
}

#cp_add_credits .billet_iframe{
	width:800px;
	margin:24px auto;
}

#cp_add_credits .billet_iframe iframe{
	width:800px;
	height:500px;
	border:0px;
}

#cp_add_credits .billet_result{
	width:800px;
	margin-top:7px;
}

#cp_add_credits .billet_result a.js_submit{
	width:383px;
}

#cp_add_credits .debit_result{
	width:800px;
	margin-top:40px;
}

#cp_add_credits .debit_result a.js_submit{
	width:383px;
}

#cp_add_credits .add_credits_content .add_credits_brands .radio_image{
	width:105px;
	height:58px;
	background:#8c8c8c;
	border:0px;
	border-radius:0px;
	margin-bottom:0px;
}

#cp_add_credits .add_credits_content .add_credits_brands .radio_image.active{
	background:#fff;
}

#cp_add_credits .add_credits_content .add_credits_brands .radio_image input[type=radio]{
	left:12px;
	top:22px !important;
	right:auto;
}

#cp_add_credits .add_credits_content .add_credits_brands .radio_image span{
	margin-left:38px;
	margin-top:13px;
}

#cp_add_credits .add_credits_content .add_credits_brands .radio_image span.visa,#creditcard_table label span.visa{
	background:url('../../images/users/add-credits/add_credits_icons_v2.png') no-repeat 0px 0px;
}

#cp_add_credits .add_credits_content .add_credits_brands .radio_image span.mastercard,#creditcard_table label span.mastercard{
	background:url('../../images/users/add-credits/add_credits_icons_v2.png') no-repeat -113px 0px;
}

#cp_add_credits .add_credits_content .add_credits_brands .radio_image span.amex,#cp_add_credits .add_credits_content .add_credits_brands .radio_image span.americanexpress,#creditcard_table label span.amex{
	background:url('../../images/users/add-credits/add_credits_icons_v2.png') no-repeat -170px 0px;
}

#cp_add_credits .add_credits_content .add_credits_brands .radio_image span.diners,#creditcard_table label span.diners{
	background:url('../../images/users/add-credits/add_credits_icons_v2.png') no-repeat -57px 0px;
}

#cp_add_credits .add_credits_content .add_credits_brands .radio_image span.hipercard,#creditcard_table label span.hipercard{
	background:url('../../images/users/add-credits/add_credits_icons_v2.png') no-repeat -225px 0px;
}

#cp_add_credits .add_credits_content .add_credits_brands .radio_image span.elo,#creditcard_table label span.elo{
	background:url('../../images/users/add-credits/add_credits_icons_v2.png') no-repeat -280px 0px;
}

#cp_add_credits .add_credits_content .add_credits_brands .cofre_text{
	height:58px;
	line-height:58px;
	font-size:15px;
}

#cp_add_credits .add_credits_content .add_credits_debit_brands .radio_image{
	width:128px;
	height:58px;
	background:#8c8c8c;
	border:0px;
	border-radius:0px;
	margin-bottom:0px;
}

#cp_add_credits .add_credits_content .add_credits_debit_brands .radio_image.active{
	background:#fff;
}

#cp_add_credits .add_credits_content .add_credits_debit_brands .radio_image input[type=radio]{
	left:10px;
	top:22px !important;
	right:auto;
}

#cp_add_credits .add_credits_content .add_credits_debit_brands .radio_image span{
	margin-left:30px;
	margin-top:13px;
	width:30px;
	float:left;
}

#cp_add_credits .add_credits_content .add_credits_debit_brands .radio_image i{
	font-style:normal;
	line-height:58px;
	margin-left:4px;
	font-size:10px;
	font-family:'Nunito';
	font-weight:bold;
	color:#fff;
}

#cp_add_credits .add_credits_content .add_credits_debit_brands .radio_image.active i{
	color:#1a1c1c;
}

#cp_add_credits .add_credits_content .add_credits_debit_brands .radio_image span.bradesco{
	background:url(../../images/users/add-credits/add_credits_icons_v2.png) no-repeat -31px -35px;
}

#cp_add_credits .add_credits_content .add_credits_debit_brands .radio_image span.itau{
	background:url(../../images/users/add-credits/add_credits_icons_v2.png) no-repeat 0px -35px;
}

#cp_add_credits .add_credits_content .add_credits_debit_brands .radio_image span.bancodobrasil{
	background:url(../../images/users/add-credits/add_credits_icons_v2.png) no-repeat -64px -35px;
}

#cp_add_credits .add_credits_content .add_credits_debit_brands .radio_image span.banrisul{
	background:url(../../images/users/add-credits/add_credits_icons_v2.png) no-repeat -96px -35px;
}

#cp_add_credits .add_credits_content .add_credits_debit_brands .radio_image span.hsbc{
	background:url(../../images/users/add-credits/add_credits_icons_v2.png) no-repeat -128px -35px;
}

#cp_add_credits .add_credits_content .add_credits_debit_brands .radio_image span.Bradesco{
	background:url(../../images/users/add-credits/add_credits_icons_v2.png) no-repeat -31px -35px;
}

#cp_add_credits .add_credits_content .add_credits_debit_brands .radio_image span.Itau{
	background:url(../../images/users/add-credits/add_credits_icons_v2.png) no-repeat 0px -35px;
}

#cp_add_credits .add_credits_content .add_credits_debit_brands .radio_image span.BancoDoBrasil{
	background:url(../../images/users/add-credits/add_credits_icons_v2.png) no-repeat -64px -35px;
}

#cp_add_credits .add_credits_content .add_credits_debit_brands .radio_image span.Banrisul{
	background:url(../../images/users/add-credits/add_credits_icons_v2.png) no-repeat -96px -35px;
}

#cp_add_credits .add_credits_content .step_active .input_content{
	margin-bottom:10px;
}

#cp_add_credits .add_credits_content .step_active .input_content label{
	display:block;
	font-size:13px;
	color:#6d6d6d;
}

#cp_add_credits .add_credits_content .step_active .input_content input[type="text"]{
	width:250px;
	height:26px;
	padding:5px 0px 5px 8px;
	color:#ACACAC;
	background:#6d6d6d;
	border:1px solid #353535;
	border-radius:3px;
	font-size:14px;
}

#cp_add_credits .add_credits_content .step_active .input_content select{
	width:262px;
	height:38px;
	color:#ACACAC;
	background:#6d6d6d;
	border:1px solid #353535;
	border-radius:3px;
	font-size:14px;
}

html.gecko #cp_add_credits .add_credits_content .input_content select{
	padding-top:10px;
}

#cp_add_credits .add_credits_content .step_active .input_content label{
	color:#fff;
	padding-bottom:4px;
}

#cp_add_credits .add_credits_content .step_active .input_content input[type="text"]{
	background:#909090;
	color:#fff;
}

#cp_add_credits .add_credits_content .step_active .input_content select{
	background:#909090;
	color:#fff;
}

#cp_add_credits .add_credits_content .step_active .step_3_box p{
	color:#bcbcbc;
	font-size:12px;
}

#cp_add_credits .add_credits_content .step_3 a.register{
	width:688px;
}

html.ie9 #cp_add_credits .add_credits_top .steps_top span.arrow_1{
	left:226px;
}

html.ie1 #cp_add_credits .add_credits_top .steps_top span.arrow_1{
	left:226px;
}

#cp_add_credits .highlight{
	background-color:#B4B4B4;
	color:#000;
	padding:3px;
}

#cp_add_credits .highlight a{
	color:#000;
	text-decoration:underline
}

#cp_add_credits .highlight a:hover{
	color:#FFF;
	text-decoration:none
}

#cp_add_credits .highlight.green{
	background:#03e00c;
	padding:10px !important;
	text-align:center;
	border-radius:3px;
	color:#000 !important;
}

#cp_add_credits .highlight.yellow{
	background:yellow;
	padding:10px !important;
	text-align:center;
	border-radius:3px;
	color:#000 !important;
}

#creditcard_table{
	margin-bottom:20px;
	border-spacing:0;
}

#creditcard_table tbody tr{
	cursor:pointer;
}

#creditcard_table tr th{
	text-align:left;
	color:#888989;
	font-family:Verdana;
	font-size:12px;
	padding-bottom:8px;
	padding-left:8px;
	font-weight:normal;
}

#creditcard_table tr td{
	line-height:32px;
	background:#3b3e3e;
	padding:8px;
	color:#939393;
	font-family:'Nunito';
	font-size:14px;
	border-bottom:10px solid #1a1c1c;
}

#creditcard_table tr td:first-child{
	border-right:2px solid #1a1c1c;
}

#creditcard_table tr.active td{
	color:#fff;
	font-weight:bold;
	background:#595959;
}

#creditcard_table tr:hover td{
	background:#595959;
}

#creditcard_table tr td a{
	font-size:13px;
}

#creditcard_table tr td label{
	cursor:pointer;
}

#creditcard_table tr.active td a:hover{
	text-decoration:none;
	cursor:default;
}

#cofre_submit_btn{
	padding:3px;
	width:100%;
	float:right;
}

#cofre_submit_btn a{
	font-size:14px;
	width:100%;
}

#cofre_submit_btn i{
	font-style:normal;
}

#finalize_purchase{
	width:960px;
	margin:50px auto 50px;
	padding:20px;
	background:#414242;
	border-radius:2px;
}

#finalize_purchase .status_image{
	width:92px;
	height:86px;
	padding:18px 30px 18px 30px;
	background:#353535;
}

#finalize_purchase .status_image div{
	width:100%;
	height:100%;
}

#finalize_purchase .status_image .approved{
	background:url(../../images/users/add-credits/finalize_purchase_icons.png) no-repeat center 8px;
}

#finalize_purchase .status_image .rejected{
	background:url(../../images/users/add-credits/finalize_purchase_icons.png) no-repeat center -185px;
}

#finalize_purchase .status_image .pending{
	background:url(../../images/users/add-credits/finalize_purchase_icons.png) no-repeat center -92px;
}

#finalize_purchase .status_image .review{
	background:url(../../images/users/add-credits/finalize_purchase_icons.png) no-repeat center -92px;
}

#finalize_purchase .status_image .chargebacked{
	background:url(../../images/users/add-credits/finalize_purchase_icons.png) no-repeat center -185px;
}

#finalize_purchase .status_image .blocked{
	background:url(../../images/users/add-credits/finalize_purchase_icons.png) no-repeat center -185px;
}

#finalize_purchase .finalize_purchase_right{
	padding-left:170px;
}

#finalize_purchase .finalize_purchase_right h4{
	text-align:center;
	font-family:'Nunito';
	font-size:20px;
	color:#fff;
	padding:10px;
	margin-bottom:14px;
}

#finalize_purchase .approved .finalize_purchase_right h4{
	background:#4ab112;
}

#finalize_purchase .rejected .finalize_purchase_right h4{
	background:#dc4766;
}

#finalize_purchase .pending .finalize_purchase_right h4{
	background:#c1a820;
}

#finalize_purchase .review .finalize_purchase_right h4{
	background:#c1a820;
}

#finalize_purchase .chargebacked .finalize_purchase_right h4{
	background:#dc4766;
}

#finalize_purchase .blocked .finalize_purchase_right h4{
	background:#dc4766;
}

#finalize_purchase p{
	color:#c4c4c4;
	font-size:14px;
	line-height:24px;
	margin-top:25px;
}

#finalize_purchase b{
	color:#fff;
}

#finalize_purchase .purchase_data{
	width:760px;
	margin:0 auto 0px;
}

#finalize_purchase .purchase_data .left{
	width:50%;
}

#finalize_purchase .purchase_data .data_collum{
	font-family:'Nunito';
	font-size:13px;
	color:#c4c4c4;
	text-align:center;
	margin-top:8px;
}

#finalize_purchase .purchase_data .data_collum b{
	color:#fff;
}

#finalize_purchase .forms{
	width:100%;
	margin-top:40px;
}

#finalize_purchase .forms a{
}

#cp_add_credits .add_credits_content .area_desc b{
	color:#fff;
}

.billers_box_wrapper{
	padding-top:0px;
	margin:0 auto;
}

.billers_box_wrapper P{
	font-family:'Nunito',sans-serif;
	color:#A3A3A3;
	font-size:16px;
	text-align:center;
	margin-bottom:20px;
}

#billers_select{
	width:676px;
}

.billers_wrapper{
	width:460px;
	margin:0 auto;
	display:none;
}

.billers_wrapper #change_biller{
	color:rgb(0,0,218) !important;
}

.billers_wrapper #change_biller img{
	margin-left:10px;
}

#menu{
	height:51px;
	width:1200px;
	position:relative;
	top:-10px;
	z-index:10;
	margin-left:15px;
}

#menu li{
	float:left;
	margin-right:3px;
	position:relative;
	top:1px;
	-webkit-transition:all .2s ease-in;
	-moz-transition:all .2s ease-in;
	-o-transition:all .2s ease-in;
	transition:all .2s ease-in;
}

#menu li.last{
	margin-right:0px;
}

#menu li a{
	font-family:'Nunito',sans-serif;
	float:left;
	width:365px;
	font-weight:bold;
	font-size:21px;
	z-index:25;
	position:relative;
	color:#737373;
	background:url(../../images/users/menu/menu_middle.png) repeat-x;
	text-shadow:1px 1px 1px #000;
	text-align:center;
	height:50px;
	line-height:50px;
	display:block;
	-webkit-transition:all .2s ease-in;
	-moz-transition:all .2s ease-in;
	-o-transition:all .2s ease-in;
	transition:all .2s ease-in;
}

#menu li a:first-child{
	width:365px;
}

#menu li a:hover{
	text-decoration:none;
	color:#aeaeae;
	-webkit-transition:all .2s ease-in;
	-moz-transition:all .2s ease-in;
	-o-transition:all .2s ease-in;
	transition:all .2s ease-in;
}

#menu li a.follow_models{
	width:52px;
}

#menu li:hover{
	top:-2px;
	-webkit-transition:all .2s ease-in;
	-moz-transition:all .2s ease-in;
	-o-transition:all .2s ease-in;
	transition:all .2s ease-in;
}

#menu li span.menu_left{
	display:block;
	float:left;
	width:5px;
	height:48px;
	background:url(../../images/users/menu/menu_left.png) no-repeat 0px 0px;
	-webkit-transition:all .2s ease-in;
	-moz-transition:all .2s ease-in;
	-o-transition:all .2s ease-in;
	transition:all .2s ease-in;
}

#menu li span.menu_right{
	display:block;
	float:left;
	width:5px;
	height:48px;
	background:url(../../images/users/menu/menu_right.png) no-repeat 0px 0px;
	-webkit-transition:all .2s ease-in;
	-moz-transition:all .2s ease-in;
	-o-transition:all .2s ease-in;
	transition:all .2s ease-in;
}

#menu li.active{
	z-index:9999;
}

#menu li.active span.menu_left{
	background:url(../../images/users/menu/menu_left_active.png) no-repeat 0px 0px;
}

#menu li.active span.menu_right{
	background:url(../../images/users/menu/menu_right_active.png) no-repeat 0px 0px;
}

#menu li.active a{
	background:url(../../images/users/menu/menu_middle_active.png) repeat-x;
	color:#fff !important;
	text-shadow:1px 1px 1px rgba(2,78,96,0.6);
}

#menu li.active a:hover{
	line-height:50px !important;
}

#menu li.active:hover{
	margin-top:0px;
	top:1px;
}

#menu_background{
	width:1197px;
	height:8px;
	position:absolute;
	bottom:0px;
	left:1px;
	z-index:220;
}

#menu_background span.menu_bottom_left{
	display:block;
	float:left;
	width:4px;
	height:5px;
	background:url(../../images/users/menu/menu_bottom_left.png) no-repeat 0px 0px;
}

#menu_background span.menu_bottom_right{
	display:block;
	float:left;
	width:4px;
	height:5px;
	background:url(../../images/users/menu/menu_bottom_right.png) no-repeat 0px 0px;
}

#menu_background span.menu_bottom_middle{
	display:block;
	float:left;
	width:1187px;
	height:5px;
	background:url(../../images/users/menu/menu_bottom_middle.png) repeat-x;
}

#menu li.last a.follow_models span{
	display:block;
	font-family:'Nunito',sans-serif;
	width:36px;
	height:34px;
	line-height:34px;
	position:relative;
	top:2px;
	left:7px;
	text-align:center;
	font-size:12px;
	background:url(../../images/users/menu/menu_heart.png) no-repeat 0px 0px;
}

#menu li.active a.follow_models span{
	background:url(../../images/users/menu/menu_heart.png) no-repeat 0px -36px;
}

#menu li.last a.follow_models span.animation{
	-webkit-animation:favorite 1s;
	animation:favorite 1s;
	-moz-animation:favorite 1s;
}

@-webkit-keyframes favorite{
	50%{
		color:#cc477e;
		font-size:36px;
	}

	100%{
		color:#737373;
		font-size:12px;
	}

}

@-moz-keyframes favorite{
	50%{
		color:#cc477e;
		font-size:36px;
	}

	100%{
		color:#737373;
		font-size:12px;
	}

}

@keyframes favorite{
	50%{
		color:#cc477e;
		font-size:36px;
	}

	100%{
		color:#737373;
		font-size:12px;
	}

}

h1{
	color:#12b4bb;
	margin-bottom:18px;
	padding-top:15px;
}

h1 a{
	color:#12b4bb;
	text-decoration:none;
}

h1 a:hover{
	color:#12b4bb;
	text-decoration:none;
}

#cp_sitemap{
	margin:10px auto 10px;
	background:#1a1c1c;
	padding:4px;
}

#cp_sitemap .sitemap_collum1{
	width:287px;
	margin-right:20px;
	float:left;
	padding:10px;
	background:#414242;
	border-radius:3px;
}

#cp_sitemap .sitemap_collum2{
	width:287px;
	margin-right:20px;
	float:left;
	padding:10px;
	background:#414242;
	border-radius:3px;
}

#cp_sitemap .sitemap_collum3{
	width:287px;
	float:left;
	padding:10px;
	background:#414242;
	border-radius:3px;
}

#cp_sitemap h2{
	padding-bottom:24px;
	font-family:Verdana;
	font-size:20px;
}

#cp_sitemap h2 a{
	text-decoration:none;
}

#cp_sitemap .date{
	margin-bottom:20px;
	padding:10px;
}

#cp_sitemap .date span.head{
	display:block;
	font-size:12px;
	margin-bottom:15px;
	color:#efefef;
	font-style:italic;
}

#cp_sitemap .performer_box{
	display:block;
	margin-bottom:14px;
}

#cp_sitemap .performer_box span{
	float:left;
	margin-left:10px;
}

#cp_sitemap .performer_box span a{
	font-size:12px;
}

#cp_sitemap .performer_box img{
	display:block;
	float:left;
	margin-right:7px;
	border-radius:4px;
}

#content .bar_top{
	margin-bottom:10px;
	margin-left:14px;
	background:#111111;
}

#content .bar_top p.remaining_time{
	line-height:22px;
	height:22px;
	float:left;
	width:952px;
	padding:3px 0px 3px 10px;
	border-radius:2px;
	color:rgb(145,145,145);
	font-size:11px;
	font-family:nunito;
}

#content .bar_top #home_search{
	width:228px;
	float:right;
	position:relative;
}

#content .bar_top #home_search .top{
	width:228px;
	height:26px;
	position:relative;
}

#content .bar_top #home_search .top input[type="text"]{
	width:186px;
	padding:4px 4px 4px 36px;
	background:#1e1e1e;
	border:1px solid #2b2b2b;
	color:#959595;
	font-family:'Nunito';
	font-size:10px;
	height:19px;
	-webkit-transition:all .2s ease-in;
	-moz-transition:all .2s ease-in;
	-o-transition:all .2s ease-in;
	transition:all .2s ease-in;
}

#content .bar_top #home_search .top input[type="text"]::-webkit-input-placeholder{
	color:#959595;
}

#content .bar_top #home_search .top input[type="text"]:-moz-placeholder{
	color:#959595;
}

#content .bar_top #home_search .top input[type="text"]::-moz-placeholder{
	color:#959595;
}

#content .bar_top #home_search .top input[type="text"]:-ms-input-placeholder{
	color:#959595;
}

#content .bar_top #home_search .top input[type="text"]:focus{
	background:#2C2C2C;
	color:#C5C5C5;
	-webkit-transition:all .2s ease-in;
	-moz-transition:all .2s ease-in;
	-o-transition:all .2s ease-in;
	transition:all .2s ease-in;
}

#content .bar_top #home_search .top span.icon{
	position:absolute;
	top:2px;
	left:2px;
	display:block;
	width:29px;
	height:25px;
	background:#2b2b2b url(../../images/users/menu/glass.png) no-repeat 7px 6px;
}

#content .bar_top #home_search .top #loading_search{
	position:absolute;
	top:7px;
	right:8px;
}

#content .bar_top #home_search .result{
	position:absolute;
	display:none;
	width:220px;
	top:30px;
	z-index:9999;
	max-height:320px;
	overflow-y:auto;
	overflow-x:hidden;
	padding:4px;
	background:#3b3b3b;
	background:rgba(59,59,59,0.92);
}

#content .bar_top #home_search .result .model_search{
	margin-top:6px;
	overflow:hidden;
	width:220px;
	background:#505050;
	position:relative;
}

#content .bar_top #home_search .result .model_search:first-child{
	margin-top:0px;
}

#content .bar_top #home_search .result .model_search img.avatar{
	float:left;
	width:62px;
	height:62px;
}

#content .bar_top #home_search .result .model_search .info_right{
	position:relative;
	left:10px;
	-webkit-transition:all .2s ease-in;
	-moz-transition:all .2s ease-in;
	-o-transition:all .2s ease-in;
	transition:all .2s ease-in;
}

#content .bar_top #home_search .result .model_search .info_right span.name{
	display:block;
	margin-top:13px;
	font-size:12px;
	font-family:'Nunito';
	color:#e6e6e6;
	margin-bottom:4px;
}

#content .bar_top #home_search .result .model_search .info_right span.search_status{
	display:block;
	font-size:10px;
	font-family:'Nunito';
	color:#e6e6e6;
}

#content .bar_top #home_search .result .model_search.free .info_right span.search_status{
	color:#54ca11;
}

#content .bar_top #home_search .result .model_search.nude .info_right span.search_status{
	color:#ffdf50;
}

#content .bar_top #home_search .result .model_search.private .info_right span.search_status{
	color:#e94890;
}

#content .bar_top #home_search .result .model_search.true_private .info_right span.search_status{
	color:#d3377d;
}

#content .bar_top #home_search .result .model_search .animation_holder{
	position:absolute;
	overflow:hidden;
	width:158px;
	height:62px;
	line-height:62px;
	top:0px;
	left:62px;
}

#content .bar_top #home_search .result .model_search a:hover{
	text-decoration:none;
}

#content .bar_top #home_search .result .model_search a.open{
	display:block;
	width:0px;
	overflow:hidden;
	color:#e6e6e6;
	background:#313131;
	text-align:center;
	font-size:10px;
	font-family:'Nunito';
	-webkit-transition:all .2s ease-in;
	-moz-transition:all .2s ease-in;
	-o-transition:all .2s ease-in;
	transition:all .2s ease-in;
}

#content .bar_top #home_search .result .model_search.free a.open{
	float:left;
}

#content .bar_top #home_search .result .model_search.nude a.open{
	float:left;
}

#content .bar_top #home_search .result .model_search.private a.open{
	float:left;
}

#content .bar_top #home_search .result .model_search.true_private a.open{
	float:left;
}

#content .bar_top #home_search .result .model_search a.open2{
	display:block;
	width:0px;
	overflow:hidden;
	color:#e6e6e6;
	background:#313131;
	text-align:center;
	font-size:10px;
	font-family:'Nunito';
	-webkit-transition:all .2s ease-in;
	-moz-transition:all .2s ease-in;
	-o-transition:all .2s ease-in;
	transition:all .2s ease-in;
}

#content .bar_top #home_search .result .model_search.free a.open2{
	background:#54ca11;
	color:#fff;
	float:left;
}

#content .bar_top #home_search .result .model_search.nude a.open2{
	background:#C9A813;
	color:#fff;
	float:left;
}

#content .bar_top #home_search .result .model_search.private a.open2{
	background:#e94890;
	color:#fff;
	float:left;
}

#content .bar_top #home_search .result .model_search.true_private a.open2{
	background:#d3377d;
	color:#fff;
	float:left;
}

#content .bar_top #home_search .result .model_search:hover .animation_holder{
	overflow:visible;
}

#content .bar_top #home_search .result .model_search:hover .info_right{
	left:150px;
	-webkit-transition:all .2s ease-in;
	-moz-transition:all .2s ease-in;
	-o-transition:all .2s ease-in;
	transition:all .2s ease-in;
}

#content .bar_top #home_search .result .model_search:hover .animation_holder a.open2{
	width:72px;
}

#content .bar_top #home_search .result .model_search:hover .animation_holder a.open{
	width:72px;
}

#content .bar_top #home_search .result .model_search.away:hover .info_right{
	left:80px;
}

#content .bar_top #home_search .result .model_search.offline:hover .info_right{
	left:80px;
}

#content .bar_top #home_search .result .model_search.true_private:hover .info_right{
	left:80px;
}

#content .load_more_home{
	display:block;
	width:1198px;
	height:44px;
	line-height:44px;
	background:#008e9c;
	text-align:center;
	color:#fff;
	font-family:'Verdana';
	font-size:18px;
	font-weight:bold;
	border-radius:2px;
	margin-left:16px;
}

#content .load_more_home:hover{
	text-decoration:none;
	background:#02737E;
}

html.mobile #content .bar_top #home_search .result .model_search:hover .info_right{
	left:10px !important;
}

html.mobile #content .bar_top #home_search .result .model_search:hover .animation_holder{
	display:none !important;
}

html.tablet #content .bar_top #home_search .result .model_search:hover .info_right{
	left:10px !important;
}

html.tablet #content .bar_top #home_search .result .model_search:hover .animation_holder{
	display:none !important;
}

#content #border #how_work .how_work_top{
	background:#414242;
	margin-bottom:16px;
	zoom:1;
	padding:10px 24px 10px 24px;
	position:relative;
	border-radius:4px;
}

#content #border #how_work p a{
	text-decoration:underline;
}

#content #border #how_work p{
	color:#fff;
	font-size:15px;
	margin-bottom:10px;
	line-height:22px;
}

#content #border #how_work .block1{
	margin-bottom:42px;
	margin-top:30px;
	position:relative;
}

#content #border #how_work .block1 span.title{
	padding-left:80px;
	display:block;
	color:#07d8e0;
	font-size:22px;
	font-weight:bold;
	margin-bottom:10px;
}

#content #border #how_work .block1 span.title1{
	background:url(../../images/users/como_funciona/icons.png) no-repeat 0px 0px;
	height:46px;
	line-height:46px;
	margin-bottom:18px;
}

#content #border #how_work .block1 span.title2{
	background:url(../../images/users/como_funciona/icons.png) no-repeat 0px -81px;
	height:46px;
	line-height:46px;
	margin-bottom:18px;
}

#content #border #how_work .block1 span.title3{
	background:url(../../images/users/como_funciona/icons.png) no-repeat 0px -236px;
	height:60px;
	line-height:23px;
	padding-top:19px;
	position:relative;
	top:-12px;
}

#content #border #how_work .block1 span.title4{
	background:url(../../images/users/como_funciona/icons.png) no-repeat 0px -142px;
	height:60px;
	line-height:23px;
	padding-top:19px;
	position:relative;
	top:-12px;
}

#content #border #how_work .block1 span.title5{
	background:url(../../images/users/como_funciona/icons.png) no-repeat 0px -334px;
	height:65px;
	line-height:65px;
	padding-top:20px;
}

#content #border #how_work .block1 .block_left,#content #border #how_work .block1 .block_right{
	width:362px;
	float:left;
	padding:26px;
	border:2px solid #3a3a3a;
	border-radius:6px;
}

html.ie7 #content #border #how_work .block1 .block_left{
	float:none !important;
}

#content #border #how_work .block1 .block_right{
	float:right;
}

#content #border #how_work .block2{
	padding:26px;
	border:2px solid #3a3a3a;
	border-radius:6px;
}

#content #border #how_work span.chat_type{
	color:#07d8e0;
	font-size:17px;
	font-weight:bold;
}

#content #border #how_work .como_p p{
	margin-bottom:14px;
	margin-top:14px;
}

#content #border #how_work .arrow_right{
	width:40px;
	height:30px;
	position:absolute;
	left:433px;
	top:178px;
	background:url(../../images/users/como_funciona/icons2.png) no-repeat 0px 0px;
}

#content #border #how_work .arrow_down{
	width:40px;
	height:30px;
	position:absolute;
	right:208px;
	top:397px;
	background:url(../../images/users/como_funciona/icons2.png) no-repeat 0px -36px;
}

#content #border #how_work .arrow_left{
	width:40px;
	height:30px;
	position:absolute;
	left:429px;
	top:118px;
	background:url(../../images/users/como_funciona/icons2.png) no-repeat 0px -69px;
}

#content #border #how_work .arrow_down2{
	top:683px !important;
	left:213px !important;
}

#content #border #how_work p.observation{
	line-height:22px;
	margin-bottom:10px;
}

#content #border #how_work h2.h2_como{
	margin-bottom:24px;
	margin-top:22px;
}

#add_review .review_item{
	margin:10px 0px;
}

#add_review h3{
	margin:15px 0px 10px 0px;
	color:#fff;
	font-size:16px;
}

#add_review .review_item .stars{
	width:275px;
	margin:0 auto;
	height:58px;
}

#add_review .review_item .stars input{
	display:none;
}

#add_review .ui-stars-star,.ui-stars-cancel{
	float:left;
	display:block;
	overflow:hidden;
	text-indent:-999em;
	cursor:pointer;
}

#add_review .ui-stars-star a,.ui-stars-cancel a{
	width:52px;
	height:52px;
	display:block;
	background:url(../../images/common/misc/star_rating.png) no-repeat top;
}

#add_review .ui-stars-star a{
	background-position:center bottom;
}

#add_review .ui-stars-star-on a{
	background:url(../../images/common/misc/star_rating.png) no-repeat top !important;
}

#add_review .ui-stars-star-hover a{
	background:url(../../images/common/misc/star_rating.png) no-repeat top !important;
}

#add_review .ui-stars-cancel-hover a{
	background-position:-289px -16px;
}

#add_review .ui-stars-star-disabled,.ui-stars-star-disabled a,.ui-stars-cancel-disabled a{
	cursor:default !important;
}

#rate_error{
	padding:10px;
	background:#eccdd7;
	display:none;
	border:1px solid #b00e31;
}

#rate_error span{
	display:block;
	text-align:center;
	color:#f9165b;
	text-align:center;
}

#user_register .register_gender_choose{
	margin-left:6px;
}

#user_register .register_gender_choose label{
	width:114px;
	height:44px;
	padding-left:31px;
	background:#414242;
	line-height:44px;
	font-family:'Nunito'
}

#user_approve_account .document_list{
	margin-top:20px;
}

#user_approve_account .document_list li{
	color:#fff !important;
	font-family:"Nunito";
	font-size:14px !important;
}

#user_approve_account .document_list li i{
	font-family:"Nunito";
	font-size:12px !important;
	text-transform:uppercase;
	font-weight:bold;
}

#user_approve_account .document_list li i.rejected{
	color:#BB0E0E;
}

#user_approve_account .document_list li i.pending{
	color:#a2a2a2;
}

#user_approve_account .document_list li i.approved{
	color:#5BDD14;
}

#user_approve_account #upload_wrapper,#user_approve_account #upload_wrapper2{
	position:relative;
}

#user_approve_account .qq-upload-button{
	border:0px;
	text-indent:-99999px;
	height:42px;
	line-height:42px;
	width:100%;
	background:none;
}

#user_approve_account .qq-upload-button input[type="file"]{
	height:44px !important;
}

#user_approve_account .qq-upload-list{
	display:none !important;
}

#user_approve_account .cp_upload_doc{
	position:absolute;
	top:0px;
	display:block;
	text-align:center;
	width:100%;
	height:42px;
	line-height:42px;
	font-family:'Nunito';
	font-size:16px;
}

#user_approve_account .cp_upload_doc.false{
	background:#aa112a;
}

#user_approve_account .cp_upload_doc.pending{
	background:#696969;
	position:static;
	cursor:default;
	text-decoration:none;
}

#user_approve_account .cp_upload_doc.approved{
	background:green;
	position:static;
	cursor:default;
	text-decoration:none;
}

#user_approve_account .user_avatar_file_loading{
	position:absolute;
	top:0px;
	left:0px;
	z-index:99999;
	width:448px;
	height:42px;
	background:rgb(66,66,66);
	background:rgba(66,66,66,0.7);
}

#user_approve_account .user_avatar_file_loading img{
	display:block;
	margin:0 auto;
	position:relative;
	top:10px;
}

#user_personal_details #validate_email{
	display:block;
	text-align:right;
	font-size:14px;
	font-family:'Nunito';
	font-weight:bold;
	margin-top:8px;
}

#user_personal_details #validate_email.pending{
	color:#a2a2a2;
	cursor:default;
}

#user_personal_details #validate_email.approved{
	color:#5BDD14;
	cursor:default;
}

#user_personal_details #validate_email.pending:hover{
	text-decoration:none;
}

#user_personal_details #validate_email.approved:hover{
	text-decoration:none;
}
.packages_inner {
	width: 90% !important;
}
-->
    </style>


<div id="content" align="center">				
				
					
				
				<span class="clr"></span>				
								
								
					
									<div id="cp_add_credits">
				<div class="add_credits_top">
				
			<span class="clr"></span>
		</div><!--add_credits_top-->		<div class="add_credits_content">						
			<div class="step_1 steps_bottom step_active">
				<div class="area_desc">									
					<h1><? echo lang('Select the Desired Credit Package')?></h1>
				</div>
				<div class="area_desc" style="padding-top: 0px !important; margin-top: -15px;">
					<p><? echo lang('Quem possui créditos pode conversar sem limite em todas as salas de chat')?></p>
				</div>
		
        
                        		<?php foreach($packages as $row => $package):?>
													
                

<div align="center" class="package_wrapper">						


		<div class="packages package_<?php echo $row?>" style="display:block;">
		                  
                          <div class="package_top"> 
					      <span class="credits"><span><?php echo $package['credits']?></span><?php echo lang('Credits')?></span>	
                          </div>
					   	<!--package_top-->	
						  <div class="strip"></div>
							
           <div class="package_bottom">									
				<span class="value"><span><?php echo lang(SETTINGS_REAL_CURRENCY_SYMBOL)?></span>
				<?php echo number_format($package['value'], 2, ',', '.')?></span>						
						      <hr>
        
        
        
				
		
		  <?php echo form_open(main_url('gateway/pagseguro'), array('id' => 'add_credits'))?>
         
             <input type="hidden" name="id" value="<?php echo $row?>">
             <input name="Enviar" type="submit"  class="bt" value="<?php echo lang('Buy')?>" >
		
		  <?php echo form_close()?>

</div></div></div>	

                               <?php endforeach;?>

					

                    
        <span class="clr"></span>	
</div>					
    	<span class="clr"></span>			
</div><!--step_1-->	
</div>
</div>					
            
            
            
				<span class="clr"></span>				
