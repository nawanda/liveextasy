$length = $('.largePhotosHolder .panel').length;
var sliderLargePhotos = [];
if($length > 0){
    var $item, $str;
    for (var i = 0; i < $length; i++) {
        $item = $('.largePhotosHolder .panel:eq('+i+')').html();
        $str = '<div class="panel">';
        $str += $item;
        $str += '</div>';
        sliderLargePhotos.push($str);
    }

    var largePhotos = new SwipeView('#largePhotos', {
    	numberOfPages: sliderLargePhotos.length
    });
    
    
    // Load initial data
    for (i=0; i<3; i++) {
    	page = i==0 ? sliderLargePhotos.length-1 : i-1;
    
    	el = document.createElement('span');
    	el.innerHTML = sliderLargePhotos[page];
    	largePhotos.masterPages[i].appendChild(el)
    }
    
    
    
    largePhotos.onFlip(function () {
    	var el, upcoming, i;
    
    	for (i=0; i<3; i++) {
    		upcoming = largePhotos.masterPages[i].dataset.upcomingPageIndex;
    
    		if (upcoming != largePhotos.masterPages[i].dataset.pageIndex) {
    			el = largePhotos.masterPages[i].querySelector('span');
    			el.innerHTML = sliderLargePhotos[upcoming];
    		}
    	}
    });
    
    largePhotos.onMoveOut(function () {
	   sliderLargePhotos.masterPages[sliderLargePhotos.currentMasterPage].className = sliderLargePhotos.masterPages[sliderLargePhotos.currentMasterPage].className.replace(/(^|\s)swipeview-active(\s|$)/, '');
    });
    
    largePhotos.onMoveIn(function () {
    	var className = sliderLargePhotos.masterPages[sliderLargePhotos.currentMasterPage].className;
    	/(^|\s)swipeview-active(\s|$)/.test(className) || (sliderLargePhotos.masterPages[sliderLargePhotos.currentMasterPage].className = !className ? 'swipeview-active' : className + ' swipeview-active');
    });
}