var	slidesPaidVideos = [];
$length = $('.paidVideosHolder .video_box').length;
$nr = 4;
var slidesPaidVideos = [];
if($length > 0){
    for (var i = 0; i < $length; i=i+$nr) {
        var $str = '<div class="panel">';
        for(var j=0; j<$nr; j++){
            $item = $('.paidVideosHolder .video_box:eq('+(i+j)+')').html()
            if($item){
                $str += '<div class="left" style="margin: 10px;">'+$item+'</div>';
            }
        }
        $str += '</div>';
        slidesPaidVideos.push($str);
    }
    
    var paidVideos = new SwipeView('#paidVideos', {
    	numberOfPages: slidesPaidVideos.length,
    	hastyPageFlip: true
    });
    
    // Load initial data
    for (i=0; i<3; i++) {
    	page = i==0 ? slidesPaidVideos.length-1 : i-1;
    
    	el = document.createElement('span');
    	el.innerHTML = slidesPaidVideos[page];
    	paidVideos.masterPages[i].appendChild(el)
    }
    
    paidVideos.onFlip(function () {
    	var el,
    		upcoming,
    		i;
    
    	for (i=0; i<3; i++) {
    		upcoming = paidVideos.masterPages[i].dataset.upcomingPageIndex;
    
    		if (upcoming != paidVideos.masterPages[i].dataset.pageIndex) {
    			el = paidVideos.masterPages[i].querySelector('span');
    			el.innerHTML = slidesPaidVideos[upcoming];
    		}
    	}
    });
}