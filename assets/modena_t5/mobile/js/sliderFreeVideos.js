$length = $('.freeVideosHolder .video_box').length;
$nr = 4;
var slidesFreeVideos = [];
if($length > 0){
    for (var i = 0; i < $length; i=i+$nr) {
        var $str = '<div class="panel">';
        for(var j=0; j<$nr; j++){
            $item = $('.freeVideosHolder .video_box:eq('+(i+j)+')').html()
            if($item){
                $str += '<div class="left" style="margin: 10px;">'+$item+'</div>';
                //$str += '<div class="left">'+$item+'</div>';
            }
        }
        $str += '</div>';
        //console.log($str);
        slidesFreeVideos.push($str);
    }


    var freeVideos = new SwipeView('#freeVideos', {
    	numberOfPages: slidesFreeVideos.length,
    	hastyPageFlip: true
    });
    
    
    // Load initial data
    for (i=0; i<3; i++) {
    	page = i==0 ? slidesFreeVideos.length-1 : i-1;
    
    	el = document.createElement('span');
    	el.innerHTML = slidesFreeVideos[page];
    	freeVideos.masterPages[i].appendChild(el)
    }
    
    
    
    freeVideos.onFlip(function () {
    	var el, upcoming, i;
    
    	for (i=0; i<3; i++) {
    		upcoming = freeVideos.masterPages[i].dataset.upcomingPageIndex;
    
    		if (upcoming != freeVideos.masterPages[i].dataset.pageIndex) {
    			el = freeVideos.masterPages[i].querySelector('span');
    			el.innerHTML = slidesFreeVideos[upcoming];
    		}
    	}
    });
}