<?xml version="1.0"?><language

	invitationW2WHeader="Invitation to see the viewer's cam"
	userInvitedW2W="  A user has invited you to see him on web cam!"

	pauseBroadcasting="Pause"
	pauseBroadcastTip="Temporarily stop your video steam so no user can see you."
    unpauseBroadcasting="Unpause"
    unpauseBroadcastingTip="Restart your video stream for all users in room to see."
    performerVideoPaused="Performer temporarily paused the video stream. Stay tuned to see the rest of the show."

    performerSidePausedText = "Your video stream is paused so your viewers cannot see nor hear you. To start streaming, press 'Unpause' in your screen's upper right. Note that accepting a nude or private invitation, unpauses your stream automatically."

	noMicAlert="The microphone does not appear to be working or its level is too low. Please note that mobile device streaming will not work properly unless the audio stream is available! To tweak the sound, press 'Cancel' and edit the Microphone settings."


	giftPrice = "Price:"
	giftCurrency = "Chips"
	giftFree = "Free"
	giftQuantity = "Quantity:"
	giftSend ="Send"
	giftAnonymously = "Anon"
	giftAnonymTip = "Only the performer will know who sent the gift."	
	giftPopupHeader="Gift"
	giftPerfPop1="You have received a gift of"
	giftPerfPop2="in total value of"
	giftPerfPop3="from"		
	giftConfPop1="You have successfully sent a gift of"
	giftConfPop2="in total value of"	
	giftUserPop1="The performer received a gift of"
	giftUserPop2="in total value of"
	giftUserPop3="from"	 
	giftErrorHeader="Could not send gift."

	display="Display"

	tipPleaseEnterGoalText = "To continue to goal chat please tip between"

	translate = "Translate"
	iSpeak  = "I speak"
	theySpeak = "They speak"
	iSpeakTip = "Translate what they say."
	theySpeakTip = "Translate what I say."
	
	privateTrue = "True Private"
	goalTimeForActivityOver = "Time set to do goal performance has expired."
	goalTimeoutExpired = "Time to reach goal expired without reaching the desired amount."
	goalSystemHeader = "Goal Status"

	maxLengthIs = "Maximum value is: "
	minLengthIs = "Minimum value is: "
	fieldIsRequired = "This field is required."
	maxValueIs = "Maximum value is: "
	minValueIs = "Minimum value is: "
	valueTooSmall = "This value is too small."
	valueTooLarge = "This value is too large."
	minimumCharacters ="Minimum number of characters is: "
	maximumCharacters ="Maximum number of characters is: "
	
	gatheredSoFar = "Gathered so far:"
	timeToReachGoal = "Time to reach goal:"
	valueRequired = "Please provide a valid value."
	tipTooSmall = "Tip smaller than the minimum value."
	tipTooLarge = "Tip greater than the maximum value."
	iWill ="I will"
	forLabel ="for"
	forTip="Number of minutes you will do the 'I will...' show"
	mins = "minutes"
	whenText = "if a goal of"
	isReachedText = "is reached"
	withinText = "within"
	withinTip = "If the amount is not received within this time, the goal is reset."
	startGoalButton ="Start Goal"
	stopGoalButton = "Stop Goal"
	goalTIp = "The desired goal."
	
	denyInvitesLabel="Deny Requests"
	denyInvitesTip="Automatically deny all invitations to other chat modes."

	youAreInNude="You are now in a nude show!"
	youAreInPrivate="You are now in a private show!"
	youAreInTruePrivate="You are now in a true private show!"
	waitingForPerformerToAnswer="Waiting for performer to confirm the invitation!"
	invitationNudeHeader="Invitation to nude chat"

	askforNude="Go Nude"
	notEnoughCreditsForNude="You don't have enough chips to enter nude chat."
	
	yourInvNudeRejected="        The performer is not yet ready to enter a nude chat. Please try again later."
	yourInvNudeAccepted="        Your invitation to nude chat has been accepted by the performer."
	proceedNude="Proceed"

	userInvitedNude="  A user has invited you to a nude chat!"








	copyToClipboard="Copy"
	configYourDevices="Configure Your &#60;b&#62;Devices&#60;/b&#62;"
	
	chooseQuality="Please choose quality scheme:"
	lowBut="Low"
	medBut="Medium"
	highBut="High"
	customBut="Custom"
	goBut="Go Online"
	chooseTypeOptionText="Choose online type:"
	broadThisCam="Broadcast this cam"
	volume="Volume:"
	normalRatio="Normal 4:3"
	wideRatio="Wide 16:9"
	testMic="Test Mic"
	ok="OK"
	micTestVolume="Detected Volume"
	microphoneIsWorking="The selected microphone seems to be working correctly."
	microphoneNotWorking="The selected microphone does NOT seem to work."
	inputDevicesMuted="The devices have been muted because you denied access in the settings pop-up."
	micTestTitle="Testing Currently Selected Microphone"
	camResolution="Resolution:  "
	pixels="px"
	frameRate="Framerate: "
	fps="fps"
	quality="Video Quality: "
	percent="%"
	bandwidth="Bandwidth: "
	kBs="kBs"
	unlimited = "Unlimited"
	variable = "Variable"
	welcome="Welcome"
	camera="Camera"
	cam="Cam "
	all="All"
	takeBreak="Take a Break!"
	takeBreakTip="Exit Chat and Take a Break"
	settingsTabLabel="Settings"
	mp3PlayerTabLabel="Music Player"
	userListTabLabel="User List"
	micVolume="Mic. Volume:"
	recCamera="Rec. Camera:"
	record="Record"
	recordTip="Start Recording Video"
	recordStop="Stop Rec."
	recordStopTip="Stop Recording Video"
	recordingBlinker="Rec."
	listChipsColumn="Chips"
	listNicknameColumn="Name"
	listActionsColumn="Actions"
	songColumnHeader="Song Name"
	kickBut="Kick"
	kickButTip="Kick this user from the chat."
	banBut="Ban"
	banButTip="Ban this user from entering your chat."
	send="Send"
	typeHere="Type your text here..."
	
	chatTypeOptionCount="3"
	chatTypeOption1="Free"
	chatTypeOption2="Nude Only"
	chatTypeOption3="Private Only"
	
	available="Available"
	private="Private"
	nude="Nude"
	free="Free"
	
	warning=" Warning! "
	connFailed="	Connection to server failed. Please try again. If problem persists, please contact your administrator."
	connRejected="	Connection rejected by server. Please re-login and try again."
	connClosed="	Connection closed or reset by server. Please refresh the page to reconnect."
	connClosedViewer="	Performer entered a private chat or has logged off."
	
	connInvFailed="	Private Invitation;  Connection to server failed. Please try again. If problem persists, please contact your administrator."
	connInvRejected="	Private Invitation;  Connection rejected by server. Please re-login and try again."
	
	chatSounds="Beep"
	userlistSounds="Beep On User Entrance"
	broadMic="Broadcast Audio"
	
	goBackOnline="Go Back Online"
	breakTimeHeader="On a break"
	breakTimeBody="   When you are ready to go back online, please press 'Go Back Online'."
	
	accept="Accept"
	deny="Deny"
	invitationPrivateHeader="Invitation to private chat"
	userInvited="  A user has invited you to a private chat!"
	username="Username: "
	chips_="Chips: "
	
	dressTimeHeader="Dress Time"
	dressTimeBody="   Private Chat is over. Please dress up and go back online."
	dressTimeBodySpy="   Private Chat is over. Performer is now dressing up."
	
	tipPerformerHeader="Tip Received"
	tipPerformer1="You have received a"
	tipPerformer2="tip from"
	tipPerformerBut="Tip Performer"
	tip="Tip"
	
	openPartnerVideo="Open viewer's video"
	openPartnerTip="see the viewer"
	closePartnerVideo="Close viewer's video"
	closePartnerTip="hide the viewer"

	viewerVol="Viewer Volume"
	
	noCameraHeader="No Camera Detected"
	noCameraBody="To go online you must have a working web cam connected to your computer. If you have a device connected, please make sure it is not in use by another process. Refresh the page to retry."	
	
	denyCameraHeader="Camera Access Denied"
	denyCameraBody="To go online you must accept the device security policy."
	
	play="Play"
	stop="Stop"
	noSongPlaying="No song playing"
	nowPlaying="Playing: "
	
	xmlBut="Download XML"
    url="URL:"
	stream="Stream:"
	
	showViewerList="Show User List"
	exit="Exit"
	exitToolTip="Exit Chat"
	
	askforPrivate="Go Private"
	musicPlayer="Music Player"
	musicPlayerVolume="Music Player Volume"
	performerVolume="Performer's Volume"
	imageZoom="Video Zoom"
	
	minimumToolTip="Minimize"
	largeToolTip="Large View"
	fullScreenToolTip="Full Screen"
	cameraSettingsTip = "Camera Settings" 
	micSettingsTip = "Microphone Settings" 
	
	waitingForReply="	You have invited the performer to private chat. Waiting for reply...  This may take a few seconds."
	invitationRejected="Invitation Rejected"
	invitationAccepted="Invitation Accepted"
	yourInvRejected="        The performer is not yet ready to enter a private chat. Please try again later."
	yourInvAccepted="        Your invitation to private chat has been accepted by the performer."
	cancel="Cancel"
	proceedPrivate="Private"
	proceedTruePrivate="True Private"
	chipsMin="chips/min"
	noPeeking="No peeking allowed!"	
	
	beenBanned="        You have been BANNED from this performer's chatroom. "
	beenKicked="        You have been KIKED out from this performer's chatroom. "
	perfLeft="        Performer left the chatroom."
	notConnected="        You are not connected to server. Please refresh page."
	connOver="        Your free time has expired"
	connOverLackOfChips="        You ran out of chips. Please buy more chips and re-enter chat"
		
	choiceYes="Yes"
	choiceNo="No"
	
	peekModalHeader="Entering Peek"
	peekModalText="Peek fee is"
	peekOK="Continue"
	peekCancel="Cancel"
	peekModelText2=". Do you wish to continue?"
    
	choicePeekHeader="Peek Option"
	choicePeekBody="The performer has entered private chat. Do you wish to take a peek?"
	choiceNudeHeader="Nude Chat Option"
	choiceNudeBody="The performer has entered a nude chat. Do you wish to join?"

	nudeModalHeader="Entering Nude Show"
	nudeModalText="Nude fee is"
	nudeModelText2=". Do you wish to continue?"
	
	tipSubmitLabel="Send Tip"
	tipCancelLabel="Cancel"
	tipIsLess="is less than the minimum amount of"
	tipIsMore="is more than the maximum amount of"
	tipHeaderText="Tip Performer"
	tipLoadingText="Please wait while the application initializes... "
	tipPleaseEnterText="Please enter a tip between"
	tipIncorrectText="The value is not valid."
	tipAnd="and"
	tipErrorText1="To be able to tip this performer you must buy more credit."
	tipErrorText2="You must be logged in order to tip the performer"
	tipAmountText="Tip amount:"
	tipOkButtonLabel="Close"
	tipReceivedHeader="Tip Sent"
	tipYouSent="You have successfully tipped the performer"
	tipHeSent="has tiped the performer"
	tipHeaderHim="Tip Sent"
	tipSuccess="Successfully Tiped"
	tipCredits="credits"
	tipFailedMes="The tip was NOT sent.  Please try again later."
	tipFailedHeader="Tip Sending Failed"
	
	topicLabel = "New Topic:"
	clearTopicLabel = "Clear"
	activeTopic = "Topic:"
	update="Update"
	noActiveTopic = "no topic"
	
	openW2WMode="Show Your Webcam"
	closeW2WMode ="Close Your Webcam"
		
	
	dontShow="Don't show this again"
	
	okLaDecon="OK"
	
	tipLabelText="Tip"

	chatType="Chat Type"
	
	mic="Mic:"
	
	gotoNude="Go Nude"
	gotoPrivate="Go Private"
	
	microPhoneSoundBroadcast="Broadcast microphone"
	peekAllowText="Allow Peeking"
	
	feeMessagePrivate="Chips per minute for private: "
	feeMessageTruePrivate="Chips per minute true private: "
	atLeastOneCam="        Please choose at least one cam in order to continue to chat room. To do so, tick on 'Broadcast this cam' from any of the available cams"
	
	userListTag="User List"
	activityLogTag="Activity Log"
	controlsTag="Controls"

	recordFromCam="Record cam:"

	close="Close"
	open = "Open"
	
	fullscreen="Fullscreen"
	bigscreen="Big Screen"
	normalscreen="Small Screen"
	broadcastSound="Microphone"
	broadcast="Broadcast"
	stopBroadcast="Stop Broadcast"
	allCams="All Cams"
		
	pleaseChooseNick="Please choose a nickname to use in this chatroom."
	gotoChat="Go to chat!"
	nickname="Nickname:"
	mustSpecifyNick="        Please specify a nickname before continuing to chatroom!"

	performerLogViewerEntered=" has entered chatroom."
	performerLogViewerLeft=" has left chatroom."
	performerLogStoppedRec="Video recording stopped."
	performerLogStartRec="Video recording started."
	performerLogChangedStatusTo="Changed status to: "
	
	notEnoughCreditsForPrivate="You don't have enough chips to enter private chat."
	
	welcomeMessage="Welcome to my chat room big boy! :P"
	
>

</language>