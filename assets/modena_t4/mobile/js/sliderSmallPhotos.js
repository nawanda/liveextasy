$length = $('.smallPhotosHolder .smallphoto_box').length;
$nr = 6;
var slidesSmallPhotos = [];
var dots = document.querySelectorAll('#small-photos-nav li');
if($length > 0){
    for (var i = 0; i < $length; i=i+$nr) {
        var $str = '<div class="panel">';
        for(var j=0; j<$nr; j++){
            $item = $('.smallPhotosHolder .smallphoto_box:eq('+(i+j)+')').html()
            if($item){
                $str += '<div class="left">'+$item+'</div>';
            }
        }
        $str += '<div class="clear"></div></div>';
        //console.log($str);
        slidesSmallPhotos.push($str);
    }


    var smallPhotos = new SwipeView('#smallPhotos', {
    	numberOfPages: slidesSmallPhotos.length,
    	hastyPageFlip: true
    });
    
    
    // Load initial data
    for (i=0; i<3; i++) {
    	page = i==0 ? slidesSmallPhotos.length-1 : i-1;
    
    	el = document.createElement('span');
    	el.innerHTML = slidesSmallPhotos[page];
    	smallPhotos.masterPages[i].appendChild(el)
    }
    
    
    
    smallPhotos.onFlip(function () {
    	var el, upcoming, i;
    
    	for (i=0; i<3; i++) {
    		upcoming = smallPhotos.masterPages[i].dataset.upcomingPageIndex;
    
    		if (upcoming != smallPhotos.masterPages[i].dataset.pageIndex) {
    			el = smallPhotos.masterPages[i].querySelector('span');
    			el.innerHTML = slidesSmallPhotos[upcoming];
    		}
    	}
        
       	document.querySelector('#small-photos-nav .selected').className = '';
    	dots[smallPhotos.pageIndex+1].className = 'selected';
    });
}