jQuery(function($){
	
	
	//###################################################
	//#################  IE 7 RESTRICTION  ##############
	if($.browser.msie && $.browser.version < 8 ){
		$("#warpper").fancybox({
			'href'			: base_url+'home/browser_restrictions',
			'type'			: 'iframe',
			'height'		: 400,
			'width'			: 700,
			'enableEscapeButton' : false,
			'titleShow' : false,
			'hideOnOverlayClick' : false,
			'showCloseButton' : false
			});
			
			setTimeout(function(){
				$("#warpper").trigger('click');
			}, 100);
	}
	
	
	var main_warpper_min_height = $(document).height()-152;
	var warpper_min_height = $(document).height()-87;

	$('#main #page_bg').css('min-height', $(document).height());
	$('#main #warpper').css('min-height', main_warpper_min_height);
	
	$('#page').css('min-height',warpper_min_height - $('#header').height()-20);
	$('#main #page').css('min-height',main_warpper_min_height - $('#header').height()-20);
	$('#main #order_credits').css('height',$('#warpper').height() - $('#header').height()-160);
	
	$('#studio #warpper, #performer #warpper,  #affiliate #warpper').css('min-height', warpper_min_height);
	$('#studio #left_menu, #performer #left_menu, #affiliate #left_menu').css('min-height',$('#warpper').height() - $('#header').height()-95);
	
	
	$('#main #left_menu').css('min-height',$('#warpper').height() - $('#header').height()-90);
	
	
	
	var isInIFrame = (window.location != window.parent.location) ? true : false;
	if(!isInIFrame){
		
	
		
	}


	
	
	//###################################################
	//#################  IE 7 FIX  ######################
	
	if($.browser.msie && 7 < $.browser.version  && $.browser.version < 9){
		$('#big_menu .item:last-child').addClass('last-child');
	}
	
	
	
	$('#header_control').click(function(){
		show_hide_header();
	})
	
	$('#header #box_title .icon, #header #box_title .login_btn').click(function(){
		show_hide_user_box();
	})


});



function show_hide_user_box(){
	var header_class = $('#header').attr('class');
	if(header_class  == 'small_header'){
		show_hide_header();
	}else{
		if($('#header #welcome_user_box #box_content').css('display') != 'none'){
			$('#header #welcome_user_box #box_content').slideUp(1000, function(){
				$('#header #welcome_user_box #box_title .icon').removeClass('dark_gray_up_arrow');
				$('#header #welcome_user_box #box_title .icon').addClass('dark_gray_down_arrow');
			})
		}else if( $('#header #welcome_user_box #box_content').css('display') == 'none'){
			$('#header #welcome_user_box #box_content').slideDown(1000, function(){
				$('#header #welcome_user_box #box_title .icon').removeClass('dark_gray_down_arrow');
				$('#header #welcome_user_box #box_title .icon').addClass('dark_gray_up_arrow');
			})
		}
	}
}


function show_hide_header(){
	
	var header_class = $('#header').attr('class');
	var main_warpper_min_height = $(document).height()-152;
	if(main_warpper_min_height < $(window).height()){
		$('#main #warpper').css('min-height', main_warpper_min_height + 65);	
	}
	

	$('#header').slideUp(1000, function(){
		if(header_class == 'small_header'){ //is shown small header and must change to big header
			$('#header').removeClass('small_header');
			//show_hide_user_box('show');
			$('#header #welcome_user_box #box_content').css('display', 'block');
			$('#header #welcome_user_box #box_title .icon').addClass('dark_gray_up_arrow');
			$('#header #welcome_user_box #box_title .icon').removeClass('dark_gray_down_arrow');
			setCookie('header', 'big');
			
			$('#show_hide_header').html(hide_header);
			$("#header_arrow").removeClass("icon dark_gray_down_arrow").addClass("icon dark_gray_up_arrow");
			
		} else {
			$('#header').addClass('small_header');
			//show_hide_user_box('hide');
			$('#header #welcome_user_box #box_content').css('display', 'none');
			$('#header #welcome_user_box #box_title .icon').removeClass('dark_gray_up_arrow');
			$('#header #welcome_user_box #box_title .icon').addClass('dark_gray_down_arrow');
			setCookie('header', 'small');
			
			$('#show_hide_header').html(show_header);
			$("#header_arrow").removeClass("icon dark_gray_up_arrow").addClass("icon dark_gray_down_arrow");
			
		}
		$('#header').slideDown(1000);
		
	});

}


function two(x) {return ((x>9)?"":"0")+x}

function time(ms) {
var sec = Math.floor(ms/1000)
ms = ms % 1000
//t = three(ms)

var t = '';
var min = Math.floor(sec/60)
sec = sec % 60
t = two(sec)

var hr = Math.floor(min/60)
min = min % 60
t = two(min) + ":" + t

var day = Math.floor(hr/60)
hr = hr % 60
t = two(hr) + ":" + t
//t = day + ":" + t
//console.log(t);
return t
}