<?php

/**
 * @author Aurel Pop
 */
class Performers_goals extends CI_Model {
    private $performers_goals;
    function __construct() {
        parent::__construct();
        
        $this->performers_goals = 'performers_goals';
    }
    
    /**
     * @param array $goal
     * @return int $goalId
     */
    function add($goal) {
        $this->db->trans_start();
        
        $this->db->insert($this->performers_goals, $goal);
        $goalId = $this->db->insert_id();
        
        $this->db->trans_complete();
        
        return $goalId;
    }
    
    /**
     * @param array $goal
     * @return boolean
     */
    function update($goal = array()) {
        $goalId = $goal['id'];
        unset($goal['id']);
        
        $result = $this->db->where('id', $goalId)->update($this->performers_goals, $goal);
        return $result;
    }
    
    /**
     * @param int $goalId
     * @return stdClass $goal
     */
    function getById($goalId) {
        $goal = $this->db->where('id',$goalId)->limit(1)->get($this->performers_goals)->row();
        return $goal;
    }
    
    /**
     * 
     * @param type $performer_id
     * @param type $status
     * @param type $new_status
     */
    function updateAllByPerformerId($performer_id, $status = 'started', $new_status = 'failed') {
        $this->db->where('status', $status);
        $this->db->update($this->performers_goals, array('status' => $new_status));
    }
}

