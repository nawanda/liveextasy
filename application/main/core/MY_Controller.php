<?php

/**
 * General controller
 * @author Andrei
 *
 */
class MY_Controller extends CI_Controller {

    public $user;
    public $is_purified;

    /**
     * Construieste obiectul $user
     * @return unknown_type
     */
    function __construct() {
        parent::__construct();

        $this->user = $this->access->get_account();

        //filtrele pt performers
        $this->is_purified = FALSE;

        if (SETTINGS_DEBUG) {
            $this->output->enable_profiler();
        }

        if ($this->session->userdata('type') == 'studio') {
            redirect(main_url() . STUDIOS_URL);
        } elseif ($this->session->userdata('type') == 'performer') {
            redirect(main_url() . PREFORMERS_URL);
        }
    }

}

/**
 * Users controller
 * @author Andrei
 *
 */
class MY_Users extends MY_Controller {

    public $active_session = NULL;

    /**
     * Restrictioneaza accessul catre controllerul de users
     * @return null
     */
    function __construct() {
        parent::__construct();
        $this->access->restrict('users');

        $this->load->model('watchers');
        $watcher = $this->watchers->get_one_active_session_by_user_id($this->user->id);

        //ii scad chipsurile consumate
        if ($watcher) {
            $this->user->credits -= $watcher->user_paid_chips;
            $this->active_session = $watcher;
        }
    }

}

/**
 * Controllerul de FMS
 * @author Andrei
 *
 */
class MY_FMS extends CI_Controller {

    /**
     * Constructor pentru FMS
     * @return null
     */
    function __construct() {
        parent::__construct();
        $hash = $this->input->post('hash');
        if (defined('FMS_SECRET_HASH') && $hash !== FMS_SECRET_HASH) {
           // write_request(APPPATH . 'logs/fms/denys.txt', 'status=deny&log=bad_secret_hash', true, true);
        }
    }

}