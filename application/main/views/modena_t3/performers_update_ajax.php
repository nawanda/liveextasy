<div id="performer_list">
	<?php if(sizeof($performers) > 0):?>
		<script type="text/javascript" src="<?php echo assets_url()?>js/preview.js"></script>				
		<?php foreach($performers as $performer):?>
			<?php $this->load->view('performer',array('performer'=>$performer))?>
		<?php endforeach?>
	<?php endif?>
	<div class="clear"></div>
	<?php if($pagination):?>
		<?php echo $pagination;?>
	<?php endif?>
	<div class="clear"></div>			
</div> 