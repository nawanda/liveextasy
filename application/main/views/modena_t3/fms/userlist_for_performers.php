<?php echo '<?xml version="1.0"?>'?>
<list>
	<?php if(sizeof($userlist) > 0):?>
		<?php foreach($userlist as $user):?>
			<viewer
				username="<?php echo $user->username?>"
				<?php if($user->user_id) :?>				
					userid="v<?php echo $user->user_id?>"
				<?php else:?>
					<?php  preg_match('/[0-9]+/i', $user->username,$matches)?>
					userid="v-<?php echo $matches[0]?>"
				<?php endif?>
				chipsum="<?php echo round($user->credits - $user->user_paid_chips)?>"
				color="#654399"
				<?php $this->load->library('ip2location')?>
				<?php $country_code = $this->ip2location->getCountryShort(long2ip($user->ip))?>
				flag="<?php echo assets_url().'images/flags/'. ( isset($country_code) ? $country_code : 'not_known').'.png'?>" 
				country="<?php echo isset($country_code) ? $countries[strtoupper($country_code)] : null?>" 
			>
			</viewer>
		<?php endforeach?>
	<?php endif?>	
</list>