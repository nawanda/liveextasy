<div id="menu">
	<div class="item">
		<span class="bg_l "></span><span class="bg_m ">
			<a href="<?php echo base_url()?>" class="menu_item ">
				<span class="toupper"><?php echo lang('Online Chat')?></span>
			</a>
		</span><span class="bg_r "></span>
	</div>
	<div class="item">
		<span class="bg_l "></span><span class="bg_m ">
			<a href="<?php echo site_url('performers')?>" class="menu_item">
				<?php $menu_item = lang('Our Models')?>
				<span class="toupper"><?php echo $menu_item?></span>
			</a>
		</span><span class="bg_r "></span>
	</div>
	<div class="item">
		<span class="bg_l "></span><span class="bg_m ">
			<a href="<?php echo site_url('favorites')?>" class="menu_item">
				<?php $menu_item = lang('Favorites')?>
				<span class="toupper"><?php echo $menu_item?></span>
			</a>
		</span><span class="bg_r "></span>
	</div>	
	<div class="item">
		<span class="bg_l "></span><span class="bg_m ">
			<a href="<?php echo site_url('videos')?>" class="menu_item">
				<?php $menu_item = lang('Videos')?>
				<span class="toupper"><?php echo $menu_item?></span>
			</a>
		</span><span class="bg_r "></span>
	</div>
	<?php if($this->user->id > 0):?>
		<div class="item">
			<span class="bg_l "></span><span class="bg_m ">
				<a href="<?php echo site_url('add-credits')?>" class="menu_item">
					<?php $menu_item = lang('Order Credits')?>
					<span class="toupper"><?php echo $menu_item?></span>
				</a>
			</span><span class="bg_r "></span>
		</div>
		<div class="item">
			<span class="bg_l "></span><span class="bg_m ">
				<a href="<?php echo site_url('messenger')?>" class="menu_item">
					<?php $menu_item = lang('Messenger')?>
					<span class="toupper"><?php echo $menu_item?></span>
				</a>
			</span><span class="bg_r "></span>
		</div>
		<div class="item">
			<span class="bg_l "></span><span class="bg_m ">
				<a href="<?php echo site_url('account')?>" class="menu_item">
					<?php $menu_item = lang('My account')?>
					<span class="toupper"><?php echo $menu_item?></span>
				</a>
			</span><span class="bg_r "></span>
		</div>
	<?php endif;?>
	<div class="clear"></div>
</div>