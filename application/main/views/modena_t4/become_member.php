<?$this->load->view('includes/head')?>
	
	<body style="background: transparent; background-image: none;">
		<div id="become_member">
			<div class="fancy_box_close" onclick="parent.$.fancybox.close();"><img src="<?php echo assets_url()?>images/spacer.gif" width="16" height="16"/></div>
			<div class="content">
				

				<div class="form" style="margin-top:70px;">
					<?php echo form_open('register','id="register" name="register"')?>
					<span class="italic white"><?php echo lang('Username:')?></span><br/>
					<?php echo form_input('username', set_value('username'), 'style="width:310px;" tabindex="1"')?>
					<span class="input_helper_l" style="width:293px;"></span>
					<span class="input_helper_line italic f10"><?php echo form_error('username')?form_error('username'):($this->input->post('username')?NULL:lang('Username length should be between 4 and 25 chars?'))?></span>
					

					<div style="width:160px; float:left;">
						<div style="margin-top:15px;">
							<span class="italic white"><?php echo lang('Email Address:')?></span><br/>
							<?php echo form_input('email', set_value('email'), 'style="width:147px;" tabindex="2"')?>
							<span class="input_helper_l" style="width:135px;"></span>
							<span class="input_helper_line  italic f10"><?php echo form_error('email')?form_error('email'):($this->input->post('email')?NULL:lang('Email Address must be valid!'))?></span>
						</div>
						<div style="margin-top:15px;">
							<span class="italic white"><?php echo lang('Password:')?></span><br/>
							<?php echo form_password('password', NULL, 'style="width:147px;" tabindex="4"')?>
							<span class="input_helper_l" style="width:135px;"></span>
							<span class="input_helper_line italic f10"><?php echo form_error('password')?form_error('password'):($this->input->post('password')?NULL:lang('Password must be valid!'))?></span>
						</div>
					</div>
					<div style="width:160px; float:left;">
						<div style="margin-top:15px;">
							<span class="italic white"><?php echo lang('Repeat Email Address:')?></span><br/>
							<?php echo form_input('repeat_email', set_value('repeat_email'), 'style="width:147px;" tabindex="3"')?>
							<span class="input_helper_l italic f10" style="width:135px;"></span>
							<span class="input_helper_line italic f10"><?php echo form_error('repeat_email')?form_error('repeat_email'):($this->input->post('repeat_email')?NULL:lang('Email Address must match!'))?></span>
						</div>
						<div style="margin-top:15px;">
							<span class="italic white"><?php echo lang('Repeat Password:')?></span><br/>
							<?php echo form_password('repeat_password', NULL, 'style="width:147px;" tabindex="5"')?>
							<span class="input_helper_l" style="width:135px;"></span>
							<span class="input_helper_line italic f10"><?php echo form_error('repeat_password')?form_error('repeat_password'):($this->input->post('repeat_password')?NULL:lang('Password must match!'))?></span>
						</div>
					</div>
					
					<div class="clear"></div>

					<div class="buttons">
						<button class="black_btn bold" style="text-transform: uppercase;" type="submit" ><?php echo sprintf(lang('Click here to %s become a %s member'),'<br/>','<br/>') ?></button>
						<span class="black_btn white bold" style="text-transform: uppercase;" onclick="parent.$.fancybox.close();">
							<?php $log_in_here_red = '<span class="red">'. lang('log in here') . '</span>'; ?>
							<?php echo sprintf(lang('Already a %s member? %s'),'<br/>','<br/>') ?>
							<?php echo $log_in_here_red ?>
						</span>
					</div>

				<?php echo form_close()?>
				</div>
			</div>

		</div>
	</body>
</html>