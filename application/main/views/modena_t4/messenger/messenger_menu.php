<div id="menu">
	<ul>
		<li class="inbox selected" id="inbox">
			<div>
				<span class="icon messenger_inbox"></span>
				<span><?php echo lang('Inbox')?></span>
				<?php echo ($unread_number > 0)? '<span class="nr">'.$unread_number.'</span>' : null ?>

			</div>
		</li>
		<li id="sent">
			<div>
				<span class="icon messenger_sent"></span>
				<span><?php echo lang('Sent')?></span>
			</div>
		</li>
		<li id="trash">
			<div>
				<span class="icon messenger_trash"></span>
				<span><?php echo lang('Trash')?></span>
			</div>
		</li>
	</ul>
	<div class="info"></div>
</div>