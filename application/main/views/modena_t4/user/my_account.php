<div class="content">			
	<? $this->load->view('includes/_title_bar', array('title' => 'My account')) ?>	
	<p><?php echo lang('Hello')?><strong> <?php echo $this->user->username?></strong>! <?php echo sprintf(lang('You have %s new messages'),$unread_msgs) ?>.</p>
	<p><?php echo sprintf(lang('You have %s chips'), $this->user->credits)?>. <?php echo lang('To order more click ')?> <a href="<?php echo site_url('add-credits')?>" class="red bold" ><?php echo lang('here') ?></a>!</p>				
</div>