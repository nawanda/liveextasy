<div class="content">
	<? $this->load->view('includes/_title_bar', array('title' => 'Cancel account')) ?>	
	<div class="dark_gray italic" id="userAccount" style="padding: 10px; text-align: justify;">
		<div class="form" style="text-align:center;"> 					
			<?php echo  form_open() ?>
				<?php echo form_hidden('account',1) ?>
				<span><?php echo lang('Cancelling your account cannot be reversed, are you sure you want to continue?') ?></span>
				<div class="clear"></div>		
				<div class="buttons">
					<?php echo form_submit('submit', lang('Yes'), 'class="red" style="width:100px; margin:3px 0px;" onclick="return confirm(\'' . lang('Cancel account action is irevesible, are you sure you want to cancel it?') . '\')"') ?>
					<?php echo form_button('but', lang('No'), 'class="red"  style="width:100px; margin:3px 0px;" onclick="document.location = \'' . base_url() .'\'")') ?>								
				</div>
			<?php echo form_close() ?>
		</div>
	</div>		
</div>