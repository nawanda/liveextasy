	<div class="content" style="padding:0px;">
		<div style="padding:0px 10px">
			<? $this->load->view('includes/_title_bar', array('title' => 'Order chips')) ?>
		</div>

		<div id="order_credits">
			
			<div class="fl_l">
				<span class="cards"></span>
			</div>
			<div class="fl_r">
			
				<div class="title">
					<?php echo lang('Select Package')?>
				</div>
			
				<?php echo form_open('', array('id' => 'add_credits'))?>
				
					<?php foreach($packages as $row => $package):?>
					<div class="pack fl_l dark_gray" >
						<label>
						<?php echo form_radio('package', $row)?>
						<span class="bold"><?php echo $package['credits']?></span>
						<?php if(  SETTINGS_CURRENCY_TYPE): //TODO TEST?>
							<?php echo lang(SETTINGS_SHOWN_CURRENCY)?>
						<?php endif?>
						
						<?php if($package['bonus'] != 0):?>
							+ <?php echo $package['bonus']?> <?php echo lang('Bonus')?>
							
						<?php endif?> 
						= <span class="currency red"><?php echo SETTINGS_REAL_CURRENCY_SYMBOL.$package['value']?></span>
						<br/>
						<span class="description" style="padding-left: 25px;"><?php echo sprintf(lang('as low as %s per chips'), SETTINGS_REAL_CURRENCY_SYMBOL.round($package['value'] /($package['credits']+ $package['bonus']),2))?></span>
						</label>
					</div>

					<?php endforeach;?>
				<div class="clear"></div>
				<div class="buy_button">
					<?php echo form_submit('submit',lang('Buy chips'), 'class=red')?>
				</div>
				<?php echo form_close()?>
			</div>
		</div>	
		<div class="clear"></div>
	</div>