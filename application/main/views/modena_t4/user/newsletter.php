<div class="content">
	<? $this->load->view('includes/_title_bar', array('title' => 'Newsletter Settings')) ?>	
	<div class="dark_gray italic" id="user_settings">
		<?php echo  form_open() ?>
			<div>
				<label><span class="dark_gray italic bold"><?php echo lang('Newsletter') ?>:</span></label>
				<?php echo form_dropdown('newsletter',$newsletter,set_value('newsletter', $user->newsletter)) ?>
				<span class="error" htmlfor="newsletter" generated="true"><?php echo form_error('newsletter')?></span>
			</div>
			<div>
				<?php echo form_submit('submit', lang('SAVE'), 'class="red"') ?>			
			</div>
		<?php echo form_close() ?>
	</div>		
</div>