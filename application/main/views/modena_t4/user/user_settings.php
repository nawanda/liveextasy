<script type="text/javascript">
jQuery(function($){

	$.validator.setDefaults({
		validClass:"success",
		errorElement: "span",
		errorPlacement: function(error, element) {
			error.appendTo($(element).next('span'));
			
		}
	});
		
	$(".change_pass").validate({		
		success: function(label) {
	   },
		rules: {
		    old_password: {
	   			required: true,
				minlength: 5
			},
			new_password: {
		   		required: true,
				minlength: 5
			}
		}, 
		messages: {
			old_password: 					"<?php echo lang('Password must have at least 5 characters') ?>",
			new_password:					"<?php echo lang('Password must have at least 5 characters') ?>"
		}
	});
}); 
</script>	
<div class="content">
	<? $this->load->view('includes/_title_bar', array('title' => 'My Account Page')) ?>	
	<div class="gray italic" id="user_settings">
		<?php echo form_open('', 'class="change_pass"')?>
			<div class="dark_gray italic register_performer" style="padding-top:0;">
				<div>
					<label><span class="bold"><?php echo lang('Old Password') ?>:</span></label>
					<?php echo form_password('old_password')?>
					<span class="error" htmlfor="old_password" generated="true"><?php echo form_error('old_password')?></span>
				</div>
				<div>
					<label><span class="bold"><?php echo lang('New Password') ?>:</span></label>
					<?php echo form_password(array('name' => 'new_password', 'id' => 'new_password'))?>
					<span class="error" htmlfor="new_password" generated="true"><?php echo form_error('new_password')?></span>
				</div>
				<div>
					<?php echo form_submit('submit',lang('Save'), 'class="red"  ')?>
				</div>
			</div>
		<?php echo form_close() ?>
		<div class="clear"></div>
	</div>
</div>