<script type="text/javascript" src="<?php echo assets_url()?>js/jquery.validate.js"></script>
<script type="text/javascript">
jQuery(function($){

	$.validator.setDefaults({
		validClass:"success",
		errorElement: "span",
		errorPlacement: function(error, element) {
			error.appendTo($(element).next('span').next('span'));
		}
	});

	
	$(".contact_us").validate({
		 
		success: function(label) {
	    	label.addClass("valid");
	   },
		rules: {
			subject: {
				required: true,
				minlength: 2,
				maxlength: 255
			},
			message: {
				required: true,
				minlength: 2,
				maxlength: 1500
			}
		}, 
		messages: {
			subject: 					"<?php echo lang('Invalid subject')?>",
			message: 					"<?php echo lang('Invalid message')?>"
		}
	});
}); 
</script>
<?php echo form_open('performers/contact', 'class="contact_us"') ?>
<div class="dark_gray italic profile_contact">
		<?php echo form_hidden('performer_id', $performer->id ) ?>
	<div>
		<label class="narrow_label bold"><?php echo lang('Subject') ?></label>
		<?php echo form_input(array('name' => 'subject', 'id' => 'subject'), set_value('subject'))?>
		<span></span>
		<span><span class="error" htmlfor="subject" generated="true"></span></span>
	</div>
	<div>
		<label class="narrow_label bold"><?php echo lang('Message') ?></label>
		<?php echo form_textarea(array('name' => 'message', 'id' => 'message') , set_value('message'))?>
		<span></span>
		<span><span class="error" htmlfor="message" generated="true"></span></span>				
	</div>
	<div>
		<label  class="narrow_label"></label>
		<?php echo form_submit('submit', lang('Send'), 'class="red"') ?>
	</div>
</div>
<?php echo form_close()?>