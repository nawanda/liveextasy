<?php 
$data                           = array();
$data['pageTitle']              = 'Sign&#45;Up';
$data['pageSubtitle']           = 'Sign&#45;Up';
$data['titleFirst']             = 'Sign&#45;Up';
$data['titleSecond']            = '';
$data['pageId']                 = 'userregister';
$data['page']                   = 'register';
$data['title_bar_red_arrow']    = TRUE;

$this->load->view('template', $data); 