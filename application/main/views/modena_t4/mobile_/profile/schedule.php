<div class="performer_profile performer_schedule" >
    <div data-role="collapsible-set" data-theme="a" data-inset="false" style="width: 300px; margin: 0 auto;">
        <?php $empty = TRUE; ?>
        <?php for($i=0; $i<=6; $i++):?>
            <?php if(isset($mobile_schedule[$i])):?>
                <?php $empty = FALSE; ?>
        <div class="schedule-box" data-role="collapsible">
		<h3><?=lang("schedule_day_".$i);?></h3>
            <div>
                <div class="triangle-left left"></div>
                <div class="information left">
                    <ul>
                                <?php foreach($mobile_schedule[$i] as $interval):?>
                                    <?php $interval = explode(':', $interval)?>
                        <li class="on">Online from <?php echo $interval[0]?> to <?php echo (isset($interval[1]))?intval($interval[1])+1:intval($interval[0])+1;?></li>
                                <?php endforeach?>
                    </ul>
                </div>
                <div class="triangle-right left"></div>
                <div class="clear"></div>
            </div>
        </div>
            <?php endif ?>
        <?php endfor?>
        <?php if($empty):?>
        <p>There are no schedules.</p>
        <?php endif?>
        <div class="clear"></div>

    </div>
    <?php  $this->load->view('includes/performer_profile_tabs'); ?>
</div>
