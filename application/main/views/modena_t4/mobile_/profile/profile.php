<div class="performer_profile performers_photos">
        <div id="largePhotos">
        <div data-role="none">
            <div class="largePhotos-container">
                <div class="swiper-wrapper">
                  <!-- AVATAR
                    <div class="swiper-slide red-slide">
                    <div class="panel">
                        <span class="left-arrow" onclick="largePhotos.swipePrev()"></span>
                        <img src="<?php echo ($performer->avatar != '') ? main_url('uploads/performers/' . $performer->id . '/medium/' . $performer->avatar) : assets_url().'images/poza_tarfa_medium.jpg'?>"/>
                        <span class="right-arrow" onclick="largePhotos.swipeNext()"></span>
                    </div>
                  </div>
                  -->
                      <?php foreach ($photos as $photo): ?>
                            <div class="swiper-slide ">
				<div class="panel">
                                    <span class="left-arrow" onclick="largePhotos.swipePrev()"></span>
			        	<img src="<?php echo main_url('uploads/performers/' . $performer->id .'/' . $photo->name_on_disk)?>" title="<?php echo $photo->title?>" />        		
                                        <span class="right-arrow" onclick="largePhotos.swipeNext()"></span>
                                </div>
                            </div>
                 
			<?php endforeach ?>
                  
                </div>
              </div>
        </div>
              <script>
              var largePhotos = new Swiper('.largePhotos-container',{
                createPagination: false
              })
              </script>
    </div>
    
    <!-- <img src="/images/performer_profile_sample.jpg" class="profile-image" /> -->
    <div class="about-me">
        <div class="inner">
            <h1 class="title"><span>About</span><span>Me</span></h1>
            <p class="text"><?php echo $performer->description?></p>
        </div>
    </div>
    <ul class="profile-information">
        <?php
            $details = array('')
        ?>
        <li>
            <div class="group">
                <span class="item bold"><?php echo lang('Height') ?>:</span>
                <span class="item"><?php echo lang($performer->height)?></span>
                <div class="clear"></div>
            </div>
        </li>
        <li>
            <div class="group">
                <span class="item bold"><?php echo lang('Age') ?>:</span>
                <span class="item"><?php echo floor((time() - $performer->birthday)/31556926);?></span>
                <div class="clear"></div>
            </div>
        </li>
        <li>
            <div class="group">
                <span class="item bold"><?php echo lang('Hair color') ?>:</span>
                <span class="item"><?php echo lang($performer->hair_color)?></span>
                <div class="clear"></div>
            </div>
        </li>
        <li>
            <div class="group">
                <span class="item bold"><?php echo lang('Ethnicity') ?>:</span>
                <span class="item"><?php echo lang($performer->ethnicity)?></span>
                <div class="clear"></div>
            </div>
        </li>
        <li>
            <div class="group">
                <span class="item bold"><?php echo lang('Cup size') ?>:</span></span>
                <span class="item"><?php echo lang($performer->cup_size)?></span>
                <div class="clear"></div>
            </div>
        </li>
        <li>
            <div class="group">
                <span class="item bold"><?php echo lang('Gender') ?>:</span>
                <span class="item"><?php echo lang($performer->gender)?></span>
                <div class="clear"></div>
            </div>
        </li>
        <li>
            <div class="group">
                <span class="item bold"><?php echo lang('Weight') ?>:</span>
                <span class="item"><?php echo lang($performer->weight)?></span>
                <div class="clear"></div>
            </div>
        </li>
        <li>
            <div class="group">
                <span class="item bold"><?php echo lang('Eye color') ?>:</span>
                <span class="item"><?php echo lang($performer->eye_color)?></span>
                <div class="clear"></div>
            </div>
        </li>
        <li>
            <div class="group">
                <span class="item bold"><?php echo lang('Language') ?>:</span>
                <span class="item">		
                    <?php foreach($languages as $language): ?>				
			<img src="<?php echo assets_url()?>images/flags/<?php echo strtoupper($language)?>.png">
		   <?php endforeach;?>
                </span>
                <div class="clear"></div>
            </div>
        </li>
        <li>
            <div class="group">
                <span class="item bold"><?php echo lang('Body build') ?>:</span>
                <span class="item"><?php echo lang($performer->build)?></span>
                <div class="clear"></div>
            </div>
        </li>
        <?php //$performer->what_turns_me_on $performer->what_turns_me_off ?>
    </ul>
    <?php  $this->load->view('includes/performer_profile_tabs'); ?>
</div>