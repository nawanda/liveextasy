<div class="performer_box">
    <?php if($performer->is_online): ?>
        <div class="status-flag"></div>
    <?php endif?>
    <img src="<?php echo  ( ! (file_exists('uploads/performers/' . $performer->id . '/medium/' . $performer->avatar) && $performer->avatar))? assets_url().'images/poza_tarfa.png':site_url('uploads/performers/' . $performer->id . '/medium/' . $performer->avatar)?>" />
    <div class="buttons">
        <?php if($performer->is_online) :?>
            <?php $button = $this->performers->display_button_mobile($performer)?>
            <?php if($button['require_login'] && $this->user->id < 1):?>
                <a class="chat-button flogin"><span class="chat-icon display-none"></span><span class="title"><?php echo $button['name']?></span></a>
            <?php else:?>
                <a href="<?php echo $button['link'] ?>" class="chat-button" rel="external"><span class="chat-icon display-none"></span><span class="title"><?php echo $button['name']?></span></a>
            <?php endif?>            
            <a data-ajax="false" href="<?php echo site_url($performer->nickname)?>" class="profile-button"><span class="profile-icon"></span><span class="title">Profile</span></a>
        <?php else:?>
            <a data-ajax="false" href="<?php echo site_url($performer->nickname)?>" class="profile-button" style="width:100%"><span class="profile-icon"></span><span class="title">Profile</span></a>
        <?php endif?>       
        <div class="clear"></div>
    </div>
    <div class="clear"></div>
    <div class="information">
        <span class="center-text"><?php echo $performer->nickname?></span>
    </div>
</div>