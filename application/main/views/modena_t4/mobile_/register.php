<div class="form" style="margin-top:20px; width: 80%;margin: 20px auto 10px auto">
	<?php echo form_open('register','id="register" name="register" data-ajax="false"')?>
        <div>
            <span class="italic white"><?php echo lang('Username:')?></span><br/>
            <?php echo form_input('username', set_value('username'), 'data-theme="a" data-inline="true"')?>
            <span class="input_helper_l" style="width:293px;"></span>
            <span class="input_helper_line italic f10"><?php echo form_error('username')?form_error('username'):($this->input->post('username')?NULL:lang('Username length should be between 4 and 25 chars?'))?></span>
        </div>
        <div>
                <span class="italic white"><?php echo lang('Email Address:')?></span><br/>
                <?php echo form_input('email', set_value('email'), 'data-theme="a" data-inline="true"')?>
                <span class="input_helper_l"></span>
                <span class="input_helper_line  italic f10"><?php echo form_error('email')?form_error('email'):($this->input->post('email')?NULL:lang('Email Address must be valid!'))?></span>
        </div>
        <div>
                <span class="italic white"><?php echo lang('Repeat Email Address:')?></span><br/>
                <?php echo form_input('repeat_email', set_value('repeat_email'), 'data-theme="a" data-inline="true"')?>
                <span class="input_helper_l italic f10" style="width:135px;"></span>
                <span class="input_helper_line italic f10"><?php echo form_error('repeat_email')?form_error('repeat_email'):($this->input->post('repeat_email')?NULL:lang('Email Address must match!'))?></span>
        </div>
        <div>
                <span class="italic white"><?php echo lang('Password:')?></span><br/>
                <?php echo form_password('password', NULL, 'data-theme="a" data-inline="true"')?>
                <span class="input_helper_l" style="width:135px;"></span>
                <span class="input_helper_line italic f10"><?php echo form_error('password')?form_error('password'):($this->input->post('password')?NULL:lang('Password must be valid!'))?></span>
        </div>
        <div>
                <span class="italic white"><?php echo lang('Repeat Password:')?></span><br/>
                <?php echo form_password('repeat_password', NULL, 'data-theme="a" data-inline="true"')?>
                <span class="input_helper_l" style="width:135px;"></span>
                <span class="input_helper_line italic f10"><?php echo form_error('repeat_password')?form_error('repeat_password'):($this->input->post('repeat_password')?NULL:lang('Password must match!'))?></span>
        </div>	
	<div class="clear"></div>
        <button class="black_btn bold" style="text-transform: uppercase;" type="submit" data-theme="a" class="red" style="margin: 3px auto;" ><?php echo sprintf(lang('Sign Up'),'','') ?></button>
<?php echo form_close()?>
</div>