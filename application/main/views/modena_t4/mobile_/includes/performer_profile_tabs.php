<div class="navbar-tabs-holder">
    <ul class="profile-tabs">
        <li><a data-ajax="false" href="<?php site_url($performer->nickname)?>?tab=profile"><span class="profile-icon"></span><span class="title">Profile</span></a></li>
        <!--<li class="active"><a href="_performerphotos.php"><span class="photos-icon"></span><span class="title">Photos</span></a></li>-->
        <!--<li><a data-ajax="false" href="<?php site_url($performer->nickname)?>?tab=videos"><span class="videos-icon"></span><span class="title">Videos</span></a></li>-->
        <li><a data-ajax="false" href="<?php site_url($performer->nickname)?>?tab=schedule"><span class="schedule-icon"></span><span class="title">Schedule</span></a></li>
    </ul>
    <div class="clear"></div>
</div>