<?php
$title_array = explode('-', $pageTitle);
$title = $title_array[0];

if(isset($page_subtitle)) {
    $title = $page_subtitle;
}

$title = substr($title, 0, 20).((strlen($title)> 20)?'...':'');

if(!isset($title_bar_red_arrow)) {
    $title_bar_red_arrow = TRUE;
}
?>

<div class="title-bar">
    <?php if(isset($title_bar_red_arrow) && $title_bar_red_arrow):?>
    <a href="<?php echo base_url()?>" class="red-arrow left" data-ajax="false"></a>
    <?php endif?>
    <h1 class="title left"><?php echo $title;?></h1>
    <a href="#" class="menu-icon right" data-slidemenu="#slidemenu" data-slideopen="false" data-icon="smico" data-corners="false" data-iconpos="notext"></a>
    <div class="clear"></div>
</div>