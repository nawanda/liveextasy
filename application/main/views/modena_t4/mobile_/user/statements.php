<table class="statements" width="100%" data-role="none">
                    <tr class="table-header">
                        <td class="center-text">Date</td>
                        <td>Performer</td>
                        <td>Type</td>
                        <td class="center-text">Amount</td>
                    </tr>

                    <?php $total = 0; ?>
                    <?php if(sizeof($watchers) == 0):?>
                            <tr>
                                    <td colspan="4" style="text-align:center"><?php echo lang('You have no statements') ?></td>
                            </tr>
                    <?php else:?>						
                            <?php $i = 0;?>
                            <?php foreach($watchers as $watcher ):?>
                                    <?php $total += $watcher->user_paid_chips ?>
                                    <tr class="<?php echo ($i % 2==0)?'even':'odd'?>">
                                            <td class="center-text"><?php echo date('Y-m-d H:i:s',$watcher->start_date)?></td>
                                            <td><?php echo $watcher->performer?></td>
                                            <td><?php echo lang($watcher->type)?></td>
                                            <td class="center-text"><?php echo print_amount_by_currency($watcher->user_paid_chips)?></td>
                                    </tr>
                                    <?php $i++;?>
                            <?php endforeach;?>
                    <?php endif;?>
</table>
<div class="statements-total">
    <span style="margin-right: 30px;">Total: <?php echo ($total > 0)?print_amount_by_currency($total):'0.00' ?></span>
</div>