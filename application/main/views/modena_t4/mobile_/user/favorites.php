<div class="content" style="text-align: center">			
	<?php if( ! is_array($performers)):?>
		<div class="helvetica italic"><?php echo lang('You have no favorite performers!') ?></div>
	<?php else:?>
		<?php foreach( $performers as $performer ):?>
			<?php $this->load->view('mobile/performer',array('performer'=>$performer))?>
		<?php endforeach?>					
	<?php endif;?>
	<?php echo $pagination;?>
	<div class="clear"></div>
</div>