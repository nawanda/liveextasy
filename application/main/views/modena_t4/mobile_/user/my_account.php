        <div class="login_panel account">
        <?php echo form_open('login', 'id="login_form" data-ajax="false"') ?>
            <div class="username-form">
                <span class="username-icon"></span>
                <input type="text" value="<?php echo lang('Hello')?> <?php echo $user->username?>!" name="username" disabled="disabled" data-role="none" />
                <div class="clear"></div>
            </div>
            <div class="password-form">
                <span class="token-icon"></span>
                <input type="text " value="<?php echo $user->credits ?> <?php echo lang('chips')?>" name="credits" disabled="disabled" placeholder="Password" data-role="none" />
                <div class="clear"></div>
            </div>
            <input type="button" data-ajax="false" class="goTo" onclick="window.location.replace('<?php echo site_url('add-credits') ?>')" value="<?php echo lang('Buy chips')?>" name="submit2" data-role="none" />
        <?php echo form_close()?>
        </div>
<!--
<p><?php echo lang('Hello')?> <span class="bold"><?php echo $user->username?></span>!</p>
<p><?php echo lang('chips')?>: <?php echo $user->credits ?></p>
<p class="center-text"><a href="<?php echo site_url('add-credits')?>" class="red-btn bold" ><?php echo lang('Buy chips')?></a></p>

-->
