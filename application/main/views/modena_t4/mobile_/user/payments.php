<table class="statements" width="100%" data-role="none">
    <tr class="table-header">
        <td class="center-text">Date</td>
        <td class="center-text">Paid</td>
        <td class="center-text">Received</td>
        <!--
        <td class="center-text">Type</td>
        <td class="center-text">Payment ID</td>
        -->
    </tr>

    <?php $total = 0?>
    <?php if(sizeof($credits) == 0):?>
    <tr>
        <td colspan="3" style="text-align:center"><?php echo lang('You have no payments') ?></td>
    </tr>
    <?php else:?>
        <?php $i = 0;?>
        <?php foreach($credits as $credit ):?>
            <?php $total += $credit->amount_paid?>
            <tr class="<?php echo ($i % 2==0)?'even':'odd'?>">
                <td class="center-text"><?php echo date('Y-m-d H:i:s',$credit->date)?></td>
                <td class="center-text"><?php echo print_amount_by_currency($credit->amount_paid,TRUE,TRUE)?></td>
                <td class="center-text"><?php echo print_amount_by_currency($credit->amount_received)?></td>
                <!--
                <td class="center-text"><?php echo lang($credit->type)?></td>
                <td class="center-text"><?php echo $credit->invoice_id?></td>
                -->
            </tr>
            <?php $i++;?>
        <?php endforeach;?>
    <?php endif;?>
</table>
<div class="statements-total">
    <span style="margin-right: 30px;">Total: <?php echo print_amount_by_currency($total,TRUE,TRUE)?></span>
</div>
