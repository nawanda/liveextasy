<script src="<?php echo assets_url()?>mobile/stream/js/socket.io/socket.io.js"></script>
<script src="<?php echo assets_url()?>mobile/stream/js/moment.min.js"></script>
<script>
    var socket =  io.connect('<?=SOCKET_URL.':'.SOCKET_PORT?>');
    socket.on('connect', function() {
        socket.emit('register', {
            uniqueId : "<?php echo $params['uniqId']?>",
            username : "<?php echo $params['userName']?>",
            pasword : "<?php echo isset($params['pasword']) ? $params['pasword']: '' ?>",
            performerId : "<?php echo $params['performerId']?>",
            userId : "<?php echo $params['userId']?>",
            sessionType : "<?php echo $params['sessionType']?>",
            ip: "<?php echo $this->input->ip_address()?>",
            truePrivate: false,
            userType: "viewer",
            device: "<?=getDevice()?>"
        });
    });

    var height = window.innerHeight - 104 - 270;
    
    $(function(){
      $("a").each(function(){
        $(this).attr("rel","external");
      });

     // $('#chat_container').css('height', height);
    });

    socket.on('message', function(data, ackServerCallback) {
        
        if(data.type == "image") {
            $("#image").attr('src', 'data:image/jpg;base64,'+ data.image);
        } else if (data.type == "requestResult"){
            output('<p class="' + "response" + '"><span class="current"> '+"Info"+ ':</span> ' + data.message + '</p>');
        } else {
            $username = (data.hasOwnProperty('username')  && data.username)?data.username:'Info';
            $message = $('<div/>').html(data.message).text();
            
            if(data.username == "<?php echo $params['userName']?>") {
                    output('<p class="' + data.type + '"><span class="current"> ' + $username + ':</span> ' + $message + '</p>');				
            } else {
                    output('<p class="' + data.type + '"><span class="user"> ' + $username + ':</span> ' + $message + '</p>');
            }
        }
    });

    socket.on('disconnect', function() {
        $.ajax({
            url	: '<?=site_url("home/setNotification/")?>',
            type: 'POST',
            data: {success: false, message: 'You have been disconnected!'}
        }).done(function(result) {
            window.location.href = "<?=site_url()?>";
        });
    });

    function sendMessage() {
        var message = $('#inputBox').val();
        
        if(message == '') return;
        $('#inputBox').val('');
        
        var jsonObject = {'@class': 'eu.agilio.actions.Message',
            message: message};
        console.log("DEBUG: " + JSON.stringify(jsonObject, replacer));
        socket.json.send(jsonObject);
    }
    
    function sendTip() {
        $amount = $('.tip_amount').val();
        var jsonObject = {
            '@class': 'eu.agilio.actions.Tip',
            amount: $amount,
            username: "<?php echo $params['userName']?>"
        };
        console.log("DEBUG: " + JSON.stringify(jsonObject, replacer));
        socket.json.send(jsonObject);
        $( "#popupTip" ).popup( "close" );
    }
    
    function sendInvite($type) {
        var jsonObject = {
            '@class': 'eu.agilio.actions.Invite',
            chatType: $type,
        };
        console.log("DEBUG: " + JSON.stringify(jsonObject, replacer));
        socket.json.send(jsonObject);
        $( "#popupRequestChat" ).popup( "close" );
    }
    
    function replacer(key, value) {
        if (typeof value === 'number' && !isFinite(value)) {
            return String(value);
        }
        return value;
    }

    function output(message) {
        $('#main_chat_container').prepend(message);
    }

</script>