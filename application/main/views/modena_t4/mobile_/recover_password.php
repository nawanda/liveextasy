<div style="width: 80%;margin: 20px auto 10px auto">			
<?php echo form_open('', 'data-ajax="false"')?>
        <div>
                <span class="italic dark_gray"><?php echo lang('User name')?>*:</span><br/>
                <?php echo form_input('username', set_value('username'), 'data-theme="a" data-inline="true"')?>
                <span class="italic red"><?php echo form_error('username', ' ', ' ')?></span>
        </div>
        <div>
                <span class="italic dark_gray"><?php echo lang('Email address')?>*:</span><br/>
                <?php echo form_input('email', set_value('email'), 'data-theme="a" data-inline="true"')?>
                <span class="italic red"><?php echo form_error('email', ' ', ' ')?></span>
        </div>
        <button data-theme="a" class="red" style="margin: 3px auto;"  type="submit"> <?php echo lang('Recover')?> </button>
<?php echo form_close('')?>
</div>