<?php if(isset($page_refresh)):?>
	<!-- refresh la live performers --> 
	<script type="text/javascript">
		var auto_refresh = setInterval(
		function()
		{	
			if(!filter_applied){
				$('#performer_list').load(document.location.href);
			}
		}, 30000);
	</script>
<?php endif?>

<?php $this->load->view('includes/filters')?>	
<div class="content fl_l" style="width:715px;">
	<?php //$this->load->view('includes/_search')?>
	<div id="applied_filters">
	
		<? $this->load->view('includes/_title_bar', array('title' => lang('Applied Filters'))) ?>
		<?php echo form_open('', array('method' => 'get', 'id' => 'form_applied_filters'))?>
			<?php if($this->uri->segment(1) == ''):?>
				<?php echo form_hidden('filters[is_online]', '1');?>
			<?php endif?>
		<?php echo form_close()?>
		<div class="clear"></div>
	</div>
		
		<? $this->load->view('includes/_title_bar', array('title' => ($this->uri->segment(1) == 'performers') ? 'All Performers' : 'Live Performers')) ?>
		<div class="clear"></div>	
		
		<div id="performer_list">
			<?php if(sizeof($performers) > 0):?>
				<script type="text/javascript" src="<?php echo assets_url()?>js/preview.js"></script>			
				<?php foreach($performers as $performer):?>
					<?php $this->load->view('performer',array('performer'=>$performer))?>
				<?php endforeach?>
			<?php else:?>
				<div class="no_results"><?php echo lang('There are no performers online')?></div>			
			<?php endif?>
			<div class="clear"></div>
			<?php if($pagination):?>
				<div class="h_line_sep"></div>
				<?php echo $pagination;?>
			<?php endif?>
			<div class="clear"></div>			
		</div>
		
		<?php if(isset($performers_random) && sizeof($performers_random) > 0):?>
			
		<? $this->load->view('includes/_title_bar', array('title' => 'Random Performers')) ?>
			<div class="clear"></div>		
			<?php foreach($performers_random as $performer):?>
					<?php $this->load->view('performer',array('performer'=>$performer))?>
			<?php endforeach?>
			<div class="clear"></div>				
		<?php endif?>
			
		<?php if(isset($performers_in_private) && sizeof($performers_in_private) > 0):?>
			<? $this->load->view('includes/_title_bar', array('title' => 'Performers currently in private')) ?>
			<div class="clear"></div>		
			<?php foreach($performers_in_private as $performer):?>
					<?php $this->load->view('performer',array('performer'=>$performer))?>
			<?php endforeach?>
			<div class="clear"></div>				
		<?php endif?>
			
		<?php if(isset($videos_free) && sizeof($videos_free) > 3):?>
			<div style="width:955px;">	
				<? $this->load->view('includes/_title_bar', array('title' => 'Free videos')) ?>
				<div class="clear"></div>		
				<?php foreach($videos_free as $video):?>
					<?php $this->load->view('videos/listing',array('video' => $video))?>
				<?php endforeach?>
			</div>
			<div class="clear"></div>
		<?php endif?>


		<?php if(isset($videos_paid) && sizeof($videos_paid) > 3):?>
			<div style="width:955px;">	
				<? $this->load->view('includes/_title_bar', array('title' => 'Paid videos')) ?>
				<div class="clear"></div>		
				<?php foreach($videos_paid as $video):?>
					<?php $this->load->view('videos/listing',array('video' => $video))?>
				<?php endforeach?>
			</div>
			<div class="clear"></div>
		<?php endif?>	
		
	</div>
	
<div class="clear"></div>
	<script type="text/javascript">
		function previewOpenVideoInModal(video_id){
			$.fancybox({
				'overlayShow': true,
				'scrolling': 'no',
				'type': 'iframe',
				'width':800,
				'height':600,				
				'showCloseButton'	: true,						
				'href': '<?php echo site_url('videos/view')?>' + '/' + video_id,
				'overlayColor': "#FFF",
				'overlayOpacity': "0.3"
			});		
		}	

		function pay_video(video_id){
			$.ajax({
				url: '<?php echo site_url('videos/view/')?>' + '/'+ video_id, 					
				complete: function(html) { 
					$.blockUI({ message: html.responseText});
				}
			});	
			return false;
		}
	</script>
