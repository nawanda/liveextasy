<?php  $filter_array = $this->input->get('filters', TRUE); //pentru repopulare + XSS CLEAN
if(isset($filter_array['price_range'])) {
	$prices = explode('-', $filter_array['price_range'][0]);
	$min_price = $prices[0];
	$max_price = $prices[1];
}

if(isset($filter_array['age_range'])) {
	$ages = explode('-', $filter_array['age_range'][0]);
	$min_age = $ages[0];
	$max_age = $ages[1];
}
?>

	<script type="text/javascript" src="<?php echo assets_url()?>js/jquery-ui-1.8.16.custom.min.js"></script>
	<script type="text/javascript">
		//Pentru timeout la search-uri
		var slide_timeout;
		var nick_timeout;
		var filter_applied = false;


		function slide_finished(){
	       clearTimeout(slide_timeout);
	       slide_timeout = setTimeout('perform_ajax_request()',500);
		}

		function type_finished(){
		   clearTimeout(nick_timeout);
		   nick_timeout = setTimeout('perform_ajax_request()',750);
		}
		
		jQuery(function($){
			$( "#slider" ).slider({
				range: true,
				min: <?php echo MIN_PRIVATE_CHIPS_PRICE ?>,
				max: <?php echo MAX_PRIVATE_CHIPS_PRICE ?>,
				values: [ <?php echo isset($min_price)?$min_price:MIN_PRIVATE_CHIPS_PRICE ?>, <?php echo isset($max_price)?$max_price:MAX_PRIVATE_CHIPS_PRICE ?> ],
				slide: function( event, ui ) {
					var val_text = ui.values[ 0 ] + " <?php echo SETTINGS_SHOWN_CURRENCY?>" + " - " + ui.values[ 1 ] + " <?php echo SETTINGS_SHOWN_CURRENCY?>";
					var val = ui.values[ 0 ] + '-' + ui.values[ 1 ];
					$("#price" ).html(val_text);
					$('#price_range').val(val);
					update_range_filter(event, val, val_text);
					slide_finished();
				}
			});
			
			$( "#price" ).html($( "#slider" ).slider( "values", 0 ) + " <?php echo SETTINGS_SHOWN_CURRENCY?>" +
			" - " + $( "#slider" ).slider( "values", 1 ) + " <?php echo SETTINGS_SHOWN_CURRENCY?>");
			$('#price_range').val($( "#slider" ).slider( "values", 0 ) + '-' + $( "#slider" ).slider( "values", 1 ));

			$( "#age_slider" ).slider({
				range: true,
				min: 18,
				max: 50,
				values: [ <?php echo isset($min_age)?$min_age:'18' ?>, <?php echo isset($max_age)?$max_age:'50' ?> ],
				slide: function( event, ui ) {
					var val_text = ui.values[ 0 ] + " <?php echo lang('years') ?>" + " - " + ui.values[ 1 ] + " <?php echo lang('years')?>";
					var val = ui.values[ 0 ] + '-' + ui.values[ 1 ];
					$("#age" ).html(val_text);
					$('#age_range').val(val);
					update_range_filter(event, val, val_text);
					slide_finished();
				}
			});
			
			$( "#age" ).html( $( "#age_slider" ).slider( "values", 0 ) + " <?php echo lang('years') ?>" +
			" - " + $( "#age_slider" ).slider( "values", 1 ) + " <?php echo lang('years') ?>");
			$('#age_range').val($( "#age_slider" ).slider( "values", 0 ) + '-' + $( "#age_slider" ).slider( "values", 1 ));


			//AJAX SEARCH 
			$(window).load(function() {
				$('.search_input').keyup(function() {
					if($('.search_input').val().length >= 3 || $('.search_input').val().length == 0) {
						type_finished();
					}
				});
					
			    $('#form_search').submit(function() {
			    	perform_ajax_request();
			        return false;
			    });

			    $('#form_search :input:not(input[type=text])').change(function() {
				    perform_ajax_request();
			    });
			    

			});

		});
		
	

		function perform_ajax_request() {
			//if($('.search_input').val().length >= 3 || $('.search_input').val().length == 0) {
				$.ajax({
		        	url: "<?php echo site_url('performers/search')?>?" + $('#form_applied_filters').serialize(),
		            type: 'get',
		            dataType: "html",
		            success: function(text) {
			                	$('#performer_list').html(text);
			                	filter_applied = true;
		            }
		        });
			//}
		}

		function get_ajax_page(element) {
			$.ajax({
	        	url: element.href,
	            type: 'get',
	            dataType: "html",
	            success: function(text) {
		             $('#performer_list').html(text);
	            }
	    	});
		}

		function reset_form() {
			document.getElementById('form_search').reset();
			$( "#price" ).html(<?php echo MIN_PRIVATE_CHIPS_PRICE ?> + " <?php echo SETTINGS_SHOWN_CURRENCY?>" +
					" -  " + <?php echo MAX_PRIVATE_CHIPS_PRICE ?> + " <?php echo SETTINGS_SHOWN_CURRENCY?>");
					$('#price_range').val('<?php echo MIN_PRIVATE_CHIPS_PRICE ?>-<?php echo MAX_PRIVATE_CHIPS_PRICE ?>');
			$( "#age" ).html( "0 <?php echo lang('years') ?>" +
					" - " + "50 <?php echo lang('years') ?>");
					$('#age_range').val('18-50');
			$("#slider").slider("values",0 , $("#slider").slider("option", "min") );
			$("#slider").slider("values",1 , $("#slider").slider("option", "max") );
			$("#age_slider").slider("values",0 , $("#age_slider").slider("option", "min") );
			$("#age_slider").slider("values",1 , $("#age_slider").slider("option", "max") );
			perform_ajax_request();
		}
		
		
		
		
		jQuery(function($){
			$('#filters > li').click(function(){
				if( $(this).attr('class') == 'selected'){
					$(this).removeClass('selected');
					$(this).find('ul').slideUp();
					$(this).find('span').removeClass('filter_arrow_up');
					$(this).find('span').addClass('filter_arrow_down');
					return false;
				}
				
				//get selected item
				var s_ele = $('#filters li.selected');
				s_ele.removeClass('selected');
				s_ele.find('ul').slideUp();
				s_ele.find('span').removeClass('filter_arrow_up');
				s_ele.find('span').addClass('filter_arrow_down');
				
				$(this).find('span').addClass('filter_arrow_up');
				$(this).find('span').removeClass('filter_arrow_down');
				$(this).addClass('selected');
				$(this).find('ul').slideDown();
			})
			
			
			$('#filters > li > ul > li').click(function(){
				var app_filters_div = $('div#applied_filters');
				var app_filters_form = $('form#form_applied_filters');
				var filter = $(this).parent('ul').attr('filter');
				var filter_val = $(this).attr('filter');
				var is_applied  = false;
				var elem = $(this);
				var filter_text = elem.parent('ul').parent('li').find('.filter_name').text();
				var filter_val_text = elem.text();
			
				
				
				if(filter == 'age_range' || filter == 'price_range'){
					return false;
				}
				//check if is applied this filter
				is_applied = app_filters_form.find('div[filter="'+ filter+'.'+filter_val +'"]');
				
				//this filter is applied, remove the filter;
				if(is_applied.length > 0){
					is_applied.find('.filter_remove').trigger('click');
					return false;
				}
				
				add_filter(elem, filter, filter_val, filter_text, filter_val_text);
				return false;
			})
		});
		
		function update_range_filter(event, val, val_text){
			var filter = $(event.target).parent('div').parent('li').parent('ul').attr('filter');
			var filter_text = $(event.target).parent('div').parent('li').parent('ul').parent('li').find('.filter_name').text();
			var app_filters_div = $('div#applied_filters');
			var is_applied = app_filters_div.find('div[filter="'+ filter+'"]');
			
			if(is_applied.length == 0){
				add_filter($(this), filter, val, filter_text, val_text);
			}
			
			is_applied.find('input').val(val);
			is_applied.find('span:last').text(filter_text +': ' +val_text);
			
			
		}
		
		function add_filter(elem, filter, filter_val, filter_text, filter_val_text){
			
			var app_filters_div = $('div#applied_filters');
			var app_filters_form = $('form#form_applied_filters');
			if(app_filters_div.css('display') == 'none'){
				app_filters_div.css('display', 'block');
			}
			
			//create filter element
			var filter_elem = $('<div>');
				filter_elem.addClass('filter_item');
				if(filter == 'price_range' || filter == 'age_range'){
					filter_elem.attr('filter', filter);
				}else{
					filter_elem.attr('filter', filter+'.'+filter_val);
				}
				

			var span_remove = $('<span class="icon filter_remove"></span>');
				span_remove.bind('click', remove_filter);

			//append filter element
			filter_elem.append('<input type="hidden" name="filters['+ filter +'][]" value="'+ filter_val +'" />');
			filter_elem.append(span_remove);
			filter_elem.append('<span>'+ filter_text +':'+ filter_val_text +'</span>');
			filter_elem.appendTo(app_filters_form);

			elem.find('span.icon').removeClass('filter_add');
			elem.find('span.icon').addClass('filter_remove');
			elem.addClass('selected');
			perform_ajax_request();
		}
		
		
		
		function remove_filter(){
				var filter_elem = $(this).parent('div');
				var filters = filter_elem.attr('filter').split('.');
				var filter_list_elem = $('ul[filter="'+filters[0]+'"] li[filter="'+filters[1]+'"]').find('span.icon');
				filter_list_elem.removeClass('filter_remove');
				filter_list_elem.addClass('filter_add');
				filter_list_elem.parent('li').removeClass('selected');
				filter_elem.remove();
				perform_ajax_request();
				
				if($('#form_applied_filters').find('div').length == 0){
					$('div#applied_filters').css('display', 'none')
				}
				return false;
			}
		
	</script>

<div id="right_filters">
	<span class="strip tl"></span>
	<span class="title">
		<?=lang('Apply <span class="fg_demi">Custom Filters</span>')?>
	</span>
	<ul id="filters">
		<li filter>
			<span class="icon filter_arrow_down"></span>
			<span class="filter_name"><?=lang('Price')?></span>
			<ul filter="price_range">
				<li>
					<div class="price_range_pos"> <span id="price"></span></div>	
					<br/>
					<div id="slider_container">
						<div id="slider"></div>
					</div>
				</li>
			</ul>
		</li>
		<li>
			<span class="icon filter_arrow_down"></span>
			<span class="filter_name"><?=lang('Age')?></span>
			<ul filter="age_range">
				<li>
					<div class="age_range_pos"> <span id="age"></span></div>
					<br/>
					<div id="slider_container_2">
						<div id="age_slider"></div>
					</div>
				</li>
			</ul>
		</li>
		<?php $this->load->view('includes/_filter_item', array('filter' => $category, 'filter_key' => 'category'))?>
		<?php $this->load->view('includes/_filter_item', array('filter' => $gender, 'filter_key' => 'gender'))?>
		<?php $this->load->view('includes/_filter_item', array('filter' => $sexual_prefference, 'filter_key' => 'sexual_prefference'))?>
		<?php $this->load->view('includes/_filter_item', array('filter' => $ethnicity, 'filter_key' => 'ethnicity'))?>
		<?php $this->load->view('includes/_filter_item', array('filter' => $language, 'filter_key' => 'language'))?>
		<?php $this->load->view('includes/_filter_item', array('filter' => $height, 'filter_key' => 'height'))?>
		<?php $this->load->view('includes/_filter_item', array('filter' => $weight, 'filter_key' => 'weight'))?>
		<?php $this->load->view('includes/_filter_item', array('filter' => $build, 'filter_key' => 'build'))?>
		<?php $this->load->view('includes/_filter_item', array('filter' => $eye_color, 'filter_key' => 'eye_color'))?>
		<?php $this->load->view('includes/_filter_item', array('filter' => $hair_color, 'filter_key' => 'hair_color'))?>
		<?php $this->load->view('includes/_filter_item', array('filter' => $hair_length, 'filter_key' => 'hair_length'))?>
	</ul>
	<div class="box black">
		<span class="title">
			<?php echo lang('Models<br/> <span class="bold">Wanted</span>')?>
		</span>
		<button class="red" onclick="document.location.href='<?php echo site_url(PREFORMERS_URL.'/register')?>'"><?php echo lang('Signup as <span class="bold">Model</span>')?></button>
		<span class="description">
			<?php echo lang('Stripper, Lingerie, Thong, Sex Symbol, Sensuality, Buttocks')?>
		</span>
	</div>
	<div class="box gray">
		<span class="title">
			<?php echo lang('Affiliate<br/> <span class="bold">Wanted</span>')?>
		</span>
		<span class="description">
			<?php echo lang('Stripper, Lingerie, Thong, Sex Symbol, Sensuality, Buttocks')?>
		</span>
		<button class="red" onclick="document.location.href='<?php echo site_url(AFFILIATES_URL.'/register')?>'"><?php echo lang('Signup as <span class="bold">Affiliate</span>')?></button>
	</div>
</div>