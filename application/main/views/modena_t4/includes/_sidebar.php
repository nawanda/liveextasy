<div id="user_settings" >
	<div id="left_menu" class="fl_l">
		<div class="menu_item <?=($this->uri->segment(2) == 'settings') ? 'selected' : NULL ?>" onclick="document.location.href='<?php echo site_url('user/settings')?>';">
			<span class="icon"></span>
			<a href="<?php echo site_url('settings')?>"><?php echo lang('Password') ?></a>
		</div>			
		<div class="menu_item <?=($this->uri->segment(1) == 'newsletter') ? 'selected' : NULL ?>" onclick="document.location.href='<?php echo site_url('newsletter')?>';">
			<span class="icon"></span>
			<a href="<?php echo site_url('newsletter')?>"><?php echo lang('Newsletter') ?></a>
		</div>
		<div class="menu_item <?=($this->uri->segment(1) == 'cancel-account') ? 'selected' : NULL ?>" onclick="document.location.href='<?php echo site_url('cancel-account')?>';">
			<span class="icon"></span>
			<a href="<?php echo site_url('cancel-account')?>"><?php echo lang('Cancel account') ?></a>
		</div>				
		<div class="clear"></div>
	</div>

	<div class="fl_r">
		<?php
			if(isset($page)){
				$this->load->view($page);
			}
		?>
	</div>
</div>
<div class="clear"></div>