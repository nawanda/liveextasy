<div id="big_menu">
	<div class="left_bg"></div>
	<div id="header_control">
	
	<?php if (isset($_COOKIE['header'])): ?>
		<?php if ($_COOKIE['header'] == 'big'): ?>
			<span id="header_arrow" class="icon dark_gray_up_arrow"></span>
		<?php else: ?>
			<span id="header_arrow" class="icon dark_gray_down_arrow"></span>
		<?php endif; ?>
	<?php else: ?>	
		<span id="header_arrow" class="icon dark_gray_up_arrow"></span>
	<?php endif; ?>	
	
	<?php if (isset($_COOKIE['header'])): ?>	
		<span class="text" id="show_hide_header"><?php echo ($_COOKIE['header'] == 'big') ? lang('Hide Header') : lang('Show Header') ?></span>
	<?php else: ?>
		<span class="text" id="show_hide_header"><?php echo lang('Hide Header') ?></span>
	<?php endif ?>
	
	</div>
	<div class="item strip <?=($this->uri->segment(1) == '') ? 'selected' : NULL?>">
		<a href="<?php echo base_url()?>" class="menu_item">
			<?php $menu_items = explode(' ', lang('Online Models'), 2)?>
			<span class="fg_book" ><?php echo ucfirst($menu_items[0])?></span><br/>
			<span class="fg_demi" ><?php echo (isset($menu_items[1]))?ucfirst($menu_items[1]):'&#160;'?></span>
		</a>
	</div>
	<div class="item strip <?=($this->uri->segment(1) == 'performers') ? 'selected' : NULL?>">
		<a href="<?php echo site_url('performers')?>" class="menu_item">
			<?php $menu_items = explode(' ', lang('View all Models'), 3)?>
			<span class="fg_book " ><?php echo ucfirst($menu_items[0].' '.$menu_items[1])?></span><br/>
			<span class="fg_demi" ><?php echo (isset($menu_items[2]))?ucfirst($menu_items[2]):''?></span>
		</a>
	</div>
	<div class="item strip <?=($this->uri->segment(1) == 'videos') ? 'selected' : NULL?>">
		<a href="<?php echo site_url('videos')?>" class="menu_item">
			<?php $menu_items = explode(' ', lang('All Videos'), 2)?>
			<span class="fg_book" ><?php echo ucfirst($menu_items[0])?></span><br/>
			<span class="fg_demi" ><?php echo (isset($menu_items[1]))?ucfirst($menu_items[1]):'&nbsp;'?></span>
		</a>
	</div>
	<div class="item strip <?=($this->uri->segment(1) == 'add-credits') ? 'selected' : NULL?>">
		<a href="<?php echo site_url('add-credits')?>" class="menu_item">
			<?php $menu_items = explode(' ', lang('Order Credits'), 2)?>
			<span class="fg_book " ><?php echo ucfirst($menu_items[0])?></span><br/>
			<span class="fg_demi" ><?php echo ucfirst($menu_items[1])?></span>
		</a>
	</div>
	<div class="item strip <?=($this->uri->segment(1) == 'favorites') ? 'selected' : NULL?> ">
		<a href="<?php echo site_url('favorites')?>" class="menu_item">
			<?php $menu_items = explode(' ', lang('Your Favorites'), 2)?>
			<span class="fg_book " ><?php echo ucfirst($menu_items[0])?></span><br/>
			<span class="fg_demi" ><?php echo ucfirst($menu_items[1])?></span>
		</a>
	</div>	
</div>
