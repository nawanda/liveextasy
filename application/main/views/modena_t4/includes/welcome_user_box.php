<div id="welcome_user_box">
	<span class="strip tl"></span>
	<span class="strip tr"></span>
	<div id="box_title" class="white">
		<?php if ($this->user->id > 0): ?>
			<span><?php echo lang('Welcome back') ?> &nbsp;<span class="red"><?php echo $this->user->username ?></span></span>
		<?php else: ?>
			<span><?php echo sprintf(lang('Welcome Guest! <span class="login_btn red">Log in</span> or <a href="%s" class="red signup">Register</a>'), site_url('register')) ?> </span>
		<?php endif; ?>
		<span class="fl_r icon dark_gray_up_arrow"></span>
	</div>
	<div id="box_content">

		<?php if ($this->user->id > 0): ?>
			<?php $bold_credits = '<span class="red bold" id="head_user_chips">' . print_amount_by_currency($this->user->credits, FALSE, FALSE, FALSE) . '</span>' ?>
		
			<div class="fl_l credits">
				<?=$bold_credits?><br/>
				<button class="red" style="margin-top:10px;" onclick="document.location.href='<?php echo site_url('add-credits') ?>'"><?=lang('Buy chips')?></button>
			</div>
			<div class="fl_l menu">
				<div class="menu_item">
					<a href="<?php echo site_url('user/settings') ?>">
						<span class="icon settings"></span>
						<span><?php echo lang('Settings') ?></span>
					</a>
				</div>
				<div class="menu_item">
					<a href="<?php echo site_url('statement') ?>">
						<span class="icon statement"></span>
						<span><?php echo lang('Statements') ?></span>
					</a>
				</div>
				<div class="menu_item">
					<a href="<?php echo site_url('payments') ?>">
						<span class="icon statement"></span>
						<span><?php echo lang('Payments') ?></span>
					</a>
				</div>
				<div class="menu_item">
					<a href="<?php echo site_url('messenger') ?>">
						<span class="icon messenger"></span>
						<span><?php echo lang('Messenger') ?></span>
					</a>
				</div>
				<div class="menu_item">
					<a href="<?php echo site_url('logout') ?>">
						<span class="icon logout"></span>
						<span><?php echo lang('Logout') ?></span>
					</a>
				</div>
			</div>
		<div class="clear"></div>
		<?php else: ?>
			<script>
				jQuery(function($){
					$('#account').change(function(){
						var accountType = $(this).val();
						if(accountType == ''){
							$('#login_form').attr('action', '<?= main_url() ?>login');
						}else{
							$('#login_form').attr('action', '<?= main_url() ?>' + accountType + '/login');
						}
					})
				});
			</script>
			<div class="top_text_box gray">
				<span class="icon lock fl_r"></span>
				<?php echo lang('Please log in with your user details') ?>
			</div>
			<div class="top_login">
				<?php echo form_open('login', 'id="login_form"') ?>
				<div class="fields">
					<label class="gray"><?= lang('Username:') ?></label><?php echo form_input('username', set_value('username', lang('username')), 'style="width: 173px;"  onfocus="if(this.value == \'' . lang('username') . '\') { this.value = \'\' }" onblur="if(this.value.length == 0) { this.value = \'' . lang('username') . '\' }"') ?><br/>
					<label class="gray"><?= lang('Password:') ?></label><?php echo form_password('password', set_value('password', lang('password')), 'style="width: 173px;" onfocus="if(this.value == \'' . lang('password') . '\') { this.value = \'\' }" onblur="if(this.value.length == 0) { this.value = \'' . lang('password') . '\' }"') ?>
					<span>&nbsp;&nbsp;&nbsp;<a href="<?php echo site_url('forgot-password') ?>" class="gray forgot_password"><?php echo lang('Forgot Password?') ?></a></span>
					<label class="gray"><?= lang('Account:') ?></label><?php echo form_dropdown('account', array('' => lang('Member'), 'performer' => lang('Performer'), 'studio' => lang('Studio'), 'affiliate' => lang('Affiliate')), set_value('account', lang('account')), 'style="width: 184px;" id="account" ') ?><br/>
					<label></label><?php echo form_submit('login', lang('Log in to Account'), 'class="red" style="width:184px;"') ?>
				</div>
				<?php echo form_close() ?>
			</div>
		<?php endif ?>

	</div>
</div>