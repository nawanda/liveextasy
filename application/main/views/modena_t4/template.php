<?php $this->load->view('includes/head')?>
	<body id="main">
		<div id="page_bg">
			<div id="warpper">
				<div id="header" style="background-image: url(<?php echo assets_url()?>/images/header_girl_<?php echo mt_rand(1, 4)?>.png)" class="<?php echo (get_cookie('header') == 'small') ? 'small_header' : NULL?>">
					<?php $this->load->view('includes/logo_box')?>
					<?php $this->load->view('includes/welcome_user_box')?>

					<div class="clear"></div>
				</div>
				<div class="clear"></div>
				
				
				<?php $this->load->view('includes/menu');?>
					
				<div id="page">
					<?php $this->load->view('includes/errors');?>
					
					<?php	
						if(isset($_sidebar) && $_sidebar){
							$this->load->view('includes/_sidebar');
						}elseif(isset($page)){						
							$this->load->view($page);
						}
					?>
					
				</div>
			</div>
			<div class="clear"></div>
			<?php $this->load->view('includes/footer')?>
		</div>
	</body>
</html>