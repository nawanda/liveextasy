<div class="w-col w-col-6 paddingzero">
  <ul class="m5-secondary-nav w-clearfix">
		<li class="m5-sec-link">
			<a class="m5-link"  href="<?php echo base_url()?>">
				<?php echo lang('Online Models')?>
			</a>
		</li>
		<li class="m5-sec-link">
			<a class="m5-link"  href="<?php echo site_url('performers')?>">
				<?php echo lang('Performers')?>
			</a>
		</li>
		<li class="m5-sec-link">
			<a class="m5-link"  href="<?php echo site_url('videos')?>">
				<?php echo lang('Videos')?>
			</a>
		</li>
		<li class="m5-sec-link">
			<a class="m5-link"  href="<?php echo site_url('favorites')?>">
				<?php echo lang('Favorites')?>
			</a>
		</li>
	</ul>
</div>
<div class="w-col w-col-6">
	<div class="form-block w-clearfix w-form">
		<form class="m5-search-form w-clearfix" id="email-form" name="email-form" action="<?php echo site_url('performers')?>">
			<input class="m5-search-input w-input" data-name="Field 2" id="search" maxlength="256" name="search" placeholder="Nickname" type="text" value="<?php if(isset($_GET['search'])) echo $_GET['search']; ?>">
			<input class="submit-button w-button" data-wait="Please wait..." type="submit" value="Search"></form>
	</div>
</div>
