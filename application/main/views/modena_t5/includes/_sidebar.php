<div class="shadow">
	<div id="user_settings" >
		<div class="w-tabs" data-duration-in="300" data-duration-out="100">
			<div class="w-tab-menu">
				<div class="m5-tab-link-simple w-inline-block <?=($this->uri->segment(1) == 'settings') ? 'w--current' : NULL ?>" onclick="document.location.href='<?php echo site_url('settings')?>';">
					<a class="m5-tab-link-simple w-inline-block w-tab-link <?=($this->uri->segment(1) == 'settings') ? 'w--current' : NULL ?>" href="<?php echo site_url('settings')?>"><?php echo lang('Password') ?></a>
				</div>			
				<div class="m5-tab-link-simple w-inline-block <?=($this->uri->segment(1) == 'newsletter') ? 'w--current' : NULL ?>" onclick="document.location.href='<?php echo site_url('newsletter')?>';">
					<a class="m5-tab-link-simple w-inline-block w-tab-link <?=($this->uri->segment(1) == 'newsletter') ? 'w--current' : NULL ?>" href="<?php echo site_url('newsletter')?>"><?php echo lang('Newsletter') ?></a>
				</div>
				<div class="m5-tab-link-simple w-inline-block <?=($this->uri->segment(1) == 'cancel-account') ? 'w--current' : NULL ?>" onclick="document.location.href='<?php echo site_url('cancel-account')?>';">
					<a class="m5-tab-link-simple w-inline-block w-tab-link <?=($this->uri->segment(1) == 'cancel-account') ? 'w--current' : NULL ?>" href="<?php echo site_url('cancel-account')?>"><?php echo lang('Cancel account') ?></a>
				</div>
			</div>				
			<div class="clear"></div>
		</div>
	</div>
	
	<div class="tab-pane w--tab-active w-tab-pane">
		<div class="m5-tab-content">
	<?php
		if(isset($page)){
			$this->load->view($page);
		}
	?>
		</div>
	</div>
</div>