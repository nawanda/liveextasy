<div class="search">
	<a href="<?php echo site_url('videos')?>" class="<?php echo ($this->video_type === NULL)?'red_btn':'black_btn'?>" ><?php echo lang('All videos')?></a>
	<a href="<?php echo site_url('videos')?>?type=free"  class="<?php echo ($this->video_type === 0)?'red_btn':'black_btn'?>"><?php echo lang('Free videos')?></a>
	<a href="<?php echo site_url('videos')?>?type=paid" class="<?php echo ($this->video_type === 1)?'red_btn':'black_btn'?>"><?php echo lang('Paid videos')?></a>
</div>