<div class="m5-main-nav w-nav-menu" role="navigation">
	<div class="box-nav">

		<?php if ($this->user->id > 0): ?>

		<div class="login-navbar">

		<?php $bold_credits = '<span class="white bold" id="head_user_chips">' . print_amount_by_currency($this->user->credits, FALSE, FALSE, FALSE) . '</span>' ?>
			<div class="fl_l credits">
				<?=$bold_credits?><br/>
				<button class="yellow-button bold" style="margin-top:10px;" onclick="document.location.href='<?php echo site_url('add-credits') ?>'"><?=lang('Buy chips')?></button>
			</div>
			<div class="border-div"></div>

			<span class="welcome-txt"><?php echo lang('Welcome back') ?> &nbsp;
				<span><?php echo $this->user->username ?></span>
			</span>
			<a href="<?php echo site_url('messenger') ?>" class="message"><i class="fa fa-inbox" aria-hidden="true"></i> Message</a>

		</div>	
		<?php else: ?>

		<div class="logout-navbar">

			<?php echo sprintf(lang('<a href="%s" class="m5-emphasize-link m5-nav-link w-nav-link">Register</a> <a class="m5-nav-link w-nav-link" href="'.site_url('login').'" >Login</a>'), site_url('register')) ?>

		</div>
		
		<?php endif; ?>

	</div>	
	<div class="box-nav-content">
		<?php if ($this->user->id > 0): ?>

			<div class="fl_l menu">
				<div class="menu_item">
					<a href="<?php echo site_url('settings') ?>">
						<i class="fa fa-cogs" aria-hidden="true"></i>
						<span><?php echo lang('Settings') ?></span>
					</a>
				</div>
				<div class="menu_item">
					<a href="<?php echo site_url('payments') ?>">
						<i class="fa fa-money" aria-hidden="true"></i>
						<span><?php echo lang('Payments') ?></span>
					</a>
					
				</div>
				<div class="menu_item">
					<a href="<?php echo site_url('statement') ?>">
						<i class="fa fa-check-circle" aria-hidden="true"></i>
						<span><?php echo lang('Statements') ?></span>
					</a>	
				</div>
				<div class="menu_item">
					<a href="<?php echo site_url('logout') ?>">
						<i class="fa fa-sign-out" aria-hidden="true"></i>
						<span><?php echo lang('Logout') ?></span>
					</a>
				</div>
			</div>
		<div class="clear"></div>
		<?php else: ?>

		<?php endif ?>
	</div>
</div>
