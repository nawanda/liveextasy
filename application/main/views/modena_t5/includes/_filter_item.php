<?php if(is_array($filter) && count($filter)):?>

<li>
	<?php foreach ($filter as $key => $val):?>
		<?php if($key == ''):?>
			<span class="icon filter_arrow_down"></span>
			<span class="filter_name"><?=lang($val)?></span>
			<ul filter="<?=$filter_key?>">
		<?php else: ?>
			<li filter="<?=$key?>">
				<span class="icon <?=(isset($filter_array[$filter_key]) && array_search($key, $filter_array[$filter_key])) ? 'filter_remove' : 'filter_add'?>"></span><?=$val?>
			</li>
		<?php endif; ?>
		<?php endforeach;?>
	</ul>
</li>
<?php endif ?>