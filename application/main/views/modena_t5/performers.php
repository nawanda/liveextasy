<?php //if(isset($page_refresh)):?>
	<!-- refresh la live performers -->
	<script type="text/javascript">
		var auto_refresh = setInterval(
		function(){
			$('#performer_list').load(document.location.href);
		}, 30000);
	</script>
<?php //endif ?>


<!-- jQuery rating -->

<div class="content fl_l" style="min-width: 100%">
	<div id="applied_filters" style="display: none;">

		<? $this->load->view('includes/_title_bar', array('title' => lang('Applied Filters'))) ?>
		<?php echo form_open('', array('method' => 'get', 'id' => 'form_applied_filters'))?>
			<?php if($this->uri->segment(1) == ''):?>
				<?php echo form_hidden('filters[is_online]', '1');?>
			<?php endif?>
		<?php echo form_close()?>
	</div>

		<? $this->load->view('includes/_title_bar', array('title' => ($this->uri->segment(1) == 'performers') ? 'All Performers' : 'Live Performers')) ?>

		<div id="performer_list" class="performer_list">
			<?php if(sizeof($performers) > 0):?>
				<script type="text/javascript" src="<?php echo assets_url()?>js/preview.js"></script>
				<?php foreach($performers as $performer):?>
					<?php $this->load->view('performer',array('performer'=>$performer))?>
				<?php endforeach?>
			<?php else:?>
				<div class="no_results"><?php echo lang('There are no performers online')?></div>
			<?php endif?>
			<?php if($pagination):?>
				<div class="h_line_sep"></div>
				<?php echo $pagination;?>
			<?php endif?>
		</div>
		<div class="clear"></div>
		
		<?php if(isset($performers_random) && sizeof($performers_random) > 0):?>

		<? $this->load->view('includes/_title_bar', array('title' => 'Random Performers')) ?>
		<div class="performer-random">
			<?php foreach($performers_random as $performer):?>
					<?php $this->load->view('performer',array('performer'=>$performer))?>
			<?php endforeach?>
		<?php endif?>
		</div>
		<?php if(isset($performers_in_private) && sizeof($performers_in_private) > 0):?>
			<? $this->load->view('includes/_title_bar', array('title' => 'Performers currently in private')) ?>
			<div class="clear"></div>
			<div class="private-performer">
				<?php foreach($performers_in_private as $performer):?>
						<?php $this->load->view('performer',array('performer'=>$performer))?>
				<?php endforeach?>
			</div>
			<div class="clear"></div>
		<?php endif?>

	</div>

<div class="clear"></div>


<script type="text/javascript">
	$(document).ready(function() {
		$(".stars").stars({
			cancelShow: false,
			disabled: true,
			split: 4
		});
	});
</script>