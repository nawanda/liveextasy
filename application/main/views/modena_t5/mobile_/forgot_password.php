<?php 
$data                           = array();
$data['pageTitle']              = 'Forgot Password?';
$data['pageSubtitle']           = 'Forgot Password?';
$data['titleFirst']             = 'Forgot Password?';
$data['titleSecond']            = '';
$data['pageId']                 = 'recover_password';
$data['page']                   = 'recover_password';
$data['title_bar_red_arrow']    = TRUE;

$this->load->view('template', $data); 