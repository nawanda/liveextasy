<div class="performer_profile performer_videos">
<div data-role="collapsible-set" data-theme="a" data-inset="false" style="width: 100%; margin:0 auto">
    <div data-role="collapsible" data-collapsed="false">
        <h3>Free Videos</h3>
        <div data-role="listview" data-inset="false">
            <div id="freeVideos">
                <?php for($i=1; $i<=22; $i++):?>
                    <div class="left">
                       <div class="video_box">
                            <img src="images/video_box_sample.jpg" style="width: 140px;" />
                        </div>
                    </div>
                <?php endfor?>
                <div class="clear"></div>
            </div>
        </div>
    </div>
    
    <div data-role="collapsible">
        <h3>Paid Videos</h3>
        <div data-role="listview" data-inset="false">
            <div id="paidVideos">
                <?php for($i=1; $i<=22; $i++):?>
                    <div class="left">
                        <div class="video_box">
                            <img src="images/video_box_sample.jpg" style="width: 140px;" />
                        </div>
                   </div>
                <?php endfor?>
                 <div class="clear"></div>
            </div>
        </div>
    </div>
</div>
<!--
    <h1 class="center-text">Free Videos</h1>
	<div id="freeVideos">
        <?php for($i=1; $i<=22; $i++):?>
            <div class="left">
                <div class="video_box">
    <img src="images/video_box_sample.jpg" style="width: 140px;" />
</div>
            </div>
        <?php endfor?>
        <div class="clear"></div>
    </div>
    
    <h1 class="center-text">Paid Videos</h1>
    <div id="paidVideos">
        <?php for($i=1; $i<=22; $i++):?>
            <div class="left">
                <div class="video_box">
    <img src="images/video_box_sample.jpg" style="width: 140px;" />
</div>
            </div>
        <?php endfor?>
         <div class="clear"></div>
    </div>
     -->
        <?php  $this->load->view('mobile/includes/performer_profile_tabs'); ?>
</div>
