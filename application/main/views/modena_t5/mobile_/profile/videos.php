<div class="performer_profile performer_videos">
    <div class="content">
        <div data-role="controlgroup" data-type="horizontal" data-mini="false" data-theme="a" style="text-align: center;">
            <a href="<?php echo current_url()?>?tab=videos&type=free"  class="" data-theme="a" data-role="button"><?php echo lang('Free videos')?></a>
            <a href="<?php echo current_url()?>?tab=videos&type=paid" class="" data-theme="a" data-role="button"><?php echo lang('Paid videos')?></a>
        </div>
        <script type="text/javascript" src="<?php echo assets_url()?>js/swfobject.js"></script>
        <script type="text/javascript" src="<?php echo assets_url()?>js/jquery.qtip-1.0.0-rc3.min.js"></script>
        <script type="text/javascript">
            function previewOpenVideoInModal(video_id){
                $.fancybox({
                    'overlayShow': true,
                    'scrolling': 'no',
                    'type': 'iframe',
                    'width':800,
                    'height':600,
                    'showCloseButton'	: true,
                    'href': '<?php echo site_url('videos/view')?>' + '/' + video_id,
                    'overlayColor': "#FFF",
                    'overlayOpacity': "0.3"
                });
            }

            function pay_video(video_id){
                $.ajax({
                    url: '<?php echo site_url('videos/view/')?>' + '/'+ video_id,
                    complete: function(html) {
                        $.blockUI({ message: html.responseText});
                    }
                });
                return false;
            }

            function register(){
                $.fancybox({
                    'showCloseButton'	: false,
                    'padding'			: 0,
                    'overlayColor'		: '#fff',
                    'overlayOpacity'	: 0.28,
                    'type'				: 'iframe',
                    'href'				: '<?php echo site_url('register')?>',
                    'width'				: 630,
                    'height'			: 458
                });
            }
        </script>
        <div style="text-align: center">
            <?php if(isset($_GET['type']) && $_GET['type'] == 'paid'): ?>
                <?php if(sizeof($videos_paid) > 0):?>
                    <?php foreach($videos_paid as $video):?>

                        <?php $this->load->view('videos/videoperformer',array('video' => $video))?>
                    <?php endforeach?>
            <div class="clear"></div>
                    <?php echo $pagination;?>
                <?php else:?>
            <div class="no_results"><?php echo lang('There are no videos')?></div>
                <?php endif?>
            <div class="clear"></div>
            <?php else:?>
                <?php if(sizeof($videos) > 0):?>
                    <?php foreach($videos as $video):?>

                        <?php $this->load->view('videos/videoperformer',array('video' => $video))?>
                    <?php endforeach?>
            <div class="clear"></div>
                    <?php echo $pagination;?>
                <?php else:?>
            <div class="no_results"><?php echo lang('There are no videos')?></div>
                <?php endif?>
            <div class="clear"></div>
            <?php endif?>
        </div>
    </div>
    <?php  $this->load->view('includes/performer_profile_tabs'); ?>
</div>
