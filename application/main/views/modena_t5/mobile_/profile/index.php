<?php
if(array_key_exists('tab', $_GET)){
    $tab = $this->input->get('tab');
    if($tab == 'videos') $this->load->view('profile/videos');
    else if($tab == 'schedule') $this->load->view('profile/schedule');
    else $this->load->view('profile/profile');
} else {
    $this->load->view('profile/profile');
}