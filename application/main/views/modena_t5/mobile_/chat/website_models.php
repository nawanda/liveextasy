<?php $this->load->view('chat/socket'); ?>

<style>
	#chat_section a {
    	display: inline-block;
    	width: 30px;
    	height: 30px;
    	margin: 0 5px;
	}

</style>

<section id="page">
    <div id="chat_section">
        <section id="chat">
            <div id="chat_middle" style="position:relative">
		
		<canvas id="video-canvas" style="width:100%; height:100%; min-height:281px;"></canvas>
                <?php if($this->user->id > 0){ ?>
                <div class="chat_buttons" style="position: absolute; bottom: 55px; right: 5px; width: 100%">
                    <a href="#popupTip" data-rel="popup"  data-inline="true" rel="internal" class="popupButton" data-position-to="window" style="float:right;">
                        <img src='<?=assets_url().'mobile/images/chat/tipModel.png'?>' style='width: 30px' />
                    </a>
                    <a href="#popupRequestChat" data-rel="popup" data-inline="true" rel="internal" class="popupButton" data-position-to="window" style="float:right;">
                        <img src='<?=assets_url().'mobile/images/chat/requestChat.png'?>' style='width: 30px; position:relative;'/>
                    </a>
                    <a href="" onclick="setVolume(1)" id="vol0">
                        <img src='<?=assets_url().'mobile/images/chat/volume_mute.png'?>' style='width: 30px; height: 30px; position:relative; margin-left:10px;'/>
                    </a>
                    <a href="" onclick="setVolume(0)" style="display: none;" id="vol1">
                        <img src='<?=assets_url().'mobile/images/chat/volume_up.png'?>' style='width: 30px; height: 30px;position:relative; margin-left:10px;'/>
                    </a>


                    <div data-role="popup" id="popupTip" style="padding: 10px;">
                        <div style="float: left">
                            <label for="tip_amount">Chips</label>
                            <input type="input" value="10" name="tip_amount" class="tip_amount" style="width:45px; color: #400" />
                        </div>
                        <div style="float: right;width:50px">
                            <a style="top:5px; text-decoration: none;" class="btn btn4 chat_msg_send ui-link" href="javascript:sendTip()">Tip</a>
                        </div>
                        <div class="clear"></div>
                    </div>
                    
                    <div data-role="popup" id="popupRequestChat" style="padding: 10px;">
                        <a class="btn btn4 ui-link" style="text-decoration: none;" href="javascript:sendInvite('nude')">Request Nude</a>
                        <div style="height: 10px"></div>
                        <a class="btn btn4 ui-link" style="text-decoration: none;" href="javascript:sendInvite('private')">Request Private</a>
                    </div>
                </div>
                <?php } else { ?>
                
                <div class="chat_buttons" style="position: absolute; bottom: 55px; right: 5px; width: 100%">
                    <a href="" onclick="setVolume(1)" id="vol0">
                        <img src='<?=assets_url().'mobile/images/chat/volume_mute.png'?>' style='width: 30px; height: 30px; position:relative; margin-left:10px;'/>
                    </a>
                    <a href="" onclick="setVolume(0)" style="display: none;" id="vol1">
                        <img src='<?=assets_url().'mobile/images/chat/volume_up.png'?>' style='width: 30px; height: 30px;position:relative; margin-left:10px;'/>
                    </a>
                </div>
                
                <?php } ?>

                <div style="position: relative; width: 100%; max-width: 640px; margin: 0 auto">
                    <div style=""> 
                        <img id="image" style="display:none; width: 100%; position:absolute; left:0;top:0;" />
					<div id="iframeStream" style="display:none;"></div>

			<canvas id="video-canvas" style="display:block; width: 100%; position:absolute; left:0;top:0;"></canvas>
                    </div>
                </div>
               <div id="chat_input_cont">
                    <a class="btn btn4 chat_msg_send" style="float: right; width: 50px;margin-left: 8px" href="#"  onClick="sendMessage()">Send</a>
                    <div style="overflow: hidden; padding-left: 5px">
                        <input type="text" data-role="none" maxlength="90" style="width: 100%" placeholder="Start chatting here..." id="inputBox" value="" class="newMessage">
                    </div>
                </div>
            </div>
            <section id="chat_section" class="closed">
                <div id="chat_container" class="closed">
                    <div id="main_chat_container" class="main_chat_container">
                        <p class="system">Your name is <?php echo $params['userName']?></p>
                        <p class="system">Welcome to <?php echo $params['performerNick']?>'s Video Chat!</p>
                    </div>
                    <div id="clear"></div>
                </div>
            </section>
        </section>
    </div>
    <script>
        $(document).ready(function(){
            $('.popupButton').attr('rel','internal');
            $('#inputBox').keypress(function(e){
                code= (e.keyCode ? e.keyCode : e.which);
                if (code == 13) {
                    sendMessage();
                }
            });
            
            $('#inputBox').focus(function(){
                $('#footer').hide();
                $('.title-bar').hide();
            });
            
            $('#inputBox').blur(function(){
                $('#footer').show();
                $('.title-bar').show();
            });
        });
    </script>
    
</section>
