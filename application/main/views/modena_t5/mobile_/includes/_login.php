<div class="login_panel login">
    <?php echo form_open('login', 'id="login_form" data-ajax="false"') ?>
    <div class="username-form">
        <input type="text" value="" name="username" placeholder="Username" data-role="none" />
        <div class="clear"></div>
    </div>
    <div class="password-form">
        <input type="password" value="" name="password" placeholder="Password" data-role="none" />
        <div class="clear"></div>
    </div>
    <a href="#" class="forgot-password">Forgot your password?</a>
    <input type="submit" value="LOGIN" name="submit" data-role="none" />
    <div class="bottom-text">
        <input class="switch" data-ajax="false" type="button" value="SIGN UP" name="submit" data-role="none" />
    </div>
    <?php echo form_close()?>
</div>
<div class="login_panel signup" style="display: none">
    <?php echo form_open('register', 'name="register" id="login_form" data-ajax="false"') ?>
    <div class="username-form">
        <input type="text" value="" name="username" placeholder="Username" data-role="none" />
        <div class="clear"></div>
    </div>
    <div class="password-form">
        <input type="text" value="" name="email" placeholder="Email" data-role="none" />
        <div class="clear"></div>
    </div>
    <div class="password-form">
        <input type="text" value="" name="repeat_email" placeholder="Repeat Email" data-role="none" />
        <div class="clear"></div>
    </div>
    <div class="password-form">
        <input type="password" value="" name="password" placeholder="Password" data-role="none" />
        <div class="clear"></div>
    </div>
    <div class="password-form">
        <input type="password" value="" name="repeat_password" placeholder="Repeat Password" data-role="none" />
        <div class="clear"></div>
    </div>
    <input type="submit" value="SIGN UP" name="submit" data-role="none" />
    <div class="bottom-text">
        <input class="switch" data-ajax="false" type="submit" value="ALREADY A MEMBER" onclick="" name="submit" data-role="none" />
    </div>
    <?php echo form_close()?>
    <div class="login_panel signup_message">
        <p class="center-text bold">Please switch to portrait mode!</p>
    </div>
</div>
<div class="login_panel forgotpassword" style="display: none">
    <?php echo form_open('forgot-password', 'name="register" id="forgotpassword_form" data-ajax="false"') ?>
    <div class="username-form">
        <input type="text" value="" name="username" placeholder="Username" data-role="none" />
        <div class="clear"></div>
    </div>
    <div class="password-form">
        <input type="text" value="" name="email" placeholder="Email" data-role="none" />
        <div class="clear"></div>
    </div>
    <input type="submit" value="SUBMIT" name="submit" data-role="none" />
    <div class="bottom-text">
        <input class="switch" data-ajax="false" type="submit" value="BACK" onclick="" name="submit" data-role="none" />
    </div>
    <?php echo form_close()?>
</div>
<script>
    $(document).on('click','.login_panel.login .bottom-text input.switch',function(e){
        e.preventDefault();
        $('.login_panel.login').hide();
        $('.login_panel.forgotpassword').hide();
        $('.login_panel.signup').show();
    });
    $(document).on('click','.login_panel.login a.forgot-password',function(e){
        e.preventDefault();
        $('.login_panel.login').hide();
        $('.login_panel.signup').hide();
        $('.login_panel.forgotpassword').show();
    });
    $(document).on('click','.login_panel.signup .bottom-text input.switch',function(e){
        e.preventDefault();
        $('.login_panel.signup').hide();
        $('.login_panel.forgotpassword').hide();
        $('.login_panel.login').show();
    });
    $(document).on('click','.login_panel.forgotpassword .bottom-text input.switch',function(e){
        e.preventDefault();
        $('.login_panel.signup').hide();
        $('.login_panel.forgotpassword').hide();
        $('.login_panel.login').show();
    });
</script>
