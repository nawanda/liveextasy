<div class="search_bar" >
    <div class="search_outer">
        <div data-role="fieldcontain">
            <input type="search" name="search" placeholder="Search" data-inline="true" data-theme="a" data-mini="false" data-clear-btn="false" />
            <input id="searchbtn" type="button" value="Search" data-theme="a" class="" data-inline="true" data-mini="false"/>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
    </div>
    <div class="clear"></div>
</div>