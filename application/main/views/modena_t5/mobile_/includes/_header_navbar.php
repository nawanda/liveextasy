<div id="slidemenu">
	<h3>MENU</h3>
	<ul>
            <li><span data-role="none" class="performers-on-icon"></span><a data-role="none" data-ajax="false" href="<?php echo site_url().'home'?>">Online Models</a></li>
            <li><span data-role="none" class="performers-icon"></span><a data-role="none" href="<?php echo site_url().'performers'?>">View all Models</a></li>
            <!--<li><span data-role="none" class="videos-icon"></span><a data-role="none" href="<?php echo site_url().'videos'?>">All Videos</a></li>-->
            <li><span data-role="none" class="favorites-icon"></span><a data-role="none" data-ajax="false" href="<?php echo site_url().'favorites'?>">My Favorites</a></li>
	</ul>
    <?php if($this->user->id > 0):?>
        <div class="clear"></div>
	<h3 class="h3-account">ACCOUNT</h3>
	<ul>
            <li><span data-role="none" class="chips-icon"></span><a data-role="none" href="<?php echo site_url().'add-credits'?>">Order Chips</a></li>
            <li><span data-role="none" class="payments-icon"></span><a data-role="none" href="<?php echo site_url().'payments'?>">My Payments</a></li>
            <li><span data-role="none" class="statements-icon"></span><a data-role="none" href="<?php echo site_url().'statement'?>">My Statement</a></li>
            <li><span data-role="none" class="settings-icon"></span><a data-role="none" data-ajax="false" href="<?php echo site_url().'user/settings'?>">Password</a></li>
            <li><span data-role="none" class="logout-icon"></span><a data-role="none" data-ajax="false" href="<?php echo site_url().'logout'?>">Logout</a></li>
	</ul>
    <?php endif?>
</div>
                