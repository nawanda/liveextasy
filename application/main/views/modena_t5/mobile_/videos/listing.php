<div class="video<?php echo (($video->is_paid && $this->user->id < 1)?' signup':NULL)?>">
	<span class="video_preview">				
		<?php if($video->description):?>
			<span class="title">
				<span class="icon info south" original-title="<?php echo $video->description?>" ></span> 			
			</span>
		<?php endif?>		
		<?php if($video->is_paid):?>
			<span class="cost"><span class="dark_gray"><?php echo lang('Cost')?>: </span> <?php echo print_amount_by_currency($video->price)?></span>
		<?php endif?>
		<span class="video_length dark_gray"><?php echo lang('Time')?>: <?php echo date('i:s',$video->length)?></span>
		<span class="nickname"><a href="<?php echo site_url($video->nickname)?>" class="dark_gray"><?php echo $video->nickname?></a></span>									
	</span>				
</div>