<?php if(isset($searchBar) && $searchBar) $this->load->view('includes/_search_bar')?>
<div id="performers_list" >
    <div class="performers" style="text-align: center">
        <div class="performers-inner" style="display: inline-block; text-align: left">
            <?php if (sizeof($performers) > 0): ?>			
                <?php foreach ($performers as $performer): ?>
                    <?php $this->load->view('performer', array('performer' => $performer)) ?>
                <?php endforeach ?>
            <?php else: ?>
                <div class="no_results"><?php echo lang('There are no performers online') ?></div>			
            <?php endif ?>
            <div class="clear"></div>
        </div>
    </div>
    <?php if ($pagination): ?>
        <div class="h_line_sep"></div>
        <?php echo $pagination; ?>
    <?php endif ?>
    <div class="clear"></div>
</div>
<script>
    $(document).on('click', 'input#searchbtn', function(e) {
        perform_ajax_request();

    });
    function perform_ajax_request() {
        if ($('div.ui-page-active input[name="search"]').val().length >= 3 || $('.search_input').val().length === 0) {
            $search = 'filters%5Bnickname%5D%5B%5D=' + $('input[type="text"][name="search"]').val();
            $order = '';
            $url = "<?php echo site_url('performers/search') ?>?" + $search + '&' + $order;

            $.ajax({
                url: $url,
                type: 'get',
                dataType: "html",
                success: function(text) {
                    $('div.ui-page-active  #performers_list').css('text-align', 'center').html(text);
                }
            });
        }
    }
    
    function get_ajax_page(element) {
        $.ajax({
            url: element.href,
            type: 'get',
            dataType: "html",
            success: function(text) {
                $('div.ui-page-active  #performers_list').css('text-align', 'center').html(text);
            }
        });
    }
</script>