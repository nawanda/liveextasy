<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title><?php echo isset($titleFirst) ? $titleFirst : '';
        echo isset($titleSecond) ? $titleSecond: '' ?></title>

        <!-- iPhone Stuff -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no"/>
        <meta name="apple-mobile-web-app-capable" content="yes"/>
        <meta name="apple-mobile-web-app-status-bar-style" content="black"/>

        <!-- CSS Links -->
        <link rel="stylesheet" type="text/css" href="<?php echo assets_url()?>mobile/css/style.css" media="all" />
        <link rel="stylesheet" type="text/css" href="<?php echo assets_url()?>mobile/css/mediaqueries.css" media="all"/>

        <link rel="stylesheet" href="<?php echo assets_url()?>mobile/css/jquery.mobile-1.3.2.css" />
            
        <link rel="stylesheet" href="<?php echo assets_url()?>mobile/css/idangerous.swiper.css" />
        <link rel="stylesheet" href="<?php echo assets_url()?>mobile/css/sliders.css" />

        <!-- Scripts -->
        <script src="<?php echo assets_url()?>mobile/js/jquery-1.10.2.js"></script>
        <script src="<?php echo assets_url()?>mobile/js/idangerous.swiper-2.1.min.js"></script>
        <script src="<?php echo assets_url() ?>js/jquery.validate.js"></script>

        <!--Include JQM and JQ
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>-->
        <script src="<?php echo assets_url()?>mobile/js/jquery.animate-enhanced.min.js"></script>
        <!--JQM globals you can edit or remove file entirely... note it needs to be loaded before jquerymobile js -->
        <script src="<?php echo assets_url()?>mobile/js/jqm.globals.js"></script>
        <script src="//code.jquery.com/mobile/latest/jquery.mobile.min.js"></script>
        <!--JQM SlideMenu-->
        <link rel="stylesheet" href="<?php echo assets_url()?>mobile/css/jqm.slidemenu.css" />
        <script src="<?php echo assets_url()?>mobile/js/jqm.slidemenu.js"></script>
        
        <link rel="stylesheet" type="text/css" href="<?php echo assets_url()?>mobile/css/mobile-v5.css" media="all"/>
        <script src="//cdnjs.cloudflare.com/ajax/libs/fastclick/1.0.0/fastclick.min.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(function() {
                FastClick.attach(document.body);
            });
            </script>
    </head>
    <body>
        <?php $this->load->view('includes/_header_navbar')?>
        <div id="<?php echo (isset($pageId))?$pageId:'pageIdNotSet';?>"  data-role="page">
            <div id="header">
                <?php $this->load->view('includes/_title_bar')?>
            </div>
            <div id="container" data-role="none">
                <div id="content">                    
                    <?php $this->load->view('includes/errors');?>
                    <?php $this->load->view($page)?>
                </div>
            </div>
           
        </div>
 <?php if($this->user->id > 0):?>
                <div id="footer">
                    <a class="slide-button isDown"></a>
                    <div class="slider-content account" >
                        <?php $this->load->view('user/my_account', array('user' => $this->user))?>
                    </div>
                </div>
            <?php else: ?>
                <div id="footer">
                    <a class="slide-button isDown" style="cursor: pointer"></a>
                    <div class="slider-content" >
                        <?php $this->load->view('includes/_login')?>
                    </div>
                </div>
            <?php endif ?>
        <!-- Scripts -->
        <script src="<?php echo assets_url()?>mobile/js/footer_slider.js"></script>
    </body>
</html>