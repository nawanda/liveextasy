<?php echo form_open('', array('id' => 'add_credits', 'data-ajax' => 'false'))?>
<div class="order_chips">
	<?php foreach($packages as $row => $package):?>
    <div class="package_box">
        <div class="header">
            <div class="title left"><span><?php echo $package['name']?></span></div>
            <div class="price right"><?php echo SETTINGS_REAL_CURRENCY_SYMBOL.$package['value']?></div>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
        <div class="triangle-left left"></div>
        <div class="information left">
            <ul>
                <li><?php echo $package['credits']?><?php echo lang('chips')?></li>
                <li><?php echo sprintf(lang('as low as %s per chips'), SETTINGS_REAL_CURRENCY_SYMBOL.round($package['value'] /($package['credits']+ $package['bonus']),2))?></li>
            </ul>
            <input type="hidden" name="package" value="<?php echo $row?>" />
            <input name="submit" type="submit" data-package="<?=$row?>" value="Order Now" data-role="none" />
        </div>
        <div class="triangle-right left"></div>
        <div class="clear"></div>
    </div>  
    <?php endforeach;?>
</div>
<?php echo form_close()?>
<script>
$(document).ready(function(){
    
    $("form#add_credits input[type=submit]").click(function(e) {

        $package_id = $(e.target).data('package');
        
        $("form#add_credits input[name=package]").each(function(){
            if($(this).val() != $package_id){
                $(this).remove();
            }
        });
        
        return true;
    });
});
</script>