<div class="content center-text" style="margin-top: 20px;">
	<div class="gray italic" id="user_settings" style="width: 80%; display: inline-block">
		<?php echo form_open('', 'class="change_pass" data-ajax="false"')?>
				<div>
					<span class="bold" style="font-size: 1.5em"><?php echo lang('Old Password') ?>:</span>
					<?php echo form_password('old_password', '', 'data-theme="a" data-inline="true"')?>
					<span class="error" htmlfor="old_password" generated="true"><?php echo form_error('old_password')?></span>
				</div>
            
                                <div>
					<span class="bold" style="font-size: 1.5em"><?php echo lang('New Password') ?>:</span>
					<?php echo form_password(array('name' => 'new_password', 'id' => 'new_password', 'data-theme' => 'a'))?>
					<span class="error" htmlfor="new_password" generated="true"><?php echo form_error('new_password')?></span>
				</div>
                                
                                <div>
                                        <label><span class="dark_gray italic bold" style="font-size: 1.5em"><?php echo lang('Newsletter') ?>:</span></label>
                                        <?php echo form_dropdown('newsletter',$newsletter,set_value('newsletter', $user->newsletter), 'data-theme="a"') ?>
                                        <span class="error" htmlfor="newsletter" generated="true"><?php echo form_error('newsletter')?></span>
                                </div>
				<div>
					<?php echo form_submit('submit',lang('Save'), 'class="red"  data-theme="a"')?>
				</div>
		<?php echo form_close() ?>
            	<div class="dark_gray italic" id="userAccount" style="padding: 10px; text-align: justify;">
		<div class="form" style="text-align:center; font-size: 2em;"> 					
			<?php echo  form_open('cancel-account', 'data-ajax="false"') ?>
				<?php echo form_hidden('account',1) ?>
				<span><?php echo lang('Cancelling your account cannot be reversed!') ?></span>		
				<div class="buttons">
					<?php echo form_submit('submit', lang('Cancel Account'), 'data-inline="true" data-theme="a" class="red" style="width:100px; margin:3px 0px;" onclick="return confirm(\'' . lang('Cancel account action is irevesible, are you sure you want to cancel it?') . '\')"') ?>						
				</div>
			<?php echo form_close() ?>
		</div>
	</div>	
	</div>	     
</div>