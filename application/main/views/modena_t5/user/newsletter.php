<div class="content">
	<h3 class="m5-title"><?php echo lang('Newsletter Settings') ?></h3>	
	<div class="dark_gray italic" id="user_settings">
		<?php echo  form_open() ?>
			<div class="m5-signup-form w-form">
				<label class="m5-form-label"><?php echo lang('Newsletter') ?>:</label>
				<?php echo form_dropdown('newsletter',$newsletter,set_value('newsletter', $user->newsletter), 'class="m5-input w-input"') ?>
				<span class="error" htmlfor="newsletter" generated="true"><?php echo form_error('newsletter')?></span>
			</div>
			<div>
				<?php echo form_submit('submit', lang('SAVE'), 'class="submit-button-2 w-button"') ?>			
			</div>
		<?php echo form_close() ?>
	</div>		
</div>