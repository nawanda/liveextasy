<div class="content">			
	<? $this->load->view('includes/_title_bar', array('title' => 'Favorite performers')) ?>
	<?php if( ! is_array($performers)):?>
		<div class="helvetica italic"><?php echo lang('You have no favorite performers!') ?></div>
	<?php else:?>
		<?php foreach( $performers as $performer ):?>
			<?php $this->load->view('performer',array('performer'=>$performer))?>
		<?php endforeach?>					
	<?php endif;?>
	<?php echo $pagination;?>
	<div class="clear"></div>
</div>
<script src="<?php echo assets_url() ?>js/jquery.ui.stars.min.js"></script>