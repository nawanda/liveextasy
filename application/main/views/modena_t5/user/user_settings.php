<script type="text/javascript">
jQuery(function($){

	$.validator.setDefaults({
		validClass:"success",
		errorElement: "span",
		errorPlacement: function(error, element) {
			error.appendTo($(element).next('span'));
			
		}
	});
		
	$(".change_pass").validate({		
		success: function(label) {
	   },
		rules: {
		    old_password: {
	   			required: true,
				minlength: 5
			},
			new_password: {
		   		required: true,
				minlength: 5
			}
		}, 
		messages: {
			old_password: 					"<?php echo lang('Password must have at least 5 characters') ?>",
			new_password:					"<?php echo lang('Password must have at least 5 characters') ?>"
		}
	});
}); 
</script>	
<div class="acount-page">
	<h3 class="m5-title"><?php echo lang('Change Password') ?></h3>	
	<div id="user_settings">
		<?php echo form_open('', 'class="change_pass"')?>
			<div class="m5-signup-form w-form">
				<div class="input-fix">
					<label class="m5-form-label"><?php echo lang('Old Password') ?>:</label>
					<?php echo form_password(array('name' => 'old_password', 'class' => 'm5-input w-input'))?>
					<span class="error" htmlfor="old_password" generated="true"><?php echo form_error('old_password')?></span>
				</div>
				<div class="input-fix">
					<label class="m5-form-label"><?php echo lang('New Password') ?>:</label>
					<?php echo form_password(array('name' => 'new_password', 'id' => 'new_password', 'class' => 'm5-input w-input'))?>
					<span class="error" htmlfor="new_password" generated="true"><?php echo form_error('new_password')?></span>
				</div>
				<div>
					<?php echo form_submit('submit',lang('Save'), 'class="submit-button-2 w-button"')?>
				</div>
			</div>
		<?php echo form_close() ?>
		<div class="clear"></div>
	</div>
</div>