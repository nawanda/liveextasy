
	<div class="packages-view">  
		<h3 class="m5-title"><?php echo lang('Select Package')?></h3>
	    <h5 class="m5-subtitle">Choose your package to tip performers</h5>
	    <div class="m5-panel shadow price-panel">
	       <div class="w-row">
	        <?php foreach($packages as $row => $package):?>
	        <?php echo form_open('', array('id' => 'add_credits'))?>
	          	<div class="m5-pricepack w-col w-col-3">
					<?php if($package['best'] == 1){?>
						<span class="best-value">Best Value</span>
						<?php }  ?>
	            	<div class="m5-pricebox-container">

		              	<h1 class="m5-coins"><?php echo $package['credits']?></h1>
		
		              	<h3 class="m5-coins-text">
			              	<?php if(  SETTINGS_CURRENCY_TYPE): //TODO TEST?>
								<?php echo lang(SETTINGS_SHOWN_CURRENCY)?>
							<?php endif?>
						</h3>
						<h3 class="price-bonus">
							<?php if($package['bonus'] != 0):?>
							+ <?php echo $package['bonus']?> <span><?php echo lang('Bonus')?></span>
							<?php endif?> 
						</h3>

		              	<h4 class="m5-price"><?php echo SETTINGS_REAL_CURRENCY_SYMBOL.$package['value']?></h4>
		              	<input type="hidden" name="package" value="<?php echo $row; ?>">
		              	<?php echo form_submit('submit',lang('Buy now'), "class='m5-button-yellow w-button'")?>

	              	</div>
	          	</div>
	        <?php echo form_close()?>
	        <?php endforeach;?>
	        </div>
	    </div>
	    <div class="pay-notice center">
	        <img src="<?php echo assets_url()?>images/purchase-cards.png?>" alt="">
	        <h3 class="white bold">Discreet, Secure & 24/7 Costumer Support.</h3>
			<p class="white">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. </p>
	    </div>
	</div>