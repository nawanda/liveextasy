
    <script type="text/javascript" src="<?php echo assets_url()?>swf/assets/js/swfobject.js"></script>

      <script type="text/javascript">
            // For version detection, set to min. required Flash Player version, or 0 (or 0.0.0), for no version detection. 
            var swfVersionStr = "0.0.0";
            // To use express install, set to playerProductInstall.swf, otherwise the empty string. 
            var xiSwfUrlStr = "playerProductInstall.swf";
            var flashvars = {};
            
            flashvars.skin = "modern"
            flashvars.assetDir = "<?php echo assets_url()?>swf/assets"
            flashvars.themeFile="<?php echo assets_url()?>swf/assets/files/modern/theme.xml" 
            flashvars.languageFile="<?php echo main_url()?>fms/translation/<?php echo $params['performerId']?>"     
            flashvars.configFile="<?php echo assets_url()?>swf/assets/files/config.xml" 
            flashvars.smileysFile="<?php echo assets_url()?>swf/assets/files/smileys.xml"
            flashvars.censureFile="<?php echo main_url()?>swf/assets/files/censure.xml"
            flashvars.sitePath="<?php echo main_url()?>"
            flashvars.nudeLink="<?php echo site_url('room/'.$params['performerNick'].'/nude')?>"
            flashvars.peekLink="<?php echo site_url('room/'.$params['performerNick'].'/peek')?>"
            
            flashvars.rtmp="<?php echo $params['rtmp']?>"
            
            flashvars.pasword="<?php echo $params['pasword']?>"
            flashvars.userName="<?php echo $params['userName']?>"
            
            flashvars.nickColor="0x123FF0"
            
            flashvars.performerId = "<?php echo $params['performerId']?>"
            flashvars.fmsId="<?php echo $params['fmsId']?>"
            flashvars.uniqId="<?php echo $params['uniqId']?>"
            
            flashvars.userId="<?php echo $params['userId']?>";         
            flashvars.sessionType="<?php echo $params['sessionType']?>";
            flashvars.chips=<?php echo $params['chips']?>;
            flashvars.minChipsForPrivate=<?php echo $params['minChipsForPrivate']?>;
            flashvars.performersFeePerMinutePrivate=<?php echo $params['performersFeePerMinutePrivate']?>;
            flashvars.performersFeePerMinuteTruePrivate=<?php echo $params['performersFeePerMinuteTruePrivate']?>;
            flashvars.performersFeePerMinutePeek=<?php echo $params['performersFeePerMinutePeek']?>;
            flashvars.performersFeePerMinuteNude=<?php echo $params['performersFeePerMinuteNude']?>;
            flashvars.redirectLink="<?php echo $params['redirectLink']?>";
            flashvars.performerNick="<?php echo $params['performerNick']?>";
            flashvars.performerNickColor="<?php echo $params['performerNickColor']?>";
            flashvars.noChat="<?php echo $params['noChat']?>";
            flashvars.showTipButton="<?php echo $params['showTipButton']?>";
            flashvars.minChipsForNude=1;
            flashvars.goalStatus = "true"
            
            flashvars.logoImage = "<?=assets_url().'images/watermark.png'?>"  
            flashvars.logoBottom = "10"  
            flashvars.logoRight= "10"
            //flashvars.logoTop = "10"  
            //flashvars.logoLeft= "10"
            
            var params = {};
            params.quality = "high";
            params.scale="exactfit";
            params.allowscriptaccess = "always";
            params.allowfullscreen = "true";
            params.wmode = "direct";
            var attributes = {};
            attributes.id = "ViewerApp";
            attributes.name = "ViewerApp";
            attributes.align = "middle";
            attributes.style=" margin-top:-3px;";
            swfobject.embedSWF(
                "<?php echo main_url()?>assets/swf/ViewerApp.swf", "flashContent", 
                "100%", "600", 
                swfVersionStr, xiSwfUrlStr, 
                flashvars, params, attributes);
            // JavaScript enabled so display the flashContent div in case it is not replaced with a swf object.
            swfobject.createCSS("#flashContent", "display:block;text-align:left;");
        </script>
    </head>
    <body>
        <!-- SWFObject's dynamic embed method replaces this alternative HTML content with Flash content when enough 
             JavaScript and Flash plug-in support is available. The div is initially hidden so that it doesn't show
             when JavaScript is disabled.
        -->
        
            <div id="flashContent">
                <p>
                    To view this page ensure that Adobe Flash Player version 
                    0.0.0 or greater is installed. 
                </p>
                <script type="text/javascript"> 
                    var pageHost = ((document.location.protocol == "https:") ? "https://" : "http://"); 
                    document.write("<a href='http://www.adobe.com/go/getflashplayer'><img src='" 
                                    + pageHost + "www.adobe.com/images/shared/download_buttons/get_flash_player.gif' alt='Get Adobe Flash player' /></a>" ); 
                </script> 
            </div>
        <noscript>
            <!-- width="983" height="600" -->
            <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="100%" height="600" id="ViewerApp">
                <param name="movie" value="<?php echo main_url()?>assets/swf/ViewerApp.swf" />
                <param name="quality" value="high" />
                <param name="bgcolor" value="#ffffff" />
                <param name="allowScriptAccess" value="always" />
                <param name="allowFullScreen" value="true" />
                <param name="sAlign" value="tl" />
                <param name="scale" value="exactfit" />
                <param name="themeFile" value="<?php echo assets_url()?>swf/assets/files/theme.xml"/>
                <param name="languageFile" value="<?php echo main_url()?>fms/translation/<?php echo $params['performerId']?>"/>
                <param name="configFile" value="<?php echo assets_url()?>swf/assets/files/config.xml"/>
                <param name="smileysFile" value="<?php echo assets_url()?>swf/assets/files/smileys.xml"/>
                <param name="censureFile" value="<?php echo main_url()?>censure.xml"/>
                <param name="sitePath" value="<?php echo main_url()?>"/>
                <param name="nudeLink" value="<?php echo site_url('room/'.$params['performerNick'].'/nude')?>"/>
                <param name="peekLink" value="<?php echo site_url('room/'.$params['performerNick'].'/peek')?>"/>

                <param name="rtmp" value="<?php echo $params['rtmp']?>" />
                <param name="pasword" value ="<?php echo $params['pasword']?>"  />
                <param name="userName" value ="<?php echo $params['userName']?>"  />
                <param name="nickColor" value="0xFF0010"  />
                <param name="performerId" value = "<?php echo $params['performerId']?>"  />
                <param name="fmsId" value="<?php echo $params['fmsId']?>"  />    
                <param name="uniqId" value="<?php echo $params['uniqId']?>" />     

                <param name="userId" value="<?php echo $params['userId']?>"/>          
                <param name="sessionType" value="<?php echo $params['sessionType']?>"/> 
                <param name="chips" value=<?php echo $params['chips']?>/> 
                <param name="minChipsForPrivate" value=<?php echo $params['minChipsForPrivate']?>/> 
                <param name="performersFeePerMinutePrivate" value=<?php echo $params['performersFeePerMinutePrivate']?>/> 
                <param name="performersFeePerMinuteTruePrivate" value=<?php echo $params['performersFeePerMinuteTruePrivate']?>/> 
                <param name="performersFeePerMinuteNude" value=<?php echo $params['performersFeePerMinuteNude']?>/> 
                <param name="redirectLink" value="<?php echo $params['redirectLink']?>"/> 
                <param name="performerNick" value="<?php echo $params['performerNick']?>"/> 
                <param name="performerNickColor" value="<?php echo $params['performerNickColor']?>"/> 
                <param name="noChat" value="<?php echo $params['noChat']?>"/> 
                <param name="showTipButton" value="<?php echo $params['showTipButton']?>"/>                
                
                <!--[if !IE]>-->
                <object type="application/x-shockwave-flash" data="<?php echo main_url()?>assets/swf/ViewerApp.swf" width="100%" height="600">
                    <param name="quality" value="high" />
                    <param name="bgcolor" value="#ffffff" />
                    <param name="allowScriptAccess" value="always" />
                    <param name="allowFullScreen" value="true" />
                    <param name="sAlign" value="tl" />
                    <param name="scale" value="exactfit" />
                    <param name="themeFile" value="<?php echo assets_url()?>swf/assets/files/theme.xml"/>
                    <param name="languageFile" value="<?php echo main_url()?>fms/translation/<?php echo $params['performerId']?>"/>
                    <param name="configFile" value="<?php echo assets_url()?>swf/assets/files/config.xml"/>
                    <param name="smileysFile" value="<?php echo assets_url()?>swf/assets/files/smileys.xml"/>
                    <param name="censureFile" value="<?php echo main_url()?>censure.xml"/>
                    <param name="sitePath" value="<?php echo main_url()?>"/>
                    <param name="nudeLink" value="<?php echo site_url('room/'.$params['performerNick'].'/nude')?>"/>
                    <param name="peekLink" value="<?php echo site_url('room/'.$params['performerNick'].'/peek')?>"/>
                    
                    <param name="rtmp" value="<?php echo $params['rtmp']?>" />
                    <param name="pasword" value ="<?php echo $params['pasword']?>"  />
                    <param name="userName" value ="<?php echo $params['userName']?>"  />
                    <param name="nickColor" value="0xFF0010"  />
                    <param name="performerId" value = "<?php echo $params['performerId']?>"  />
                    <param name="fmsId" value="<?php echo $params['fmsId']?>"  />    
                    <param name="uniqId" value="<?php echo $params['uniqId']?>" />     

                    <param name="userId" value="<?php echo $params['userId']?>"/>          
                    <param name="sessionType" value="<?php echo $params['sessionType']?>"/> 
                    <param name="chips" value=<?php echo $params['chips']?>/> 
                    <param name="minChipsForPrivate" value=<?php echo $params['minChipsForPrivate']?>/> 
                    <param name="performersFeePerMinutePrivate" value=<?php echo $params['performersFeePerMinutePrivate']?>/> 
                    <param name="performersFeePerMinuteTruePrivate" value=<?php echo $params['performersFeePerMinuteTruePrivate']?>/> 
                    <param name="performersFeePerMinuteNude" value=<?php echo $params['performersFeePerMinuteNude']?>/> 
                    <param name="redirectLink" value="<?php echo $params['redirectLink']?>"/> 
                    <param name="performerNick" value="<?php echo $params['performerNick']?>"/> 
                    <param name="performerNickColor" value="<?php echo $params['performerNickColor']?>"/> 
                    <param name="noChat" value="<?php echo $params['noChat']?>"/> 
                    <param name="showTipButton" value="<?php echo $params['showTipButton']?>"/>           
                    
                <!--<![endif]-->
                <!--[if gte IE 6]>-->
                    <p> 
                        Either scripts and active content are not permitted to run or Adobe Flash Player version
                        0.0.0 or greater is not installed.
                    </p>
                <!--<![endif]-->
                    <a href="http://www.adobe.com/go/getflashplayer">
                        <img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash Player" />
                    </a>
                <!--[if !IE]>-->
                </object>
                <!--<![endif]-->
            </object>
        </noscript> 

