<div class="m5-container w-container">
      <h3 class="m5-title">Free Signup</h3>
      <h5 class="m5-subtitle">Please fill out the form with your information</h5>
      <div class="m5-panel shadow">
        <h4 class="m5-panel-heading">Personal information</h4>
        <div class="m5-signup-form w-form">
					<?php echo form_open('register','id="register" name="register"')?>
						<label class="m5-form-label"><?php echo lang('Username:')?></label>
						<div class="input-fix">
							<?php echo form_input('username', set_value('username'), 'class="m5-input w-input" placeholder="Enter your username"' )?>
							<div class="error-input">
								<?php echo form_error('username')?form_error('username'):($this->input->post('username')?NULL:"")?>
							</div>
						</div>	
						<div class="input-fix">
							<label class="m5-form-label"><?php echo lang('Email Address:')?></label>
							<?php echo form_input('email', set_value('email'), 'class="m5-input w-input" placeholder="Enter your email address"')?>
							<div class="error-input">
								<?php echo form_error('email')?form_error('email'):($this->input->post('email')?NULL:"")?>
							</div>
						</div>
						<div class="input-fix">
							<label class="m5-form-label"><?php echo lang('Repeat Email Address:')?></label>
							<?php echo form_input('repeat_email', set_value('repeat_email'), 'class="m5-input w-input" placeholder="Repeat Email Address:"')?>
							<div class="error-input">
								<?php echo form_error('repeat_email')?form_error('repeat_email'):($this->input->post('repeat_email')?NULL:"")?>
							</div>
						</div>
						<div class="input-fix">
							<label class="m5-form-label"><?php echo lang('Password:')?></label>
							<?php echo form_password('password', NULL, 'class="m5-input w-input"')?>
							<div class="error-input">
								<?php echo form_error('password')?form_error('password'):($this->input->post('password')?NULL:"")?>
							</div>	
						</div>
						<div class="input-fix">
							<label class="m5-form-label"><?php echo lang('Repeat Password:')?></label>
							<?php echo form_password('repeat_password', NULL, 'class="m5-input w-input"')?>
							<div class="error-input">
								<?php echo form_error('repeat_password')?form_error('repeat_password'):($this->input->post('repeat_password')?NULL:"")?>
							</div>            			
            			</div>
            			<div class="checkbox-field m5-agree w-checkbox">
							<input class="w-checkbox-input" data-name="Checkbox" id="checkbox" name="checkbox" type="checkbox">
							<label class="m5-agree-text w-form-label" for="checkbox">I agree to the <a href="<?php echo base_url()?>documents/tos">Terms and Conditions</a></label>
						</div>
						<input class="submit-button-2 w-button" data-wait="Please wait..." type="submit" value="Continue">
					<?php echo form_close()?>
          <div class="w-form-done">
            <div>Thank you! Your submission has been received!</div>
          </div>
          <div class="w-form-fail">
            <div>Oops! Something went wrong while submitting the form</div>
          </div>
        </div>
      </div>
    </div>
