<?php $this->load->view('includes/header_modal')?>
	<body>
		<div id="browsers_restrictions">
			<h1 style="text-align: center">Your browser is too old!</h1>
			<p>
				Your browser is over 6 years old and thus cannot run this website's features. We recommend you install another browser or update your current browser to view this site's content and experience a safer Internet!
			</p>
			<br/>
			<p>
				 We recommend you to use the following browsers.
			</p>
			<div style="text-align: center">
				<img src="<?php echo assets_url()?>images/browsers_accepted.png" />
			</div>
		</div>
	</body>
</html>