<script type="text/javascript">
	function endShow(type){
		if(type == 'public'){
			document.location = '<?php echo site_url()?>';
		} else {
			document.location = '<?php echo site_url($params['performerNick'] . '/review?id=' . $params['uniqId'])?>';
		}
	}
        
        function showRegisterForm(){
            $.fancybox({
                'showCloseButton'	: false,
                'padding'			: 0,
                'overlayColor'		: '#000  ',
                'overlayOpacity'	: 0.6,
                'type'				: 'iframe',
                'titleShow'			: false,
                'href'				: '<?php echo site_url().'register'?>',
                'width'				: 630,
                'height'			: 458
            });
        }
        function privateCalledByAnon(){
            showRegisterForm();
        }
        function nudeCalledByAnon(){
            showRegisterForm();
        }
        
        function chatDisabledInFlash(){
            showRegisterForm();
        }		
	
	function update_chips(){
		var user_id = <?php echo $this->user->id?>;
		$.ajax({
        	url: "<?php echo site_url('user/update_chips')?>",
            type: 'get',
            dataType: "json",
            success: function(response) {
				
				$('#user_chips').html(response.credits);
				$('#head_user_chips').html(response.credits);
				
				if(!response.rem_time){
					response.rem_time = 'n/a'
				}
				
				$('#time_remaining').html(response.rem_time + ' <?php echo lang('minute(s)')?>');
            }
        });
		if(user_id > 0){
			setTimeout(update_chips, 5000);
		}
	}


	function small1(){
		$('#flashContent').find('object').height('540');
	}

	function large1(){
		$('#flashContent').find('object').height('750');
	}
	
	window.history.forward();
	function noBack(){ window.history.forward(); }	
	
	$(document).ready(function(){
		noBack();
	<?php if($this->user->id > 0):?>
		update_chips();
	<?php endif?>
	});
</script>
	<div class="content">
		<div class="title">
			<?php echo lang('Performer\'s Chat Room') ?>
		</div>
			<!-- <div id="chat_info">
				<div>
					<?php if($params['sessionType'] == 'public' || $params['sessionType'] == 'private'):?>
						<span class="info_name italic darek_gray"><?php echo lang('Private chat rate')?>:</span>
						<span class="info_value italic red"><?php echo print_amount_by_currency($params['performersFeePerMinutePrivate'])?>/<?php echo lang('minute')?></span>

						<span class="info_name italic darek_gray"><?php echo lang('True private chat rate')?>:</span>
						<span class="info_value italic red"><?php echo print_amount_by_currency($params['performersFeePerMinuteTruePrivate'])?>/<?php echo lang('minute')?></span>
					<?php elseif($params['sessionType'] == 'private_public'):?>

						<span class="info_name italic darek_gray"><?php echo lang('Nude chat rate')?>:</span>
						<span class="info_value italic red"><?php echo print_amount_by_currency($params['performersFeePerMinuteNude'])?>/<?php echo lang('minute')?></span>

						<span class="info_name italic darek_gray"><?php echo lang('Private chat rate')?>:</span>
						<span class="info_value italic red"><?php echo print_amount_by_currency($params['performersFeePerMinutePrivate'])?>/<?php echo lang('minute')?></span>

						<span class="info_name italic darek_gray"><?php echo lang('True private chat rate')?>:</span>
						<span class="info_value italic red"><?php echo print_amount_by_currency($params['performersFeePerMinuteTruePrivate'])?>/<?php echo lang('minute')?></span>

					<?php elseif($params['sessionType'] == 'peek'):?>
						<span class="info_name italic darek_gray"><?php echo lang('Peek chat rate')?>:</span>
						<span class="info_value italic red"><?php echo print_amount_by_currency($params['performersFeePerMinutePeek'])?>/<?php echo lang('minute')?></span>
					<?php endif ?>
				</div>
				<?php if($this->user->id > 0):?>
					<div>
						<span class="info_name italic darek_gray"><?php echo lang('Your account balance')?>:</span>
						<span class="info_value italic red" id="user_chips"></span>
						<span class="info_name italic darek_gray" ><?php echo lang('Available private chat')?>:</span>
						<span class="info_value italic red" id="time_remaining"></span>
					</div>
				<?php endif?>
			</div> -->
		<div class="clear"></div>
		<div id="profile" style="background-color: #404040 !important; min-height: 70px;" class="m5-panel-room  m5-panel ">
		
			<div class="menu">
				<div class="menu_item" id="profile">
					<img class="m5-video-img" src="<?php echo  ( ! (file_exists('uploads/performers/' . $performer->id . '/medium/' . $performer->avatar) && $performer->avatar))? assets_url().'images/poza_tarfa.png':site_url('uploads/performers/' . $performer->id . '/medium/' . $performer->avatar)?>" style="width:65px !important;"/>
				</div>

				<div class="menu_item" id="pictures">
					<div class="rate">
						<span style="float: left"><?php echo $performer->nickname; ?></span>
						<br>
						<fieldset class="rate" style="">
							<?php 
								$ratingstars = round(2*$performer->rating);
								// echo $ratingstars;
								$stars = array_fill(1,10,0);
								for($i=1; $i<=10; $i++){
									if($i<=$ratingstars){
										$stars[$i]=1;
									}
								}
								for($i=10; $i>=1; $i--){
									$class='';
									if($i%2==1){
										$class .= 'half';
									}
									if($stars[$i]==1){
										$class .= ' rate-selected';
									}
									echo "<label class='".$class."'></label> \n";
								}
							?>
						</fieldset>
					</div>
					</div>
					<!-- <div class="menu_item" id="videos">
						<a href="<?php echo site_url($params['performerNick'] . '?tab=videos')?>" target="_blank"><span id="videos" class="btn"><?php echo sprintf(lang('My Videos (%s)'),$videos_nr)?></span></a>
					</div>
					<div class="menu_item" id="schedule">
						<a href="<?php echo site_url($params['performerNick'] . '?tab=schedule')?>" target="_blank"><span id="schedule" class="btn"><?php echo lang('Schedule') ?></span></a>
					</div>
					<div class="menu_item" id="contact">
						<a href="<?php echo site_url($params['performerNick'] . '?tab=contact')?>" target="_blank"><span id="contact" class="btn"><?php echo lang('Contact') ?></span></a>
					</div> -->
					<div class="room-buttons">
						<a class="nodeco" target="_blank" href="<?php echo site_url($performer->nickname) ?>">
							<i class="fa fa-user rounded-icon" aria-hidden="true"></i>
						</a>
						<a class="nodeco" target="_blank" href="<?php echo site_url($performer->nickname)?>?tab=photos">
							<i class="fa fa-camera rounded-icon" aria-hidden="true"></i>
						</a>
						<a class="nodeco" target="_blank" href="<?php echo site_url($performer->nickname)?>?tab=contact">
							<i class="fa fa-comments-o rounded-icon" aria-hidden="true"></i>
						</a>
						<?php if(isset($performer->favorite_id) && $performer->favorite_id!=0){ $var = "fa-heart"; } else{ $var="fa-heart-o"; }?>
						<!-- 	<a class="nodeco" onclick="favorite('<?php echo site_url(''.$var.'/'.$performer->nickname)?>', false);">
								<i class="fa fa-heart rounded-icon" aria-hidden="true"></i>
							</a>
					-->
						<a class="nodeco favorite-btn">
							<i class="fa <?php echo $var; ?> rounded-icon" aria-hidden="true"></i>
						</a>

					</div>

				</div>
		</div>
	</div>

	<script type="text/javascript">
		var isFav = <?php if(isset($performer->favorite_id) && $performer->favorite_id!=0){echo "true";}else{echo "false";} ?>;
		var performerNick = "<?php echo $performer->nickname; ?> ";
		var link = "<?php echo site_url(); ?>";
		$('.favorite-btn').click(function(){
			if(!isFav){
				$.ajax({
				  url: link+"add-favorite/"+performerNick,
				  context: document.body
				}).done(function() {
					$('.fa-heart-o').removeClass('fa-heart-o').addClass('fa-heart');
					alert(performerNick + " added to Favorites");
				});
			}
			else{
				$.ajax({
				  url: link+"remove-favorite/"+performerNick,
				  context: document.body
				}).done(function() {
					$('.fa-heart').removeClass('fa-heart').addClass('fa-heart-o');
					alert(performerNick + " removed from Favorites");
				});
			}
			isFav = !isFav;
		});
	</script>

	<div style="width:100%; text-align: center;">
		<?php echo $this->load->view('chat_component')?>
	</div>

	<div class="room-infobox">

		<?php if($params['sessionType'] == 'public' || $params['sessionType'] == 'private'):?>

			<span class="info_name italic darek_gray"><?php echo lang('Nude chat rate')?>:</span>
			<span class="info_value italic yellow-info"><?php echo print_amount_by_currency($params['performersFeePerMinuteNude'])?>/<?php echo lang('minute')?></span>

			<span class="info_name italic darek_gray"><?php echo lang('Private chat rate')?>:</span>
			<span class="info_value italic yellow-info"><?php echo print_amount_by_currency($params['performersFeePerMinutePrivate'])?>/<?php echo lang('minute')?></span>

			<span class="info_name italic darek_gray"><?php echo lang('True private chat rate')?>:</span>
			<span class="info_value italic yellow-info"><?php echo print_amount_by_currency($params['performersFeePerMinuteTruePrivate'])?>/<?php echo lang('minute')?></span>

		<?php elseif($params['sessionType'] == 'private_public'):?>

			<span class="info_name italic darek_gray"><?php echo lang('Nude chat rate')?>:</span>
			<span class="info_value italic yellow-info"><?php echo print_amount_by_currency($params['performersFeePerMinuteNude'])?>/<?php echo lang('minute')?></span>

			<span class="info_name italic darek_gray"><?php echo lang('Private chat rate')?>:</span>
			<span class="info_value italic yellow-info"><?php echo print_amount_by_currency($params['performersFeePerMinutePrivate'])?>/<?php echo lang('minute')?></span>

			<span class="info_name italic darek_gray"><?php echo lang('True private chat rate')?>:</span>
			<span class="info_value italic yellow-info"><?php echo print_amount_by_currency($params['performersFeePerMinuteTruePrivate'])?>/<?php echo lang('minute')?></span>

		<?php elseif($params['sessionType'] == 'peek'):?>
			<span class="info_name italic darek_gray"><?php echo lang('Peek chat rate')?>:</span>
			<span class="info_value italic yellow-info"><?php echo print_amount_by_currency($params['performersFeePerMinutePeek'])?>/<?php echo lang('minute')?></span>
		<?php endif ?>
		<?php if($this->user->id > 0):?>
			<div>
				<span class="info_name italic darek_gray"><?php echo lang('Your account balance')?>:</span>
				<span class="info_value italic yellow-info" id="user_chips"></span>
				<span class="info_name italic darek_gray" ><?php echo lang('Available private chat')?>:</span>
				<span class="info_value italic yellow-info" id="time_remaining"></span>
			</div>
		<?php endif?>	
	</div>

	<?php if(sizeof($performers) > 1):?>	
		<div class="title">
			<?php echo lang('Other performers') ?>
		</div>		
		<div id="performer_list">
						
				<?php foreach($performers as $performer):?>
					<?php if($performer->nickname === $params['performerNick']) continue;//nu afisez performerul current care e in chat?>
					<?php $this->load->view('performer',array('performer'=>$performer))?>
				<?php endforeach?>	
			
		</div>
	<?php endif?>
	<div class="clear"></div>