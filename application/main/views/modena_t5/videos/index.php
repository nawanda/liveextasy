<script type="text/javascript" src="<?php echo assets_url()?>js/jquery-1.5.2.min.js"></script>
<script type="text/javascript" src="<?php echo assets_url()?>js/jquery.blockUI.js"></script>
<script type="text/javascript" src="<?php echo assets_url()?>addons/fancybox/jquery.fancybox-1.3.4.pack.js"></script> 
<div class="content videos">		
	<? $this->load->view('includes/_title_bar', array('title' => 'Performers videos')) ?>
	<?php $this->load->view('includes/_videos_top')?>	
	<script type="text/javascript" src="<?php echo assets_url()?>js/swfobject.js"></script>
	<script type="text/javascript" src="<?php echo assets_url()?>js/jquery.qtip-1.0.0-rc3.min.js"></script>
	<script type="text/javascript">
		function previewOpenVideoInModal(video_id){
			$.fancybox({
				'overlayShow': true,
				'scrolling': 'no',
				'type': 'iframe',
				'width':800,
				'height':640,				
				'showCloseButton'	: true,						
				'href': '<?php echo site_url('videos/view')?>' + '/' + video_id,
				'overlayColor': "#FFF",
				'overlayOpacity': "0.3"
			});		
		}	

		function pay_video(video_id){
			$.ajax({
				url: '<?php echo site_url('videos/view/')?>' + '/'+ video_id, 					
				complete: function(html) { 
					$.blockUI({ message: html.responseText});
				}
			});	
			return false;
		}
					
		function register(){
			// $.fancybox({
			// 	'showCloseButton'	: false,
			// 	'padding'			: 0,
			// 	'overlayColor'		: '#fff',
			// 	'overlayOpacity'	: 0.28,
			// 	'type'				: 'iframe',
			// 	'href'				: '<?php echo site_url('register')?>',
			// 	'width'				: 630,
			// 	'height'			: 458
			// });
			window.location.href = '<?php echo site_url('register')?>';
		}				
	</script>	
	<div class="m5-panel shadow">
		<?php if(sizeof($videos) > 0):?>	
			<?php foreach($videos as $video):?>
				<?php $this->load->view('videos/listing',array('video' => $video))?>
			<?php endforeach?>
			<div class="clear"></div>
			<?php echo $pagination;?>
		<?php else:?>
			<div class="no_results"><?php echo lang('There are no videos')?></div>			
		<?php endif?>
	</div>
	<div class="clear"></div>
</div>

