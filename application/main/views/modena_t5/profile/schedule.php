<span><?php echo lang('Current server time:')?> <?php echo date('r')?></span>
<table id="schedule" align="center">
    <tr>
        <td class="head_hour"></td>
<?php for ($i = 0; $i < 24; $i++): 
			$x = ($i <= 9) ? '0'.$i.'h' : $i.'h';
			$y = ($i <= 8) ? '0'.($i+1).'h' : ($i+1).'h';
	?>
        <td class="head_hour"><?php echo $x ?><br/><?php echo $y ?></td>
<?php endfor ?>
    </tr>
    
<?php foreach ($schedule['map'] as $day => $hours): ?>
    <tr>
    	
        <td class="day"><?php echo lang($schedule['complete_days_of_week'][$day]) ?></td>
        <?php 
            $counter = 0; 
            foreach ($hours as $hour):
                if ($hour != 0):
        ?>
                    <td class="<?php echo $day ?> <?php echo $counter ?> selected hour"></td>
        <?php   else: ?>
                    <td class="<?php echo $day ?> <?php echo $counter ?> hour"></td>
        <?php   endif; 
                $counter++;
            endforeach;
        ?>
    </tr>
<?php endforeach ?>
</table>
