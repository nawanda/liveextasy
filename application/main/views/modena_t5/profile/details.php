<div class="m5-profile-box">
	<div class="m5-label-profile">VItal information</div>
	<div class="w-row">
		<div class="w-col w-col-6">
			<ul class="unordered-list-2 w-list-unstyled">
				<li class="m5-profile-info-item w-clearfix">
					<div class="m5-right-align-info">
						<div class="m5-info-label-small"><?php echo lang('Nickname') ?></div>
					</div>
					<div class="m5-left-align-info">
						<div class="m5-info-text"><?php echo $performer->nickname?></div>
					</div>
				</li>
				<li class="m5-profile-info-item w-clearfix">
					<div class="m5-right-align-info">
						<div class="m5-info-label-small"><?php echo lang('Height') ?></div>
					</div>
					<div class="m5-left-align-info">
						<div class="m5-info-text"><?php echo lang($performer->height)?></div>
					</div>
				</li>
				<li class="m5-profile-info-item w-clearfix">
					<div class="m5-right-align-info">
						<div class="m5-info-label-small"><?php echo lang('Age') ?></div>
					</div>
					<div class="m5-left-align-info">
						<div class="m5-info-text"><?php echo floor((time() - $performer->birthday)/31556926);?></div>
					</div>
				</li>
				<li class="m5-profile-info-item w-clearfix">
					<div class="m5-right-align-info">
						<div class="m5-info-label-small"><?php echo lang('Hair color') ?></div>
					</div>
					<div class="m5-left-align-info">
						<div class="m5-info-text"><?php echo lang($performer->hair_color)?></div>
					</div>
				</li>
				<li class="m5-profile-info-item w-clearfix">
					<div class="m5-right-align-info">
						<div class="m5-info-label-small"><?php echo lang('Ethnicity') ?></div>
					</div>
					<div class="m5-left-align-info">
						<div class="m5-info-text"><?php echo lang($performer->ethnicity)?></div>
					</div>
				</li>
				<li class="m5-profile-info-item w-clearfix">
					<div class="m5-right-align-info">
						<div class="m5-info-label-small"><?php echo lang('Cup size') ?></div>
					</div>
					<div class="m5-left-align-info">
						<div class="m5-info-text"><?php echo lang($performer->cup_size)?></div>
					</div>
				</li>
			</ul>
		</div>
		<div class="w-col w-col-6">
			<ul class="unordered-list-2 w-list-unstyled">
				<li class="m5-profile-info-item w-clearfix">
					<div class="m5-right-align-info">
						<div class="m5-info-label-small"><?php echo lang('Gender') ?></div>
					</div>
					<div class="m5-left-align-info">
						<div class="m5-info-text"><?php echo lang($performer->gender)?></div>
					</div>
				</li>
				<li class="m5-profile-info-item w-clearfix">
					<div class="m5-right-align-info">
						<div class="m5-info-label-small"><?php echo lang('Weight') ?></div>
					</div>
					<div class="m5-left-align-info">
						<div class="m5-info-text"><?php echo lang($performer->weight)?></div>
					</div>
				</li>
				<li class="m5-profile-info-item w-clearfix">
					<div class="m5-right-align-info">
						<div class="m5-info-label-small"><?php echo lang('Eye color') ?></div>
					</div>
					<div class="m5-left-align-info">
						<div class="m5-info-text"><?php echo lang($performer->eye_color)?></div>
					</div>
				</li>
				<li class="m5-profile-info-item w-clearfix">
					<div class="m5-right-align-info">
						<div class="m5-info-label-small"><?php echo lang('Language') ?></div>
					</div>
					<div class="m5-left-align-info">
						<div class="m5-info-text">
							<?php foreach($languages as $language): ?>
								<img src="<?php echo assets_url()?>images/flags/<?php echo strtoupper($language)?>.png">
							<?php endforeach;?></div>
					</div>
				</li>
				<li class="m5-profile-info-item w-clearfix">
					<div class="m5-right-align-info">
						<div class="m5-info-label-small"><?php echo lang('Body build') ?></div>
					</div>
					<div class="m5-left-align-info">
						<div class="m5-info-text"><?php echo lang($performer->build)?></div>
					</div>
				</li>
			</ul>
		</div>
	</div>
</div>
<div class="m5-profile-box">
	<div class="m5-label-profile"><?php echo lang('Profile description :')?></div>
	<div class="div-block-2">
		<div class="m5-main-text"><?php echo $performer->description?></div>
	</div>
</div>
<div class="m5-profile-box">
	<div class="m5-label-profile"><?php echo lang('What turns me on :')?></div>
	<div class="div-block-2">
		<div class="m5-main-text"><?php echo $performer->what_turns_me_on?></div>
	</div>
</div>
<div class="m5-profile-box">
	<div class="m5-label-profile"><?php echo lang('What turns me off :')?></div>
	<div class="div-block-2">
		<div class="m5-main-text"><?php echo $performer->what_turns_me_off?></div>
	</div>
</div>
