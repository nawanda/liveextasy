<div class="clear"></div>
<?php if (is_array($reviews) && count($reviews) > 0): ?>
<div id="reviews" style="width: 100%;">

	<div class="clear"><br></div>	

	<?php foreach ($reviews as $review): ?>
		<div class="review_item m5-panel shadow">
			<div class="white_h_sep"><img src="<?php echo assets_url() ?>images/white_line_sep.png" width="100%" height="1" /></div>
			<div class="stars">
				<fieldset class="rate" style="">
					<?php 
						$ratingstars = round(2*$review->rating);
						// echo $ratingstars;
						$stars = array_fill(1,10,0);
						for($i=1; $i<=10; $i++){
							if($i<=$ratingstars){
								$stars[$i]=1;
							}
						}
						for($i=10; $i>=1; $i--){
							$class='';
							if($i%2==1){
								$class .= 'half';
							}
							if($stars[$i]==1){
								$class .= ' rate-selected';
							}
							echo "<label class='".$class."'></label> \n";
						}
					?>
				</fieldset>
			</div>
			<div class="title">
				<?php echo $review->user ?>
				<span class="dark_gray"> on <?php echo date('d M Y H:i', $review->add_date) ?></span>
			</div>
			<div class="clear"></div>
			<div class="comment dark_gray"><?php echo $review->message ?></div>

		</div>
	<?php endforeach ?>
	<div id="pagination">
		<?php echo $pagination ?>
	</div>
	<div style="clear:both"></div>
</div>
<?php endif ?>
