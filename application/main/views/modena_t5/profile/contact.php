<script type="text/javascript" src="<?php echo assets_url()?>js/jquery.validate.js"></script>
<script type="text/javascript">
jQuery(function($){

	$.validator.setDefaults({
		validClass:"success",
		errorElement: "span",
		errorPlacement: function(error, element) {
			error.appendTo($(element).next('span').next('span'));
		}
	});

	
	$(".contact_us").validate({
		 
		success: function(label) {
	    	label.addClass("valid");
	   },
		rules: {
			subject: {
				required: true,
				minlength: 2,
				maxlength: 255
			},
			message: {
				required: true,
				minlength: 2,
				maxlength: 1500
			}
		}, 
		messages: {
			subject: 					"<?php echo lang('Invalid subject')?>",
			message: 					"<?php echo lang('Invalid message')?>"
		}
	});
}); 
</script>

<div class="m5-signup-form w-form">
	<?php echo form_open('performers/contact', 'class="contact_us"') ?>
	<?php echo form_hidden('performer_id', $performer->id ) ?>
	<label class="m5-form-label"><?php echo lang('Subject') ?></label>
	<?php echo form_input(array('name' => 'subject', 'id' => 'subject'), set_value('subject'), 'class="m5-input w-input"')?>
	<span><span class="error" htmlfor="subject" generated="true"></span></span>
	<label class="m5-form-label"><?php echo lang('Message') ?></label>
	<?php echo form_textarea(array('name' => 'message', 'id' => 'message') , set_value('message'), 'class="m5-textarea w-input"')?>
	<span><span class="error" htmlfor="message" generated="true"></span></span>		
	<?php echo form_submit('submit', lang('Send'), 'class="submit-button-2 w-button"') ?>
</div>
<?php echo form_close() ?>
