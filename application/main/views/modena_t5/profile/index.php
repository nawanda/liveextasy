
<!-- jQuery rating -->
<script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);
</script>
<script src="<?php echo assets_url() ?>js/jquery.ui.stars.min.js"></script>
<!-- jQuery rating -->

<script type="text/javascript">
	var pictures_offset = 0,
    pictures_limit  = 8;

	function load_prev() {
		pictures_offset -= pictures_limit;
		load_pictures(pictures_limit, pictures_offset);
	}

	function load_next() {
		pictures_offset += pictures_limit;
		load_pictures(pictures_limit, pictures_offset);
	}

	$(document).ready(function() {

	//When page loads...
	$("div.btn div:first").addClass("active").show(); //Activate first tab

		$('.menu_item').click(function(){
			
			$('.menu_item.selected').removeClass('selected');
			$(this).addClass('selected');
			
			$('.slide').hide();
			$('#'+$(this).attr('id') + '_content').show();
			if($(this).attr('id') != 'profile'){
				$('#reviews').hide();
			}else{
				$('#reviews').show();
			}
			
			if($(this).attr('id') == 'pictures' || $(this).attr('id') == 'videos'){
				$('#profile .fl_r').hide();
			}else{
				$('#profile .fl_r').show();
			}
				var warpper_min_height = $(window).height() -115;
				
				var new_min_height = $('#'+$(this).attr('id') + '_content').height() + 155;
				
				if(warpper_min_height < new_min_height){
					warpper_min_height = new_min_height;
				}
				
				
				var content_height = $('#'+$(this).attr('id') + '_content').height();
				var min_content_height = warpper_min_height - $('#header').height() - 55;
				if(content_height < min_content_height){
					content_height = min_content_height;
					
				}
					
				$('#warpper').css('min-height', warpper_min_height);
				$('.black_box .content').css('min-height',content_height);	
		});

		$('.send_message').click(function(){
			$('.slide').hide();
			$('#contact_content').show();
			return false;
		});

		$(".stars").stars({
			cancelShow: false,
			disabled: true,
			split: 4,
			callback: function(ui, type, value) {
				$.ajax({
					url: "<?php echo site_url('performers/rate') ?>",
					type: "post",
					data: {
						performer_id: <?php echo $performer->id ?>,
						rating:       value
					}
				});
			}
		});

	});
</script>
<?php 
	if(isset($_GET['tab']) && $_GET['tab']){
		$tab=$_GET['tab'];
	}else{$tab='';}
?>

<script>
	function editlink(link){
		window.history.pushState('', '', link);
	}
</script>

<div>
	<div class="m5-container w-container">
		<h3 class="heading m5-page-title m5-perf-name"><?php echo $performer->nickname ?></h3>
		<div class="w-tabs shadow" data-duration-in="300" data-duration-out="100">
			<div class="w-tab-menu">
				<a class="m5-tab-link-simple <?php if($tab=="profile" || $tab=='') echo "w--current"; ?>  w-inline-block w-tab-link" data-w-tab="Tab 1" onclick="editlink('<?= $performer->nickname ?>?tab=profile')">
					<div>Profile</div>
				</a>
				<a class="m5-tab-link-simple <?php if($tab=="photos") echo "w--current"; ?> w-inline-block w-tab-link" data-w-tab="Tab 2" onclick="editlink('<?= $performer->nickname ?>?tab=photos')" id="profile_photos_tab">
					<div class="m5-tab-simple text-block"><?php echo lang('Photos') ?></div>
				</a>
				<a class="m5-tab-link-simple w-inline-block w-tab-link <?php if($tab=="videos") echo "w--current"; ?>" data-w-tab="Tab 3" onclick="editlink('<?= $performer->nickname ?>?tab=videos')">
					<div class="m5-tab-simple text-block"><?php echo lang('Videos') ?></div>
				</a>
				<a class="m5-tab-link-simple w-inline-block w-tab-link <?php if($tab=="schedule") echo "w--current"; ?>" data-w-tab="Tab 4" onclick="editlink('<?= $performer->nickname ?>?tab=schedule')">
					<div class="m5-tab-simple text-block"><?php echo lang('Schedule') ?></div>
				</a>
				<a class="m5-tab-link-simple <?php if($tab=="contact") echo "w--current"; ?> w-inline-block w-tab-link" data-w-tab="Tab 5" onclick="editlink('<?= $performer->nickname ?>?tab=contact')">
					<div class="m5-tab-simple text-block"><?php echo lang('Contact') ?></div>
				</a>				
			</div>
			<div class="m5-tab-content-actual w-tab-content">
				<div class="m5-tab-content w-tab-pane <?php if($tab=='profile' || $tab=='') echo "w--tab-active"; ?>" data-w-tab="Tab 1">
					<div class="m5-tab-content">
						<div class="w-row">
							<div class="w-col w-col-6">
								<img src="<?php echo ($performer->avatar != '') ? main_url('uploads/performers/' . $performer->id . '/' . $performer->avatar) : assets_url().'images/poza_tarfa_medium.jpg'?>"/>
								<?php if(isset($rating)):?>
									<!-- TODO stars -->
								<?php endif?>
							</div>
							<div class="w-col w-col-6">
								<div class="m5-top-profile-bar w-clearfix">
									<h3 class="heading m5-perf-name"><?php echo $performer->nickname ?></h3>
									<a style="cursor: pointer;" class="m5-dim-link m5-link<?php echo ($this->user->id > 0)?NULL:' signup '?>" onclick="document.location='<?php echo site_url(($favorite)? 'remove-favorite/' . $performer->nickname : 'add-favorite/' . $performer->nickname)?>'"><?php echo ( ! $favorite)?lang('Add to favourites'):lang('Remove favorite') ?></a>
								</div>
								<?php $this->load->view('profile/details') ?>
							</div>
							<?php $this->load->view('profile/reviews') ?>
						</div>
					</div>
				</div>
				<div class="m5-tab-content <?php if($tab=='photos') echo "w--tab-active"; ?>  w-tab-pane" data-w-tab="Tab 2">
					<div class="w-row">
						<?php $this->load->view('profile/photos') ?>
					</div>
				</div>
				<div class="m5-tab-content w-tab-pane <?php if($tab=='contact') echo "w--tab-active"; ?>" data-w-tab="Tab 3">
					<div class="w-row">
						<?php $this->load->view('profile/videos') ?>
					</div>
				</div>
				<div class="m5-tab-content w-tab-pane <?php if($tab=='schedule') echo "w--tab-active"; ?>" data-w-tab="Tab 4">
					<div class="w-row">
						<div id="schedule_content"><?php $this->load->view('profile/schedule') ?></div>
					</div>
				</div>
				<div class="m5-tab-content <?php if($tab=='contact') echo "w--tab-active"; ?> w-tab-pane" data-w-tab="Tab 5">
					<div class="w-row">
						<?php if ($this->user->id > 0)://Verific daca userul e logat pentru a putea trimite mesaje?>
						<?php $this->load->view('profile/contact'); ?> 
						<?php else: ?>						  	  
							<br /><div class="error_mess"><span class="icon lock"></span><?php echo lang('You must be logged in to use this feature.') ?></div>	
						<?php endif; ?>
					</div>
				</div>

			</div>

		</div>
	</div>
</div>

<!-- <script src="<?php echo assets_url()?>js/webflow.js" type="text/javascript"></script> -->
