<script type="text/javascript" src="<?php echo assets_url()?>js/jquery.validate.js"></script>
<script type="text/javascript">
jQuery(function($){

	$.validator.setDefaults({
		validClass:"success",
		errorElement: "span",
		errorPlacement: function(error, element) {
			error.appendTo($(element).next('span'));
		}
	});
	
	$(".contact_us").validate({
		success: function(label) {
	    	label.addClass("valid");
	   },
		rules: {
			subject: {
				required: true,
				minlength: 2,
				maxlength: 255
			},
			email: {
				required: true,
				email: true
			},
			message: {
				required: true,
				minlength: 2,
				maxlength: 1500
			}
		}, 
		messages: {
			subject: 	"<?php echo lang('Please enter a valid subject') ?>",
			email: 		"<?php echo lang('Please enter a valid email') ?>",
			message: 	"<?php echo lang('Please enter a valid message') ?>"
		}
	});
}); 
</script>
	<div class="content contact">
		<h3 class="titlev2"><?php echo lang('Contact us') ?></h3>
		<div class="m5-panel shadow">		
			<div id="profile">
				<div class="left" style="text-align: left;">
						<?php echo form_open('', 'class="contact_us"') ?>
						<div class="dark_gray italic contact">
							<div>
								<label><span class="bold"><?php echo lang('Subject') ?>:</span></label>
								<div class="input-fix">
									<?php echo form_input(array('name' => 'subject', 'id' => 'subject'), set_value('subject'), 'class="m5-input w-input"' )?>	
									<span class="error" htmlfor="subject" generated="true"></span>			
								</div>	
							</div>
							<div>
								<label><span class="bold"><?php echo lang('Email') ?>:</span></label>
								<div class="input-fix">
									<?php echo form_input('email',set_value('email'),'id="email" class="m5-input w-input" ')?>
									<span class="error" htmlfor="email" generated="true"></span>	
								</div>			
							</div>																
							<div>
								<label><span class="bold"><?php echo lang('Message') ?>:</span></label>
								<div class="input-fix">
									<?php echo form_textarea(array('name' => 'message', 'id' => 'message') , set_value('message'), 'class="m5-textarea w-input"' )?>
									<span class="error" htmlfor="message" generated="true"></span>
								</div>
							</div>
							<div>
								<label></label>
								<?php echo form_submit('submit', lang('Submit'), 'class="submit-button-2 w-button"')?>
							</div>
						</div>
						<?php echo form_close()?>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</div>