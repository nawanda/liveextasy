<script>
				jQuery(function($){
					$('#account').change(function(){
						var accountType = $(this).val();
						if(accountType == ''){
							$('#login_form').attr('action', '<?= main_url() ?>login');
						}else{
							$('#login_form').attr('action', '<?= main_url() ?>' + accountType + '/login');
						}
					})
				});
			</script>
<div class="m5-container w-container">
	<h3 class="m5-title"><?php echo lang('Login') ?></h3>
	<h5 class="m5-subtitle"><?php echo lang('Access your account here') ?></h5>
	<div class="m5-panel shadow">
		<h4 class="m5-panel-heading"><?php echo lang('Please log in with your user details') ?></h4>
		<div class="m5-signup-form w-form">
			<?php echo form_open('login', 'id="login_form"') ?>
				<label class="m5-form-label"><?= lang('Username:') ?></label>
				<?php echo form_input('username', set_value('username', lang('username')), 'class="m5-input w-input"  onfocus="if(this.value == \'' . lang('username') . '\') { this.value = \'\' }" onblur="if(this.value.length == 0) { this.value = \'' . lang('username') . '\' }"') ?>
				<div class="relativ">
					<label class="m5-form-label"><?= lang('Password:') ?></label>
					<?php echo form_password('password', set_value('password', lang('password')), 'class="m5-input w-input" onfocus="if(this.value == \'' . lang('password') . '\') { this.value = \'\' }" onblur="if(this.value.length == 0) { this.value = \'' . lang('password') . '\' }"') ?>
					<label class="m5-forgot-pass m5-form-label"><a href="#" id="myBtn" class="forgot_password"><?php echo lang('Forgot Password?') ?></a></span></label>
				</div>
				<label class="m5-form-label"><?= lang('Account:') ?></label><?php echo form_dropdown('account', array('' => lang('Member'), 'performer' => lang('Performer'), 'studio' => lang('Studio'), 'affiliate' => lang('Affiliate')), set_value('account', lang('account')), 'class="m5-input w-input" id="account" ') ?>
				<!-- <div class="checkbox-field m5-agree w-checkbox">
					<input class="w-checkbox-input" data-name="Checkbox" id="checkbox" name="checkbox" type="checkbox">
					<label class="m5-agree-text w-form-label" for="checkbox">Remember me</label>
				</div> -->
				<?php echo form_submit('login', lang('Log in to Account'), 'class="submit-button-2 w-button"') ?>
			<?php echo form_close() ?>
			<div class="w-form-done">
				<div>Thank you! Your submission has been received!</div>
			</div>
			<div class="w-form-fail">
				<div>Oops! Something went wrong while submitting the form</div>
			</div>
		</div>
	</div>
</div>


<!-- The Modal -->
<div id="myModal" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <span class="close">&times;</span>
    	<div class="m5-signup-form w-form">
			<h3 class="m5-title"><?PHP echo lang('Forgot password?')?></h3>
			
		<?php echo form_open(site_url('forgot-password'));?>
			<div>
				<label class="m5-form-label"><?php echo lang('User name')?>*:</label><br/>
				<?php echo form_input('username', set_value('username'), ' class="m5-input w-input"')?>
				<span class="italic red"><?php echo form_error('username', ' ', ' ')?></span>
			</div>
		<br/>
			<div>
				<label class="m5-form-label"><?php echo lang('Email address')?>*:</label><br/>
				<?php echo form_input('email', set_value('email'), 'class="m5-input w-input"')?>
				<span class="italic red"><?php echo form_error('email', ' ', ' ')?></span>
			</div>

			<div style="margin-top:8px; text-align: center;">
				<button class="submit-button-2 w-button"  type="submit" style="width:130px; display: inline-block;"> <?php echo lang('Submit')?> </button>
			</div>
		<?php echo form_close('')?>
	</div>
  </div>

</div>

<script>
	// Get the modal
var modal = document.getElementById('myModal');

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on the button, open the modal 
btn.onclick = function() {
    modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}
</script>