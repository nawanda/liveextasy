<!-- PUT YOUR EMAIL SUBJECT IN HTML title TAG -->
<title>Welcome to {site_name}</title>

<center>
	<table cellpadding="0" cellspacing="0" style="width: 600px; font-family: Arial; font-size: 14px; text-align: left; color: #505050; border: solid #DDDDDD 1px; padding: 0px; margin-top: 0px;">
		<tr>
			<td style="padding: 0px; margin: 0px;">
			
			</td>
		</tr>

		<tr>
			<td style="padding: 20px 20px; line-height: 20px;">
				
				<!-- START CONTENT -->
				
				Olá <b>{username}</b>,
				<br /><br />
				Você recebeu está mensagem devido ao cancelamento da sua conta no <span style="font-weight: bold; color: #000000;">{site_name}</span> !
				<br /><br />
				Sua conta foi cancelada!
				
				<!-- END CONTENT -->
				 
				<br />
				Atenciosamente,
				<br />
				Time {site_name} 
				<br />
				<a href="{site_url}" target="_blank" style="font-size: 14px; color: #505050;">{site_url}</a>
			</td>
		</tr>

		<tr>
			<td>
				<div style="text-align: center; background-color: #FAFAFA; padding: 10px 0px; border-top: dotted #DDDDDD 1px; font-size: 12px;">
					<hr style="border: none; border-top: dotted #DDDDDD 1px;" />
					<span style="color: #707070; text-align: left;">Copyright    2007-<?php echo date('Y')?> {site_name}. Todos os direitos reservados.</span>
				</div>
			</td>
		</tr>

	</table>
</center>