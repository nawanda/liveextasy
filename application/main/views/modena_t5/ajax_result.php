<?php foreach($performers as $performer):?>
	<?php $this->load->view('performer',array('performer' => $performer))?>
<?php endforeach?>
<div class="clear"></div>
<div id="performer_list_pages">
	<?php if ($pagination): ?>
		<div class="h_line_sep"></div>
		<?php echo $pagination; ?>
	<?php endif ?>
</div>
<div class="clear"></div>
