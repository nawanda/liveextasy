<?php

/**
 * @author Aurel Pop
 */
class Migration_Add_performers_goals extends Migration {
    function up(){
        $this->migrations->verbose AND print "Adding performers_goals";
		
                $this->db->query("CREATE TABLE `performers_goals` (
                                    `id` int(11) NOT NULL AUTO_INCREMENT,
                                    `performer_id` int(11) NOT NULL,
                                    `action` text,
                                    `goal_reached` tinyint(1) DEFAULT '0',
                                    `goal` float DEFAULT '0',
                                    `gained` float DEFAULT '0',
                                    `status` enum('started','reached','failed','finished') DEFAULT 'started',
                                    PRIMARY KEY (`id`)
                                   )");
                
                $this->db->query("ALTER TABLE  `watchers` ADD  `goal_id` INT NULL DEFAULT NULL");
		$this->db->query("ALTER TABLE  `watchers_old` ADD  `goal_id` INT NULL DEFAULT NULL");
    }
    
    function down() {
        
    }
}

