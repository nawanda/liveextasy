<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_start_chat_text_to_performer_profile extends Migration {
    
    function up() {
        $this->migrations->verbose AND print "Adding field start_chat_text";
        $this->dbforge->add_column('performers_profile',
            array('start_chat_text'=>array(
                'type'          => 'CHAR',
                'constraint'    => "250",
                'null'          => TRUE
            )
        ));
        
    }
        
    function down(){
        $this->dbforge->drop_column('performers_profile','start_chat_text');
    }
}
