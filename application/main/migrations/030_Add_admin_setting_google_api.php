<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_admin_setting_google_api extends Migration {
	
	function up() {
		$this->migrations->verbose AND print "Adding license field";
		
                $this->db->query("INSERT INTO `settings` (`name`, `value`, `type`, `title`, `description`) VALUES 
				('separator_Google settings',		'null',						'separator',	'Google API Settings',	null)
		");
                $this->db->query("INSERT INTO `settings` (`name`, `value`, `type`, `title`, `description`) VALUES 
				('google_translation_enable',		'0',						'boolean',	'Enable Google Translation',	null)
		");
		$this->db->query("INSERT INTO `settings` (`name`, `value`, `type`, `title`, `description`) VALUES 
				('google_api_key',			'null',						'string',	'Google API Key',	null)
		");
		
	}
		
	function down(){
		
	}
}

