<?php

/**
 * @author Aurel Pop
 */
class Migration_Add_socket_settings extends Migration {
    function up(){
        $this->migrations->verbose AND print "Adding socket settings";
		
                $this->db->query("INSERT INTO  `settings` (`id` ,`name` ,`value` ,`type` ,`title` ,`description`)
                                    VALUES 
                                    (NULL ,  'separator_Socket',  'null',  'separator',  'Socket', NULL),
                                    (NULL ,  'socket_url',  'null',  'string',  'URL ', NULL),
                                    (NULL ,  'socket_port',  '1338',  'string',  'Port', NULL),
                                    (NULL ,  'socket_secret_key',  'null',  'string',  'Secret Key', NULL),
                                    (NULL ,  'socket_http_port',  '8000',  'string',  'Http Port', NULL)"
                        );
						
			     $this->db->query("INSERT INTO  `settings` (`id` ,`name` ,`value` ,`type` ,`title` ,`description`)
                                    VALUES 
                                    (NULL ,  'separator_gateways_settings',  'null',  'separator',  'Gateway Info', NULL),
                                    (NULL ,  'pagseguro_email',  'null',  'string',  'Pagseguro Email ', NULL),
                                    (NULL ,  'pagseguro_token',  'null',  'string',  'Pagseguro Token', NULL)"
                        );
    }
    
    function down() {
        
    }
}
