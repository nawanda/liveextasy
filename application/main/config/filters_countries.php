<?php
$config['filters']['language'] = array(lang('language') => array (
lang('deutsch') => 'de',
lang('english') => 'en',
lang('espanol') => 'es',
lang('francais') => 'fr',
lang('italiano') => 'it',
lang('portuguese') => 'pt',
lang('romana') => 'ro',
lang('Spain') => 'ES'
)
);
