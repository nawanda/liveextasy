<?php
$config['packages'] = array(
	'1'=>array(
		'name'		=> 'Pack 1',
		'credits'	=> 50,
		'bonus'		=> 10,
		'currency'	=> 'usd',
		'value'		=> 50,
		'best'		=> 0
	),
	
	'2'=>array(
		'name'		=> 'Pack 2',
		'credits'	=> 225,
		'bonus'		=> 50,
		'currency'	=> 'usd',
		'value'		=> 225,
		'best'		=> 0
	),
	
	'3'=>array(
		'name'		=> 'Pack 3',
		'credits'	=> 499,
		'bonus'		=> 99,
		'currency'	=> 'usd',
		'value'		=> 499,
		'best'		=> 1
	),		
	'4'=>array(
		'name'		=> 'Pack 3',
		'credits'	=> 1200,
		'bonus'		=> 289,
		'currency'	=> 'usd',
		'value'		=> 1200,
		'best'		=> 0
	)	
);