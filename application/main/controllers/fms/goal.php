<?php

/**
 * @author Aurel Pop
 * @example CURL -XPOST www.site.com/goal.php -d "uniqId=%UNIQUEID%&userId=%PERFORMERID%&pasword=%PASSWORD%&type=%TYPE%&goal=%GOAL%&action=%ACTIONTEXT%&id=%GOALID%&gained=%GAINED%&goalReached=[false|true]"
 */
class Goal_controller extends CI_Controller {
    private $kickNonPayingUsers;
    private $file_log;
    private $params;
    private $paramsPattern;
    private $responsePattern;
    private $responsePatternWithMessage;
    
    function __construct() {
        parent::__construct();
        $this->kickNonPayingUsers = false;
        $this->file_log           = APPPATH . 'logs/fms/goal.txt';
        $this->paramsPattern      = array();
        $this->params             = array();
        
        $this->responsePattern            = 'id=%s&kickNonPaying=%s&allow=%s';
        $this->responsePatternWithMessage = 'id=%s&kickNonPaying=%s&allow=%s&message=%s';
        
        $this->load->helper('generic');
        $this->load->model('performers');
        $this->load->model('performers_goals');
    }

    function index() {
        $this->generateParams();

        if(!$this->validateRequest()) {
            $this->respondToFMS($this->params['goalId'], 'false');
        }
        
        $this->{$this->params['type']}();   
    }
    
    private function start() {
        if(!$this->validateStartRequest()) {
             $this->respondToFMS($this->params['goalId'], 'false');
        }
        
        $goal = array(
            'performer_id' => $this->params['performerId'],
            'action'       => $this->params['action'],
            'goal_reached' => false,
            'goal'         => $this->params['goal'],
            'gained'       => 0,
            'status'       => 'started'
        );
        
        $goalId = $this->performers_goals->add($goal);
        if(!$goalId) {
            $this->respondToFMS($this->params['goalId'], 'false');
        } else {
            $this->respondToFMS($goalId, 'true');
        }
    }
    
    private function stop() {
        if(!$this->validateStopRequest()) {
            $this->respondToFMS($this->params['goalId'], 'false');
        }
        
        if($this->params['gained'] < $this->params['goal']) {
            $this->failed();
        } else {
            $this->finished();
        }
    }
    
    private function success() {
        if(!$this->validateSuccessRequest()) {
            $this->respondToFMS($this->params['goalId'], 'false');
        }
        
        $goal = $this->performers_goals->getById($this->params['goalId']);
        $goal->status = 'reached';
        $this->performers_goals->update((array)$goal);
        
        $this->respondToFMS($this->params['goalId'], 'true');
    }
    
    private function failed() {
        $goal = $this->performers_goals->getById($this->params['goalId']);
        if(!$goal) {
            $this->respondToFMS($this->params['goalId'], 'false');
        } 
        
        $goal->status = 'failed';
        $this->performers_goals->update((array)$goal);

        $this->respondToFMS($this->params['goalId'], 'true');
    }
    
    
    private function finished() {
        $goal = $this->performers_goals->getById($this->params['goalId']);
        if(!$goal) {
            $this->respondToFMS($this->params['goalId'], 'false');
        } 
        
        $goal->status = 'finished';
        $this->performers_goals->update((array)$goal);

        $this->respondToFMS($this->params['goalId'], 'true');
    }
    
    /**
     * @global $_POST
     * @property-write $params
     * @property-write $paramsPattern
     */
    private function generateParams(){
        $this->prepareParams();
        foreach($this->paramsPattern as $key => $param) {
            $this->params[$param] = $this->input->get_post($key);
        }
        
        foreach($this->params as $key => $param) {
            $var = $param;
            $int   = filter_var($var, FILTER_VALIDATE_INT);
            $float = filter_var($var, FILTER_VALIDATE_FLOAT);
            $bool  = filter_var($var, FILTER_VALIDATE_BOOLEAN);
            
            if($int != FALSE) {
                $this->params[$key] = $int;                    
            } else if($float != FALSE) {
                $this->params[$key] = $float;
            } else if($bool != NULL) {
                $this->params[$key] = $bool;
            }
        }
    }
    
    /**
     * @property $params['performerId'] & $params['password']
     * @return boolean
     */
    private function validateRequest() {
        // Get performer by id
        $performer = $this->performers->get_one_by_id($this->params['performerId']);
        
        // Check if performer
        if(!$performer) {
            return FALSE;
        }
        
        // Check if performer->password = password
        if($performer->password != $this->params['password']){
             return FALSE;
        }
        
        return TRUE;
    } 
    
    /**
     * @property $params['id'] is NULL
     * @property $params['gained'] is ZERO OR NULL
     * @property $params['goalReached'] is FALSE OR NULL
     * @return boolean
     */
    private function validateStartRequest() {
        if($this->params['goalId']) {
            $this->respondToFMS($this->params['goalId'], 'false','test2');
            return FALSE;
        }
        
        if(isset($this->params['gained']) && $this->params['gained']) {
           // $this->respondToFMS($this->params['goalId'], 'false', 'start validation failed2');
            $this->respondToFMS($this->params['goalId'], 'false', var_export($this->params, TRUE));
                return FALSE;
        }

        return TRUE;
    }
    
    /**
     * @property $params['id'] is NOT NULL
     * @return boolean
     */
    private function validateStopRequest() {
        if(!$this->params['goalId']) {
            return FALSE;
        }
        
        $goal = $this->performers_goals->getById($this->params['goalId']);
        if (!$goal || $goal->performer_id != $this->params['performerId']) {
            return FALSE;
        }
        
        return TRUE;
    }
    
    /**
     * @property $params['id'] is NOT NULL
     * @property $params['gained'] EQUALS $params['goal']
     * @property $params['goalReached'] is TRUE
     * @return boolean
     */
    private function validateSuccessRequest() {
        if (!$this->params['goalId']) {
            return FALSE;
        }

        if (!$this->params['goalReached']) {
            return FALSE;
        }

        $goal = $this->performers_goals->getById($this->params['goalId']);
        if (!$goal || $goal->performer_id != $this->params['performerId']) {
            return FALSE;
        }

        return TRUE;
    }
    
    private function prepareParams() {
        $this->paramsPattern = array(
            'uniqId'      => 'uniqId',      // Performer's session uniqId [USELESS] 
            'userId'      => 'performerId', // Performer's id
            'pasword'     => 'password',    // Performer's password
            'type'        => 'type',        // {start,stop,success} => failed if type=stop and gained<goal
            'goal'        => 'goal',        // how many chips to gather for goal
            'action'      => 'action',      // the goal's text
            'id'          => 'goalId',      // null if this is a start event 
            'gained'      => 'gained',      // how many chips gathered so far
            'goalReached' => 'goalReached'  // true | false
        );
        $this->params       = array();
    }
    
    private function respondToFMS($goalId, $status = TRUE, $message = FALSE, $exit = TRUE) {
        if(!$message){
            $response = sprintf($this->responsePattern, $goalId, $this->kickNonPayingUsers, $status);
        } else {
            $response = sprintf($this->responsePatternWithMessage, $goalId, $this->kickNonPayingUsers, $status, $message);
        }
        
        write_request($this->file_log, $response, FALSE, $exit);
    }
}