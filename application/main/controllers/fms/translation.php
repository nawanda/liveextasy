<?php

class Translation_controller extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function index($performer_id = false) {

        $data['start_chat_text'] = NULL;

        if ($performer_id > 0) {
            $this->load->model('performers_profile');

            $profile = $this->performers_profile->get_one_by_id($performer_id);

            if (is_object($profile)) {
                $data['start_chat_text'] = $profile->start_chat_text;
            }
        }
        header("Content-Type:text/xml");
        $this->load->view('fms/translations', $data);
    }
}