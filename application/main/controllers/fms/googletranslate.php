<?php

class Googletranslate_controller extends CI_Controller {
    private $googleAPIKey;
    private $googleAPIUrl;
    private $debugLevel;
    
    function __construct() {
        parent::__construct();
        $this->googleAPIKey = GOOGLE_API_KEY;
        $this->googleAPIUrl = "https://www.googleapis.com/language/translate/v2/";
        if($this->input->post('debugLevel')){
            $this->debugLevel = $this->input->post('debugLevel');
        }else{ 
            $this->debugLevel   = 0;
        }        
        //Validate Performer
        $this->load->model('performers');
        $performer = $this->performers->get_one_by_id($this->input->post("performerId"));
        if(!$performer) die('Hack Attempt! No performer');
        if($performer->password !== $this->input->post("pasword")) die('Hack Attempt!');
        
        //Validate Chatroom
        if(!$this->input->post("uniqId")) die('Hack Attempt! No uniqueId');
    }

    //CURL -XPOST "http://dev.garessio.ro/modenalatest/fms/googletranslate" -d "message=test"
    function index() {
        $googleParams= "key=".$this->googleAPIKey;
        $googleParams.= "&target=".$this->input->post("destLang");
        $googleParams.= "&q=".rawurlencode($this->input->post("message"));

        //Add detect API if necesarry
        $ch = curl_init($this->googleAPIUrl.'?'.$googleParams);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);  
        curl_setopt($ch, CURLOPT_POST, false);
        curl_setopt($ch, CURLOPT_VERBOSE, true);

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $response = curl_exec($ch); 
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        
        $resultArray = json_decode($response, true);
        
        //Debug:
        if($this->debugLevel){
            switch($this->debugLevel){
                case 1: var_dump($this->googleAPIUrl.'?'.$googleParams);die;
                case 2: var_dump($response);die;
                case 3:
                default: var_dump($resultArray);die;
            }
        }
        if($httpCode == 200){ 
            $translated = $resultArray['data']['translations'][0]['translatedText'];
        } else {
            $translated = $this->input->post("message").'-!-';
        }
        
        die("message=".$translated);
    }
}