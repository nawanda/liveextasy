<?php
/**
 * Clasa construita pentru eventualel actiuni ce trebuie sa fie publice pentru toti useri/performeri/studio/admin in caz ca acestia vor sa apeleze actiunea ca fiind logati
 * Cand is logati nu ii lasa sa apeleze /main
 * 
 * @author Andrei
 *
 */
class Actions_controller extends CI_Controller{
	
	/*
	 * Constructor
	 */
	function __construct(){
		parent::__construct();		
	}
	
	/**
	* Schimba limba
	* @param unknown_type $lang
	* @author Baidoc
	*/
	function language($lang = FALSE){
		if( ! $lang ){
			$this->session->set_flashdata('msg',array('success'=>FALSE,'message'=>lang('Invalid language!')));
			redirect();
		}
		
		$languages = $this->config->item('lang_avail');
		
		if( ! isset( $languages[$lang]) ){
			$this->session->set_flashdata('msg',array('success'=>FALSE,'message'=>lang('Invalid language!')));
			redirect();				
		}
		
		$this->lang_detect->changeLang($lang);
		
		redirect($this->config->item('base_url'));
	}
				
}