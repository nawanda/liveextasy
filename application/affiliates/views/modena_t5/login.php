<div class="m5-panel shadow">
	<div id="login">
		<div class="login_box">
			<div class="title">
				<?php echo lang('Affiliate Area')?>
			</div>
			<div class="m5-signup-form w-form">
				<?php echo form_open('') ?>
				<div class="b_sep_line">
					<span class="m5-form-label"><?php echo lang('Your name') ?>*:</span><br/>
					<?php echo form_input('username', set_value('username'), 'class="m5-input w-input"') ?>
					<span class=""><?php echo form_error('username') ?></span>
				</div>
				<div class="b_sep_line">
					<span class="m5-form-label"><?php echo lang('Your password') ?>*:</span><br/>
					<?php echo form_password('password', set_value('password'), 'class="m5-input w-input"') ?>
					<span class=""><?php echo form_error('password') ?></span>
				</div>

				<div style="margin-top:8px; text-align: center;">
					<button class="submit-button-2 w-button"  type="submit" style="width:225px;"> <?php echo lang('Login as affiliate') ?> </button><br/>
					<div class="m5-forgot-pass">
						<a href="#" id="myBtn" class="forgot_password"><?php echo lang('Forgot Password?') ?></a>
						&nbsp;&nbsp;|&nbsp;&nbsp;
						<a href="<?php echo site_url('register')?>" class="red f13" ><?php echo lang('Register') ?></a>
					</div>
				</div>
				<?php echo form_close('') ?>
			</div>
		</div>
	</div>

	<div class="clear"></div>
</div>

<!-- The Modal -->
<div id="myModal" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <span class="close">&times;</span>
    	<div class="m5-signup-form w-form">
			<h3 class="m5-title"><?PHP echo lang('Forgot password?')?></h3>

		<?php echo form_open('forgot-password')?>
			<div>
				<label class="m5-form-label"><?php echo lang('User name')?>*:</label><br/>
				<?php echo form_input('username', set_value('username'), ' class="m5-input w-input"')?>
				<span class="italic red"><?php echo form_error('username', ' ', ' ')?></span>
			</div>
		<br/>
			<div>
				<label class="m5-form-label"><?php echo lang('Email address')?>*:</label><br/>
				<?php echo form_input('email', set_value('email'), 'class="m5-input w-input"')?>
				<span class="italic red"><?php echo form_error('email', ' ', ' ')?></span>
			</div>

			<div style="margin-top:8px; text-align: center;">
				<button class="submit-button-2 w-button"  type="submit" style="width:130px; display: inline-block;"> <?php echo lang('Submit')?> </button>
			</div>
		<?php echo form_close('')?>
	</div>
  </div>

</div>

<script>
	// Get the modal
var modal = document.getElementById('myModal');

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on the button, open the modal
btn.onclick = function() {
    modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}
</script>
