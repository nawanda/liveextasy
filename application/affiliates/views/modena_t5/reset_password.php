
<div class="content">
		<div class="white performers-details-titles">
			<h4><?php echo lang('Reset password') ?></h4>
		</div>
		<?php echo form_open($form_link)?>
		<div class="m5-signup-form w-form"   id="user_settings">

			<!--  PASSWORD -->
			<div>
				<label><span class="m5-form-labelv2"><?php echo lang('Password')?></span></label>
				<?php echo form_password('password',set_value('password'),'class="m5-inputv2 w-input"')?>
				<span class="error message" htmlfor="password" generated="true"></span>
			</div>

			<!--  REPEAT PASSWORD -->
			<div>
				<label><span class="m5-form-labelv2"><?php echo lang('Repeat password')?></span></label>
				<?php echo form_password('rep_password',set_value('rep_password'),'class="m5-inputv2 w-input"')?>
				<span class="error message" htmlfor="rep_password" generated="true"></span>
			</div>
			<br/>
			<div>
				<?php echo form_submit('submit', lang('Update'), 'class="submit-button-2 w-button"') ?>
			</div>
		</div>
		<?php echo form_close()?>
		<div class="clear"></div>
	</div>
