<link rel="stylesheet" type="text/css" href="<?php echo assets_url()?>css/table.css" media="screen" />
	<div class="content">
		<div class="white performers-details-titles">
			<h4><?php echo lang('Payments') ?></h4>
			<p><?php echo lang('You can see your payments here.') ?></p>
		</div>
			<!--  <p>There are <strong><?php echo count($credits)?></strong> payments</p>  -->
			<div>
				<table class="data display datatable" cellspacing="0" cellpadding="0" style="width:100%;">
					<thead>
						<tr>
							<th style="width: 25%; white-space: nowrap;"><?php echo lang('Paid Date') ?></th>
							<th style="width: 20%; white-space: nowrap;"><?php echo lang('Paid interval') ?></th>
							<th style="width: 20%; white-space: nowrap;"><?php echo lang('Amount') ?></th>
						</tr>
					</thead>
					<tbody style="text-align: center; color: #000;">
						<?php if( sizeof($credits) == 0 ):?>
							<tr>
								<td colspan="3"><?php echo lang('You have no payments') ?></td>
							</tr>
						<?php else:?>
							<?php $i = 0?>
							<?php foreach($credits as $credit ):?>
								<tr class="<?php echo ($i % 2==0)?'even':'odd'?>">
									<td><?php echo date('Y-m-d', $credit->paid_date) ?></td>
									<td><?php echo date('Y-m-d', $credit->from_date)?> - <?php echo date('Y-m-d', $credit->to_date)?></td>
									<td><?php echo print_amount_by_currency($credit->amount_chips,TRUE)?></td>
								</tr>
								<?php $i++ ?>
							<?php endforeach;?>
						<?php endif;?>
					</tbody>
				</table>
				<?php echo $pagination?>
			</div>
			<div class="clear"></div>
	</div>
