<div class="w-tab-menu center performer-menu">
	<div class="m5-tab-link-simple  <?=($this->uri->segment(2) == 'change_password' || ($this->uri->segment(1) == 'settings' && !$this->uri->segment(2))) ? 'selected' : NULL ?> w-inline-block w-tab-linkv2" onclick="document.location.href='<?php echo site_url('settings/change_password')?>';">
		<a href="<?php echo site_url('settings/change_password')?>"><?php echo lang('Change password') ?></a>
	</div>
	<div class="m5-tab-link-simple  <?=($this->uri->segment(2) == 'personal_details') ? 'selected' : NULL ?> w-inline-block w-tab-linkv2" onclick="document.location.href='<?php echo site_url('settings/personal_details')?>';">
		<a href="<?php echo site_url('settings/personal_details')?>"><?php echo lang('Personal details') ?></a>
	</div>
	<div class="m5-tab-link-simple <?=($this->uri->segment(2) == 'payment') ? 'selected' : NULL ?> w-inline-block w-tab-linkv2" onclick="document.location.href='<?php echo site_url('settings/payment')?>';">
		<a href="<?php echo site_url('settings/payment')?>"><?php echo lang('Payments') ?></a>
	</div>
</div>
<div class="m5-tab-content-actualv2 w-tab-content shadowv3">
	<?php
		if(isset($page)){
			$this->load->view($page);
		}
	?>
</div>
<div class="clear"></div>
