	<div class="content">
				<h3 class="account-page white">My Account page</h3>
				<p>Check here to see your balance and account information.</p>
				<div class="m5-panel-gray shadowv2" style="padding: 10px 0;">
				<div>
				<?php $credits_red = '<span class="white">' . print_amount_by_currency($earning, TRUE) . '</span>' ?>
				<p class="s-margin"><?php echo lang('Your account has').' '.$credits_red ?></p>
				<p class="s-margin"><?php echo lang('Please send all users to this URL') ?>: <a class="white" href="<?php echo main_url('ads/0/1/' . $this->user->hash) ?>"><?php echo main_url('ads/0/1/' . $this->user->hash) ?></a></p>
				<?php if($this->user->release < convert_chips_to_value($earning)):?>
					<p class="s-margin"><span><?php echo lang('You have been marked for payment.')?></span></p>
				<?php else: ?>
					<?php $need_more  = '<span class="white">' . print_amount_by_currency(convert_value_to_chips(abs($this->user->release - convert_chips_to_value($earning))), TRUE) . '</span>' ?>
					<p class="s-margin"><?php echo sprintf(lang('You need %s more and we\'ll send you the payment!'), $need_more)?></p>
				<?php endif; ?>
				<p class="s-margin"><?php echo sprintf(lang('Your commission is %s from each transaction referred users make.'), SETTINGS_TRANSACTION_PERCENTAGE.'%') ?><p>
			</div>
		</div>
		<div class="clear"></div>

		<script type="text/javascript" src="<?php echo assets_url()?>js/jquery.strtotime.min.js"></script>
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
			<script type="text/javascript">
				$(function() {


					$( ".datepickerStart" ).datepicker({ maxDate: '<?php echo date('d-m-Y') ?>',  dateFormat: 'dd-mm-yy'});
					$( ".datepickerEnd" ).datepicker({ maxDate: '<?php echo date('d-m-Y') ?>',  dateFormat: 'dd-mm-yy'});


				});

				function show_chart(id){

					$('#statistics .displayed').hide();
					$('#chart_'+id).addClass('displayed');
					$('#chart_'+id).show();
				}
			</script>
		<div class="m5-panel-gray shadowv2 relativ">
			<div class="title-statistics">
				<h4 class="white"><?php echo sprintf(lang('%s\'s Statistics'),$this->user->username)?></h4>
				<p><?php echo lang('Here is your detailed information on earnings for your ads.');?></p>
			</div>
			<div class="f-right white" style="padding-bottom: 10px;">
				<?php echo form_open(current_url())?>
				<div style="display:inline-block; "><?php echo lang('Start date')?><br/><?php echo form_input("start_date",  $start_date, 'class="datepickerStart m5-input w-input" readonly="readonly"  style="width:140px;"')?></div>
				<div style="display:inline-block;"><?php echo lang('End date')?><br/><?php echo form_input("end_date", $end_date, ' class="datepickerEnd m5-input w-input"  readonly="readonly" style="width:140px;"')?></div>
				<div style="display: inline-block; top:20px; vertical-align: middle; color: #000; padding: 3px 0;"><input type="submit" value="Refresh" class='submit-button w-button'/></div>
				<?php echo form_close()?>
			</div>

			<div class="clear"></div>
			<?php if ($chart_affiliates != ''): ?>
			<script type="text/javascript" src="<?php echo assets_url() ?>js/highcharts/highcharts.src.js"></script>
			<script type="text/javascript" src="<?php echo assets_url() ?>js/highcharts/modules/exporting.js"></script>
			<script type="text/javascript" src="<?php echo assets_url() ?>js/highcharts/themes/gray.js"></script>
			<!-- Additional files for the Highslide popup effect -->
			<script type="text/javascript" src="<?php echo assets_url() ?>js/highcharts/highslide-full.min.js"></script>
			<script type="text/javascript" src="<?php echo assets_url() ?>js/highcharts/highslide.config.js" charset="utf-8"></script>
			<link rel="stylesheet" type="text/css" href="<?php echo assets_url() ?>css/highslide.css" />

			<script type="text/javascript">

				var chart;
				$(document).ready(function() {

					// define the options

					chart = new Highcharts.Chart({


						chart: {
							renderTo: 'chart_affiliates',
							marginBottom:50,
							height:450,
							zoomType: 'x'
						},

						title: {
							text: "<?php echo lang('Ads traffic statistics')?>"
						},

						subtitle: {
							text: null
						},

						xAxis: {
							type: 'datetime',
							dateTimeLabelFormats: { // don't display the dummy year
								month: '%e. %b',
								year: '%b'
							},
							maxZoom: 28 * 24 * 3600000, // fourteen days
							tickWidth: 10,
							gridLineWidth: 1,
							labels: {
							}
						},

						yAxis: [{ // left y axis
								title: {
									text: null
								},
								labels: {
									formatter: function() {
										return Highcharts.numberFormat(this.value, 0);
									}
								},
								showFirstLabel: false
							}],

						legend: {
							align: 'right',
							verticalAlign: 'top',
							y: 20,
							x: 5,
							floating: false,
							width: 180,
							itemWidth: 180,
							style:{
								display: 'block'
							},
							//margin:100,
							borderWidth: 0
						},


						tooltip: {
							shared:true,
							formatter: function() {
								var tooltip = '';
								tooltip += '<b>'+Highcharts.dateFormat('%e %b. %Y', this.x)+'<b><br/>';
								for(var i in this.points){

									tooltip += '<span><b style="color:'+this.points[i].series.color+'">'+ this.points[i].series.name +'</b>: '+this.points[i].y +'  </span> <br/>';
								}


								return tooltip;
							}
						},
						plotOptions: {
							series: {
								cursor: 'pointer',
								point: {
									events: {
										click: function() {
											hs.htmlExpand(null, {
												pageOrigin: {
													x: this.pageX,
													y: this.pageY
												},
												headingText: this.series.name,
												maincontentText: Highcharts.dateFormat('%e %b. %Y', this.x) +':<br/> '+
													this.y ,
												width: 200
											});
										}
									}
								},
								marker: {
									enabled: false,
									states: {
										hover: {
											enabled: true,
											radius: 5
										}
									}
								}
							}
						},

						series: [<?php echo $chart_affiliates ?>]
					});



				});

			</script>


			<div id="chart_affiliates"  style="width:100%; max-width: 960px;" >

			</div>
		<?php else:?>
			<div id="chart_affiliates" style="width:965px; text-align: center;">
			<?php echo lang('Not available')?>
			</div>
		<?php endif ?>
	</div>
</div>
