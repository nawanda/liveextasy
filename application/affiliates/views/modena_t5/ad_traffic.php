<script type="text/javascript" src="<?php echo assets_url()?>js/jquery.strtotime.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo assets_url()?>css/table.css" media="screen" />
<script type="text/javascript">
jQuery(function($){
	$(".datepickerStart" ).datepicker({ maxDate: '<?php echo date('d-m-Y')?>',  dateFormat: 'dd-mm-yy'});
	$(".datepickerEnd" ).datepicker({ maxDate: '<?php echo date('d-m-Y')?>',  dateFormat: 'dd-mm-yy'});
	$( ".datepicker" ).datepicker({ dateFormat: 'yy-mm-dd' });
	$('.action').click(function(){
		document.location = '<?php echo site_url()?>traffic/' + $('input[name=start_date]').val() + '/' + $('input[name=end_date]').val();
		return false;
	});
});
</script>
		<div class="content">
			<div class="white performers-details-titles">
				<h4><?php echo lang('Your Ad trafic ') ?></h4>
				<p><?php echo lang('You can see your ad trafic here.') ?></p>
			</div>
			<div class="m5-panel-gray shadowv2 relativ">
				<div class="f-right white" style="padding-bottom: 10px;">
					<?php echo form_open(current_url())?>
					<div style="display:inline-block; "><?php echo lang('Start date')?><br/><?php echo form_input("start_date",  date('Y-m-d', $start), 'class="datepickerStart m5-input w-input" readonly="readonly"  style="width:140px;"')?></div>
					<div style="display:inline-block;"><?php echo lang('End date')?><br/><?php echo form_input("end_date", date('Y-m-d', $end), ' class="datepickerEnd m5-input w-input"  readonly="readonly" style="width:140px;"')?></div>
					<div style="display: inline-block; margin-top: -3px; vertical-align: middle;"><input class="submit-button w-button" type="submit" value="Refresh"/></div>
					<?php echo form_close()?>
				</div>
				<div class="clear"></div>
				<table cellspacing="0" cellpadding="0" style="width:100%;" class="data display datatable">
					<thead>
						<tr>
							<th style="white-space: nowrap; text-align: left;"><?php echo lang('Name')?></th>
							<th style="width: 150px; white-space: nowrap; text-align: left;"><?php echo lang('Views')?></th>
							<th style="width: 150px; white-space: nowrap; text-align: left;"><?php echo lang('Hits')?></th>
							<th style="width: 100px; white-space: nowrap; text-align: left;"><?php echo lang('Registers')?></th>
							<th style="width: 100px; white-space: nowrap; text-align: left;"><?php echo lang('Transactions')?></th>
							<th style="width: 100px; white-space: nowrap; text-align: left;"><?php echo lang('Earnings')?></th>
						</tr>
					</thead>
					<tbody class="black">
						<?php if(sizeof($ad_zones) == 0):?>
							<tr>
								<td colspan="6"><?php echo lang('There are no entires')?></td>
							</tr>
						<?php else :
							$i = 0;
							foreach($ad_zones as $ad_zone):?>
								<tr class="data display datatable">
									<td style="text-align: left;"><?php echo $ad_zone->name?></td>
									<td style="text-align: left;"><?php echo (isset($ad_zone->view)) ? $ad_zone->view : '0'?></td>
									<td style="text-align: left;"><?php echo (isset($ad_zone->hit)) ? $ad_zone->hit : '0'?></td>
									<td style="text-align: left;"><?php echo (isset($ad_zone->register)) ? $ad_zone->register : '0'?></td>
									<td style="text-align: left;"><?php echo (isset($ad_zone->transaction)) ? $ad_zone->transaction : '0'?></td>
									<td style="text-align: left;"><?php echo print_amount_by_currency($ad_zone->earnings, TRUE)?></td>

								</tr>
						<?php
							$i++;
						 	endforeach;
						endif?>
					</tbody>
				</table>

			</div>
		</div>
			<div class="clear"></div>
		</div>
