   <script type="text/javascript">
    function selectText() {
        if (document.selection) {
        var range = document.body.createTextRange();
            range.moveToElementText(document.getElementById('selectable'));
        range.select();
        }
        else if (window.getSelection) {
        var range = document.createRange();
        range.selectNode(document.getElementById('selectable'));
        window.getSelection().addRange(range);
        }
    }
    </script>

		<div class="content m5-panel-gray shadowv3">
      <div class="white performers-details-titles">
				<h4><?php echo lang('Get ad code') ?></h4>
				<p><?php echo lang('Use this code') ?></p>
			</div>

			<div class="affiliate_get_code " style="padding: 20px; padding-top: 0px;">
				<p class="white">
					<?=lang('Put this code in your html page, where you want to show the add!')?>
				</p>
				<div class="code_box m5-panel-gray shadowv2 " id="selectable" onclick="selectText()" style="text-align:justify; margin-top: 20px; margin-bottom: 20px; padding: 15px;">
					<?=  htmlentities($code)?>
				</div>
				<div style="margin:8px; margin-bottom: 20px; text-align: right;">
					<button class="submit-button w-button" style="float: right;" onclick="document.location.href='<?=site_url('promo')?>'"> <?php echo lang('Go back to your ad zones')?> </button><br/>
				</div>
			</div>

			<div class="clear"></div>
		</div>
