<div class="content affiliate-personal-details">
			<div class="white performers-details-titles">
				<h4><?php echo lang('Personal Details Page') ?></h4>
				<p><?php echo lang('You can edit your personal details page here.') ?></p>
			</div>
			<div id="affiliate_settings">
			<?php echo form_open('settings/personal_details')?>
			<div class="m5-signup-form w-form">

				<!--  FIRST NAME -->
				<div>
					<label><span class="m5-form-labelv2"><?php echo lang('First name')?>:</span></label>
					<?php echo form_input('first_name',set_value('first_name', $this->user->first_name),'class="m5-inputv2 w-input"')?>
					<span class="error" htmlfor="first_name" generated="true"></span>
				</div>

				<!--  LAST NAME -->
				<div>
					<label><span class="m5-form-labelv2"><?php echo lang('Last name')?>:</span></label>
					<?php echo form_input('last_name',set_value('last_name', $this->user->last_name),'class="m5-inputv2 w-input"')?>
					<span class="error" htmlfor="last_name" generated="true"></span>
				</div>

				<!-- ADDRESS -->
				<div>
					<label><span class="m5-form-labelv2"><?php echo lang('Address')?>:</span></label>
					<?php echo form_input('address',set_value('address', $this->user->address),'class="m5-inputv2 w-input"')?>
					<span class="error" htmlfor="address" generated="true"></span>
				</div>

				<!-- CITY -->
				<div>
					<label><span class="m5-form-labelv2"><?php echo lang('City')?>:</span></label>
					<?php echo form_input('city',set_value('city', $this->user->city),'class="m5-inputv2 w-input"')?>
					<span class="error" htmlfor="city" generated="true"></span>
				</div>

				<!-- ZIP -->
				<div>
					<label><span class="m5-form-labelv2"><?php echo lang('Zip')?>:</span></label>
					<?php echo form_input('zip',set_value('zip', $this->user->zip),'class="m5-inputv2 w-input"')?>
					<span class="error" htmlfor="zip" generated="true"></span>
				</div>

				<script type="text/javascript">
					$(function(){
						if($('#country').val() == 'US'){
							$('#state').show();
						}

						$('#country').change(function(){
								if($('#country').val() == 'US'){
									$('#state').slideDown();
									$('input[name=state]').val('');
								} else {
									$('#state').slideUp();
									$('input[name=state]').val('state');
								}
						});
					});
				</script>

				<!-- COUNTRY -->
				<div>
					<label><span class="m5-form-labelv2"><?php echo lang('Country')?>:</span></label>
					<?php echo form_dropdown('country', $countries, set_value('country', $this->user->country_code),'id="country"')?>
					<span class="error" htmlfor="country" generated="true"></span>
				</div>

				<!-- STATE -->
				<div id="state" style="display:none">
					<label><span class="m5-form-labelv2"><?php echo lang('State')?>:</span></label>
					<?php echo form_input('state',set_value('state', $this->user->state),'class="m5-inputv2"')?>
					<span class="error" htmlfor="state" generated="true"></span>
				</div>


				<div>
					<label><span class="m5-form-labelv2"><?php echo lang('Phone number')?>:</span></label>
					<?php echo form_input('phone',set_value('phone', $this->user->phone),'class="m5-inputv2 w-input"')?>
					<span class="error" htmlfor="phone" generated="true"></span>
				</div>
				<div>
					<?php echo form_submit('submit', lang('Update'), 'class="submit-button-2 w-button"')?>
				</div>
			</div>
			<?php echo form_close()?>
			<div class="clear"></div>
		</div>
		</div>
