<div class="content">
		<div class="white performers-details-titles">
			<p><?php echo lang('You can change password here') ?></p>
		</div>
		<div id="affiliate_settings">
			<?php echo form_open('settings/change_password')?>
			<div class="m5-signup-form w-form">
				<!--  PASSWORD -->
				<div>
					<label><span class="m5-form-labelv2"><?php echo lang('Old password')?>:</span></label>
					<?php echo form_password('old_password',set_value('old_password'),'class="m5-inputv2 w-input"')?>
					<span class="error" htmlfor="old_password" generated="true"></span>
				</div>

				<!--  PASSWORD -->
				<div>
					<label><span class="m5-form-labelv2"><?php echo lang('Password')?>:</span></label>
					<?php echo form_password('password',set_value('password'),'class="m5-inputv2 w-input"')?>
					<span class="error" htmlfor="password" generated="true"></span>
				</div>

				<!--  REPEAT PASSWORD -->
				<div>
					<label><span class="m5-form-labelv2"><?php echo lang('Repeat password')?>:</span></label>
					<?php echo form_password('rep_password',set_value('rep_password'),'class="m5-inputv2 w-input"')?>
					<span class="error" htmlfor="rep_password" generated="true"></span>
				</div>
					<div>
					<?php echo form_submit('submit', lang('Update'), 'class="submit-button-2 w-button"')?>
				</div>
			</div>
			<?php echo form_close()?>
			<div class="clear"></div>
		</div>
	</div>
