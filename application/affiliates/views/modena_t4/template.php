<?php $this->load->view('includes/head') ?>
	<body id="affiliate">
		<div id="page_bg">
			<div id="warpper">
				<div id="header">
					<?php $this->load->view('includes/header.php')?>
					<div class="clear"></div>
				</div>
				<div id="page" class="dark_gray">
					
					<?php ($this->user->id > 0 ) ? $this->load->view('includes/menu') : NULL?>
					<?php $this->load->view('includes/errors');?>
					
					<?php
					if(isset($_sidebar) && $_sidebar){
						$this->load->view('includes/_sidebar');
					}elseif(isset($page) && !(isset($_payments_sidebar) && $_payments_sidebar)){						
						$this->load->view($page);
					}
					
					if(isset($_payments_sidebar) && $_payments_sidebar) {
						$this->load->view('includes/_my_payments_sidebar');
					}
				?>
				</div>
			</div>
			<div class="clear"></div>
			<?php $this->load->view('includes/footer') ?>
		</div>
	</body>
</html>