   <script type="text/javascript">
    function selectText() {
        if (document.selection) {
        var range = document.body.createTextRange();
            range.moveToElementText(document.getElementById('selectable'));
        range.select();
        }
        else if (window.getSelection) {
        var range = document.createRange();
        range.selectNode(document.getElementById('selectable'));
        window.getSelection().addRange(range);
        }
    }
    </script>

		<div class="content">
			<?php 
			if(isset($page_title) && $page_title != ''):
			?>
				<div class="title">
					<?=$page_title?>
				</div>
			
			<?php endif ?>				
			
			<div class="affiliate_get_code">
				<p>
					<?=lang('Put this code in your html page, where you want to appeare the ad!')?>
				</p>
				<div class="code_box" id="selectable" onclick="selectText()" style="text-align:justify;">
					<?=  htmlentities($code)?>
				</div>
				<div style="margin-top:8px; text-align: right;">
					<button class="red"  onclick="document.location.href='<?=site_url('promo')?>'"> <?php echo lang('Go back to your ad zones')?> </button><br/>
				</div>
			</div>
			
			<div class="clear"></div>
		</div>