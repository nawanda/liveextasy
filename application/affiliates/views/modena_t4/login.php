	<div class="content">
		<div id="login">
		<?php if (isset($page_title) && $page_title != ''):?>
			<div class="title">
				<?=$page_title?>
			</div>
		<?php endif ?>		
		<div class="details">
			<div class="fl_r how_it_works white">
				<h2>How it works</h2>
				<p>
					<?php echo lang('If you have a website, you can apply to become a member of our affiliate program and earn money by referring visitors to our website.') ?>	
				</p>
				<p>
					<?php echo sprintf(lang('Each time a user buys chips, you will receive %s of their value. There is no time limit and you do not have to wait for users to spend bought chips on the website; this means that if in 10 years a user referred by your website buys 100 chips, your account will be funded with the real currency-equivalent of 30 chips at that precise time.'), SETTINGS_TRANSACTION_PERCENTAGE.'%') ?>
				</p>
			</div>
			<div class="title"><?php echo lang('Welcome to our affiliate program!') ?></div>
			<ul>
				<li><?php echo lang('Join our <span class="bold red">affiliate</span> program now!')?></li>
				<li><?php echo lang('Start making load of <span class="bold red">cash</span> instantly!')?></li>
				<li><?php echo lang('You can <span class="bold red">earn</span> up to <span class="bold red">$5000</span> a month')?></li>
			</ul>
			<a style="margin-left : 350px;" href="<?php echo site_url('register') ?>" class="black_btn signup_btn"><?php echo lang('Sign up now') ?></a>

		</div>
			
		<div class="login_box">
			<div style="padding:20px;">
				<?php echo form_open('') ?>
				<div class="b_sep_line">
					<span class="italic"><?php echo lang('Your name') ?>*:</span><br/>
					<?php echo form_input('username', set_value('username')) ?>
					<span class=""><?php echo form_error('username') ?></span>
				</div>
				<div class="b_sep_line">
					<span class="italic"><?php echo lang('Your password') ?>*:</span><br/>
					<?php echo form_password('password', set_value('password')) ?>
					<span class=""><?php echo form_error('password') ?></span>
				</div>

				<div style="margin-top:8px;">
					<button class="red"  type="submit" style="width:225px;"> <?php echo lang('Login as affiliate') ?> </button><br/>
					<a href="<?php echo site_url('home/forgot_password') ?>" class="red f13 forgot_password" style="text-transform: none; text-align: center; display:block;"><?php echo lang('Forgot Password?') ?></a>
				</div>
				<?php echo form_close('') ?>
			</div>
		</div>
	</div>
</div>
<div class="clear"></div>