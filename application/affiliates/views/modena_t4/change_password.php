	<div class="content">
		<?php
		if(isset($page_title) && $page_title != ''):
		?>
			<div class="title">
				<?=$page_title?>
			</div>
		
		<?php endif ?>		
		<div id="affiliate_settings">
			<?php echo form_open('settings/change_password')?>
			<div class="dark_gray italic" style="margin: 0px auto;">
				<!--  PASSWORD -->
				<div>
					<label><span class="bold"><?php echo lang('Old password')?>:</span></label>
					<?php echo form_password('old_password',set_value('old_password'))?>
					<span class="error" htmlfor="old_password" generated="true"></span>
				</div>

				<!--  PASSWORD -->
				<div>
					<label><span class="bold"><?php echo lang('Password')?>:</span></label>
					<?php echo form_password('password',set_value('password'))?>
					<span class="error" htmlfor="password" generated="true"></span>
				</div>

				<!--  REPEAT PASSWORD -->
				<div>
					<label><span class="bold"><?php echo lang('Repeat password')?>:</span></label>
					<?php echo form_password('rep_password',set_value('rep_password'))?>
					<span class="error" htmlfor="rep_password" generated="true"></span>
				</div>
					<div>
					<?php echo form_submit('submit', lang('Update'), 'class="red"')?>
				</div>
			</div>
			<?php echo form_close()?>
			<div class="clear"></div>
		</div>
	</div>