<div id="main_nav">
<!--	<span class="strip tl"></span>
	<span class="strip tr"></span>-->
	<span class="strip bl"></span>
	<span class="strip br"></span>
	<ul>
		<li><a href="<?=site_url('')?>" class="white <?=(!$this->uri->segment(1)) ? 'selected' : NULL?>"><?=lang('My Account')?></a></li>
		<li><a href="<?=site_url('settings')?>" class="white <?=($this->uri->segment(1) == 'settings') ? 'selected' : NULL?>"><?=lang('My Settings')?></a></li>
		<li><a href="<?=site_url('promo')?>" class="white <?=($this->uri->segment(1) == 'promo') ? 'selected' : NULL?>"><?=lang('Promo Tools')?></a></li>
		<li><a href="<?=site_url('payments')?>" class="white <?=($this->uri->segment(1) == 'payments') ? 'selected' : NULL?>"><?=lang('My Payments')?></a></li>
		<li><a href="<?=site_url('traffic')?>" class="white <?=($this->uri->segment(1) == 'traffic') ? 'selected' : NULL?>"><?=lang('Traffic')?></a></li>
	</ul>
</div>
