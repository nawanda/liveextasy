<div id="footer" class="">
	<span class="strip bl"></span>
	<span class="strip br"></span>
	
	<div class="fl_l">
		<div class="logo">
			<a href="<?php echo ($this->user->id > 0) ? site_url() : main_url()?>">
				<img alt="logo" src="<?php echo assets_url('modena_t4/images/logo_footer.png') ?>" />
			</a>
		</div>
		<div class="copyright">
			<?php echo sprintf(lang('&copy; 2012 %s  |  All rights reserved.'), SETTINGS_SITE_TITLE)?>	
		</div>
		<div id="langs">
			<?php 
			$langs_av = $this->config->item('lang_avail');
			if(count($langs_av) > 1):
				foreach($langs_av as $row => $language):
			?>
				<a href="<?php echo main_url('language/'.$row)?>" title="<?php echo sprintf(lang('Change language to %s'),$language)?>"><img src="<?php echo assets_url()?>images/flags/<?php echo strtoupper($row)?>.png" alt="<?php echo $language?>" /></a>&nbsp;
			<?php 
				endforeach;
			endif;
			?>

		</div>
	</div>
	<div class="fl_r">
		<ul>
			<li><a href="<?php echo main_url(PREFORMERS_URL)?>"><?php echo lang('Performers <span class="fg_demi">Area</span>')?></a></li>
			<li><a href="<?php echo main_url(PREFORMERS_URL . '/register')?>" id="become_performer"><?php echo lang('Performers <span class="fg_demi">wanted</span>')?></a></li>
			<li><a href="<?php echo main_url(STUDIOS_URL)?>"><?php echo lang('Studios <span class="fg_demi">Area</span>')?></a></li>
			<li><a href="<?php echo main_url(AFFILIATES_URL)?>"><?php echo lang('Affiliates <span class="fg_demi">Area</span>')?></a></li>
		</ul>
		<div class="links">
			<a href="<?php echo main_url('documents/2257')?>"><?php echo lang('18 U.S.C 2257')?></a>
			&nbsp;&nbsp;|&nbsp;&nbsp;
			<a href="<?php echo main_url('documents/policy')?>"><?php echo lang('Privacy policy')?></a>
			&nbsp;&nbsp;|&nbsp;&nbsp;
			<a href="<?php echo main_url('documents/tos')?>"><?php echo lang('Terms & conditions')?></a>
			&nbsp;&nbsp;|&nbsp;&nbsp;
			<a href="<?php echo main_url('contact')?>"><?php echo lang('Contact us')?></a>
		</div>
	</div>
	
	<div class="clear"></div>
</div>

<?php if($this->agent->is_browser('Internet Explorer')):?>
<script type="text/javascript">
	jQuery(function($){
		$('button').click(function(){
			if($(this).parent().is("a")){
				document.location.href = $(this).parent().attr('href'); 
			}
		});
	});
</script>
<?php endif?>




<?php
if(is_file(BASEPATH.'../assets/txt/analytics.txt')){
	echo file_get_contents(BASEPATH.'../assets/txt/analytics.txt');
}
?>