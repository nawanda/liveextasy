

<?php if($this->user->id > 0):?>
<div class="site_name">
	<a href="<?php echo ($this->user->id > 0) ? site_url() : main_url() ?>">
		<img alt="logo" src="<?php echo assets_url('modena_t4/images/logo_middle.png') ?>" />
	</a>
	<br/>
	<span class="fg_book white" style="text-transform: uppercase;"><?php echo lang('Affiliate Administration panel')?></span>
</div>
<div class="black_box">
	<span class="strip bl"></span>
	<span class="strip br"></span>
	<span class="fg_book white"><?php echo lang('Welcome').' '.$this->user->username?></span>
	<a href="<?php echo site_url('home/logout')?>"><span class="icon logout"></span> <span class="gray"><?php echo lang('Logout')?></span></a>
</div>
<?php else: ?>
<div class="site_name">
	<a href="<?php echo ($this->user->id > 0) ? site_url() : main_url() ?>">
		<img alt="logo" src="<?php echo assets_url('modena_t4/images/logo_middle.png') ?>" />
	</a>
	<br/>
	<span class="fg_book white" style="text-transform: uppercase;"><?php echo lang('Affiliate Signup')?></span>
</div>
<?php endif?>