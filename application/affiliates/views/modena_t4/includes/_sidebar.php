<div id="left_menu">
	<div class="menu_item <?=($this->uri->segment(2) == 'change_password' || ($this->uri->segment(1) == 'settings' && !$this->uri->segment(2))) ? 'selected' : NULL ?>" onclick="document.location.href='<?php echo site_url('settings/change_password')?>';">
		<span class="icon"></span>
		<a href="<?php echo site_url('settings/change_password')?>"><?php echo lang('Change password') ?></a>
	</div>
	<div class="menu_item <?=($this->uri->segment(2) == 'personal_details') ? 'selected' : NULL ?>" onclick="document.location.href='<?php echo site_url('settings/personal_details')?>';">
		<span class="icon"></span>
		<a href="<?php echo site_url('settings/personal_details')?>"><?php echo lang('Personal details') ?></a>
	</div>
	<div class="menu_item <?=($this->uri->segment(2) == 'payment') ? 'selected' : NULL ?>" onclick="document.location.href='<?php echo site_url('settings/payment')?>';">
		<span class="icon"></span>
		<a href="<?php echo site_url('settings/payment')?>"><?php echo lang('Payments') ?></a>
	</div>
</div>
<div class="fl_r">
	<?php
		if(isset($page)){
			$this->load->view($page);
		}
	?>
</div>
<div class="clear"></div>
