<?php
/**
 * Genereaza tabelul corespunzator orarului performarilor.
 * @author Bogdan
 * @param performer_id
 * @return string
 */
if ( ! function_exists('render_schedule')) {
	
    function render_schedule($schedules, $as_string = FALSE) {

        $days_of_week = array('M', 'T', 'W', 'T', 'F', 'S', 'S');
        $map          = array();
        for ($i = 0; $i < 7; $i++){
        	
            for ($j = 0; $j < 24; $j++){
                $map[$i][$j] = 0;
            }
            
        }
        
        foreach ($schedules as $schedule){
            $map[$schedule->day_of_week][$schedule->hour] = 1;
        }
        
        return array('days_of_week' => $days_of_week,'map'=> $map);
    }
}

/**
 * Genereaza orarului performarilor [mobile].
 * @author Aurel
 * @param $data
 * @return array
 */
if(! function_exists('render_schedule_mobile')){
    function render_schedule_mobile($data){
        $mobile_schedule = array();
        foreach($data as $schedule){
            if(empty($mobile_schedule[$schedule->day_of_week + 1])) $mobile_schedule[$schedule->day_of_week +1] = array($schedule->hour);
            else $mobile_schedule[$schedule->day_of_week+1] = array_merge ($mobile_schedule[$schedule->day_of_week+1], array($schedule->hour));
        }

        foreach($mobile_schedule as $key => $day){
            sort($mobile_schedule[$key]);
            $mobile_schedule[$key] = group_nums($mobile_schedule[$key]);
        }
        return $mobile_schedule;
    }
}
