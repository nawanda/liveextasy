<?php
$lang['required']			= "Il campo %s è richiesto.";
$lang['isset']				= "Il campo %s è vuoto.";
$lang['valid_email']		= "Il campo %s richiede un indirizzo email valido.";
$lang['valid_emails']		= "Il campo %s richiede un indirizzo email valido.";
$lang['valid_url']			= "Il campo %s richiede un URL valido.";
$lang['valid_ip']			= "Il campo %s richiede un IP valido.";
$lang['min_length']			= "Il campo %s richiede almeno %s caratteri.";
$lang['max_length']			= "Il campo %s non può superare %s caratteri.";
$lang['exact_length']		= "Il campo %s deve avere esattamente %s caratteri.";
$lang['alpha']				= "Il campo %s richiede caratteri alfanumerici.";
$lang['alpha_numeric']		= "Il campo %s field può contenere soltanto caratteri alfanumerici.";
$lang['alpha_dash']			= "Il campo %s richiede caratteri alfanumerici.";
$lang['numeric']			= "Il campo %s richiede esclusivamente numeri.";
$lang['is_numeric']			= "Il campo %s richiede esclusivamente numeri.";
$lang['integer']			= "Il campo %s richiede un intero.";
$lang['regex_match']		= "Il campo %s è invalido.";
$lang['matches']			= "Il campo %s non corrisponde al campo %s field.";
$lang['is_natural']			= "Il campo %s richiede numeri positivi.";
$lang['is_natural_no_zero']	= "Il campo %s richiede un numero maggiore di 0";
$lang['decimal']			= "Il campo %s richiede un numero decimale.";
$lang['less_than']			= "Il campo %s richiede un valore minore di %s.";
$lang['greater_than']		= "Il campo %s richiede un valore maggiore di %s.";
/* End of file form_validation_lang.php */
/* Location: ./system/language/english/form_validation_lang.php */