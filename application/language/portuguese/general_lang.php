﻿<?php
$lang['join member']='Cadastre-se como Usuário';
$lang['join performer']='Cadastre-se como Modelo';
$lang['select the desired credit package']= "Selecione um pacote de Crédito Desejado";
$lang['who has credits can chat without limit in all chat rooms']= "Quem possui créditos pode conversar sem limite em todas as salas de chat";
$lang['upload a file']= "Carregar um Arquivo";
$lang['	performer entered a private chat or has logged off.'] = 'Modelo entrou em um chat privado ou saiu da sala.';
$lang['your free time has expired'] = 'Seu tempo livre acabou, Por favor registre-se para ter um tempo livre maior';
$lang['        your free time has expired'] = 'Seu tempo livre acabou, Por favor registre-se para ter um tempo livre maior';
$lang['i affirm have more than 18 years'] = 'EU AFIRMO SER MAIOR DE 18 ANOS';
$lang['only for over 18 years!'] = 'APENAS PARA MAIORES DE 18 ANOS!';
$lang[' exit '] = 'SAIR';
$lang['this site has erotic content and is not permitted by law (article 5 of the civil code) for people with less than 18 years! if you still do not have completed 18 years, do not proceed!'] = 'ESTE SITE POSSUI CONTEÚDO ERÓTICO E NÃO É PERMITIDO POR LEI (ARTIGO 5 DO CÓDIGO CIVIL) PARA PESSOAS COM MENOS DE 18 ANOS! CASO VOCÊ AINDA NÃO TENHA COMPLETADO 18 ANOS, NÃO PROSSIGA!';
$lang['menu']='MENU';
$lang['account']='CONTA';
$lang['my favorites']='Meus Favoritos';
$lang['my statement']='Meus Relatórios';
$lang['logout']='Sair';
$lang['Password']='Senha';
$lang['all models']='Modelos no Site';
$lang['schedule']='Agenda';
$lang['profile']='Perfil';
$lang['already a member']='JÁ SOU MEMBRO';
$lang['forgot your password?']='Esqueceu sua senha?';
$lang['online now']='On-line Agora';
$lang['there are no one online']='Não há modelos on-line no momento.';
$lang['adultcontent'] = '#### APENAS PARA MAIORES DE 18 ANOS! #### ESTE SITE POSSUI CONTEÚDO ERÓTICO E NÃO É PERMITIDO POR LEI (ARTIGO 5 DO CÓDIGO CIVIL) PARA PESSOAS COM MENOS DE 18 ANOS! CASO VOCÊ AINDA NÃO TENHA COMPLETADO 18 ANOS, NÃO PROSSIGA!';
$lang['langselected'] = 'br';
$lang['tipPleaseEnterGoalText'] = 'Para continuar a meta de bate-papo por favor ponta entre';
$lang['404 page not found']	= 'A página solicitada não foi encontrada';
$lang['the page you requested was not found.'] = 'Clique em voltar verifique o link e tente novamente';
$lang['su']			= 'D';
$lang['mo']			= 'S';
$lang['tu']			= 'T';
$lang['we']			= 'Q';
$lang['th']			= 'Q';
$lang['fr']			= 'S';
$lang['sa']			= 'S';
$lang['sun']		= 'Dom';
$lang['mon']		= 'Seg';
$lang['tue']		= 'Ter';
$lang['wed']		= 'Qua';
$lang['thu']		= 'Qui';
$lang['fri']		= 'Sex';
$lang['sat']		= 'Sab';
$lang['sunday']		= 'Domingo';
$lang['monday']		= 'Segunda';
$lang['tuesday']	= 'Terça';
$lang['wednesday']	= 'Quarta';
$lang['thursday']	= 'Quinta';
$lang['friday']		= 'Sexta';
$lang['saturday']	= 'Sábado';
$lang['jan']		= 'Jan';
$lang['feb']		= 'Fev';
$lang['mar']		= 'Mar';
$lang['apr']		= 'Abr';
$lang['may']		= 'Mai';
$lang['jun']		= 'Jun';
$lang['jul']		= 'Jul';
$lang['aug']		= 'Ago';
$lang['sep']		= 'Set';
$lang['oct']		= 'Out';
$lang['nov']		= 'Nov';
$lang['dec']		= 'Dez';
$lang['january']	= 'Janeiro';
$lang['february']	= 'Fevereiro';
$lang['march']		= 'Março';
$lang['april']		= 'Abril';
$lang['mayl']		= 'Maio';
$lang['june']		= 'Junho';
$lang['july']		= 'Julho';
$lang['august']		= 'Agosto';
$lang['september']	= 'Setembro';
$lang['october']	= 'Outubro';
$lang['november']	= 'Novembro';
$lang['december']	= 'Dezembro';
$lang['year'] = 'Ano';
$lang['years'] = 'Anos';
$lang['month'] = 'Mês';
$lang['months'] = 'Meses';
$lang['week'] = 'Semana';
$lang['weeks'] = 'Semanas';
$lang['day'] = 'Dia';

$lang['hour'] = 'Hora';
$lang['hours'] = 'Horas';
$lang['minute'] = 'Minuto';
$lang['minutes'] = 'Minutos';
$lang['second'] = 'Segundo';
$lang['seconds'] = 'Segundos';

$lang['select credit pack!'] = 'Selecione o Pacote de Créditos Desejado!';
$lang['you must be logged in to access this page!'] = 'Você deve estar logado para acessar esta página!';
$lang['how it works'] = 'Como Funciona.';
$lang['welcome to our affiliate program!'] = 'Bem vindo ao programa Afiliados!';
$lang['buy'] = 'COMPRAR';	
$lang['print your name']='Escreva seu nome';
$lang['review your signature']='Veja sua assinatura';
$lang['or draw your signature']='Ou desenhe sua assinatura';
$lang['type it']='Escrito';
$lang['draw it']='Desenhar';
$lang['Clear']='Limpar';
$lang['admins']='Administradores';
$lang['username']='Usuário';
$lang['password']='Senha';
$lang['edit admin account']='Editar conta admin';
$lang['add admin account']='Adicionar conta admin ';
$lang['admin account was saved successfully!']='Conta de administrador foi salva com sucesso!';

$lang['admin account was not saved! please try again!']='Conta de administrador não foi salva! Por favor, tente outra vez! ';
$lang['invalid id!']='Id inválida!';
$lang['this admin does not exist!']='Este administrador não existe!';
$lang['admin account was successfully deleted!']='Conta de administrador excluída com sucesso! ';
$lang['admin account cannt be deleted! please try again!']='Conta de administrador não pode ser apagado! Por favor, tente outra vez! ';
$lang['name']='Nome';
$lang['minimum amount']='Valor mínimo';
$lang['status']='Status';
$lang['field']='Campo';
$lang['select currency']='Selecione a Moeda';
$lang['select status']='Selecione o Status';
$lang['approved']='Aprovado';
$lang['rejected']='Rejeitado';
$lang['add payment method']='Método de Pagamento ';
$lang['you must insert at least 1 field!']='Tem de inserir pelo menos 1 campo!';
$lang['payment method was saved successfully!']='Método de pagamento salvo com sucesso!';
$lang['payment method was not saved! please try again!']='O Método de pagamento não foi salvo! Por favor, tente outra vez! ';
$lang['your payment methods']='Formas de pagamento';
$lang['invalid payment!']='Pagamento inválido!';
$lang['edit payment method']='Editar método de pagamento';
$lang['you must have at least 1 field!']='Você deve ter pelo menos um campo! ';
$lang['payment method edited successfully!']='Método de pagamento editado com sucesso!';
$lang['an error has occured! please try again!']='Ocorreu um erro! Por favor, tente outra vez! ';
$lang['payment method deleted successfully!']='Método de pagamento excluído com sucesso! ';
$lang['select actor']='Selecionar Ator';
$lang['select action on']='Selecionar Ação sobre';
$lang['select action']='Selecionar Ação';
$lang['system logs']='Sistema de Logs';
$lang['categories']='Categorias';
$lang['no parent']='Sem parente ';
$lang['edit category']='Editar categoria';
$lang['add category']='Adicionar categoria';
$lang['category was saved successfully!']='Categoria salva com sucesso!';
$lang['category was not saved! please try again!']='A categoria não foi salva! Por favor, tente outra vez! ';
$lang['you cannt delete this category because there are assigned performers to it.']='Você não pode apagar esta categoria porque não são atribuídos a nenhuma modelo. ';
$lang['this category does not exist!']='Esta categoria não existe! ';
$lang['delete the subcategories underneath this category first!']='Apagar as subcategorias sob esta categoria em primeiro lugar! ';
$lang['category was successfully deleted!']='Categoria foi excluído com sucesso! ';
$lang['category cannot be deleted! please try again!']='Categoria não pode ser apagado! Por favor, tente outra vez! ';
$lang['statistics']='Estatísticas';
$lang['supported languages']='Os idiomas suportados';
$lang['code']='Código';
$lang['title']='Título';
$lang['languge added!']='Linguagem adicionada!';
$lang['invalid language id!']='Id língua inválida!';
$lang['language updated successfully!']='Língua atualizado com sucesso! ';
$lang['invalid languages id!']='Línguas ID inválida!';
$lang['language does not exist!']='Língua não existe! ';
$lang['language deleted!']='Língua apagada!';
$lang['analytics code']='Código Analytics';
$lang['the %s is not writable. please set chmod 777 for this file!']='O %s não é gravável. Por favor, defina CHMOD 777 para este arquivo! ';
$lang['the code was succesfully saved!']='O código foi salvo com sucesso !';
$lang['whitelisted ips']='IPS lista branca';
$lang['blacklisted ips']='IPS Na lista negra';
$lang['blacklisted countries']='Países na lista negra';
$lang['blacklist']='Lista negra';
$lang['details saved!']='Detalhes salvo!';
$lang['this username is invalid!']='Este nome de usuário é inválido! ';
$lang['this performer does not exist!']='Modelo inexistente! ';
$lang['this is not a valid image!']='Isso não é uma imagem válida!';
$lang['image uploaded succesfully!']='Imagem enviada com sucesso!';
$lang['photo id is missing!']='Foto ID está faltando! ';
$lang['invalid photo!']='Foto inválida!';
$lang['an error occured!']='Um erro ocorreu! ';
$lang['photo has been succesfully deleted!']='A foto foi excluído com sucesso!';
$lang['banned users']='Usuários banidos';
$lang['invalid session id!']='ID sessão inválida!';
$lang['database error!']='Erro de banco de dados! ';
$lang['ban has been removed!']='Ban foi removido!';
$lang['reviews']='Avaliações';
$lang['invalid review id!']='Id avaliação inválida!';
$lang['review doesnt exist!']='Revisão não esta disponível!';
$lang['performer doesnt exist!']='Modelo inexistente!';
$lang['edit']='Editar';
$lang['review was saved!']='Avaliação foi salvo!';
$lang['review was not saved! please try again!']='Revisão não foi salva! Por favor, tente outra vez! ';
$lang['review was deleted!']='Review foi eliminado!';
$lang['review was not deleted! please try again!']='Revisão não foi excluída! Por favor, tente outra vez! ';
$lang['country']='País';
$lang['add supported language']='Adicionar idioma suportado';
$lang['this language was successfully deleted!']='Esta linguagem foi excluído com sucesso!';
$lang['this language cannt be added! please try again!']='Essa linguagem pode \' t ser adicionado! Por favor, tente outra vez! ';
$lang['this language does not exist!']='Esta linguagem não existe!';
$lang['this language cannt be deleted! please try again!']='Essa linguagem pode \' t ser apagado! Por favor, tente outra vez! ';
$lang['users']='utilizadores';
$lang['this user does not exist!']='Este usuário não existe!';
$lang['email']='E-mail';
$lang['account']='Conta';
$lang['user account was saved successfully!']='Conta do usuário foi salvo com sucesso!';
$lang['user account was not saved! please try again!']='Conta de usuário não foi salva! Por favor, tente outra vez! ';
$lang['sessions']='sessões';
$lang['user sessions']='sessões de usuários';
$lang['payments']='Pagamentos';
$lang['user payments']='pagamentos do usuário ';
$lang['user account was successfully deleted!']='Conta de usuário foi excluído com sucesso! ';
$lang['user account cannt be deleted! please try again!']='Conta de usuário não pode ser apagado! Por favor, tente outra vez! ';
$lang['amount']='Valor';
$lang['credits updated succesfully!']='Créditos atualizado com sucesso! ';
$lang['affiliates']='Afiliados';
$lang['this affiliate does not exist!']='Este Afiliado não existe! ';
$lang['pending']='Pendentes';
$lang['site name']='Nome do site';
$lang['site url']='URL do Site';
$lang['first name']='Primeiro nome';
$lang['last name']='Sobrenome';
$lang['phone']='Telefone';
$lang['affiliate account was saved successfully!']='Conta Afiliado salvo com sucesso!';
$lang['affiliate account was not saved! please try again!']='Conta de afiliado não foi salva! Por favor, tente outra vez! ';
$lang['affiliate account was successfully deleted!']='Conta de Afiliado foi excluído com sucesso! ';
$lang['affiliate account cannt be deleted! please try again!']='Conta de afiliados não pode ser apagado! Por favor, tente outra vez! ';
$lang['signups']='Inscrições';
$lang['affiliate signups']='inscrições afiliados';
$lang['direct link']='Ligação direta';
$lang['traffic']='tráfego';
$lang['affiliate traffic']='Tráfego de afiliados ';
$lang['affiliate payments']='pagamentos de afiliados ';
$lang['ads']='Anúncios';
$lang['affiliate ads']='anúncios de afiliados ';
$lang['emails templates']='E-mails Templates';
$lang['the template was succesfully saved!']='O modelo foi sucesso salvo! ';
$lang['fms']='FMS';
$lang['max hoster performers']='Hospedagem max para modelos';
$lang['fms for video']='FMS para Vídeo';
$lang['fms for preview']='FMS para visualização';
$lang['fms for delete']='FMS para Excluir';
$lang['fms for test']='FMS para test';
$lang['active']='Ativo';
$lang['inactive']='Inativo';
$lang['fms added!']='FMS adicionado!';
$lang['invalid fms id!']='Id FMS inválida!';
$lang['fms updated successfully!']='FMS atualizado com sucesso! ';
$lang['fms does not exist!']='FMS não existe! ';
$lang['fms deleted!']='FMS apagado!';
$lang['to pay performers']='Para pagar modelos';
$lang['to pay studios']='Para pagar estúdios';
$lang['to pay affiliates']='Para pagar Afiliados ';
$lang['payment summary']='Resumo de Pagamento ';
$lang['all']='Todos';
$lang['performers']='Modelos';
$lang['studios']='Estúdios';
$lang['email subject']='Assunto do E-mail';
$lang['email body']='Corpo do e-mail ';
$lang['newsletter']='Informativos';
$lang['invalid acount type!']='Tipo de conta inválida!';
$lang['invalid acount status!']='Status da conta inválida!';
$lang['newsletter was saved and will be sent to %s accounts!']='Newsletter foi gravada e será enviado para contas %s!';
$lang['free']='Gratis';
$lang['nude']='Simples';
$lang['private']='Privado';
$lang['nickname']='Apelidio';
$lang['firstname']='Primeiro nome';
$lang['lastname']='Sobrenome';
$lang['address']='Endereço';
$lang['city']='Cidade';
$lang['zip']='Cep';
$lang['state']='Estado';
$lang['true private chips price']='Preço Créditos Exclusivo';
$lang['private chips price']='Preço Créditos Privado ';
$lang['nude chips price']='Preço Créditos Simples ';
$lang['peek chips price']='Preço Créditos Espião ';
$lang['max nude watchers']='Max observadores Simples';
$lang['performer account was saved successfully!']='Conta modelo foi salvo com sucesso!';
$lang['performer account was not saved! please try again!']='Conta modelo não foi salva! Por favor, tente outra vez! ';
$lang['year']='Ano';
$lang['month']='Mês';
$lang['day']='Dia';
$lang['select gender']='Selecione o sexo';
$lang['select ethnicity']='Sua etnia';
$lang['select sexual prefference']='Selecione a preferência sexual';
$lang['select height']='Sua Altura';
$lang['select weight']='Seu peso ';
$lang['select hair color']='Selecione a cor do cabelo';
$lang['select hair length']='Selecione o Tamanho do cabelo';
$lang['select eye']='Seus olhos';
$lang['select build']='Sua Constituição';
$lang['select cup size']='Modelo de Corpo';
$lang['description']='Descrição';
$lang['turn on']='Ligar';
$lang['turn off']='Desligar';
$lang['gender']='Sexo';
$lang['ethnicity']='Origem';
$lang['sexual preference']='Preferência sexual';
$lang['height']='Altura';
$lang['weight']='Peso';
$lang['hair color']='A cor do cabelo ';
$lang['hair length']='Tamanho do cabelo';
$lang['eye color']='A cor dos olhos ';
$lang['build']='Físico';
$lang['cup_size']='Medidas de Bojo';
$lang['profile'] = 'Perfil';
$lang['performer update was not saved! please try again!']='Atualização de modelo não foi salva! Por favor, tente outra vez! ';
$lang['performer account was successfully updated!']='Conta de modelo atualizado com sucesso! ';
$lang['category']='Categoria';
$lang['performer categories saved.']='Categorias de modelo salvas.';
$lang['performer account was successfully suspended!']='Conta de modelo foi suspensa com sucesso!';
$lang['please try again!']='Por favor, tente outra vez! ';
$lang['performer id']='Id Modelo';
$lang['contract status']='Estado de contrato ';
$lang['photo id status']='Foto ID Status';
$lang['your contract was %s']='Seu contrato era %s ';
$lang['your photo id was %s']='Sua identificação com foto era %s';
$lang['photos']='Fotos';
$lang['this is not a valid image.']='Isso não é uma imagem válida. ';
$lang['photo has been succesfully deleted.']='Foto foi excluído com sucesso! ';
$lang['changes saved!']='Changes salvo!';
$lang['videos']='Vídeos';
$lang['performer videos']='Vídeos da Modelo';
$lang['invalid video id!']='Id vídeo inválido!';
$lang['this video does not exist!']='Este vídeo não existe!';
$lang['paid']='Pago';
$lang['data was saved successffuly!']='Os dados foram salvos successffuly!';
$lang['performer sessions']='Sessões da Modelo ';
$lang['performer payments']='Pagamentos da Modelo ';
$lang['invalid performer!']='Modelo inválida!';
$lang['performer is currently offline!']='Modelo está atualmente offline!';
$lang['this studio does not exist!']='Este estúdio não existe! ';
$lang['percentage']='Percentual';
$lang['studio account was saved successfully!']='Conta Studio foi salvo com sucesso!';
$lang['studio account was not saved! please try again!']='Conta Studio não foi salva! Por favor, tente outra vez! ';
$lang['studio account was successfully deleted!']='Conta Studio foi excluído com sucesso! ';
$lang['studio account cannot be deleted! please try again!']='Conta Studio não pode ser apagado! Por favor, tente outra vez! ';
$lang['studio']='Estúdio';
$lang['studio payments']='pagamentos Studio ';
$lang['studio id']='Id Studio';
$lang['file %s not exist! please create it and set chmod 777 rights!']='Arquivo %s doesn \'t existe! Por favor, crie-lo e definir CHMOD 777 direitos! ';
$lang['file %s is not writable! please set chmod 777 rights!']='Arquivo %s não é gravável! Por favor, defina CHMOD 777 direitos! ';
$lang['action restricted by demo mode!']='Ação restrito por modo demo!';
$lang['settings updated!']='Settings atualizado!';
$lang['failed to save settings!']='Falha ao salvar as configurações!';
$lang['settings']='Painel';
$lang['music']='Music';
$lang['music manager']='Manager Music';
$lang['music manager - add']='Manager Música - Adicionar';
$lang['song added!']='Song adicionado!';
$lang['music manager - edit']='Manager Música - Edit';
$lang['song edited!']='Song editado!';
$lang['song deleted!']='Song apagado!';
$lang['is paid']='É pago';
$lang['price']='Preço';
$lang['save description']='Salvar description';
$lang['id']='ID';
$lang['actions']='Ações';
$lang['there are no entries']='Não existem entradas';
$lang['are you sure you want to delete this admin account?']='Você tem certeza de que deseja excluir este conta de administrador';
$lang['add new admin account']='Adicionar nova conta admin ';
$lang['no page set.']='No conjunto de páginas.';
$lang['contracts']='contratos';
$lang['date']='Data';
$lang['status is %s']='Status é %s';
$lang['download contract']='Baixar Contrato';
$lang['reject contract']='Rejeitar Contrato ';
$lang['approve contract']='Aprovar contrato ';
$lang['studio has no uploaded contracts']='Studio não tem contratos enviados.';
$lang['start date']='Data Inicial';
$lang['end date']='Data Final';
$lang['period']='Período';
$lang['chat type']='Tipo Bate-papo ';
$lang['sessions not found.']='As sessões não encontrado.';
$lang['type']='Tipo';
$lang['duration']='Duração';
$lang['ip']='IP';
$lang['fee per minute']='Taxa por minuto';
$lang['user paid chips']='User pago Créditos';
$lang['site chips']='Site Créditos';
$lang['studio chips']='Studio Créditos';
$lang['performer chips']='Créditos da Modelo';
$lang['user id']='ID do usuário';
$lang['performer video id']='Id vídeo da Modelo';
$lang['account information']='Informações de conta';
$lang['affiliate status']='Estado de afiliados ';
$lang['register ip']='Ip registrado';
$lang['register date']='Data de registro';
$lang['payment information']='Informações de pagamento';
$lang['payment method not set']='Método de pagamento não definido. ';
$lang['update account']='Atualizar Conta';
$lang['no payments found.']='Nenhum pagamento encontrado.';
$lang['paid date']='Data Pago';
$lang['amount chips']='Valor Créditos';
$lang['payment info']='Info Pagamento';
$lang['comments']='Comentários';
$lang['%s chips']=' %s Créditos';
$lang['payments info for <strong>%s</strong> account.']='Info Pagamentos para <strong> %s </ strong> conta.';
$lang['length']='Duração';
$lang['price/minute']='Preço /minuto';
$lang['total paid']='Total pago';
$lang['site earning']='Ganhando Site';
$lang['studio earning']='Ganhando Studio';
$lang['performer earning']='Ganhando como Modelo';
$lang['video id']='Id Vídeo';
$lang['there are no payments to pay!']='Não há pagamentos para pagar! ';
$lang['interval']='Período';
$lang['details']='Detalhes';
$lang['payment date']='Data de Pagamento';
$lang['views']='Visualizações';
$lang['hits']='cliques';
$lang['registers']='Registros';
$lang['transactions']='operações';
$lang['earnings']='resultados';
$lang['no traffic found.']='Nenhum tráfego encontrado.';
$lang['save']='SALVAR';
$lang['link']='Link';
$lang['parent id']='ID Parent';
$lang['are you sure you want to delete this category?']='Você tem certeza que deseja apagar esta categoria';
$lang['add new category']='Adicionar nova categoria. ';
$lang['not available']='Não Disponível';
$lang['earnings statistics']='resultados estatísticas ';
$lang['watchers statistics']='Vigilantes estatísticas';
$lang['signups statistics']='estatísticas inscrições ';
$lang['affiliates ads traffic statistics']='Afiliados anúncios estatísticas de tráfego';
$lang['payments statistics']='pagamentos estatísticas ';
$lang['transactions statistics']='operações estatísticas ';
$lang['statisics']='Statisics';
$lang['to pay']='Pagar';
$lang['bans']='proibições';
$lang['languages']='línguas';
$lang['templates']='Modelos';
$lang['edit account']='Editar Conta';
$lang['view sessions']='Ver sessões';
$lang['view payments']='Ver pagamentos';
$lang['change password']='Alterar a senha';
$lang['log out']='Sair';
$lang['toolbox']='Caixa de ferramentas';
$lang['affiliate login']='Área de Afiliados';
$lang['totals']='Totais';
$lang['viewers']='Vistos';
$lang['affiliates ads traffic']='Tráfego ads dos afiliados';
$lang['view ads']='Ver anúncios';
$lang['signups users']='dos usuários inscrições ';
$lang['email templates']='Modelos de e-mail ';
$lang['analytics']='Analytics';
$lang['checkpoints']='Pontos de verificação';
$lang['view performers']='Ver Modelos';
$lang['edit profile']='Editar perfil';
$lang['edit photos']='Editar fotos';
$lang['edit videos']='Editar Videos ';
$lang['chat logs']='logs de bate-papo ';
$lang['administration panel']='Painel de Administração';
$lang['welcome administrator']='Administrador Welcome';
$lang['my account']='Minha Conta';
$lang['new messages']='Novas mensagens';
$lang['server time']='Hora de servidor';
$lang['away']='Ausente';
$lang['live broadcasting..']='Transmitir ao vivo .. ';
$lang['user username']='Usar nome do usuário';
$lang['performer nickname']='Apelido para Modelo';
$lang['ban expire date']='Proibição data expirar ';
$lang['user ip']='Ip User';
$lang['user']='Usuário';
$lang['ban date']='Data Ban';
$lang['expire ban']='Expire proibição';
$lang['ban time remaining']='Tempo Ban restantes ';
$lang['session duration']='A duração da sessão ';
$lang['user paid']='Usuário paga';
$lang['session type']='O tipo de sessão ';
$lang['unban']='Unban';
$lang['are you sure you want to unban this user?']='Você tem certeza que deseja remover a expulsão deste usuário?';
$lang['actor']='Ator';
$lang['actor id']='ID de Ator';
$lang['action on']='Ação sobre ';
$lang['action on id']='Ação para o ID ';
$lang['action']='Ação';
$lang['action comment']='Commentar Ação';
$lang['are you sure you want to delete this language?']='Você tem certeza de que deseja excluir este idioma?';
$lang['add new supported language']='Adicionar novo idioma suportado ';
$lang['e-mail']='E-mail';
$lang['gateway']='Gateway';
$lang['affiliate id']='ID do Afiliado';
$lang['credits']='Créditos';
$lang['are you sure you want to delete this user?']='Você tem certeza de que deseja excluir este usuário?';
$lang['performer photos']='Fotos da Modelo';
$lang['are you sure you want to delete this photo?']='Você tem certeza que deseja deletar essa foto? ';
$lang['this performer has no uploaded photos.']='Este Modelo não carregou fotos.';
$lang['dashboard']='Painel';
$lang['no users found.']='Nenhum usuário encontrado.';
$lang['this field is required!']='Este campo é obrigatório!';
$lang['review']='Rever';
$lang['no reviews.']='Nenhum comentário.';
$lang['message']='Mensagem';
$lang['rating']='Rating';
$lang['are you sure you want to delete this review?']='Você tem certeza que quer apagar este comentário';
$lang['edit language']='Editar Linguagem ';
$lang['add language']='Adicionar idioma';
$lang['add']='Adicionar';
$lang['are you sure you want to delete the language?']='Você tem certeza que deseja excluir a língua? ';
$lang['there are no languages']='Não há línguas. ';
$lang['add new language']='Adicionar novo idioma';
$lang['this performer has no videos.']='Modelo sem vídeos.';
$lang['payment']='Pagamento';
$lang['income summary']='Resumo de Renda';
$lang['performer amount']='Quantia da Modelo ';
$lang['studio amount']='Quantia do Studio';
$lang['performer earnings']='Ganhos da Modelo';
$lang['studio earnings']='Ganhos do Studio';
$lang['add date']='Adicionar data';
$lang['log']='Log';
$lang['chat log not found.']='Chat Log não encontrado.';
$lang['are you sure you want to delete this affiliate?']='Você tem certeza de que deseja excluir este filial?';
$lang['added field']='O campo Adicionado ';
$lang['remove']='Remover';
$lang['add payment methods']='Adicionar Formas de pagamento';
$lang['add field']='Adicionar campo ';
$lang['extra field']='Extra campo';
$lang['are you sure you want to delete this payment method?']='Você tem certeza de que deseja excluir este método de pagamento';
$lang['delete payment method']='Excluir método de pagamento ';
$lang['you have no payment methods set']='Você não tem métodos de pagamento definido';
$lang['view payment methods']='Formas de pagamento';
$lang['edit fms %s']='Edit FMS %s';
$lang['max hosted performers']='Hospedagem maxima de modelos. ';
$lang['fms test']='Testar FMS';
$lang['add fms']='Adicionar FMS';
$lang['fmss']='FMSS';
$lang['current hosted performers']='Modelos Atualmente hospedadas';
$lang['are you sure you want to delete this fms? by deleting the fms you will delete all videos from it']='Você tem certeza de que deseja excluir este FMS? Ao excluir os FMS você irá eliminar todos os vídeos hospedados lá. ';
$lang['there are no fmss!']='Não há FMSS!';
$lang['add new fms']='Adicionar novos FMS';
$lang['templates not found.']='Modelo não encontrado. ';
$lang['profile information']='As informações de perfil ';
$lang['update categories']='categorias update ';
$lang['photo ids']='Foto IDs';
$lang['performer']='Modelo';
$lang['download photo id']='Baixar Foto ID';
$lang['reject photo id']='Rejeitar Foto ID ';
$lang['approve photo id']='Aprovar Foto ID ';
$lang['performer has no uploaded photo ids']='Modelo não tem IDs de fotos enviados.';
$lang['performer has no uploaded contracts']='Modelo não tem contratos enviados.';
$lang['studio status']='Estatuto Studio ';
$lang['see all contracts']='Ver todos os contratos';
$lang['update studio']='Studio Update';
$lang['add credits']='Adicionar Créditos';
$lang['amount to add/remove']='Valor a Adicionar / Remover ';
$lang['apply changes']='Aplicar alterações';
$lang['there are no performers.']='Não há modelos. ';
$lang['country code']='Country Code';
$lang['parent']='Pai';
$lang['update']='Update';
$lang['admin account']='Conta Admin';
$lang['edit song']='Edit música';
$lang['upload']='Upload';
$lang['add song']='Adicionar música ';
$lang['song']='Song';
$lang['songs']='Songs';
$lang['src']='Src';
$lang['are you sure you want to delete this song?']='Você tem certeza que quer apagar essa música?';
$lang['add new song']='Adicionar nova música';
$lang['user status']='Status de usuário';
$lang['update user']='User Update';
$lang['amount paid']='Valor pago';
$lang['amount received']='Creditos recebidos';
$lang['invoice id']='Id Invoice';
$lang['refunded']='Reembolsado';
$lang['account type']='Tipo de conta';
$lang['account status']='Status da conta';
$lang['send']='Enviar';
$lang['contract']='Contrato';
$lang['photo id']='Foto da Identidade';
$lang['is online']='Está Online';
$lang['spy']='Espiar';
$lang['online hd']='Online HD';
$lang['online type']='Tornar Online';
$lang['in private']='Em privado ';
$lang['peek']='Espiar';
$lang['are you sure you want to delete this performer?']='Você tem certeza de que deseja excluir esta modelo';
$lang['save title']='Salvar título';
$lang['sexual prefference']='Preferência sexual';
$lang['cup size']='Tamanho Cup';
$lang['birthday']='Data de Nascimento';
$lang['what turns me on']='O que me excita ';
$lang['what turns me off']='O que não me excita ';
$lang['update profile']='Atualizar Perfil';
$lang['are you sure you want to delete this studio?']='Você tem certeza de que deseja excluir este Studio?';
$lang['administration']='Administração';
$lang['m<span>odena</span>c<span>am</span> a<span>dministration</span>']='Akinews <span>administration</ span>';
$lang['login']='Iniciar Sessão';
$lang['memcache is not installed!']='Memcache não está instalado!';
$lang['yes']='SIM';
$lang['no']='NÃO';
$lang['can\'t connect to memcache server!']='Impossível conectar ao servidor Memcache! ';
$lang['successfully connected to memcache server!']='Conectado com êxito ao servidor Memcache! ';
$lang['update settings']='Atualizar configurações';
$lang['ads not found.']='Os anúncios não encontrado.';
$lang['created date']='Data Criado';
$lang['performer overall status']='Status geral da Modelo';
$lang[' photo id status']='Estatuto Foto ID ';
$lang['see all photo id pictures']='Veja todas as fotos IDs';
$lang['is online hd']='Está Online HD';
$lang['maximum nude watchers']='observadores Máximo Simples';
$lang['is imported category id']='É importado id categoria';
$lang['paid photo gallery price']='Galeria de Fotos Paga';
$lang['website percentage']='Percentagem site ';
$lang['update performer']='Atualizar Modelo';
$lang['edit %s email templates, file: %s ']='Editar %s modelos de email, file: %s ';
$lang['from %s to %s']='A partir de %s para %s ';
$lang['no payment methods available']='Não há formas de pagamento disponíveis.';
$lang['choose payment method']='Como deseja receber?';
$lang['demo mode!']='O modo de demonstração!';
$lang['chips']='Créditos';
$lang['invalid/expired token!']='Token inválido / expirado! ';
$lang['invalid username!']='Username inválido!';
$lang['repeat password']='Repita a senha';
$lang['reset password']='Esqueci minha senha';
$lang['password saved, you can login.']='Password salvo, você pode fazer o login.';
$lang['forgot password']='Esqueceu a senha';
$lang['please check your mail to reset your password!']='Por favor, verifique seu e-mail para redefinir sua senha!';
$lang['my photos']='Fotos';
$lang['photo']='Foto';
$lang['gallery']='Galeria';
$lang['add photo']='Adicionar foto';
$lang['free gallery']='Galeria Livre';
$lang['paid gallery']='Galeria Pago';
$lang['photo added!']='Foto adicionada!';
$lang['invalid photo id!']='ID foto inválida!';
$lang['you are trying to edit a photo that does not belong to you.']='Você não pode editar uma foto que não pertence a você.';
$lang['edit photo']='Editar foto';
$lang['you cannot change the type of your main photo!']='Você não pode mudar o tipo da sua foto principal!';
$lang['an error occured, the photo has been deleted!']='Ocorreu um erro, a foto foi deletada!';
$lang['photo edited successfully!']='Foto editada com sucesso!';
$lang['you are trying to delete a photo that does not belong to you.']='Você não pode excluir uma foto que não pertence a você!';
$lang['you cannot delete the avatar.']='Você não pode excluir o Avatar (Foto de Perfil).';
$lang['an error occured']='Ocorreu um erro desconhecido.';
$lang['selected photo is not yours!']='Foto selecionada não é sua! ';
$lang['you cannot set a paid photo as avatar!']='Você não pode definir uma foto como avatar pago! ';
$lang['tos']='tos';
$lang['photo_id']='Id foto';
$lang['choose your country']='Escolha o seu país';
$lang['performer register step 1']='Registo passo performer 1 ';
$lang['confirmation email.']='Email de confirmação';
$lang['welcome email.']='Email de boas-vindas ';
$lang['a confirmation email has been sent!']='Um email de confirmação foi enviado!';
$lang['payment_method']='Receber dinheiro em';
$lang['release amount']='Valor para Saque';
$lang['performer register step 2']='Registo passo performer 2 ';
$lang['performer register step 3']='Registo passo performer 3 ';
$lang['performer register step 4']='Registo passo performer 4 ';
$lang['database error. please try again later!']='Erro de banco de dados. Por favor, tente novamente mais tarde! ';
$lang['what_turns_me_on']='What_turns_me_on';
$lang['what_turns_me_off']='What_turns_me_off';
$lang['sexual_prefference']='Preferência sexual';
$lang['hair_color']='A cor do cabelo ';
$lang['hair_length']='Comprimento do cabelo';
$lang['eye_color']='A cor dos olhos ';
$lang['avatar']='Avatar';
$lang['lang']='Lang';
$lang['performer register step 5']='registo passo performer 5 ';
$lang['select hair lenght']='Comprimento do cabelo Select ';
$lang['select eye color']='Cor dos olhos Select';
$lang['account created successfully']='Conta criada com sucesso!';
$lang['my photo id\'s']='Minha foto id';
$lang['you already have an approved photo id!']='Você já tem uma foto de id aprovada!';
$lang['you exceeded the maximum allowed photo id number!']='Você excedeu o número máximo permitido de identificação com fotografia!';
$lang['add photo id']='Adicionar Foto ID ';
$lang['photo id added!']='Foto ID adicionado!';
$lang['cannot delete a photo id that is not yours!']='Não é possível excluir uma foto de identificação que não é seu! ';
$lang['cannot delete an approved photo id!']='Não é possível excluir uma foto de identificação aprovado!';
$lang['could not delete the photo id!']='Não foi possível excluir o documento com foto!';
$lang['photo id removed!']='Foto ID removido!';
$lang['invalid fms. please contact admin!']='FMS inválidos. Entre em contato com administrador! ';
$lang['broadcast']='Broadcast';
$lang['you are already online!']='Você já está online! ';
$lang['paymentdates']='paymentDates';
$lang['rules']='Regulamento';
$lang['subject']='Assunto';
$lang['contact']='Contato';
$lang['message sent!']='Mensagem enviada!';
$lang['error sending message!']='Erro ao enviar mensagem!';
$lang['faq']='PERGUNTAS FREQUENTES';
$lang['invalid/expired activation code!']='Código inválido / expirado ativação! ';
$lang['account is not in pending. you cannot reactivate!']='Conta não está pendente. Você não pode reativar! ';
$lang['invalid activation code!']='Código de ativação inválida! ';
$lang['e-mail address successfully verified.']='Endereço de E-mail verificado com sucesso. ';
$lang['my contracts']='Meus contratos ';
$lang['you already have an approved contract!']='Você já tem um contrato aprovado! ';
$lang['you exceeded the maximum allowed contract number!']='Você excedeu o número máximo permitido contrato! ';
$lang['add contract']='Adicionar contrato';
$lang['contract added!']='Contrato adicionado!';
$lang['invalid contract!']='Contrato inválido! ';
$lang['cannot delete a contrat that is not yours!']='Não é possível excluir um contrat que não é seu! ';
$lang['cannot delete an approved contrat!']='Não é possível excluir um contrat aprovado!';
$lang['could not delete the contract!']='Não foi possível excluir o contrato!';
$lang['contract removed!']='Contrato retirada!';
$lang['messenger']='Mensageiro';
$lang['no messages in this folder!']='Nenhuma mensagem nesta pasta!';
$lang['folder']='Pasta';
$lang['message id']='Id mensagem';
$lang['error please try again!']='Erro! Por favor, tente outra vez! ';
$lang['message_id']='Id mensagem';
$lang['message was moved to trash!']='A mensagem foi movida para o lixo! ';
$lang['message was deleted!']='A mensagem foi apagado!';
$lang['performer login']='Login Modelos';
$lang['my settings']='Minhas configurações';
$lang['personal details saved!']='Dados pessoais salvo! ';
$lang['start chat text']='Start chat de texto';
$lang['profile settings']='Definições de perfil ';
$lang['profile details saved!']='Detalhes do perfil salvo!';
$lang['payment settings']='Configurações de pagamento ';
$lang['payment method edited succesfully.']='Método de pagamento editado com sucesso. ';
$lang['pricing settings']='Definições de Preços ';
$lang['pricings saved!']='Pricings salvo!';
$lang['old password']='Senha Antiga';
$lang['new password']='Nova Senha';
$lang['confirm password']='Confirme Sua Senha';
$lang['password changed successfully!']='Senha alterada com sucesso! ';
$lang['countries']='Países';
$lang['states']='Estados';
$lang['banned zones']='Zonas Banidas';
$lang['changes done successfully!']='Changes feito com sucesso!';

$lang['categories settings']='categorias configurações';
$lang['categories saved!']='Categorias salvo! ';
$lang['schedule settings']='Configurações de agendamento ';
$lang['page is empty!']='Página está vazia! ';
$lang['my videos']='Meus Vídeos';
$lang['the video you are trying to edit is not yours!']='O vídeo que você está tentando editar não é sua!';
$lang['edit video']='Editar Vídeo ';
$lang['database error. please retry!']='Erro de banco de dados. Por favor, tente novamente! ';
$lang['invalid fms!']='FMS inválida!';
$lang['invalid video id']='ID do vídeo inválido.';
$lang['the video you are trying to edit is not yours']='O vídeo que você está tentando editar não é sua!';
$lang['terms and conditions']='Termos e Condições';
$lang['please select a category!']='Por favor, selecione uma categoria! ';
$lang['please enter a valid price']='Por favor insira um preço válido';
$lang['signup step 4 - pricings ']='Cadastre-se o passo 4 - Preço';
$lang['true private chat %s/minute']='Chat Eclusivo %s/minuto';
$lang['this is the price per minute that a user will have to pay for a true private chat session. the difference between a true private chat and a private chat is that a true private chat session does not allow peeking users.']='Este é o preço por minuto que um usuário terá de pagar para uma verdadeira sessão de bate-papo privado. A diferença entre um verdadeiro bate-papo privado e um bate-papo privado é que uma verdadeira sessão de bate-papo privado não permite espreitar os usuários. ';
$lang['must be between %s as %s']='Deve ser entre %s e %s';
$lang['private chat %s/minute']='Chat Privado %s/Minuto';
$lang['this is the price per minute that a user will have to pay for a private chat sessions. the difference between a private chat and a true private chat is that a private chat session allows other users to \'peek\'.']='Este é o preço por minuto que um usuário vai ter que pagar por sessões de chat privados. A diferença entre uma conversa privado e um verdadeiro bate-papo privado é que uma sessão de bate-papo privado permite que outros usuários\'espiem\'.';
$lang['peek chat %s/minute']='Chat modo Espião %s/Minuto';
$lang['this is the price per minute that a user will have to pay in in order to be able to peek at a private chat session. peeking is available for private chat sessions, but not true private ones.']='Este é o preço por minuto que um usuário terá de pagar em, a fim de ser capaz de espreitar uma sessão de bate-papo privado. Espreitar está disponível para sessões de chat privados, mas não verdadeiros os privados. ';
$lang['nude chat %s/minute']='Chat Simples %s/Minuto';
$lang['this is the price per minute that a user has to pay for a nude chat session. a nude chat session allows several users to talk to a performer and all of them have to pay this fee.']='Este é o preço por minuto que um usuário tem de pagar por uma sessão de bate-papo nu. Uma sessão de bate-papo nu permite que vários usuários para conversar com uma modelo e todos eles tem que pagar esta taxa. ';
$lang['this is the price that a user has to pay in order to gain access to your paid photo gallery. once a user has paid this fee, they will have access to your gallery indefinitely.']='Este é o preço que um usuário tem que pagar para ter acesso a sua galeria de fotos pago. Depois que o usuário paga esta taxa, eles terão acesso à sua galeria indefinidamente. ';
$lang['continue']='Continuar';
$lang['please select your payment method']='Por favor, selecione o método de pagamento';
$lang['this field is required']='Este campo é obrigatório';
$lang['signup step 2 - payment']='Passo Inscrição 2 - Pagamento';
$lang['payment method']='Receber dinheiro em';
$lang['min. %s %s']='Min.  %s %s ';
$lang['please upload your photo id!']='Por favor envie o seu ID foto!';
$lang['please upload your photo id']='Por favor envie o seu ID foto';
$lang['photo ids - add']='Foto IDs - Adicionar';
$lang['you can upload jpg/png/pdf/zip files']='Você pode fazer upload de arquivos jpg / png / pdf / zip ';
$lang['Upload a file']='Carregar um Arquivo';
$lang['you are allowed to upload max 1 file!']='Você tem permissão para enviar max 1 arquivo!';
$lang['error please try again']='Erro! Por favor, tente outra vez! ';
$lang['photo ids - summary']='Foto IDs - Resumo';
$lang['view']='View';
$lang['you have no photo ids added']='Você não tem IDs foto acrescentado ';
$lang['are you sure you want to delete this item']='Você tem certeza de que deseja excluir este item';
$lang['performer\'s categories page']='Página de Categorias';
$lang['welcome back']='Bem vindo de volta';
$lang['%s unread message(s)']='Mensagem não lida %s (s)';
$lang['your account has %s']='A sua conta tem  %s';
$lang['the equivalent of %s.']='O equivalente a %s. ';
$lang['you have been marked for payment.']='Você foi marcado para o pagamento.';
$lang['you need %s more and we\'ll send you the payment!']='Você precisa de mais %s e nós enviaremos o seu pagamento! ';
$lang['frame']='Frame ';
$lang['all girls']='Todas as meninas ';
$lang['alternative girls']='Garotas alternativas';
$lang['asians']='asians';
$lang['bbw / plump']='BBW / Plump';
$lang['big ass']='Bundas Grandes';
$lang['others']='Outros';
$lang['personal details']='Dados pessoais';
$lang['pricing']='Preço';
$lang['banned locations']='locais proibidos ';
$lang['schedule']='Agenda';
$lang['earnigns']='resultados';
$lang['advanced search']='Pesquisa Avançada';
$lang['performer\'s area']='Área Exclusiva Para Modelos';
$lang['$$performers wanted$$']='$$ Seja Modelo $$';
$lang['privacy policy']='Política de Privacidade';
$lang['terms & conditions']='Termos e Condições';
$lang['contact us']='Entre Em Contato Conosco';
$lang['choose your language:']='Escolha o seu idioma:';
$lang['change language to %s']='Alterar o idioma para %s ';
$lang['modenacam - all rights reserved &copy; 2007 - %s']='Akinews - Todos os direitos reservados &copy	 2017 - %s';
$lang['&copy; 2012 %s  |  all rights reserved.']='&copy; 2017 %s  |  Todos os direitos reservados.';
$lang['this site contains sexually explicit material. enter this site only if you are over the age of 18!']='Este site contém material sexualmente explícito. Digite este site apenas se tiver mais de 18 anos de idade!';
$lang['18 u.s.c 2257 record-keeping requirements compliance statement ']='18 USC 2257 Record-Keeping Requirements Compliance Statement ';
$lang['go online']='Ir Online';
$lang['my payments']='Meus pagamentos ';
$lang['performer\'s personal details page']='Página de Detalhes Pessoais';
$lang['go online now!']='Ir on-line agora! ';
$lang['go online!']='Ir On-line!';
$lang['web based encoder advantages']='Vantagens de ir online através do decodificador';
$lang['does not require 3rd party apps or plug-ins']='Não requer aplicações de terceiros ou plug-ins';
$lang['automated bandwidth selection based on speed test']='Seleção de largura de banda automatizada com base em teste de velocidade';
$lang['pre-set low, medium and high quality settings']='Configurações pré-estabelecido como Baixa, Média e Alta qualidade';
$lang['low resource usage ']='Baixo consumo de recursos ';
$lang['go online using fmle / wirecast']='Ir On-line usando FMLE / wirecast ';
$lang['fmle encoder advantages']='encoders FMLE / wirecast vantagens ';
$lang['h.264/mp3 video/audio encoding technology']='H.264 / MP3 tecnologia de vídeo / codificação de áudio';
$lang['customizable individual quality settings']='Definições de qualidade individual personalizáveis ​​';
$lang['the result is hd video and audio']='O resultado é áudio e vídeo HD ';
$lang['less bandwidth usage']='Uso de banda Menos';
$lang['1. what are they?']='1. Quais são FMLE / wirecast';

$lang['these are 3rd party applications that have to be installed on your windows or mac computer. they are video and audio encoders that compress the video from your camera and stream it to 20vercam server with a higher quality than the web based application.'] = 'Estes são aplicativos de terceiros que precisam ser instalados no seu computador Windows ou Mac. Eles são codificadores de vídeo e áudio que comprimem o vídeo de sua câmera e transmiti-lo para 20VerCam servidor com uma qualidade superior ao aplicativo baseado na web';

$lang['2. who is using it?']='2. Quem está usando';
$lang['all professionals from the entertainment industry are using it.']='Todos os profissionais da indústria de entretenimento estão a usá-lo.';
$lang['3. how much does it cost?']='3. Quanto custa isso? ';
$lang['adobe\'s fmle is freeware software, but wirecast is a commercial application.']='Adobe \' FMLE é um software freeware, mas wirecast é uma aplicação comercial. ';
$lang['4. flash media live encoder contains spyware or adware since it\'s free?']='4. O Flash Media Live Encoder contém spyware ou adware?';
$lang['no, the application is clean and safe. it is built by adobe. official home page.']='Não, a aplicação é limpa e segura. Ele é construído pela Adobe. Leia mais sobre o assunto na página oficial do produto';
$lang['5. do i need to run both? which one should i choose?']='5. Preciso correr tanto? Qual deles devo escolher? ';
$lang['sections of complete and powerful solutions, you can choose what you want.']= 'Se trata de soluções completas e poderosas, você pode escolher o que desejar.';
$lang['start now!']='Comece agora! ';
$lang['step1: installation of adobe live encoder or wirecast']='Passo 1: Instalação do Adobe Vivo Encoder ou wirecast ';
$lang['download the adobe application here . install it and run it. mac users, please visit adobe\'s site for the latest installation kit. wirecast users, please consult the official site in order to get license.']='Faça o download do aplicativo Adobe aqui. Instale-o e execute-o. Usuários de Mac, por favor visite o site da Adobe \ para o kit de instalação mais recente. Usuários wirecast, por favor consulte o site oficial, a fim de obter licença. ';
$lang['step 2: configuration']='Passo 2: Configuração';
$lang['download the xml configuration file <a href="%s">here</a>, run flash media live encoder, go to file > open profile and select the recently downloaded configuration file. select the video and audio device you\'re going to use and press the start button. you can adjust the video settings to adapt the stream to your hardware and internet connection.'] = 'Baixe o arquivo de configuração XML <a href="%s">aqui</a>, execute o Flash Media Live Encoder, vá para o arquivo> Abra o perfil e selecione o arquivo de configuração baixado recentemente. Selecione o dispositivo de vídeo e áudio que você vai usar e pressione o botão Iniciar. Pode ajustar as definições de vídeo para adaptar o fluxo ao hardware e à ligação à Internet. ';
$lang['forgot password?']='Não Lembra?';
$lang['your username']='O seu username';
$lang['your email']='O seu email';
$lang['please upload your contract!']='Por favor, carregar o seu contrato!';
$lang['please enter your name'] = 'Por favor, insira o seu nome';
$lang['please enter a nickname']='Por favor, digite um apelido';
$lang['please enter a valid email address']='Por favor insira um endereço de e-mail válido';
$lang['password must have at least 3 characters']='A senha deve ter pelo menos 3 caracteres. ';
$lang['please enter a valid first name']='Digite um nome válido!';
$lang['please enter a valid last name']='Por favor insira um sobrenome válido!';
$lang['please enter a valid address']='Por favor insira um endereço válido!';
$lang['please enter a valid city']='Digite uma cidade válida!';
$lang['please enter a valid zip']='Por favor insira um zip válido!';
$lang['please enter a valid state']='Por favor, entrar em um estado válido!';
$lang['please enter a valid country']='Por favor insira um país válido!';
$lang['please enter a valid phone']='Por favor insira um número de telefone válido!';
$lang['please agree with the terms and conditions']='Por favor, leia e concorde com os nossos TOS! ';
$lang['signup step 1 - personal information']='Passo Inscrição 1 - Informações pessoais';
$lang['you are allowed to upload max 2 files!']='Você está autorizado a fazer upload de arquivos max 2!';
$lang['i agree the terms of service']='Eu concordo com os Termos de Serviço ';
$lang['my schedule']='Meu Calendário';
$lang['current server time:']='Hora atual do servidor:';
$lang['please enter a valid subject']='Por favor insira um assunto válido!';
$lang['please enter a valid message']='Insira uma mensagem válida!';
$lang['submit']='Enviar';
$lang['not available!']='Não Disponível!';
$lang['please select a language!']='Por favor, selecione um idioma!';
$lang['please enter a valid bank name']='Por favor, insira um nome bancária válida';
$lang['please enter a valid owner']='Por favor insira um proprietário válido';
$lang['please enter a valid iban']='Por favor insira um IBAN válido';
$lang['please enter a valid swift']='Por favor insira um SWIFT válido';
$lang['please enter a valid category']='Por favor, indique uma categoria válida ';
$lang['please enter a valid gender']='Por favor insira um gênero válido';
$lang['please enter a valid description']='Digite uma descrição válida';
$lang['please enter valid data in thsi field']='Por favor insira dados válidos neste domínio ';
$lang['please enter a valid sexual preference']='Por favor, indique uma preferência sexual válido ';
$lang['please enter a valid ethnicy']='Por favor insira um etnia válido';
$lang['please enter a valid height']='Digite uma altura válida';
$lang['please enter a hair color']='Por favor, indique uma cor de cabelo ';
$lang['please enter a valid hair lenght']='Por favor insira um comprimento do cabelo válido';
$lang['please enter a valid eye color']='Por favor insira um cor dos olhos válido';
$lang['please enter a valid build']='Por favor, indique uma compilação válido ';
$lang['please enter a valid language']='Por favor insira um idioma válido';
$lang['please enter a valid day']='Digite um dia válido';
$lang['please enter a valid month']='Por favor insira um mês válido';
$lang['please enter a valid year']='Por favor insira um ano válido';
$lang['username must have at least 3 characters']='Nome de usuário precisa ter pelo menos 3 caracteres';
$lang['nickname must have at least 3 characters']='Nome de Utilizador deve ter pelo menos 3 caracteres';
$lang['email must be valid']='E-mail deve ser válido ';
$lang['first name must have at least 3 characters']='Primeiro nome deve ter pelo menos 3 caracteres ';
$lang['lastname must have at least 3 characters']='Last nome deve ter pelo menos 3 caracteres';
$lang['addres must have at least 3 characters']='O endereço deve ter pelo menos 3 caracteres';
$lang['city must have at least 3 characters']='City deve ter pelo menos 3 caracteres';
$lang['zip must have at least 3 characters']='Zip deve ter pelo menos 3 caracteres';
$lang['state must have at least 3 characters']='Estado deve ter pelo menos 3 caracteres ';
$lang['country must have at least 3 characters']='País deve ter pelo menos 3 caracteres';
$lang['phone must have at least 3 characters']='Telefone deve ter pelo menos 3 caracteres ';
$lang['contract not added']='Contrato não adicionou ';
$lang['payment must have at least 3 characters']='Input pagamento deve ter pelo menos 3 caracteres';
$lang['terms of service']='Termos de Serviço';
$lang['you must agree to terms of conditions']='Você deve concordar com os Termos e condições!';
$lang['back']='Voltar';
$lang['next']='Next';
$lang['bank name']='Banco Name';
$lang['bacnk name must have at least 3 characters']='Nome do banco deve ter pelo menos 3 caracteres!';
$lang['account owner']='Conta do proprietário';
$lang['account must have at least 3 characters']='A conta deve ter pelo menos 3 caracteres ';
$lang['iban']='IBAN';
$lang['iban must have at least 3 characters']='IBAN deve ter pelo menos 3 caracteres';
$lang['swift']='SWIFT';
$lang['swift must have at least 3 characters']='SWIFT deve ter pelo menos 3 caracteres';
$lang['percaentage must be valid']='Percentual deve ser válido ';
$lang['signup step 3 - select category']='Passo Inscrição 3 - Selecione a Categoria';
$lang['category must be selected']='Categoria deve ser selecionado ';
$lang['signup step 4 - about yourself']='Cadastre-se o passo 4 - Sobre Yourself';
$lang['field must have at least 3 characters']='O campo deve ter pelo menos 3 caracteres. ';
$lang['description must have at least 3 characters']='A descrição deve ter pelo menos 3 caracteres. ';
$lang['field must be valid']='O campo deve ser válido.';
$lang['hair lenght']='Comprimento do cabelo';
$lang['register']='Registrar';
$lang['performer\'s profile page']='Página de Perfil';
$lang['my account page']='Página da Minha Conta ';
$lang['you have been accesed on videochat %s times.']='Você foi acessado %s vezes no video chat.';
$lang['you have %s unread message(s).']='Você tem %s mensagen(s) não lida(s). ';
$lang['your contract is %s']='O seu contrato é %s';
$lang['your photo id is %s']='Sua identificação com foto é %s';
$lang['your account is active!']='Sua conta está ativa!';
$lang['view performer\'s manual']='VER O MANUAL DA SESSÃO MODELOS';
$lang['%s\'s statistics']='Estatísticas de %s';
$lang['%s\'s earnings statistics']=' %s \' s estatísticas ganhos ';
$lang['%s\'s watchers statistics']=' %s \' s observadores estatísticas ';
$lang['please upload your contract']='Por favor, carregar o seu contrato!';
$lang['contracts - add contract']='Contratos - Adicionar contrato';
$lang['you are allowed to upload max 3 files!']='Você está autorizado a fazer upload de arquivos max 3!';
$lang['contracts - summary']='Os contratos - Resumo ';
$lang['you have no contracts added']='Você não tem contratos enviado!';
$lang['photo title']='Título da Foto';
$lang['please upload your photo']='Por favor, enviar sua foto';
$lang['please upload a photo']='Por favor, fazer upload de uma foto';
$lang['you are allowed to upload max 1 photo at once!']='Você tem permissão para enviar max 1 foto de uma vez! ';
$lang['avatar was set!']='Avatar foi escolhido!';
$lang['free photos']='Fotografias';
$lang['set as avatar']='Definir como Avatar (Foto de Perfil)';
$lang['paid photos']='Fotos pagas ';
$lang['delete']='Apagar';
$lang['view earnings']='Ver Ganhos';
$lang['you have no earnings']='Você não tem ganhos';
$lang['unread']='Não lido';
$lang['move to trash']='Mover para lixeira ';
$lang['inbox']='Entrada';
$lang['sent']='Enviadas';
$lang['trash']='Lixo';
$lang['sent items']='Itens Enviados';
$lang['reply to']='Responder para';
$lang['(user)']='(Usuário)';
$lang['re']='RE';
$lang['loading...']='Carregando ...';
$lang['confirm!']='Confirme!';
$lang['are you sure you want to delete this message?']='Você tem certeza de que deseja excluir esta mensagem';
$lang['there are no videos!']='Não existem vídeos!';
$lang['please select at least one language!']='Por favor, selecione pelo menos uma língua! ';
$lang['please upload your avatar!']='Por favor envie seu avatar (Foto de Perfil)!';
$lang['please enter a valid birthday']='Por favor insira um aniversário válido';
$lang['please enter at least 8 characters']='Por favor, insira pelo menos 8 caracteres';
$lang['please upload your avatar']='Por favor, carregar o seu avatar (Foto de Perfil)';
$lang['please enter a valid weight']='Por favor insira um peso válido';
$lang['please enter a valid cup size']='Por favor insira um copo tamanho válido';
$lang['please choose at least one language']='Por favor, escolha pelo menos uma língua';
$lang['signup step 5 - about you']='Cadastre-se o passo 5 - Sobre o que você';
$lang['you are allowed to upload max 1 photo!']='Você tem permissão para enviar max 1 foto!';
$lang['performer\'s banned locations page']='Página de Locais Banidos';
$lang['performer\'s pricing page']='Página de Configuração de Preços';
$lang['password must have at least 5 characters']='A senha deve ter pelo menos 5 caracteres! ';
$lang['passwords do not match']='As senhas não são iguais ';
$lang['account summary - settings']='Resumo da conta - Configurações ';
$lang['save changes']='Salvar alterações';
$lang['register now']='Cadastre-se';
$lang['your name']='Seu Nome';
$lang['your password']='Sua Senha';
$lang['login as performer']='Entrar como modelo';
$lang['payment details']='Informações para Pagamento';
$lang['live broadcasting - ']='A transmissão ao vivo - ';
$lang['you have no payments']='Você não tem pagamentos. ';
$lang['please select at least one category']='Por favor, selecione pelo menos uma categoria';
$lang['&copy; 2012 %s  |  all rights reserved.'] = 'Akinews - All rights reserved &copy; 2007 - %s';
$lang['performers <span class="fg_demi">area</span>'] = 'Área de <span class="fg_demi">Modelos</span>';
$lang['performers <span class="fg_demi">wanted</span>'] = 'Procura-se <span class="fg_demi">Modelos</span>';
$lang['studios <span class="fg_demi">area</span>'] = 'Área de <span class="fg_demi">Estúdios</span>';
$lang['affiliates <span class="fg_demi">area</span>'] = 'Área de <span class="fg_demi">Afiliados</span>';
$lang['18 u.s.c 2257']='18 U.S.C 2257 ';
$lang['performer administration panel']='MODELOS- Painel de administração';
$lang['welcome']='Bem-vindo';
$lang['logout']='Sair';
$lang['performer signup']='Cadastro de Modelo';
$lang['these are 3rd party applications that have to be installed on your windows or mac computer. they are video and audio encoders that compress the video from your camera and stream it to modenacam server with a higher quality than the web based application.']='Estas são as aplicações 3rd party que têm de ser instalado no seu computador com Windows ou MAC. Eles são de áudio e vídeo codificadores que comprimem o vídeo de sua câmera e transmiti-lo para o servidor Akinews com uma qualidade superior à da aplicação baseada na web. ';
$lang['user name']='Usuário';
$lang['email address']='Endereço de email';
$lang['cost']='Custo';
$lang['time']='Time';
$lang['performer area']='Área de Modelos';
$lang['welcome to my chat room big boy! :p']='Bem-vindo à minha sala de chat! ';
$lang['your account is not approved!']='A sua conta não está aprovado!';
$lang['invalid private chips price. click %s to set it!']='Preço Créditos privado inválida. Clique %s para defini-la! ';
$lang['here']='Aqui';
$lang['contract not approved!']='Contrato não aprovado! ';
$lang['photo id not approved!']='Foto ID não aprovado!';
$lang['your payments']='Seus pagamentos';
$lang['your ad zones traffic']='Seu tráfego das zonas de anúncios. ';
$lang['affiliate account was created succesfully!']='Conta Affiliate foi criado com sucesso!';
$lang['affiliate account was not created. please try again!']='Conta de afiliados não foi criado. Por favor, tente outra vez! ';
$lang['welcome to your account ']='Bem-vindo à sua conta ';
$lang['account is not in pending. you cannot activate! ']='Conta não está pendente. Você não pode ativar! ';
$lang['account activated successfully!']='Conta ativada com sucesso!';
$lang['failed to send the confirmation email. please retry!']='Falha ao enviar o e-mail de confirmação. Por favor, tente novamente! ';
$lang['password saved, you can login!']='Password salvo, você pode fazer o login!';
$lang['your ad zones']='Suas zonas de anúncios';
$lang['create ad zone']='Criar Zona Ad';
$lang['ad zone was not created. please try again!']='Zona Ad não foi criado. Por favor, tente outra vez! ';
$lang['this ad zone dosen`t exist!']='Esta zona anúncio não existe!';
$lang['edit ']='Editar';
$lang[' ad zone']='Zona de anúncios';
$lang['ad zone was not updated. please try again!']='Zona Ad não foi atualizado. Por favor, tente outra vez! ';
$lang['ad zone was updated!']='Zona Ad foi atualizado!';
$lang['this ad zone not exist!']='Esta zona anúncio não existe!';
$lang['get ad code']='Obter código do anúncio';
$lang['this ad zone was deleted!']='Esta zona anúncio foi apagado! ';

$lang['password was updated successfully!']='Password foi atualizado com sucesso! ';
$lang['password was not updated! please try again!']='Senha não foi atualizado! Por favor, tente outra vez! ';
$lang['personal details was updated successfully!']='Dados pessoais foi atualizado com sucesso!';
$lang['personal details were not updated! please try again!']='Dados pessoais não foram atualizados! Por favor, tente outra vez! ';
$lang['put this code in your hatml page, where you want to appeare the ad!']='Coloque esse código em sua página html, onde deseja que o anúncio seja exibido! ';
$lang['go back to your ad zones']='Voltem para suas zonas de anúncios ';
$lang['paid interval']='Intervalo Pago';
$lang['ad zones']='zonas de anúncios';
$lang['create new zone']='Criar nova zona';
$lang['promo tools']='Ferramentas Promo';
$lang['keep me logged in']='Mantenha-me conectado';
$lang['phone number']='Número de telefone';
$lang['create new ad zone']='Criar uma nova zona de anúncios. ';
$lang['create date']='Criar date';
$lang['there are no ads.']='Não há nenhum anúncio.';
$lang['get code']='Gerar Código';
$lang['please enter your site name']='Por favor, indique o nome do site ';
$lang['please enter your site url']='Digite o URL do site';
$lang['please enter a username']='Digite um nome de usuário';
$lang['your site information']='Informações do Site';
$lang['minim %s']=' %s mínimo';
$lang['enter payment details']='Entrar Detalhes de pagamento ';
$lang['edit payment details']='Editar detalhes de pagamento ';
$lang['currency']='Moeda';
$lang['set payment method']='Definir Forma de Pagamento ';
$lang['current payment method']='Método de pagamento atual';
$lang['release']='Release';
$lang['not set']='Não informado';
$lang['change payment method:']='Alterar forma de pagamento: ';
$lang['view report']='Veja o relatório';
$lang['there are no entires']='Não há entradas';
$lang['your account has']='A sua conta tem ';
$lang['please send all users to this url']='Por favor, envie a todos os usuários para esta URL';
$lang['your commission is %s from each transaction referred users do.']='A sua comissão é de %s de cada operação prevista que os usuários fizerem.';
$lang['ads traffic statistics']='Estatísticas de Tráfego dos Anúncios';

$lang['join our <span class="bold red">affiliate</span> program now!'] = 'Junte-se ao nosso programa!';

$lang['start making load of <span class="bold red">cash</span> instantly!']='Comece a ganhar<span class="bold red"> dinheiro</span> imediatamente!';

$lang['you can <span class="bold red">earn</span> up to <span class="bold red">$5000</span> a month']='Faça mais de <span class="bold red"> R$ 5.000,00</span> por mês!';


$lang['if you have a website, you can apply to become a member of our affiliate program and earn money by referring visitors to our website.']='Se você tem um site, você pode pedir para se tornar um membro do nosso programa de afiliados e ganhar dinheiro referindo visitantes ao nosso site.';
$lang['affiliate commission is %s of the money referred users spend on chips.']='Comissão da filial é %s do dinheiro a que se refere os usuários gastam em Créditos. ';
$lang['each time a user buys chips, you will receive %s of their value. there is no time limit and you do not have to wait for users to spend bought chips on the website; this means that if in 10 years a user referred by your website buys 100 chips, your account will be funded with the real currency-equivalent of 30 chips at that precise time.'] = 'Cada vez que um usuário compra Créditos, você receberá %s do seu valor. Não há limite de tempo e você não tem que esperar para que os usuários gastem Créditos comprado no site. ';
$lang['you can view live account statistics 24/7. you will get paid as soon as your account balance reaches the desired release amount.']='Você pode ver as estatísticas de contas ao vivo 24/7. Você será pago assim que o saldo da conta não atingir o montante de libertação desejada. ';
$lang['start earning thousands of cash']='Comece a ganhar dinheiro ';
$lang['right now!']='Agora mesmo!';
$lang['all you need is a domain name to join the biggest adult video affiliation system on earth!']='Tudo o que você precisa é de um nome de domínio para se juntar ao maior sistema de afiliação de vídeo adulto na Terra! ';
$lang['login as affiliate']='Entrar como afiliado ';
$lang['ad name']='Nome Ad';
$lang['ad size']='O tamanho do anúncio';
$lang['performer status']='Estado da performer';
$lang['online']='Online';
$lang['offline']='Offline';
$lang['link location']='Localização Link ';
$lang['border color']='Color Border';
$lang['background color']='Cor de fundo';
$lang['text color']='Cor do texto';
$lang['put this code in your html page, where you want to appeare the ad!']='Coloque esse código em sua página html, onde você quer appeare o anúncio!';
$lang['affiliate administration panel']='Administração da filial painel ';
$lang['affiliate signup']='Cadastro de Parceiros ';
$lang['your information']='Suas informações';
$lang['Start making load of <span class	bold red>cash</span> instantly!']= 'Comece a divulgar <span class	bold red>cash</span> imediatamente!';
$lang['you can <span class="negrito" red> ganhar </ span> até <span class	bold red>$5000</span> a month']= 'You can <span class';
$lang['sign up now']='Inscreva-se agora ';
$lang['client']='Cliente';
$lang['potential client']='Cliente em potencial';
$lang['age_18_22']='Age 18-22';
$lang['package']='Pacote';
$lang['procesor']='Processador';
$lang['credits have been added to your account!']='Créditos foram adicionados à sua conta!';
$lang['invalid performer id!']='ID inválida!';
$lang['you already have access to %s private photo gallery!']='Você já têm acesso a galeria de fotos privado %s!';
$lang['you have unsufficient funds in your account to subscribe to this paid photo gallery!']='Você tem fundos suficientes na sua conta para subscrever este galeria de fotos pago! ';
$lang['please close all your chat sessions before you can subscribe to a photo gallery!']='Por favor, feche todas as suas sessões de bate-papo antes de subscrever uma galeria de fotos!';
$lang['leave']='Sair';
$lang['you have paid %s for %s private photo gallery.']='Você pagou %s para galeria de fotos privado %s.';
$lang['performer does not exist!']='Modelo inexistente! ';
$lang['%s is not in your favorite list!']=' %s não está na sua lista de favoritos!';
$lang['%s has been removed from your favorite list!']=' %s foi removido da sua lista de favoritos!';
$lang['performer is already in your favorites!']='Modelo já em seus favoritos!';
$lang['%s has been added to your favorites!']=' %s foi adicionado aos seus favoritos!';
$lang['favorites models']='Modelos Favoritos';
$lang['password changed successfuly!']='Senha alterada com sucesso! ';
$lang['newsletter settings']='Configurações de Informaivos';
$lang['changes saved.']='Alerações salvas.';
$lang['newsletter enabled!']='Informativos habilitados!';
$lang['newsletter disabled!']='Informativos desativados!';
$lang['cancel account']='Cancelar Conta ';
$lang['%s - account canceled']=' %s - conta cancelada';
$lang['account canceled!']='Conta cancelado!';
$lang['my statements']='Minhas declarações';
$lang['repeat email']='Repetir o e-mail';
$lang['user register']='Registo do utilizador ';
$lang['account created successfully!']='Conta criada com sucesso!';
$lang['privacy policy ']='Política de Privacidade';
$lang['18 u.s.c 2257 record-keeping requirements compliance statement']='18 USC 2257 Record-Keeping Requirements Compliance Statement ';
$lang['online models']='Modelos Online';
$lang['live performers']='Modelos ao vivo';
$lang['invalid language!']='Linguagem inválida! ';
$lang['account is not in pending. you cannot reactivate! ']='Conta não está pendente. Você não pode reativar! ';
$lang['invalid room type!']='Tipo de sala inválida!';
$lang['your account balance is too low to access %s show.']='O saldo da sua conta não é suficiente para acessar o show %s. ';
$lang['performer is currently in another room type!']='Modelo atualmente em outro tipo de quarto! ';
$lang['you are allowed to have only one session at once!']='Você está autorizado a ter apenas uma sessão de uma vez! ';
$lang['you are banned by %s!']='Você está banido por %s!';
$lang['performer cannot go online!']='Modelo impossibilitado de ir online!';
$lang['room']='Sala';
$lang['anonymus']='Anonimo';
$lang['invalid request type!']='Tipo de solicitação inválida! ';
$lang['invalid performer']='performer inexistente.';
$lang['invalid room type']='Tipo de sala inválida';
$lang['performer is currently in another room type']='performer solicitada está atualmente em um outro tipo de sala.';
$lang['no messages in this folder']='Nenhuma mensagem nesta pasta. ';
$lang['our models']='Nossas Modelos';
$lang['our performers']='Nossas Modelos';
$lang['performer is suspended!']='Modelos está suspenso!';
$lang['%s\'s profile - %s']='perfil -%s %s\'s';
$lang['no results found matching your search criteria.']='Não há resultados correspondentes aos seus critérios de busca. ';
$lang['you already have added a review to the chat session!']='Você já adicionou um comentário para a sessão de bate-papo!';
$lang['rate']='Taxa';
$lang['add review']='Adicionar crítica';
$lang['thank you for evaluation!']='Obrigado por avaliação!';
$lang['no matches found!']='Não foram encontrados jogos! ';
$lang['agree']='Concordo';
$lang['please close all your chat sessions before you can buy access to a paid video!']='Você deve fechar todas as suas sessões de bate-papo antes de você pode comprar o acesso a um vídeo pago! ';
$lang['you have unsufficient funds in your account to buy access to this paid video!']='Você tem fundos unsufficient na sua conta para comprar o acesso a um vídeo pago! ';
$lang['age']='Idade';
$lang['language']='Língua';
$lang['body build']='Constituição física';
$lang['profile description :']='Descrição do perfil:';
$lang['what turns me on :']='O que me excita: ';
$lang['what turns me off :']='O que não me excita: ';
$lang['this performer has not uploaded any pictures']='Este performer não colocou nenhuma foto. ';
$lang['send message']='Enviar Mensagem';
$lang['add favorite']='Adicionar favorito';
$lang['remove favorite']='Remover favorito ';
$lang['1 vote']='1 voto';
$lang['%s votes']=' %s votos';
$lang['my profile']='Meu perfil';
$lang['my photos (%s)']='Fotos (%s)';
$lang['my videos (%s)']='Vídeos (%s)';
$lang['you must be logged in to use this feature.']='Você deve estar logado para usar esta função!';
$lang['user\'s reviews']='Comentários de Usuários';
$lang['free videos']='Vídeos Grátis';
$lang['paid videos']='Vídeos Pagos ';
$lang['minute(s)']='Minuto (s)';
$lang['performer\'s chat room']='Sala de Bate-papo';
$lang['private chat rate']='Valor Chat Privado';
$lang['minute']='Minutos';
$lang['true private chat rate']='Valor Chat Exclusivo';
$lang['nude chat rate']='Valor Chat Simples';
$lang['peek chat rate']='Valor Chat Espião';
$lang['your account balance']='O saldo da sua conta ';
$lang['available private chat']='Bate-papo privado disponível';
$lang['other performers']='Outras Modelos';
$lang['your account have %s credits']='A sua conta tem %s créditos.';
$lang['your account has %s left']='A sua conta foi %s desconectada.';
$lang['fund your account!']='Comprar créditos!';
$lang['statements']='Extratos';
$lang['online chat']='Chat Online';
$lang['favorites']='Favoritos';
$lang['order credits']='Adquirir Créditos';
$lang['hide header']='Ocultar Cabeçalho';
$lang['show header']='Mostrar Cabeçalho';
$lang['members login']='Membros Login ';
$lang['sign up']='Inscrever-Se';
$lang['years']='anos';
$lang['search']='Pesquisar';
$lang['reset']='Reset';
$lang['all videos']='Todos os vídeos';
$lang['are you sure you want to pay %s to have access at %s private photo gallery?']='Você tem certeza que você quer pagar %s para ter acesso a galeria de fotos privado %s?';
$lang['are you sure you want to pay %s to watch this video?']='Você tem certeza que você quer pagar %s para ver este vídeo';
$lang['become a member']='Torne-se membro!';
$lang['username:']='Usuário:';
$lang['username length should be between 4 and 25 chars?']='Comprimento usuário deve ter entre 4 e 25 caracteres';
$lang['login now']='Logar agora!';

$lang['email address:']='Endereço de Email:';
$lang['email address must be valid!']='Por favor, use um e-mail válido!';
$lang['password:']='Senha:';
$lang['password must be valid!']='A senha deve ser válido! ';
$lang['repeat email address:']='Repita Email:';
$lang['email address must match!']='E-mails devem combinar!';
$lang['repeat password:']='Repita a Senha:';
$lang['password must match!']='As senhas devem corresponder!';
$lang['click here to %s become a %s member']='Registrar!';
$lang['log in here']='Entre aqui';
$lang['already a %s member? %s']='SOU MEMBRO!';
$lang['favorite performers']='Modelos favoritas';
$lang['you have no favorite performers!']='Você não tem modelos preferenciais!';
$lang['hello']='Olá';
$lang['you have %s new messages']='Você tem %s novas mensagens. ';
$lang['you have %s chips']='Você tem %s Créditos';
$lang['to order more click ']='Para encomendar mais, clique em ';
$lang['there are %s payments']='Há %s pagamentos. ';
$lang['payment id']='ID de pagamento ';
$lang['order chips']='Adquirir Créditos';
$lang['bonus']='Bonus';
$lang['there are %s statements']='Há %s declarações.';
$lang['billed amount']='Valor faturado';
$lang['you have no statements']='Não há declarações. ';
$lang['cancelling your account cannot be reversed, are you sure you want to continue?']='Cancelar a sua conta não pode ser revertida, você tem certeza que deseja continuar? ';
$lang['cancel account action is irevesible, are you sure you want to cancel it?']='Cancelar ação conta é irevesible, você tem certeza que deseja continuar? ';
$lang['your current password']='Sua senha atual';
$lang['invalid current password']='Senha atual inválido';
$lang['please type again']='Por favor, escreva de novo';
$lang['your new password']='Sua nova senha ';
$lang['password must be valid']='A senha deve ser válido! ';
$lang['repeat your new password']='Repita a nova senha';
$lang['password must match']='A senha deve corresponder ';
$lang['changes could not be saved']='As mudanças não poderiam ser salvas ';
$lang['policy']='Política';
$lang['please enter a valid email']='Por favor insira um e-mail válido!';
$lang['2257 statement']='2257 Statement';
$lang['to continue to goal chat please tip between']='Para continuar a meta de bate-papo por favor ponta entre';
$lang['translate']='Traduzir';
$lang['i speak']='Eu falo';
$lang['they speak']='Eles falam ';
$lang['translate what they say.']='Traduzir o que eles dizem.';
$lang['translate what i say.']='Traduzir o que eu digo.';
$lang['true private']='Exclusivo';
$lang['time set to do goal performance has expired.']='Time definido para fazer desempenho meta expirou. ';
$lang['time to reach goal expired without reaching the desired amount.']='Tempo para atingir meta expirou sem atingir a quantidade desejada.';
$lang['goal status']='Status Goal';
$lang['maximum value is: ']='O valor máximo é:';
$lang['minimum value is: ']='O valor mínimo é: ';
$lang['this field is required.']='Este campo é obrigatório.';
$lang['this value is too small.']='Este valor é muito pequeno.';
$lang['this value is too large.']='Esse valor é muito grande.';
$lang['minimum number of characters is: ']='Número mínimo de caracteres é: ';
$lang['maximum number of characters is: ']='O número máximo de caracteres é: ';
$lang['gathered so far:']='Reunidos até agora: ';
$lang['time to reach goal:']='Tempo para atingir objetivo:';
$lang['please provide a valid value.']='Por favor, forneça um valor válido.';
$lang['tip smaller than the minimum value.']='Ponta menor do que o valor mínimo. ';
$lang['tip greater than the maximum value.']='Tip maior do que o valor máximo.';
$lang['i will']='Eu vou';
$lang['for']='Por';
$lang['number of minutes you will do the \'i will...\' show']='Número de minutos que você vai fazer o show \' Eu vou ... \'';
$lang['minutes']='minutos';
$lang['if a goal of']='Se o objetivo de ';
$lang['is reached']='for alcançado';
$lang['within']='Durante';
$lang['if the amount is not received within this time, the goal is reset.']='Se o valor não for recebido dentro deste prazo, a meta é reposto.';
$lang['start goal']='Iniciar Goal';
$lang['stop goal']='Parar Goal';
$lang['the desired goal.']='O objetivo desejado. ';
$lang['deny requests']='Negar os pedidos';
$lang['automatically deny all invitations to other chat modes.']='Negar automaticamente todos os convites para os outros modos de bate-papo.';
$lang['you are now in a nude show!']='Você está em um show Simples!';
$lang['you are now in a private show!']='Você está em um show Privado!';
$lang['you are now in a true private show!']='Você está em um show Privado Exclusivo! ';
$lang['waiting for performer to confirm the invitation!']='Aguardando modelo confirmar o convite!';
$lang['invitation to nude chat']='Convite para bate-papo Simples';
$lang['request nude chat']='Chamar no Chat Simples ';
$lang['you não tem have enough chips to enter nude chat.']='Você não tem o suficiente para entrar Créditos no Chat Simples. ';
$lang['the performer is not yet ready to enter a nude chat. please try again later.']='Modelo ainda não está pronto para entrar no Chat Simples. Por favor, tente novamente mais tarde. ';
$lang['your invitation to nude chat has been accepted by the performer.']='Seu convite para chat Simples foi aceite.';
$lang['proceed']='Continuar';
$lang['a user has invited you to a nude chat!']='Um usuário convidou você para um bate-papo Simples!';
$lang['configure your &#60;b&#62;devices&#60;/b&#62;'] = 'Configure seu &#60;b&#62;Dispositivo&#60;/b&#62;';
$lang['please choose quality scheme:']='Por favor, escolha regime de qualidade:';
$lang['low']='Baixo';
$lang['medium']='Médio';
$lang['high']='Alto';
$lang['custom']='Personalizado';
$lang['choose online type:']='Escolha o tipo: ';
$lang['broadcast this cam']='Transmita essa cam ';
$lang['volume:']='Volume: ';
$lang['normal 4:3']='Normal 4:3';
$lang['wide 16:9']='Wide 16:9';
$lang['test mic']='Testar Mic';
$lang['ok']='ESTÁ BEM';
$lang['detected volume']='Volume Detectado';
$lang['the selected microphone seems to be working correctly.']='O microfone selecionado parece estar funcionando corretamente.';
$lang['the selected microphone does not seem to work.']='O microfone selecionado não parecem funcionar.';
$lang['the devices have been muted because you denied access in the settings pop-up.']='Os aparelhos foram silenciados porque você negado o acesso nas configurações pop-up .';
$lang['testing currently selected microphone']='Testing Microfone Atualmente Selecionado';
$lang['resolution:  ']='Resolução:';
$lang['px']='Px';
$lang['framerate: ']='Framerate:';
$lang['fps']='Fps';
$lang['video quality: ']='Qualidade de Vídeo: ';
$lang['%']='%';
$lang['bandwidth: ']='Largura de banda: ';
$lang['kbs']='KBS';
$lang['unlimited']='Ilimitado';
$lang['camera']='Camera';
$lang['cam ']='Cam';
$lang['take a break!']='Pausar! ';
$lang['exit chat and take a break']='Sair do bate-papo e fazer uma pausa ';
$lang['music player']='Music Player';
$lang['user list']='Lista de usuários';
$lang['mic. Vol:']='Mic. Vol.: ';
$lang['rec. camera:']='Grav. Câmera: ';
$lang['record']='Gravar';
$lang['start recording video']='Iniciar Gravação de Vídeo';
$lang['stop rec.']='Parar Grav. ';
$lang['stop recording video']='Parar Gravação de Vídeo';
$lang['rec.']='Grav.';
$lang['song name']='Nome da Música';
$lang['kick']='Expulsar';
$lang['kick this user from the chat.']='Expulsar este utilizador do bate-papo. ';
$lang['ban']='Banir';
$lang['ban this user from entering your chat.']='Banir este usuário de entrar em seu bate-papo.';
$lang['type your text here...']='Digite seu texto aqui ...';
$lang['0']='0';
$lang['nude only']='Somente Simples';
$lang['private only']='Somente Privado';
$lang['available']='Disponível';
$lang[' warning! ']='Atenção! ';

$lang['Connection to server failed. Please try again. If problem persists, please contact your administrator.']='conexão com o servidor falhou. por favor, tente novamente. se o problema persistir, por favor contacte o administrador.';
$lang['Connection rejected by server. Please re-login and try again.']='conexão rejeitado pelo servidor. por favor re-login e tente novamente.';
$lang['Connection closed or reset by server. Please refresh the page to reconnect.']='conexão fechada ou redefinir por servidor. por favor, atualize a página para se reconectar.';
$lang['performer entered a private chat or has logged off.']='performer entrou em um chat privado ou fez logoff. ';
$lang['connection to server failed. please try again. if problem persists, please contact your administrator.']='connection to server failed. please try again. if problem persists, please contact your administrator.';
$lang['connection rejected by server. please re-login and try again.']='connection rejected by server. please re-login and try again.';
$lang['beep on chat line']='Ativar alerta de bate-papo';
$lang['beep on user entrance']='Ativar alerta de usuário';
$lang['broadcast audio']='Transmitir áudio ';
$lang['go back online']='Voltar online';
$lang['on a break']='Em uma pausa';
$lang['   when you are ready to go back online, please press \'go back online\'.'] = '   Quando você estiver pronto para voltar a ficar online, por favor, aperte \'Voltar online\'. ';

$lang['accept']='Aceitar';
$lang['deny']='Negar';
$lang['invitation to private chat']='Convite para bate-papo privado';
$lang['  a user has invited you to a private chat!']='Um usuário te convidou para uma conversa privada! ';
$lang['username: ']='Nome de usuário:';
$lang['chips: ']='Créditos:';
$lang['dress time']='Tempo para vestir-se';
$lang['   private chat is over. please dress up and go back online.']='Bate-papo privado acabou. Por favor, vista-se e volte a ficar online. ';
$lang['   private chat is over. performer is now dressing up.']='Bate-papo privado acabou. Modelo vestindo-se agora. ';
$lang['tip received']='Sugestão recebida!';
$lang['you have received a']='Você recebeu um';
$lang['tip from']='Presente de ';
$lang['tip performer']='Presentear Modelo';
$lang['tip']='Presente';
$lang['open viewer\'s video']='Open espectador \' s video ';
$lang['see the viewer']='Ver o telespectador ';
$lang['close viewer\'s video']='Fechar espectador \' s video ';
$lang['hide the viewer']='Esconder o espectador';
$lang['no camera detected']='No Camera Detectado';
$lang['to go online you must have a working web cam connected to your computer. if you have a device connected, please make sure it is not in use by another process. refresh the page to retry.']='Para ir on-line você deve ter uma câmara de trabalho web conectado ao computador. Se você tiver um dispositivo conectado, por favor, certifique-se que não está em uso por outro processo. Atualize a página para tentar novamente. ';
$lang['camera access denied']='Camera Acesso negado ';
$lang['to go online you must accept the device security policy.']='Para ir on-line você deve aceitar a política de segurança do dispositivo.';
$lang['play']='Tocar';
$lang['stop']='Parar';
$lang['no song playing']='Não está reproduzindo nenhuma música no momento';
$lang['playing: ']='Reproduzindo:';
$lang['download xml']='Baixar XML';
$lang['url:']='URL:';
$lang['stream:']='Stream:';
$lang['show user list']='Mostrar lista de usuário';
$lang['exit']='Saída';
$lang['exit chat']='Sair do Bate-papo ';
$lang['request private chat']='Chamar no Chat privado ';
$lang['music player volume']='Volume do Player';
$lang['performer\'s volume']='performer \' Volume s ';
$lang['video zoom']='Video Zoom';
$lang['minimize']='Minimizar';
$lang['large view']='Ver Largo';
$lang['full screen']='Tela Cheia';
$lang['camera settings']='Definições da Câmara';
$lang['microphone settings']='Configurações do microfone';
$lang['	You have invited the performer to private chat. Waiting for reply...  This may take a few seconds.']='você convidou o cantor para bate-papo privado. aguardando resposta ... isso pode demorar alguns segundos. ';
$lang['invitation rejected']='Convite Rejeitado';
$lang['invitation accepted']='Convite aceito';
$lang['        the performer is not yet ready to enter a private chat. please try again later.'] = '        Modelo não disponivel para entrar em um bate-papo privado. Por favor, tente novamente mais tarde.';
$lang['your invitation to private chat has been accepted by the performer.']='Seu convite para bate-papo privado foi aceite pelo performer.';
$lang['cancel']='Cancelar';
$lang['chips/min']='Créditos / min';
$lang['no peeking allowed!']='Nenhum espreitar permitido! ';
$lang['you have been banned from this performer\'s chatroom. ']='Você foi banido da sala de chat pela performer. ';
$lang['you have been kicked out from this performer\'s chatroom. ']='Você foi expulso deste performer \' s sala de chat. ';
$lang['performer left the chatroom.']='Modelo saiu da sala de chat.';
$lang['you are not connected to server. please refresh page.']='Você não está conectado ao servidor. Por favor, atualize a página. ';
$lang['your free time has expired']='Seu tempo livre expirou ';
$lang['        you ran out of chips. please buy more chips and re-enter chat']='Você ficou sem Créditos. Por favor, compre mais Créditos e volte a poder conversar usar este tipo de chat';
$lang['entering peek']='Entrando Peek';
$lang['peek fee is']='Taxa de Peek é ';
$lang['peek option']='Opção Peek';
$lang['the performer has entered private chat. do you wish to take a peek?']='O performer entrou bate-papo privado. Você gostaria de dar uma olhada';
$lang['nude chat option']='Opção de chat simples';
$lang['the performer has entered a nude chat. do you wish to join?']='O performer entrou no Chat Simples. Deseja participar? ';
$lang['entering nude show']='Entrando no Show Simples';
$lang['nude fee is']='Taxa do Simples é ';
$lang['. do you wish to continue?']='. Você deseja continuar? ';
$lang['send tip']='Enviar Dica';
$lang['is less than the minimum amount of']='É menor do que a quantidade mínima de';
$lang['is more than the maximum amount of']='É mais do que a quantidade máxima de ';
$lang['please wait while the application initializes... ']='Por favor, aguarde enquanto o aplicativo é inicializado ... ';
$lang['please enter a tip between']='Digite uma dica entre ';
$lang['the value is not valid.']='O valor não é válido.';
$lang['and ']='e ';
$lang['to be able to tip this performer you must buy more credit.']='Para ser capaz de presentear você deve comprar mais Créditos. ';
$lang['you must be logged in order to tip the performer']='Você deve estar logado, a fim de derrubar o performer!';
$lang['tip amount:']='Quant. à Presentear: ';
$lang['close']='Fechar';
$lang['tip sent']='Presente enviado ';
$lang['you have successfully tipped the performer']='Seu presente foi enviado com sucesso! ';
$lang['has tiped the performer']='Enviou um presente de';
$lang['successfully tiped']='Presenteado enviado';
$lang['the tip was not sent.  please try again later.']='O presente não foi enviado. Por favor, tente novamente mais tarde. ';
$lang['tip sending failed']='O presente não foi enviado. Por favor, tente novamente mais tarde. ';
$lang['change topic']='Modificar para';
$lang['clear']='Apagar';
$lang['topic:']='Tópico: ';
$lang['no topic']='Sem tópico';
$lang['show your webcam to']='Mostra a sua webcam para';
$lang['close your webcam']='Feche sua webcam ';
$lang['don\'t show this again']='não mostrar isso de novo ';
$lang['chat type:']='Tipo de bate-papo:';
$lang['mic:']='Mic:';
$lang['go nude']='Ir no Simples';
$lang['go private']='Ir Privado';
$lang['broadcast microphone']='Microfone da transmissão';
$lang['allow peeking']='Permitir que espiem';
$lang['chips per minute for private: ']='Créditos por minuto para privado:';
$lang['chips per minute true private: ']='Créditos por minuto verdadeira privado:';
$lang['please choose at least one cam in order to continue to chat room. to do so, tick on \'broadcast this cam\' from any of the available cams']='Por favor, escolha pelo menos uma cam, a fim de continuar a conversar quarto. Para fazer isso, marque em \'Transmissão esta cam \' a partir de qualquer uma das salas disponíveis ';
$lang['activity log']='Registro de Atividades';
$lang['controls']='Comandos';
$lang['record cam:']='Gravar Cam:';
$lang['fullscreen']='Tela Cheia';
$lang['big screen']='Tela Grande';
$lang['small screen']='Tela Pequena';
$lang['microphone']='Microfone';
$lang['stop broadcast']='Parar Transmissão';
$lang['all cams']='Todas câmeras';
$lang['please choose a nickname to use in this chatroom.']='Por favor, escolha um apelido para usar nesta sala de bate-papo. ';
$lang['go to chat!']='Ir para conversar! ';
$lang['nickname:']='Apelidio: ';
$lang['please specify a nickname before continuing to chatroom!']='Por favor, especifique um apelido antes de continuar a sala de chat!';
$lang[' has entered chatroom.']='Entrou na sala de chat.';
$lang[' has left chatroom.']='Deixou sala de bate-papo. ';
$lang['video recording stopped.']='A gravação de vídeo parou.';
$lang['video recording started.']='A gravação de vídeo começou. ';
$lang['changed status to: ']='Status alterado para: ';
$lang['you não tem have enough chips to enter private chat.']='Você não tem o suficiente para entrar Créditos bate-papo privado. ';
$lang['copy']='Copiar';
$lang['performers videos'] = 'Vídeos';
$lang['there are no videos']='Não há vídeos';
$lang['you must rate for all performance.']='Você deve avaliar o desempenho para todos. ';
$lang['you must let a comment.']='Você deve deixar um comentário.';
$lang['please evaluate <strong>%s</strong> performance']='Por favor, avalie o desempenho de<strong> %s </ strong>';
$lang['your rating is anonymous, models can\'t see who rated them, but will help them perform better next time.']='A sua avaliação é anônima, modelos não podem ver quem esta lhe classificando, mas vai ajudá-las a ter um melhor desempenho na próxima vez. ';
$lang['surroundings, looks']='Envolvência e Aparência';
$lang['willing to please, friendly']='Disposição em fazer';
$lang['level of performance']='Desempenho no show';
$lang['language usage, comunication']='Linguagem usada e Comunicação ';
$lang['video quality, tehnical background']='Qualidade do vídeo e Ambiente ';
$lang['would you return']='Você retornaria novamente';
$lang['comment']='Comentar';
$lang['skip review']='Pular Avaliação ';
$lang['there are no performers online']='Não há modelos on-line no momento. ';
$lang['random performers']='Modelos aleatórias';
$lang['all performers'] = 'Modelos no Site';
$lang['performers currently in private']='performers atualmente em privado';
$lang['users login']='User Login';
$lang['join now']='Cadastre-se Agora';
$lang['type your performer name']='Escreva seu nome performer ';
$lang['invalid subject']='Assunto inválido ';
$lang['invalid message']='Mensagem inválida';
$lang['profile page']='Página de Perfil';
$lang['view all models']='Modelos do Site';
$lang['your favorites']='Seus Favoritos';
$lang['apply <span class="fg_demi">custom filters</span>'] = 'Personalizar <span class="fg_demi">Filtros</span>';
$lang['models<br/> <span class="bold">wanted</span>'] = 'Procura-se<br/> <span class="bold">Modelos</span>';
$lang['signup as <span class="bold">model</span>'] = 'Torne-se <span class="bold">Modelo</span>';
$lang['stripper, lingerie, thong, sex symbol, sensuality, buttocks'] = 'Stripper, Dançarinas, Modelos, Meninas, Meninos...';
$lang['affiliate<br/> <span class="bold">wanted</span>']= 'Procura-se<br/> <span class	bold>Afiliados</span>';
$lang['signup as <span class="bold">affiliate</span>']= 'Torne-se <span class bold>Afiliado</span>';
$lang['welcome guest! <span class="login_btn red">log in</span> or <a href="%s" class="red signup">register</a>'] = 'Bem vindo Visitante! <span class="login_btn red">Entre</span> ou <a href="%s" class="red signup">Registre-se</a>';
$lang['buy chips']='Comprar Créditos';
$lang['please log in with your user details']='Por favor, faça o login com seus dados';
$lang['account:']='Conta:';
$lang['member']='Usuário';
$lang['affiliate']='Afiliado';
$lang['log in to account']='Entrar';
$lang['filter your results']='Filtre seus resultados ';
$lang['select package']='Selecione o Pacote';
$lang['as low as %s per chips']='Tão baixo quanto %s por Créditos ';
$lang['cancelling your account cannot be reversed!']='Cancelar a sua conta não pode ser revertido! ';
$lang['recover']='Recuperar';
$lang['the microphone does not appear to be working or its level is too low. please note that mobile device streaming will not work properly unless the audio stream is available! to tweak the sound, press \'cancel\' and edit the microphone settings.']='O microfone não parece estar funcionando ou o seu nível é muito baixo. Por favor, note que o dispositivo móvel ao vivo não vai funcionar convenientemente, a menos que o fluxo de áudio está disponível! Para ajustar o som, pressione \'Cancelar \' e edite as configurações de microfone. ';
$lang['applied filters']='Filtros aplicada ';
$lang['free chat']='Chat gratis';
$lang['private chat']='Privado ';
$lang['nude chat']='Simples';
$lang['peek show']='Espiar';
$lang['age_range']='Faixa etária';
$lang['age_23_27']='Idade 23-27';
$lang['age_38_42']='Idade 38-42';
$lang['woman']='Mulher';
$lang['man']='Homem';
$lang['couples']='Casal';
$lang['fetish']='Fetiche';
$lang['babes']='Novas';
$lang['milf']='Maduras';
$lang['big boobs']='Peitos Grandes';
$lang['bbw']='GMB';
$lang['tattoos/piercings']='Tattoos/Piercings';
$lang['woman/man']='Mulher/Homem';
$lang['woman/woman']='Mulher/Mulher';
$lang['dominant']='Dominante';
$lang['submissive']='Submisso';
$lang['leather/latex']='Couro/Latex ';
$lang['bdsm']='BDSM';
$lang['bondage']='Bondage';
$lang['foot fetish']='Fetish por Pé';
$lang['english']='Inglês';
$lang['romanian']='Romeno';
$lang['albania']='Albânia';
$lang['bermuda']='Bermuda';
$lang['argentina']='Argentina';
$lang['male']='Masculino';
$lang['female']='Feminino';
$lang['transsexual']='Transexual';
$lang['asian']='Asiático';
$lang['ebony']='Ebony';
$lang['latin']='Latin';
$lang['white']='Branco';
$lang['straight']='Heterosexual';
$lang['gay']='Gay';
$lang['bisexual']='Bissexual';
$lang['over 195 cm']='mais de 195 cm';
$lang['185-195 cm']='185-195 cm';
$lang['174-184 cm']='174-184 cm';
$lang['163-173 cm']='163-173 cm';
$lang['152-162 cm']='152-162 cm';
$lang['under 152 cm']='Sob 152 centímetros';
$lang['over 73 kg']='mais de 73 kg';
$lang['67-73 kg']='67 -73 Kg ';
$lang['60-66 kg']='60 -66 Kg ';
$lang['53-59 kg']='53 -59 Kg ';
$lang['46-52 kg']='46 -52 Kg ';
$lang['under 46 kg']='Sob 46 kg';
$lang['auburn']='Ruivo';
$lang['black']='Negro';
$lang['blonde']='Loiro';
$lang['blue']='Azul';
$lang['brown']='Castanho';
$lang['clown hair']='Cabelo Engraçado';
$lang['fire red']='Fogo vermelho ';
$lang['orange']='Laranja';
$lang['pink']='Rosa';
$lang['other']='Outro';
$lang['bald']='Careca';
$lang['crew cut']='Escovinha';
$lang['long']='Longo';
$lang['short']='Curto';
$lang['shoulder length']='Na altura dos ombros';
$lang['green']='Verde';
$lang['grey']='Cinza';
$lang['above average']='Acima da média';
$lang['athletic']='Atlético';
$lang['average']='Médio';
$lang['large']='Grande';
$lang['muscular']='Muscular';
$lang['obese']='Obeso';
$lang['petite']='Petite';
$lang['you are trying to delete a photo that does not belong to you!']='Você está tentando excluir uma foto que não pertence a você! ';
$lang['you cannot delete the avatar!']='Você não pode excluir o avatar (Foto de Perfil)!';
$lang['photo id\'s']='Foto id\'s';
$lang['performer already have an approved photo id!']='performer já tem uma identificação com foto aprovado!';
$lang['performer exceeded the maximum allowed photo id number!']='performer excedeu o número máximo permitido de identificação com fotografia!';
$lang['invalid photo id']='ID foto inválida!';
$lang['performer already have an approved contract!']='performer já tem um contrato aprovado!';
$lang['performer exceeded the maximum allowed contract number!']='performer excedeu o número máximo permitido contrato!';
$lang['presonal details']='detalhes pessoais';
$lang['studio register']='Registo Studio ';
$lang['you don\'t have access to spy this performer!'] = 'Você não têm acesso a espiar este performer! ';
$lang['e-mail address successfully verified!']='Endereço de E-mail verificado com sucesso! ';
$lang['your account is in pending and you are not allowed to register performers!']='A sua conta está pendente e você não está autorizado a registrar modelos!';
$lang['performers status']='status das modelos';
$lang['my performers']='Minhas modelos';
$lang['choose your country..']='Escolha o seu país .. ';
$lang['register performer step 1']='Registe-se Passo performer 1';
$lang['subcategory']='Subcategoria';
$lang['register performer step 2']='Registe-se Passo performer 2';
$lang['register performer step 3']='Registe-se Passo performer 3';

$lang['register performer step 4']='Registe-se Passo performer 4';
$lang['performer does not belong to your studio!']='performer não pertence ao seu estúdio!';
$lang['studio login']='Studio Login';
$lang['payments settings']='Configurações de pagamentos ';
$lang['payment method edited succesfully!']='Método de pagamento editado com sucesso!';
$lang['percentage settings']='Definições de percentagem';
$lang['percentage saved!']='Percentual salvo! ';
$lang['account summary - payments']='Resumo da conta - Pagamentos ';
$lang['change percentage']='Anterar Percentagem ';
$lang['please choose at least on language']='Por favor, escolha pelo menos uma língua';
$lang['signup step 4 - about you']='Cadastre-se o passo 4 - Sobre o que você';
$lang['signup step 2 - select category']='Passo Inscrição 2 - Selecione a Categoria';
$lang['you must agre the terms and conditions']='Você deve concordar com os termos e condições';
$lang['lenth']='Lenth';
$lang['your earnings']='Seu lucro';
$lang['you have no sessions']='Você não tem sessões';
$lang['logs']='logs';
$lang['you have no chat logs']='Você não tem registros de chat';
$lang['total sessions time']='Total de sessões de tempo';
$lang['\'s statistics']='\' S estatísticas ';
$lang['\'s earnings statistics']='\' S estatísticas ganhos ';
$lang['\'s watchers statistics']='\' S observadores estatísticas ';
$lang['performer\'s banned zones page']='performer \' Banned Zones Página ';
$lang['performer\s pricing page']='performer \ s de Preços Página';
$lang['signup step 3 - pricings ']='Passo Inscrição 3 - Preço de';
$lang['account summary - earnings']='Resumo da conta - Earnings';
$lang['filter results']='Filtrar resultados ';
$lang['you have no earnings for this period.']='Você não tem ganhos para este período. ';
$lang['account settings - personal details']='Configurações da conta - dados pessoais dos';
$lang['please enter a valid percentage']='Por favor, insira uma porcentagem válida';
$lang['account settings - percentage']='Configurações da conta - Porcentagem ';
$lang['please select a payment method']='Por favor seleccione um método de pagamento ';
$lang['please enter a valid username']='Digite um nome de usuário válido';
$lang['studios registration']='Registro de Estúdios';
$lang['personal information']='Informações pessoais';
$lang['no camera was detected. you cannot go live without a working cammera connected to your computer.']='Nenhuma câmera foi detectado. Você não pode ir ao vivo sem um cammera trabalhar conectado ao seu computador. ';
$lang['you have denied access to the cam. please refresh the page and try again.']='Você tem negado o acesso ao cam. Por favor, atualize a página e tente novamente. ';
$lang['please wait while application initializes the tip...']='Por favor, aguarde enquanto o aplicativo inicializa a dica ... ';
$lang['and']='E';
$lang['please choose video quality scheme']='Por favor, escolha esquema qualidade de vídeo';
$lang['vu: ']='VU:';
$lang['horizontal resolution:  ']='Resolução horizontal:';
$lang['vertical resolution:  ']='Resolução vertical:';
$lang['frames per second:  ']='Frames por segundo:';
$lang['low scheme']='Low Esquema';
$lang['medium scheme']='Esquema Medium';
$lang['high scheme']='High Esquema';
$lang['quality:   ']='Qualidade:';
$lang['the performer is not yet ready to enter a private chat. please try again later.'] = 'Modelo não disponivel para entrar em um bate-papo privado. Por favor, tente novamente mais tarde.';
$lang['your invitation to private chat has been accepted by the performer.']='Seu convite para bate-papo privado foi aceite pelo performer.';
$lang['chips per minute: ']='Créditos por minuto:';
$lang['please choose at least one cam in order to continue to chat room. to do so, tick on \'broadcast this cam\' from any of the available cams']='Por favor, escolha pelo menos uma cam, a fim de continuar a conversar quarto. Para fazer isso, marque em \'Transmissão esta cam \' a partir de qualquer uma das carnes disponíveis ';
$lang['connection to server failed. please try again. if problem persists, please contact your administrator.']='A ligação ao servidor falhou. Por favor, tente novamente. Se o problema persistir, por favor, contate o administrador .';
$lang['connection rejected by server. please re-login and try again.']='Conexão rejeitada pelo servidor. Por favor, re-login e tente novamente. ';
$lang['connection closed or reset by server. please refresh the page to reconnect.']='Conexão fechada ou redefinir por servidor. Por favor, atualize a página para se reconectar. ';
$lang['performer entered a private chat or has logged off.']='performer entrou em um chat privado ou fez logoff. ';
$lang['you have been banned from this performer\'s chatroom.']='Você foi banido da sala de chat pela performer. ';
$lang['you have been kiked out from this performer\'s chatroom. ']='Você foi EXPULSO da sala de chat por esta performer. ';
$lang['        performer left the chatroom.'] = '        Modelo saiu da sala de bate-papo.';
$lang['you are not connected to server. please refresh page.']='Você não está conectado ao servidor. Por favor, atualize a página. ';
$lang['your free time has expired']='Seu tempo livre expirou ';
$lang['you ran out of chips. please buy more chips and re-enter chat']='Você ficou sem Créditos. Por favor, compre mais Créditos e voltar a entrar conversar ';
$lang['rec']='Rec';
$lang['stop rec']='Stop Rec';
$lang['chatline beep']='Tocar som de bate-papo';
$lang['user\'s volume:']='User \' s de volume: ';
$lang['private public']='Simples';
$lang['public']='Livre';
$lang['play!']='Play!';
$lang['stop!']='Parar!';
$lang['kick!']='Expulsar!';
$lang['ban!']='Banir! ';
$lang['a user has invited you to a private chat!']='Um usuário convidou você para bate-papo privado! ';
$lang['performer\'s volume:']='performer \' s de volume: ';
$lang['choose camera']='Escolha câmera';
$lang['private chat is over. please dress up and go back online.']='Bate-papo privado é longo. Por favor, vestir-se e voltar a ficar online. ';
$lang['private chat is over. performer is now dressing up.']='Bate-papo privado é longo. performer agora é vestir-se. ';
$lang['please specify a nickname before continuing to chatroom!']='Por favor, especifique um apelido antes de continuar para a sala de bate-papo!';
$lang['you have invited the performer to private chat. waiting for reply...  this may take a few seconds.']='Você convidou o cantor para bate-papo privado. Aguardando resposta ... Isso pode demorar alguns segundos. ';
$lang['nude and private']='Simples e Privado';
$lang['account summary']='Resumo da conta ';
$lang['your account has <span class="red">%s %s</span> the equivalent of<span class="red"> %s</span>.'] = 'Your account has <span class="red">%s %s</span> the equivalent of<span class="red"> %s</span>.';
$lang['your percentage']='O percentual de ';
$lang['nr. of your performers']='Nr. das suas modelos';
$lang['%s performers']=' %s Modelos';
$lang['performer contract status']='Estado de contrato performer ';
$lang['performer photo id status']='Foto performer estado ID';
$lang['add performer']='Adicionar performer';
$lang['apply filters']='Aplicar Filtros';
$lang['full name']='Nome completo';
$lang['balance']='Balance';
$lang['studios login']='Área de Estúdio';
$lang['login as studio']='Entrar como estúdio ';
$lang['studio administration panel']='Painel de administração Studio';
$lang['account summary - change password']='Resumo da conta - Alterar senha ';
$lang['you have been kiked out from this performer\'s chatroom. ']='Você foi KIKED a partir desta performer \' s sala de chat. ';
$lang['no_migrations_found']='Nenhuma migração encontrado';
$lang['multiple_migrations_version']='Várias migrações versão ';
$lang['migration_not_found']='Migration_not_found';
$lang['multiple_migrations_name']='Multiple_migrations_name';
$lang['migration_class_doesnt_exist']='Migration_class_doesnt_exist';
$lang['wrong_migration_interface']='Wrong_migration_interface';
$lang['invalid_migration_filename']='Invalid_migration_filename';
$lang['total sessions duration']='Duração total sessões';
$lang['total sessions']='Total de sessões ';
$lang['total earnings']='Total de lucros';
$lang['videos earnings']='lucro dos Vídeos';
$lang['gifts earnings']='lucro dos presentes';
$lang['photos earnings']='lucro das fotos ';
$lang['private earnings']='Lucro do Privado ';
$lang['true private earnings']='Lucro do Exclusivo';
$lang['peek earnings']='Lucros do Espião ';
$lang['nude earnings']='Lucros do Simples';
$lang['private viewers']='telespectadores privados ';
$lang['true private viewers']='telespectadores exclusivos';
$lang['peek viewers']='telespectadores espião ';
$lang['nude viewers']='dos telespectadores simples ';
$lang['free viewers']='telespectadores grátis ';
$lang['premim videos viewers']='Premim vídeos telespectadores';
$lang['paid photos viewers']='fotos pagos telespectadores';
$lang['gifts']='presentes';
$lang['admin actions']='acções admin ';
$lang['free earnings']='resultados grátis ';
$lang['the %s already exists!']='The %s já existe!';
$lang['your account has been suspended.']='Sua conta foi suspensa.';
$lang['your account is pending. please verify you email and confirm registration!']='Conta pendente. Por favor, verifique seus emails pois lhe enviamos um email para ativa-la!';
$lang['too many attempts. you can try again in 20 seconds.']='Muitas tentativas. Você pode tentar novamente em 20 segundos. ';
$lang['invalid user/password']='Usuário/Senha Inválido';
$lang['invalid old password']='Password idosa inválida';
$lang['you are not allowed to use this username!']='Você não tem permissão para usar este nome de usuário!';
$lang['invalid data.']='dados inválidos.';
$lang['email already exists in our database.']='E-mail já existe no nosso banco de dados.';
$lang['invalid user/email']='Inválido usuário / e-mail';
$lang['invalid  birthdate']='Data de nascimento inválida';
$lang['please select at least one language']='Por favor, selecione pelo menos uma língua';
$lang['invalid  language']='Linguagem inválido ';
$lang['invalid  category/subcategory']='Inválido categoria / subcategoria';
$lang['you must agree with our terms of service']='Você deve concordar com os nossos Termos de Serviço, a fim de continuar. ';
$lang['%s contract inexistent/invalid']='Para validar o contrato carregue uma foto de sua identidade';
$lang['performer photo id inexistent/invalid']='ID foto performer inexistente / inválido!';
$lang['photo does not exist.']='Foto Inexistente.';
$lang['avatar does not exist.']='Avatar Inexistente.';
$lang['invalid payment method']='Método de pagamento inválido ';
$lang['release amount cannot be under %s %s']='Montante liberação não pode estar sob %s %s';
$lang['%s chips cannot be set more then %s credits']=' %s Créditos não pode ser definido mais de %s Créditos!';
$lang['%s chips cannot be set less then %s credits']=' %s Créditos não pode ser definida a menos de %s Créditos!';
$lang['invalid %s']=' %s inválido';
$lang['invalid country']='País inválido';
$lang['please choose the type']='Por favor, escolha o tipo de';
$lang['invalid type']='Tipo inválido!';
$lang['invalid video type']='Tipo de vídeo inválido';
$lang['video price cannot be lower than %s chips']='Preço de vídeo não pode ser inferior a R$ %s! ';
$lang['video price cannot be greater than %s chips']='Preço de vídeo não pode ser maior que %s Créditos! ';
$lang['invalid folder']='Pasta inválido';
$lang['invalid message id']='Id mensagem inválido';
$lang['percentage cannot be lower than %s']='Percentual não pode ser inferior a %s ';
$lang['percentage cannot be more than %s']='Percentual não pode ter mais do que %s ';
$lang['field names can contain just alphanumeric values']='Os nomes de campo pode conter apenas valores alfanuméricos ';
$lang['application configuration settings']='Definições de configuração do aplicativo ';
$lang['enable debugging']='Ativar a depuração';
$lang['email activation']='Activation Email';
$lang['require unique emails']='Exigir-mails exclusivos ';
$lang['support email']='Email Suporte';
$lang['support name']='Name Suporte';
$lang['website name']='Nome do site ';
$lang['default theme']='Tema Padrão';
$lang['website title']='Title Site';
$lang['website description']='Descrição do site ';
$lang['website keywords']='palavras-chave website';
$lang['memcache settings']='Configurações Memcache';
$lang['enable memcache']='Ativar memcache';
$lang['website currency settings']='configurações de moeda do site ';
$lang['website currency type']='Site Tipo Moeda';
$lang['real currency name']='Nome da moeda Real';
$lang['real currency other symbol']='Moeda real outro símbolo';
$lang['chips name']='Name Créditos';
$lang['chips per currency unit']='Reais por unidade de moeda';
$lang['what currency will be printed in lang']='Que moeda vai ser impresso em lang ';
$lang['website prices settings']='Configurações de Preços do Site';
$lang['min true private chips price']='Preço Min do Chat Exclusivo';
$lang['max true private chips price']='Preço Max do Chat Exclusivo';
$lang['min private chips price']='Preço Min do Chat Privado';
$lang['max private chips price']='Preço Max do Chat Privado';
$lang['min peek chips price']='Preço Min do Chat Espião';
$lang['max peek chips price']='Preço Max do Chat Espião';
$lang['min nude chips price']='Preço Min do Chat Simples';
$lang['max nude chips price']='Preço Max do Chat Simples';
$lang['min paid video chips price']='Preço Min do vídeo paga';
$lang['max paid video chips price']='Preço Max do vídeo paga';
$lang['min paid photo chips price']='Preço Min da foto paga';
$lang['max  paid photo chips price']='Preço Max da foto paga';
$lang['chat settings']='Configurações de bate-papo ';			
$lang['free chat time limit for nonlogged users']='Prazo de bate-papo gratuito para usuários nonlogged ';			
$lang['free chat time limit for logged with no credits']='Prazo de bate-papo livre para conectado sem Créditos ';			
$lang['free chat time limit for logged with credits']='Prazo de bate-papo livre para conectado com Créditos '	;		
$lang['minimum paid chat time']='Mínimo pago tempo de bate-papo ';			
$lang['users settings']='Configurações de usuário';
$lang['ban expiration time']='Tempo de expiração Ban';			
$lang['affiliate settings']='Definições de afiliados ';			
$lang['transaction percentage']='Percentagem Transaction ';			
$lang['fms settings']='Configurações FMS';
$lang['fms secret hash']='FMS hash de segredo ';
$lang['application license key']='Chave de licença de aplicativos ';
$lang['google api settings']='Configurações de API do Google';
$lang['enable google translation']='Ativar Google Translation ';
$lang['google api key']='API Key Google ';
$lang['socket']='Soquete';
$lang['url ']='URL';
$lang['port']='Porta';
$lang['secret key']='Secret Key';
$lang['http port']='Http Porta ';
$lang['a']='A';
$lang['b']='B';
$lang['c']='C';
$lang['d']='D';
$lang['e']='E';
$lang['f']='F';
$lang['true_private']='Exclusivo';
$lang['premium_video']='Video premium';
$lang['gift']='Presente';
$lang['admin_action']='Ação Admin';
$lang['credit']='Crédito';
$lang['chargeback']='Chargeback';
$lang['void']='Vazio';

$lang['toss']='<p><strong><h3>DECLARAÇÃO DE CUMPRIMENTO  DA LEI AMERICANA<br>
  U.S.C. TITLE 18, SECTION 2257</h3></strong></p> </br></br>
Todos os modelos, atores, atrizes ou pessoas que aparecem em qualquer ato ou simulação de conduta sexual neste website, possuem ou tinham ao menos 18 anos de idade no momento da realização das gravações, fotografia ou transmissão de vídeo. </br></br>
Todas as outras representações visuais neste website são isentas da provisão da lei Americana 18 U.S.C. §2257 e 28 C.F.R. 75 porque não retratam conduta especificada na lei 18 U.S.C §2256 (2) (A) a (E) [renumerado (i) até (v)], mas são meras representações de conduta não sexualmente explícita ou são isentas pois a representação foi criada antes de 3 de Julho de 1995. </br></br>
Com respeito a qualquer imagem de representação sexual ou não neste website, todas as pessoas tinham ao menos 18 anos de idade no momento da criação destas representações. </br></br>
Os donos e operadores deste website não são os produtores primários das representações ou conteúdo aqui disponível. Todos os documentos necessários estão em posse dos produtores primários do conteúdo nos termos da lei Americana U.S.C. Title 18, Section 2257. </br></br>
<b>Registros e documentos contratuais e que comprovam a maioridade legal dos participantes em posse de:</b></br></br>
20VerCam </br>';


$lang['tos']='


<h3>Termos de Uso </h3>
			  <p>
Este contrato destina-se a pessoas interessadas em se cadastrar no website Akinews.COM para utilizar o seu serviço de transmissão de vídeo. 
</p>
O(A) pessoa física qualificada a assinar este formulário eletrônico, que assume neste ato, ser maior de idade, não estar influente sob qualquer tipo de entorpecente, doravante denominado USUÁRIO. 
<h4>
  1. OBJETO 
</h4>
<p>
O website Akinews se compromete a disponibilizar um ambiente virtual, por onde os USUÁRIOS devidamente cadastrados no website, possam interagir com pessoas, por meio de texto, áudio e/ou vídeo, em tempo real. 
</p>
<p>
O website Akinews destina-se a pessoas que tenha maioridade legal. O website contém material orientado para adultos que pode ser ofensivo ou ilegais para algumas pessoas. Por favor, não prossiga se você não possuir maioridade legal ou se sentir ofendido com tais materiais. 
</p>
<h4>
  2. CADASTRO 
</h4>
<p>
Ao efetuar o cadastro, o USUÁRIO assume que todas as informações providenciadas são verdadeiras e que tenha lido e compreendido este termo. O USUÁRIO assume ser maior de idade. 
</p>
<p>
O USUÁRIO é responsável por todo conteúdo gerado e divulgado através de sua conta no website Akinews, e neste ato, assume, apenas divulgar imagem e informações que possua direito sobre. 
</p>
<p>
O USUÁRIO assume a responsabilidade de zelar pela sua conta, não fornecer suas informações a terceiros, utilizar o website de forma honesta, não tentar burlar o sistema de segurança do website, não utilizar palavras ofensivas ou protegidas por direitos autorais, bem como, não ofender ou prejudicar outros visitantes, e/ou parceiros do website Akinews. 
</p>
<p>
No caso de acesso indevido por terceiros em sua conta, o USUÁRIO se responsabiliza a comunicar o website Akinews imediatamente, através de um dos canais de atendimento disponíveis no website. 
</p>
<p>
Os dados cadastrais fornecidos pelo USUÁRIO, não serão, em hipótese alguma, divulgados a terceiros ou utilizados para fins comerciais, sendo armazenados apenas, para fins legais e atualização cadastral. 
O Akinews não se responsabiliza pelo uso de terceiros na conta do usuário, caso ocorra um comprometimento dos dados de login. É de responsabilidade do USUÁRIO manter seus dados de login em ambiente seguro, evitando qualquer acesso não autorizado em sua conta. 
</p>
<p>
Não será permitido a venda, doação ou transferência de titularidade do cadastro do USUÁRIO para terceiros. 
</p>
<p>
É vedada a prática de quaisquer atividade ilícita dentro do website Akinews e serviço oferecido. Ficando de responsabilidade do USUÁRIO, tais práticas, sendo elas ofensivas ou não aos demais usuários do website. 
</p>
<p>
O website Akinews se reserva no direito de suspender, bloquear ou cancelar a conta do USUÁRIO, que violar qualquer um dos termos deste contrato, ou a qualquer momento que julgar necessário, devido a mau uso das funcionalidades do serviço disponibilizado. 
</p>
<p>
O Akinews se reserva no direito de bloquear o saldo de créditos da conta do(a) USUÁRIO, caso hajam suspeitas de fraudes vinculadas a modelos ou outras contas de usuário. Caso comprovada essas fraudes, O Akinews se reserva no direito de cancelar a conta do USUÁRIO e efetuar o estorno dos créditos restantes de sua conta, respeitando as normas do intermediador de pagamento. 
</p>
<h4>
  3. SERVIÇOS 
</h4>
<p>
O website Akinews disponibilizará ao USUÁRIO um ambiente virtual, por onde o USUÁRIO poderá interagir com outras pessoas do website através de texto, áudio e/ou vídeo em tempo real. O ambiente virtual será disponível 24 horas por dia, 7 dias por semana, salve períodos de interrupção por problemas maiores que possam ocorrer no sistema, ou em seu serviço de hospedagem. 
</p>
<p>
O USUÁRIO é livre para utilizar o serviço quando bem desejar e da forma que desejar, desde que não prejudique ou ofenda demais usuários ou visitantes do website. 
</p>
<h4>
  4. PAGAMENTOS 
</h4>
<p>
Para utilizar o serviço, o USUÁRIO deverá efetuar o pagamento através do sistema de cobrança Pag Seguro ( www.pagseguro.com.br ). Todo pagamento em reais é convertido em créditos na proporção de 90 centavos = 100 créditos, e creditado na conta do USUÁRIO dentro do website Akinews. 
</p>
<p>
Antes de entrar em uma das modalidades de sessão paga com outras pessoas do website, o USUÁRIO assume estar ciente da tarifa cobrada. A tarifa cobrada é expressa em créditos dentro do website, e será deduzida a cada minuto ou o equivalente, pelo tempo em que o USUÁRIO permanecer conectado em uma sessão paga. 
</p>
<p>
Em hipótese alguma os créditos utilizados durante uma sessão paga, ou doados a outro usuário do website, serão devolvidos para a conta do USUÁRIO. 
</p>
<p>
Usuários que possuírem o mínimo de 1 crédito em conta, poderá conversar sem limites na modalidade de Chat Grátis. Sendo necessário o mínimo de 2 créditos para poder iniciar uma sessão de Chat Pago. 
</p>
<p>
As sessões pagas dentro do website são denominadas de Chat Simples; Chat Privado; Chat Privado Exclusivo; Chat Espião. 
</p>
<h4>
  5. UTILIZAÇÃO 
</h4>
<p>
O USUÁRIO se compromete a utilizar o serviço com responsabilidade nas informações publicadas. Todo conteúdo publicado, seja ele foto, vídeo, texto ou áudio, através da conta do USUÁRIO nas dependências do website, é de inteira responsabilidade do USUÁRIO. 
</p>
<p>
O USUÁRIO declara ser responsável e ter direitos autorais sobre as suas publicação no website, sejam elas através de foto, vídeo, texto ou áudio. 
</p>
<p>
O USUÁRIO se compromete a não solicitar ou compartilhar contatos pessoais como, telefone, Skype, email, Facebook ou qualquer outro meio que permita o relacionamento entre dois ou mais usuários, fora das dependências do website. 
</p>
<p>
O USUÁRIO se compromete a não divulgar ou mencionar quaisquer outro website que não seja a marca Akinews.COM, dentro do website. 
</p>
<p>

O USUÁRIO se compromete a não ofender verbalmente, ou através de suas atitudes, os demais USUÁRIOS do website. 
</p>
<p>
Concordando com este Termo de Serviço, o usuário declara ter lido todas as cláusulas de UTILIZAÇÃO, as compreende e concorda com política nela descrita. A violação de quaisquer cláusula de UTILIZAÇÃO, acarretará no bloqueio permanente da conta do USUÁRIO no site, retendo o saldo remanescente em conta para cobrir eventuais estornos ou despesas. 
</p>
<h4>
  6. PRAZO, VIGÊNCIA E RESCISÃO 
</h4>
<p>
O presente contrato vigorará por tempo indeterminado, a contar a partir da data de aceitação deste, e efetuação do cadastro por parte do USUÁRIO. 
</p>
<p>
O website Akinews, se reserva no direito de rescindir o presente contrato, a qualquer momento, mediante a comunicação prévia para o e-mail cadastrado no website pelo USUÁRIO. 
</p>
<p>
O USUÁRIO poderá rescindir o presente contrato, a qualquer momento, mediante a comunicação prévia, por e-mail, disponível nos canais de atendimento do website. 
</p>
<p>
No caso de rescisão do contrato, o website Akinews se responsabiliza a cancelar a conta do USUÁRIO em até 5 dias úteis. 
</p>
<p>
Em hipótese alguma, os créditos remanescentes na conta do USUÁRIO, serão devolvidos em forma de moeda corrente ao USUÁRIO, no caso de rescisão do contrato ou cancelamento de sua conta por uso indevido. 
</p>
<h4>
  7. MODIFICAÇÕES NOS TERMOS E CONDIÇÕES 
</h4>
<p>
O website Akinews se reserva no direito de alterar ou atualizar este termos e condições de utilização do website a qualquer momento, sem haver a necessidade de comunicação prévia. 
</p>
<p>

Para dirimir eventuais dúvidas ou litígios emergentes do presente Contrato, elegem as Partes, com renúncia expressa a qualquer outro, o foro central da Comarca da Capital do Estado de São Paulo.
</p>';


$lang['pp']='
<p>
Este documento descreve os tipos de informações pessoais que recebemos quando você usa os serviços do "Akinews" e também alguns dos métodos que usamos para proteger as informações. Esperamos que isso ajude você a tomar uma decisão consciente sobre o compartilhamento de informações conosco. 
</p>
<h4>
  Objetivo 
</h4>
<p>
Este aviso contém os destaques de toda a Política de Privacidade do "Akinews", que descreve detalhadamente as práticas de privacidade que são aplicadas aos produtos e serviços do "Akinews". 
</p>
<p>
Informações pessoais e outros dados que coletamos 
O "Akinews" coleta informações pessoais quando você se cadastra em um serviço do "Akinews" ou também quando fornece informações voluntariamente. Podemos combinar as informações pessoais fornecidas por você com as informações de outros serviços do "Akinews" ou de terceiros para proporcionar uma experiência de uso melhor, incluindo a personalização do conteúdo para você. 
</p>
<p>
O "Akinews" usa cookies e outras tecnologias para aprimorar a sua experiência on-line e para saber como você usa nossos serviços com a finalidade de melhorar a sua qualidade. 
</p>
<p>
Os servidores do "Akinews" registram as informações automaticamente quando você visita nosso website ou quando usa algum de nossos produtos, incluindo o URL, o endereço IP, o tipo de navegador e o idioma, a data e a época de sua solicitação. 
</p>
<h4>
  Usos 
</h4>
<p>
Podemos usar as informações pessoais para fornecer os serviços solicitados por você, incluindo os serviços que exibem publicidade e conteúdo personalizado. 
</p>
<p>
Podemos usar as informações pessoais também para auditoria, pesquisa e análise para conduzir e aprimorar as tecnologias e os serviços do "Akinews". 
</p>
<p>
Podemos compartilhar com terceiros informações não pessoais consolidadas. 
</p>
<p>
Quando precisamos de terceiros para nos ajudar no processamento de suas informações pessoais, solicitamos que eles cumpram a nossa Política de Privacidade e as outras medidas de segurança e de confidencialidade apropriadas. 
</p>
<p>
Podemos também compartilhar informações com terceiros em circunstâncias limitadas, incluindo a necessidade de estar de acordo com o processo legal, impedindo a fraude ou o dano iminente e garantindo a segurança de nossa rede e serviços. 
</p>
<p>
O "Akinews" processa as informações pessoais em seus servidores nos Estados Unidos da América. 
</p>
<h4>
  Suas opções 
</h4>
<p>
Oferecemos a você opções quando perguntamos suas informações pessoais ou sempre que possível. Você pode obter mais informações sobre suas opções nos avisos de privacidade ou nas perguntas frequentes para serviços específicos. 
</p>
<p>
Você pode optar por não fornecer informações pessoais ou recusar os cookies em seu navegador, mas alguns de nossos recursos ou serviços podem não funcionar corretamente. 
</p>
<p>
Estamos nos esforçando para fornecer acesso às suas informações pessoais por meio de solicitação e para permitir que você corrija os dados se eles estiverem errados e exclua-os, sempre que possível.
</p>';

$lang['search for...'] = 'Procurar por';
