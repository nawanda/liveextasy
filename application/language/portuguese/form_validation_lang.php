<?php
$lang['required']			= "O campo %s é necessário.";
$lang['isset']				= "O campo %s está vazio.";
$lang['valid_email']		= "O campo %s requer um email válido.";
$lang['valid_emails']		= "O campo %s requer e-mails válidos.";
$lang['valid_url']			= "O campo %s requer uma URL válida.";
$lang['valid_ip']			= "O campo %s requer um IP válido.";
$lang['min_length']			= "O campo %s requer pelo menos %s caracteres.";
$lang['max_length']			= "O campo %s não pode exceder %s caracteres.";
$lang['exact_length']		= "O campo %s deve ter exatamente %s caracteres.";
$lang['alpha']				= "O campo %s requer somente alfanuméricos.";
$lang['alpha_numeric']		= "O campo %s deve conter apenas alfanuméricos.";
$lang['alpha_dash']			= "O campo %s requer caracteres alfanuméricos.";
$lang['numeric']			= "O campo %s requer números, somente.";
$lang['is_numeric']			= "O campo %s requer números, somente.";
$lang['integer']			= "O campo %s requer um inteiro.";
$lang['regex_match']		= "O campo %s é inválido.";
$lang['matches']			= "O campo %s não coincide com o campo %s.";
$lang['is_natural']			= "O campo %s requer números positivos.";
$lang['is_natural_no_zero']	= "O campo %s requer um número acima de zero.";
$lang['decimal']			= "O campo %s requer um número decimal.";
$lang['less_than']			= "O campo %s requer um número menor que %s.";
$lang['greater_than']		= "O campo %s requer um número maior que %s.";
/* End of file form_validation_lang.php */
/* Location: ./system/language/portuguese/form_validation_lang.php */