<script type="text/javascript">
jQuery(function($){

	$.validator.setDefaults({
		validClass:"success",
		errorElement: "span",
		errorPlacement: function(error, element) {
			error.appendTo($(element).next('span').next('span'));
		}
	});
	
	$.validator.addMethod("uniqueUserName", function(value,element) {
		return true;
	}, "Not Available!");
	
	$(".percentage").validate({			 
		success: function(label) {
	    	label.addClass("valid");
	   },
		rules: {
			percentage: {
				required: true,
				min:0,
				max:100
			}
		}, 
		messages: {
			percentage: 						"<?php echo lang('Please enter a valid percentage') ?>"
		}
	});
}); 
</script>	
		<div class="content">
			<div class="title">
				<?php  echo lang('Account Settings - Percentage')?>
			</div>
			<div class="dark_gray italic" id="studio_settings">
				<?php echo form_open(current_url(), 'class="percentage"')?>
				<div>
					<label><span class="italic bold"><?php echo lang('Percentage') ?>:</span></label>
					<?php echo form_input(array('name' => 'percentage', 'id' => 'percentage', 'value' => set_value('percentage' , $this->user->percentage))) ?>
					<span class="error message" htmlfor="percentage" generated="true" ><?php echo form_error('percentage')?></span>
				</div>
					<br/>
				<div>
					<?php echo form_submit('submit',lang('Save'), 'class="red"  ')?>
				</div>
				<?php echo form_close() ?>
			<div class="clear"></div>
		</div>
	</div>