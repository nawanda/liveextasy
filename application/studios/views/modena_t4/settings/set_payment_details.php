<div class="content" style="margin-left:50px;">
    <div class="title">
        <?php echo lang('Enter Payment Details') ?>
    </div>	
    <?php echo form_open('settings/set-payment-details/' . $this->uri->segment(3), 'class="set_payment_details"') ?>
        <?php echo form_hidden('payment_method', $this->uri->segment(3)) ?>		
        <div class="dark_gray italic payment_details" id="studio_settings">
            <?php
            $fields = unserialize($payment_details->fields);
            foreach ($fields as $field):?>
                <div>
                    <label><span class="bold" id="performerTexAlign"><?php echo $field ?>:</span></label>
                    <?php echo $escaled_field = url_title($field, '_', TRUE);?>
                    <?php echo form_input($escaled_field, set_value($escaled_field))?>
                    <span class="error" htmlfor="<?php echo $escaled_field?>" generated="true"><?php echo form_error($escaled_field)?></span>
                </div>
            <?php endforeach ?>
            <div>
                <label><span class="bold"><?php echo lang('Release Amount') ?>:</span></label>
                <?php echo form_input('rls_amount', set_value('rls_amount')) ?> 
                <span class="error" htmlfor="rls_amount" generated="true"><?php echo (form_error('rls_amount')) ? form_error('rls_amount') : sprintf(lang('Minim %s'), print_amount_by_currency($payment_details->minim_amount, TRUE, TRUE)) ?></span>					
            </div>	
            <br/>
            <label></label><?php echo form_submit('submit', lang('Submit')) ?>
        </div>
    <?php echo form_close() ?>
    <br/>
    <div class="clear"></div>
</div>