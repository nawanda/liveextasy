<script type="text/javascript">
    function confirm_delete() {
        if (confirm("<?php echo lang('Are you sure you want to delete this photo?')?>")) {
            window.location.replace('<?php echo site_url('performers/photos/delete/'.$photo->photo_id)?>');
        }
    }
</script>
	<div class="content">
		<div class="title">
			<?php echo lang('Edit Photo') ?>
		</div>
		<div id="photo">
            <?php echo form_open(current_url())?>
			<div class="dark_gray italic">
				<div>
					<label><span class="bold"><?php echo lang('Photo title') ?></span></label>
					<?php echo form_input('title', $photo->title)?>
					<span class="error" htmlfor="title" generated="true" ><?php echo form_error('title')?></span>
				</div>
				<div>
					<label><span class="bold"><?php echo lang('Gallery') ?></span></label>
					<?php echo form_dropdown('is_paid',$is_paid,set_value('is_paid',$photo->is_paid))?>
					<span class="error" htmlfor="title" generated="true" ><?php echo form_error('is_paid')?></span>
				</div>
				<div>
					<?php echo form_submit('submit', lang('Save'),'class="red" style="width:110px;"')?>
					<?php echo form_button(array('id' => 'delete-bttn', 'class'=>'red', 'content' => 'Delete', 'onclick' => 'confirm_delete()', 'style' => 'width:110px;'))?>
				</div>
                <?php echo form_close()?>
            </div>
        </div>
    </div>