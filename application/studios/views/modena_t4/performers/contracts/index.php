<link rel="stylesheet" type="text/css" href="<?php echo assets_url()?>css/table.css" media="screen" />
	<div class="content">
		<div class="title">
			<?php echo lang('Contracts - Summary') ?>
		</div>
		
		<a href="<?php echo site_url('performer/contracts/add')?>" style="float:right"><?php echo form_button('add', lang('Add contract'),'class="red"')?></a>
		<div style="clear:both"></div>
		<br />
        <table class="data display datatable" >
        	<thead>
            <tr>
                <th width="25%"><?php echo lang('Add date') ?></th>
                <th width="25%"><?php echo lang('Status') ?></th>
                <th width="25%"><?php echo lang('View') ?></th>
                <th width="25%"><?php echo lang('Actions')?></th>
            </tr>
            </thead>
            <?php if(sizeof($contracts) == 0):?>
	            <tr>
	            	<td colspan="4" style="text-align:center"><?php echo lang('You have no contracts added')?></td>
	            </tr>
            <?php else:?>      
            	<?php $i = 0 ?>      
	            <?php foreach ($contracts as $contract): ?>
					<tr class="<?php echo ($i % 2==0)?'even':'odd'?>">
	                	<td><?php echo date('Y-m-d',$contract->date)?></td>		                	
	                	<?php 
	                	if($contract->status == 'approved'):
							$class = 'approved';									                				
	                	elseif($contract->status == 'pending'):
	                		$class = 'pending';
						else:
	                		$class = 'rejected';
	                	endif;
	                	?>
	                	<td><span class="icon <?php echo $class?>"></span></td>
		                <td>
							<a href="<?php echo main_url('uploads/performers/' . $contract->performer_id . '/others/' . $contract->name_on_disk)?>" target="_blank"><span class="icon spy"></span></a>			                
		                </td>
		                <td>
		                	<a href="<?php echo site_url('performer/contracts/delete/'.$contract->id)?>" onclick="return confirm('<?php echo lang('Are you sure you want to delete this item')?>')"><span class="icon trash"></span></a>
		               </td>
		            </tr>
		            <?php $i++ ?>
	            <?php endforeach ?>
       		<?php endif?>
        </table>
		<div class="clear"></div>
	</div>