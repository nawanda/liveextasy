<script type="text/javascript">
jQuery.validator.setDefaults({
	validClass:"success",
	errorElement: "span",
	errorPlacement: function(error, element) {
		error.appendTo($(element).next('span').next('span'));
	}
});
jQuery(function($){

	$.validator.addMethod("checkcat", function(value,element) {		
		if ($("input:checkbox:checked").length  > 0){			
            return true;
		}
        else
        {       
            return false;
        }
	}, "<?php echo lang('Please select a category!')?>");
	
	$(".register_performer").validate({
		success: function(label) {
	    	label.addClass("valid")
	   },
		rules: {
			cat: {
				checkcat: true
			}
		}, 
		messages: {
			cat: 					"<?php echo lang('Please select at least one category') ?>"
		},
		debug: false
	});
});
</script>	
		<div class="content">
			<div class="title">
				<?php echo lang('Signup step 2 - Select Category') ?>
			</div>
			<?php echo form_open(current_url(), 'class="register_performer"') ?>
			<div class="dark_gray italic" id="register_performer">
				<div>
					<label><span class="bold"><?php echo lang('Categories') ?>:</span></label>
					<?php if(sizeof($cat['main_categories']) > 0): ?>
						<ul style="list-style-type: none; display:inline-block; margin-top: 0px;">
						<?php foreach($cat['main_categories'] as $main_category): ?>
							<li>
								<label  style="margin:0px; text-align: left;">
									<?php echo form_checkbox('category[]', $main_category->id,set_checkbox('category',$main_category->id))?>
									<span class="bold"><?php echo lang($main_category->name) ?></span>
								</label>
							</li>
							<?php if(sizeof($cat['sub_categories']) > 0 && isset($cat['sub_categories'][$main_category->id]) && sizeof($cat['sub_categories'][$main_category->id]) > 0):?>
								<li>
									<ul style="list-style-type: none; ">
										<?php foreach($cat['sub_categories'][$main_category->id] as $sub_category): ?>
											<li>
												<label style="margin:0px; text-align: left;">
													<?php echo  form_checkbox('category[]', $sub_category->id,set_checkbox('category',$sub_category->id))?>
													<span class="bold"><?php echo lang($sub_category->name)?></span>
												</label>
											</li>
										<?php endforeach?>
									</ul>
								</li>

							<?php endif ?>
						<?php endforeach ?>
						</ul>
					<?php endif ?>
					<?php echo form_hidden('cat',1) ?>
					<span class="error" htmlfor="cat" generated="true" style="vertical-align: top;"><?php echo form_error('category')?></span>
				</div>
				<div>
					<?php echo form_submit('submit', lang('Continue'), 'class="red"')?>
				</div>
				<div class="clear"></div>
			</div>
			<?php echo form_close()?>
		</div>