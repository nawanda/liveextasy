	<div class="content" >
		<div class="title">
			<?php echo lang('Performer\'s Profile Page')?>
		</div>
		<div id="profile" >
			<div class="left">
				<div class="dark_gray italic">
					<?php echo form_open()?>
						<div style="margin-left : -20px;">
							<label><span class="bold"><?php echo lang('Description') ?>:</span></label>
							<?php echo form_textarea('description',set_value('description', (isset($performer->description)) ? $performer->description : null))?>
							<span class="error" htmlfor="description" generated="true"><?php echo form_error('description')?></span>
						</div>
						<div style="margin-left : -20px;">
							<label><span class="bold"><?php echo lang('What turns me on') ?>:</span></label>
							<?php echo form_textarea('what_turns_me_on',set_value('what_turns_me_on', (isset($performer->what_turns_me_on)) ? $performer->what_turns_me_on : null))?>
							<span class="error" htmlfor="what_turns_me_on" generated="true"><?php echo form_error('what_turns_me_on')?></span>
						</div>
						<div style="margin-left : -20px;">
							<label><span class="bold"><?php echo lang('What turns me off') ?>:</span></label>
							<?php echo form_textarea('what_turns_me_off',set_value('what_turns_me_off', (isset($performer->what_turns_me_off)) ? $performer->what_turns_me_off : null))?>
							<span class="error" htmlfor="what_turns_me_off" generated="true"><?php echo form_error('what_turns_me_off')?></span>
						</div>
						<div style="margin-left : -20px;">
							<label><span class="bold"><?php echo lang('Birthday') ?>:</span></label>
							<?php echo form_dropdown('day',$days,set_value('day', (isset($performer->birthday)) ?  date('d',$performer->birthday) : null), 'class="small"')?>
							<?php echo form_dropdown('month',$months,set_value('month', (isset($performer->birthday)) ? date('m',$performer->birthday) : null ), 'class="small"')?>
							<?php echo form_dropdown('year',$years,set_value('year', (isset($performer->birthday)) ? date('Y',$performer->birthday) : null ), 'class="small"')?>
							<span class="error" htmlfor="birthday" generated="true"><?php echo form_error('year')?></span>
						</div>
						<div style="margin-left : -20px;">
							<label><span class="bold"><?php echo lang('Gender') ?>:</span></label>
							<?php echo form_dropdown('gender',$gender,set_value('gender', (isset($performer->gender)) ? $performer->gender : null),NULL,TRUE)?>
							<span class="error" htmlfor="gender" generated="true"><?php echo form_error('gender')?></span>
						</div>
						<div style="margin-left : -20px;">
							<label><span class="bold"><?php echo lang('Ethnicity') ?>:</span></label>
							<?php echo form_dropdown('ethnicity',$ethnicity,set_value('ethnicity', (isset($performer->ethnicity)) ? $performer->ethnicity : null),NULL,TRUE)?>
							<span class="error" htmlfor="ethnicity" generated="true"><?php echo form_error('ethnicity')?></span>
						</div>
						<div style="margin-left : -20px;">
							<label><span class="bold"><?php echo lang('Sexual prefference') ?>:</span></label>
							<?php echo form_dropdown('sexual_prefference',$sexual_prefference,set_value('sexual_prefference', (isset($performer->sexual_prefference)) ? $performer->sexual_prefference : null),NULL,TRUE)?>
							<span class="error" htmlfor="sexual_prefference" generated="true"><?php echo form_error('sexual_prefference')?></span>
						</div>
						<div style="margin-left : -20px;">
							<label><span class="bold"><?php echo lang('Height') ?>:</span></label>
							<?php echo form_dropdown('height',$height,set_value('height', (isset($performer->height)) ? $performer->height : null),NULL,TRUE)?>
							<span class="error" htmlfor="height" generated="true"><?php echo form_error('height')?></span>
						</div>
						<div style="margin-left : -20px;">
							<label><span class="bold"><?php echo lang('Weight') ?>:</span></label>
							<?php echo form_dropdown('weight',$weight,set_value('weight', (isset($performer->weight)) ? $performer->weight : null),NULL,TRUE)?>
							<span class="error" htmlfor="weight" generated="true"><?php echo form_error('weight')?></span>
						</div>
						<div style="margin-left : -20px;">
							<label><span class="bold"><?php echo lang('Hair color') ?>:</span></label>
							<?php echo form_dropdown('hair_color',$hair_color,set_value('hair_color', (isset($performer->hair_color)) ? $performer->hair_color : null),NULL,TRUE)?>
							<span class="error" htmlfor="hair_color" generated="true"><?php echo form_error('hair_color')?></span>
						</div>
						<div style="margin-left : -20px;">
							<label><span class="bold"><?php echo lang('Hair lenght') ?>:</span></label>
							<?php echo form_dropdown('hair_length',$hair_length,set_value('hair_length', (isset($performer->hair_length)) ? $performer->hair_length : null ),NULL,TRUE)?>
							<span class="error" htmlfor="hair_length" generated="true"><?php echo form_error('hair_length')?></span>
						</div>
						<div style="margin-left : -20px;">
							<label><span class="bold"><?php echo lang('Eye color') ?>:</span></label>
							<?php echo form_dropdown('eye_color',$eye_color,set_value('eye_color', (isset($performer->eye_color)) ? $performer->eye_color : null),NULL,TRUE)?>
							<span class="error" htmlfor="eye_color" generated="true"><?php echo form_error('eye_color')?></span>
						</div>
						<div style="margin-left : -20px;">
							<label><span class="bold"><?php echo lang('Cup size') ?>:</span></label>
							<?php echo form_dropdown('cup_size',$cup_size,set_value('cup_size',(isset($performer->cup_size)) ? $performer->cup_size : null),NULL,TRUE)?>
							<span class="error" htmlfor="cup_size" generated="true"><?php echo form_error('cup_size')?></span>
						</div>						
						<div style="margin-left : -20px;">
							<label><span class="bold"><?php echo lang('Build') ?>:</span></label>
							<?php echo form_dropdown('build',$build,set_value('build', (isset($performer->build)) ? $performer->build : null),NULL,TRUE)?>
							<span class="error" htmlfor="build" generated="true"><?php echo form_error('build')?></span>
						</div>	
						<div style="margin-left : -20px;">
							<?php echo form_submit('submit', lang('Save'), 'class="red"')?>
						</div>			
					<?php echo form_close()?>			
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</div>