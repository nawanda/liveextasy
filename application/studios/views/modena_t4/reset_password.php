	<div class="content">
		<?php if(isset($page_title) && $page_title != ''):?> 
            <div class="title">
	          <?php echo lang($page_title)?> 
            </div>
 	    <?php endif ?>  		
		<?php echo form_open(current_url().'?'.$_SERVER['QUERY_STRING'])?>
		<div class="gray italic reset_password"   id="user_settings" style="padding-left:140px;"> 		
			<!--  PASSWORD -->
			<div>
				<label><span class="dark_gray italic bold"><?php echo lang('Password')?></span></label>
				<?php echo form_password('password',set_value('password'))?>
				<span class="error message" htmlfor="password" generated="true"></span>
			</div>
			
			<!--  REPEAT PASSWORD -->
			<div>
				<label><span class="dark_gray italic bold"><?php echo lang('Repeat password')?></span></label>
				<?php echo form_password('rep_password',set_value('rep_password'))?>
				<span class="error message" htmlfor="rep_password" generated="true"></span>
			</div>
			<br/>
			<div>
				<?php echo form_submit('submit', lang('Update'), 'class="red"') ?> 
			</div>
		</div>
		<?php echo form_close()?>
		<div class="clear"></div>
	</div>