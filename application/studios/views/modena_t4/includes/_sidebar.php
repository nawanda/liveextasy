<div id="left_menu">
	
	<div class="menu_item <?=($this->uri->segment(3) == 'password' || ($this->uri->segment(2) == 'settings' && !$this->uri->segment(3))) ? 'selected' : NULL ?>" onclick="document.location.href='<?php echo site_url('performer/settings/password')?>';">
		<span class="icon"></span>
		<a href="<?php echo site_url('performer/settings/password')?>"><?php echo lang('Password') ?></a>
	</div>
	<div class="menu_item <?=($this->uri->segment(3) == 'profile') ? 'selected' : NULL ?>" onclick="document.location.href='<?php echo site_url('performer/settings/profile')?>';">
		<span class="icon"></span>
		<a href="<?php echo site_url('performer/settings/profile')?>"><?php echo lang('Profile') ?></a>
	</div>
	<div class="menu_item <?=($this->uri->segment(3) == 'personal-details') ? 'selected' : NULL ?>" onclick="document.location.href='<?php echo site_url('performer/settings/personal-details')?>';">
		<span class="icon"></span>
		<a href="<?php echo site_url('performer/settings/personal-details')?>"><?php echo lang('Personal details') ?></a>
	</div>
	<div class="menu_item <?=($this->uri->segment(3) == 'pricing') ? 'selected' : NULL ?>" onclick="document.location.href='<?php echo site_url('performer/settings/pricing')?>';">
		<span class="icon"></span>
		<a href="<?php echo site_url('performer/settings/pricing')?>"><?php echo lang('Pricing') ?></a>
	</div>
	<div class="menu_item <?=($this->uri->segment(3) == 'banned-zones') ? 'selected' : NULL ?>" onclick="document.location.href='<?php echo site_url('performer/settings/banned-zones')?>';">
		<span class="icon"></span>
		<a href="<?php echo site_url('performer/settings/banned-zones')?>"><?php echo lang('Banned zones') ?></a>
	</div>
	<div class="menu_item <?=($this->uri->segment(3) == 'categories') ? 'selected' : NULL ?>" onclick="document.location.href='<?php echo site_url('performer/settings/categories')?>';">
		<span class="icon"></span>
		<a href="<?php echo site_url('performer/settings/categories')?>"><?php echo lang('Categories') ?></a>
	</div>
	<div class="menu_item <?=($this->uri->segment(3) == 'schedule') ? 'selected' : NULL ?>" onclick="document.location.href='<?php echo site_url('performer/settings/schedule')?>';">
		<span class="icon"></span>
		<a href="<?php echo site_url('performer/settings/schedule')?>"><?php echo lang('Schedule') ?></a>
	</div>
	<div class="menu_item <?=($this->uri->segment(2) == 'photos') ? 'selected' : NULL ?>" onclick="document.location.href='<?php echo site_url('performer/photos')?>';">
		<span class="icon"></span>
		<a href="<?php echo site_url('performer/photos')?>"><?php echo lang('Photos') ?></a>
	</div>
	<div class="menu_item <?=($this->uri->segment(2) == 'videos') ? 'selected' : NULL ?>" onclick="document.location.href='<?php echo site_url('performer/videos')?>';">
		<span class="icon"></span>
		<a href="<?php echo site_url('performer/videos')?>"><?php echo lang('Videos') ?></a>
	</div>
	<div class="menu_item <?=($this->uri->segment(2) == 'contracts') ? 'selected' : NULL ?>" onclick="document.location.href='<?php echo site_url('performer/contracts')?>';">
		<span class="icon"></span>
		<a href="<?php echo site_url('performer/contracts')?>"><?php echo lang('Contracts') ?></a>
	</div>
	<div class="menu_item <?=($this->uri->segment(2) == 'photo_id') ? 'selected' : NULL ?>" onclick="document.location.href='<?php echo site_url('performer/photo_id')?>';">
		<span class="icon"></span>
		<a href="<?php echo site_url('performer/photo_id')?>"><?php echo lang('Photo ID') ?></a>
	</div>
</div>
<div class="fl_r">
	<?php
		if(isset($page)){
			$this->load->view($page);
		}
	?>
</div>
<div class="clear"></div>