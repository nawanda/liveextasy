<div id="left_menu">
	<div class="menu_item <?=($this->uri->segment(1) == 'settings' && $this->uri->segment(2) == '') ? 'selected' : NULL ?>" onclick="document.location.href='<?php echo site_url('settings')?>';">
		<span class="icon"></span>
		<a href="<?php echo site_url('settings')?>"><?php echo lang('Personal details') ?></a>
	</div>
	<div class="menu_item <?=($this->uri->segment(2) == 'payment') ? 'selected' : NULL ?>" onclick="document.location.href='<?php echo site_url('settings/payment')?>';">
		<span class="icon"></span>
		<a href="<?php echo site_url('settings/payment')?>"><?php echo lang('Payment details') ?></a>
	</div>
	<div class="menu_item <?=($this->uri->segment(2) == 'password') ? 'selected' : NULL ?>" onclick="document.location.href='<?php echo site_url('settings/password')?>';">
		<span class="icon"></span>
		<a href="<?php echo site_url('settings/password')?>"><?php echo lang('Change password') ?></a>
	</div>
	<div class="menu_item <?=($this->uri->segment(2) == 'percentage') ? 'selected' : NULL ?>" onclick="document.location.href='<?php echo site_url('settings/percentage')?>';">
		<span class="icon"></span>
		<a href="<?php echo site_url('settings/percentage')?>"><?php echo lang('Change percentage') ?></a>
	</div>
	<div class="menu_item <?=($this->uri->segment(1) == 'contracts') ? 'selected' : NULL ?>" onclick="document.location.href='<?php echo site_url('contracts')?>';">
		<span class="icon"></span>
		<a href="<?php echo site_url('contracts')?>"><?php echo lang('Contracts') ?></a>
	</div>
</div>
<div class="fl_r">
	<?php
		if(isset($page)){
			$this->load->view($page);
		}
	?>
</div>
<div class="clear"></div>