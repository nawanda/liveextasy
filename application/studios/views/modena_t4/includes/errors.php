<?php $messages = $this->session->flashdata('msg');
if( (isset($messages) && $messages) ):	
	if( ! $messages['success'] ):// ERRORS?>
		<div class="status error">
			<?php if(isset($messages) && $messages):?>	
				<p><span class="icon error"></span><?php echo $messages['message']?></p>
			<?php endif?>				
		</div>	
	<?php else://success?>
		<div class="status success">
			<p><span class="icon success"></span><?php echo $messages['message']?></p>
		</div>
	<?php endif?>
<?php endif?>