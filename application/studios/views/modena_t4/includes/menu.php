<div id="main_nav">
<!--	<span class="strip tl"></span>
	<span class="strip tr"></span>-->
	<span class="strip bl"></span>
	<span class="strip br"></span>
	<ul>
		<li><a href="<?=site_url('')?>" class="white <?=(!$this->uri->segment(1)) ? 'selected' : NULL?>"><?=lang('My Account')?></a></li>
		<li><a href="<?=site_url('settings')?>" class="white <?=($this->uri->segment(1) == 'settings') ? 'selected' : NULL?>" ><?=lang('Settings')?></a></li>
		<li><a href="<?=site_url('performers')?>" class="white <?=($this->uri->segment(1) == 'performers') ? 'selected' : NULL?>"><?=lang('Performers')?></a></li>
		<li><a href="<?=site_url('earnings')?>" class="white <?=($this->uri->segment(1) == 'earnings') ? 'selected' : NULL?>"><?=lang('Earnings')?></a></li>
		<li><a href="<?=site_url('payments')?>" class="white <?=($this->uri->segment(1) == 'payments') ? 'selected' : NULL?>"><?=lang('Payments')?></a></li>
	</ul>
</div>

<?php if(isset($this->performer->id)):?>
<div class="performer_menu">
	<ul>
		<li><a href="<?=site_url('performer')?>" class="white <?=(!$this->uri->segment(2)) ? 'selected' : NULL?>"><?=lang('My Account')?></a></li>
		<li><a href="<?=site_url('performer/settings')?>" class="white <?=($this->uri->segment(2) == 'settings') ? 'selected' : NULL?>" ><?=lang('My Settings')?></a></li>
		<li><a href="<?=site_url('performer/my_payments')?>" class="white <?=($this->uri->segment(2) == 'my_payments') ? 'selected' : NULL?>"><?=lang('My Payments')?></a></li>
		<li><a href="<?=site_url('performer/messenger')?>" class="white <?=($this->uri->segment(1) == 'messenger') ? 'selected' : NULL?>"><?=lang('Messenger')?></a></li>
	</ul>
</div>
<?php endif ?>
