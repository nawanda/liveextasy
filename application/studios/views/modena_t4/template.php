<?php $this->load->view('includes/head')?>
	<body id="studio">
		<div id="page_bg">
			<div id="warpper">
				<div id="header">
					<? $this->load->view('includes/header');?>
					<div class="clear"></div>				
				</div>
				<div id="page" class="dark_gray">
					
					<?php  ($this->user->id > 0 ) ? $this->load->view('includes/menu') : NULL?>
					<?php $this->load->view('includes/errors');?>	
					<?php
						if(isset($_sidebar) && $_sidebar){
							$this->load->view('includes/_sidebar');
						}
						elseif(isset($page) && !(isset($_settings_sidebar) && $_settings_sidebar) && !(isset($_payments_sidebar) && $_payments_sidebar)) {						
							$this->load->view($page);
						}

						if(isset($_payments_sidebar) && $_payments_sidebar) {
							$this->load->view('performers/includes/_my_payments_sidebar');
						}

						if(isset($_settings_sidebar) && $_settings_sidebar) {
							$this->load->view('includes/_my_settings_sidebar');
						}					
					?>
				</div>
			</div>
			<div class="clear"></div>
			<?php $this->load->view('includes/footer')?>
		</div>
	</body>
</html>

