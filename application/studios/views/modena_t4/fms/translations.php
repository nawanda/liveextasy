<?php echo '<?xml version="1.0" encoding="UTF-8"?>
<language'?>  

		configYourDevices           = "<?php echo lang('Configure Your &#60;b&#62;Devices&#60;/b&#62;');?>"
		
		chooseQuality               = "<?php echo lang('Please choose quality scheme:');?>"
		lowBut                      = "<?php echo lang('Low');?>"
		medBut                      = "<?php echo lang('Medium');?>"
		highBut                     = "<?php echo lang('High');?>"
		customBut                   = "<?php echo lang('Custom');?>"
		goBut                       = "<?php echo lang('Go Online');?>"
		chooseTypeOptionText        = "<?php echo lang('Choose online type:');?>"
		broadThisCam                = "<?php echo lang('Broadcast this cam');?>"
		volume                      = "<?php echo lang('Volume:');?>"
		normalRatio                 = "<?php echo lang('Normal 4:3');?>"
		wideRatio                   = "<?php echo lang('Wide 16:9');?>"
		testMic                     = "<?php echo lang('Test Mic');?>"
		ok                          = "<?php echo lang('OK');?>"
		micTestVolume               = "<?php echo lang('Detected Volume');?>"
		microphoneIsWorking         = "<?php echo lang('The selected microphone seems to be working correctly.');?>"
		microphoneNotWorking        = "<?php echo lang('The selected microphone does NOT seem to work.');?>"
		inputDevicesMuted           = "<?php echo lang('The devices have been muted because you denied access in the settings pop-up.');?>"
		micTestTitle                = "<?php echo lang('Testing Currently Selected Microphone');?>"
		camResolution               = "<?php echo lang('Resolution:  ');?>"
		pixels                      = "<?php echo lang('px');?>"
		frameRate                   = "<?php echo lang('Framerate: ');?>"
		fps                         = "<?php echo lang('fps');?>"
		quality                     = "<?php echo lang('Video Quality: ');?>"
		percent                     = "<?php echo lang('%');?>"
		bandwidth                   = "<?php echo lang('Bandwidth: ');?>"
		kBs                         = "<?php echo lang('kBs');?>"
		unlimited                   = "<?php echo lang('Unlimited');?>"
		welcome                     = "<?php echo lang('Welcome');?>"
		camera                      = "<?php echo lang('Camera');?>"
		cam                         = "<?php echo lang('Cam ');?>"
		all                         = "<?php echo lang('All');?>"
		takeBreak                   = "<?php echo lang('Take a Break!');?>"
		takeBreakTip                = "<?php echo lang('Exit Chat and Take a Break');?>"
		settingsTabLabel            = "<?php echo lang('Settings');?>"
		mp3PlayerTabLabel           = "<?php echo lang('Music Player');?>"
		userListTabLabel            = "<?php echo lang('User List');?>"
		micVolume                   = "<?php echo lang('Mic. Volume:');?>"
		recCamera                   = "<?php echo lang('Rec. Camera:');?>"
		record                      = "<?php echo lang('Record');?>"
		recordTip                   = "<?php echo lang('Start Recording Video');?>"
		recordStop                  = "<?php echo lang('Stop Rec.');?>"
		recordStopTip               = "<?php echo lang('Stop Recording Video');?>"
		recordingBlinker            = "<?php echo lang('Rec.');?>"
		listChipsColumn             = "<?php echo lang('Chips');?>"
		listNicknameColumn          = "<?php echo lang('Name');?>"
		listActionsColumn           = "<?php echo lang('Actions');?>"
		songColumnHeader            = "<?php echo lang('Song Name');?>"
		kickBut                     = "<?php echo lang('Kick');?>"
		kickButTip                  = "<?php echo lang('Kick this user from the chat.');?>"
		banBut                      = "<?php echo lang('Ban');?>"
		banButTip                   = "<?php echo lang('Ban this user from entering your chat.');?>"
		send                        = "<?php echo lang('Send');?>"
		typeHere                    = "<?php echo lang('Type your text here...');?>"
		
		chatTypeOptionCount         = "<?php echo lang('3');?>"
		chatTypeOption1             = "<?php echo lang('Free');?>"
		chatTypeOption2             = "<?php echo lang('Nude Only');?>"
		chatTypeOption3             = "<?php echo lang('Private Only');?>"
		
		available                   = "<?php echo lang('Available');?>"
		private                     = "<?php echo lang('Private');?>"
		nude                        = "<?php echo lang('Nude');?>"
		free                        = "<?php echo lang('Free');?>"
		
		warning                     = "<?php echo lang(' Warning! ');?>"
		connFailed                  = "<?php echo lang('	Connection to server failed. Please try again. If problem persists, please contact your administrator.');?>"
		connRejected                = "<?php echo lang('	Connection rejected by server. Please re-login and try again.');?>"
		connClosed                  = "<?php echo lang('	Connection closed or reset by server. Please refresh the page to reconnect.');?>"
		connClosedViewer            = "<?php echo lang('	Performer entered a private chat or has logged off.');?>"
		
		connInvFailed               = "<?php echo lang('	Private Invitation;  Connection to server failed. Please try again. If problem persists, please contact your administrator.');?>"
		connInvRejected             = "<?php echo lang('	Private Invitation;  Connection rejected by server. Please re-login and try again.');?>"
		
		chatSounds                  = "<?php echo lang('Beep On Chat Line');?>"
		userlistSounds              = "<?php echo lang('Beep On User Entrance');?>"
		broadMic                    = "<?php echo lang('Broadcast Audio');?>"
		
		goBackOnline                = "<?php echo lang('Go Back Online');?>"
		breakTimeHeader             = "<?php echo lang('On a break');?>"
		breakTimeBody               = "<?php echo lang('   When you are ready to go back online, please press \'Go Back Online\'.');?>"
		
		accept                      = "<?php echo lang('Accept');?>"
		deny                        = "<?php echo lang('Deny');?>"
		invitationPrivateHeader     = "<?php echo lang('Invitation to private chat');?>"
		userInvited                 = "<?php echo lang('  A user has invited you to a private chat!');?>"
		username                    = "<?php echo lang('Username: ');?>"
		chips_                      = "<?php echo lang('Chips: ');?>"
		
		dressTimeHeader             = "<?php echo lang('Dress Time');?>"
		dressTimeBody               = "<?php echo lang('   Private Chat is over. Please dress up and go back online.');?>"
		dressTimeBodySpy            = "<?php echo lang('   Private Chat is over. Performer is now dressing up.');?>"
		
		tipPerformerHeader          = "<?php echo lang('Tip Received');?>"
		tipPerformer1               = "<?php echo lang('You have received a');?>"
		tipPerformer2               = "<?php echo lang('tip from');?>"
		tipPerformerBut             = "<?php echo lang('Tip Performer');?>"
		tip                         = "<?php echo lang('Tip');?>"
		
		openPartnerVideo            = "<?php echo lang('Open viewer\'s video');?>"
		openPartnerTip              = "<?php echo lang('see the viewer');?>"
		closePartnerVideo           = "<?php echo lang('Close viewer\'s video');?>"
		closePartnerTip             = "<?php echo lang('hide the viewer');?>"
		
		viewerVol                   = "<?php echo lang('Volume:');?>"
		
		noCameraHeader              = "<?php echo lang('No Camera Detected');?>"
		noCameraBody                = "<?php echo lang('To go online you must have a working web cam connected to your computer. If you have a device connected, please make sure it is not in use by another process. Refresh the page to retry.');?>"
		
		denyCameraHeader            = "<?php echo lang('Camera Access Denied');?>"
		denyCameraBody              = "<?php echo lang('To go online you must accept the device security policy.');?>"
		
		play                        = "<?php echo lang('Play');?>"
		stop                        = "<?php echo lang('Stop');?>"
		noSongPlaying               = "<?php echo lang('No song playing');?>"
		nowPlaying                  = "<?php echo lang('Playing: ');?>"
		
		xmlBut                      = "<?php echo lang('Download XML');?>"
		url                         = "<?php echo lang('URL:');?>"
		stream                      = "<?php echo lang('Stream:');?>"
		
		showViewerList              = "<?php echo lang('Show User List');?>"
		exit                        = "<?php echo lang('Exit');?>"
		exitToolTip                 = "<?php echo lang('Exit Chat');?>"
		
		askforPrivate               = "<?php echo lang('Request Private Chat');?>"
		musicPlayer                 = "<?php echo lang('Music Player');?>"
		musicPlayerVolume           = "<?php echo lang('Music Player Volume');?>"
		performerVolume             = "<?php echo lang('Performer\'s Volume');?>"
		imageZoom                   = "<?php echo lang('Video Zoom');?>"
		
		minimumToolTip              = "<?php echo lang('Minimize');?>"
		largeToolTip                = "<?php echo lang('Large View');?>"
		fullScreenToolTip           = "<?php echo lang('Full Screen');?>"
		cameraSettingsTip           = "<?php echo lang('Camera Settings');?>" 
		micSettingsTip              = "<?php echo lang('Microphone Settings');?>" 
		
		waitingForReply             = "<?php echo lang('	You have invited the performer to private chat. Waiting for reply...  This may take a few seconds.');?>"
		invitationRejected          = "<?php echo lang('Invitation Rejected');?>"
		invitationAccepted          = "<?php echo lang('Invitation Accepted');?>"
		yourInvRejected             = "<?php echo lang('        The performer is not yet ready to enter a private chat. Please try again later.');?>"
		yourInvAccepted             = "<?php echo lang('        Your invitation to private chat has been accepted by the performer.');?>"
		cancel                      = "<?php echo lang('Cancel');?>"
		proceedPrivate              = "<?php echo lang('Private');?>"
		proceedTruePrivate          = "<?php echo lang('True Private');?>"
		chipsMin                    = "<?php echo lang('chips/min');?>"
		noPeeking                   = "<?php echo lang('No peeking allowed!');?>"	
		
		beenBanned                  = "<?php echo lang('        You have been BANNED from this performer\'s chatroom. ');?>"
		beenKicked                  = "<?php echo lang('        You have been KIKED out from this performer\'s chatroom. ');?>"
		perfLeft                    = "<?php echo lang('        Performer left the chatroom.');?>"
		notConnected                = "<?php echo lang('        You are not connected to server. Please refresh page.');?>"
		connOver                    = "<?php echo lang('        Your free time has expired');?>"
		connOverLackOfChips         = "<?php echo lang('        You ran out of chips. Please buy more chips and re-enter chat');?>"
		
		choiceYes                   = "<?php echo lang('Yes');?>"
		choiceNo                    = "<?php echo lang('No');?>"
		
		peekModalHeader             = "<?php echo lang('Entering Peek');?>"
		peekModalText               = "<?php echo lang('Peek fee is');?>"
		peekOK                      = "<?php echo lang('Continue');?>"
		peekCancel                  = "<?php echo lang('Cancel');?>"
		
		choicePeekHeader            = "<?php echo lang('Peek Option');?>"
		choicePeekBody              = "<?php echo lang('The performer has entered private chat. Do you wish to take a peek?');?>"
		choiceNudeHeader            = "<?php echo lang('Nude Chat Option');?>"
		choiceNudeBody              = "<?php echo lang('The performer has entered a nude chat. Do you wish to join?');?>"
		
		nudeModalHeader             = "<?php echo lang('Entering Nude Show');?>"
		nudeModalText               = "<?php echo lang('Nude fee is');?>"
		nudeModelText2              = "<?php echo lang('. Do you wish to continue?');?>"
		
		tipSubmitLabel              = "<?php echo lang('Send Tip');?>"
		tipCancelLabel              = "<?php echo lang('Cancel');?>"
		tipIsLess                   = "<?php echo lang('is less than the minimum amount of');?>"
		tipIsMore                   = "<?php echo lang('is more than the maximum amount of');?>"
		tipHeaderText               = "<?php echo lang('Tip Performer');?>"
		tipLoadingText              = "<?php echo lang('Please wait while the application initializes... ');?>"
		tipPleaseEnterText          = "<?php echo lang('Please enter a tip between');?>"
		tipIncorrectText            = "<?php echo lang('The value is not valid.');?>"
		tipAnd                      = "<?php echo lang('and');?>"
		tipErrorText1               = "<?php echo lang('To be able to tip this performer you must buy more credit.');?>"
		tipErrorText2               = "<?php echo lang('You must be logged in order to tip the performer');?>"
		tipAmountText               = "<?php echo lang('Tip amount:');?>"
		tipOkButtonLabel            = "<?php echo lang('Close');?>"
		tipReceivedHeader           = "<?php echo lang('Tip Sent');?>"
		tipYouSent                  = "<?php echo lang('You have successfully tipped the performer');?>"
		tipHeSent                   = "<?php echo lang('has tiped the performer');?>"
		tipHeaderHim                = "<?php echo lang('Tip Sent');?>"
		tipSuccess                  = "<?php echo lang('Successfully Tiped');?>"
		tipCredits                  = "<?php echo lang('credits');?>"
		tipFailedMes                = "<?php echo lang('The tip was NOT sent.  Please try again later.');?>"
		tipFailedHeader             = "<?php echo lang('Tip Sending Failed');?>"
		
		topicLabel                  = "<?php echo lang('Change Topic');?>"
		clearTopicLabel             = "<?php echo lang('Clear');?>"
		activeTopic                 = "<?php echo lang('Topic:');?>"
		update                      = "<?php echo lang('Update');?>"
		noActiveTopic               = "<?php echo lang('no topic');?>"
		
		openW2WMode                 = "<?php echo lang('Show Your Webcam to');?>"
		closeW2WMode                = "<?php echo lang('Close Your Webcam');?>"
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		dontShow                    = "<?php echo lang('Don\'t show this again');?>"
		
		okLaDecon                   = "<?php echo lang('OK');?>"
		
		tipLabelText                = "<?php echo lang('Tip');?>"
		
		chatType                    = "<?php echo lang('Chat Type:');?>"
		
		
		mic                         = "<?php echo lang('Mic:');?>"
		
		gotoNude                    = "<?php echo lang('Go Nude');?>"
		gotoPrivate                 = "<?php echo lang('Go Private');?>"
		
		microPhoneSoundBroadcast    = "<?php echo lang('Broadcast microphone');?>"
		peekAllowText               = "<?php echo lang('Allow Peeking');?>"
		
		feeMessagePrivate           = "<?php echo lang('Chips per minute for private: ');?>"
		feeMessageTruePrivate       = "<?php echo lang('Chips per minute true private: ');?>"
		atLeastOneCam               = "<?php echo lang('        Please choose at least one cam in order to continue to chat room. To do so, tick on \'Broadcast this cam\' from any of the available cams');?>"
		
		
		
		
		userListTag                 = "<?php echo lang('User List');?>"
		activityLogTag              = "<?php echo lang('Activity Log');?>"
		controlsTag                 = "<?php echo lang('Controls');?>"
		
		recordFromCam               = "<?php echo lang('Record cam:');?>"
		
		close                       = "<?php echo lang('Close');?>"
		
		fullscreen                  = "<?php echo lang('Fullscreen');?>"
		bigscreen                   = "<?php echo lang('Big Screen');?>"
		normalscreen                = "<?php echo lang('Small Screen');?>"
		broadcastSound              = "<?php echo lang('Microphone');?>"
		broadcast                   = "<?php echo lang('Broadcast');?>"
		stopBroadcast               = "<?php echo lang('Stop Broadcast');?>"
		allCams                     = "<?php echo lang('All Cams');?>"
		
		pleaseChooseNick            = "<?php echo lang('Please choose a nickname to use in this chatroom.');?>"
		gotoChat                    = "<?php echo lang('Go to chat!');?>"
		nickname                    = "<?php echo lang('Nickname:');?>"
		mustSpecifyNick             = "<?php echo lang('        Please specify a nickname before continuing to chatroom!');?>"
		
		
		performerLogViewerEntered   = "<?php echo lang(' has entered chatroom.');?>"
		performerLogViewerLeft      = "<?php echo lang(' has left chatroom.');?>"
		performerLogStoppedRec      = "<?php echo lang('Video recording stopped.');?>"
		performerLogStartRec        = "<?php echo lang('Video recording started.');?>"
		performerLogChangedStatusTo = "<?php echo lang('Changed status to: ');?>"
		
		notEnoughCreditsForPrivate  = "<?php echo lang('You don\'t have enough chips to enter private chat.');?>"
		
		welcomeMessage              = "<?php echo lang('Welcome to my chat room big boy! :P');?>"
		
	
	

	
	
	
>

</language>