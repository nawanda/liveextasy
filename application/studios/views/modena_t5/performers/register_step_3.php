<script type="text/javascript">
jQuery.validator.setDefaults({
	validClass:"success",
	errorElement: "span",
	errorPlacement: function(error, element) {
		error.appendTo($(element).next('span').next('span'));
	}
});
jQuery(function($){

	$.validator.addMethod("checkcat", function(value,element) {		
		if ($("input:checkbox:checked").length  > 0){			
            return true;
		}
        else
        {       
            return false;
        }
	}, "<?php echo lang('Please select a category!')?>");
	
	$(".register_performer").validate({
		success: function(label) {
	    	label.addClass("valid")
	   },
		rules: {
			true_private_chips_price: {				
				required: true,
				minlength: 1	,
				number : true		
			},
			private_chips_price: {				
				required: true,
				minlength: 1,
				number : true				
			},
			peek_chips_price: {				
				required: true,
				minlength: 1,
				number : true				
			},
			nude_chips_price: {				
				required: true,
				minlength: 1,
				number : true				
			},
			paid_photo_gallery_price: {				
				required: true,
				minlength: 1,
				number : true				
			}		
		}, 
		messages: {
			true_private_chips_price: 					"<?php echo lang('Please enter a valid price') ?>",
			private_chips_price: 						"<?php echo lang('Please enter a valid price') ?>",
			peek_chips_price: 							"<?php echo lang('Please enter a valid price') ?>",
			nude_chips_price: 							"<?php echo lang('Please enter a valid price') ?>",
			paid_photo_gallery_price: 					"<?php echo lang('Please enter a valid price') ?>"
		},
		debug: false
	});
});
</script>	

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <!-- <link rel="stylesheet" href="/resources/demos/style.css"> -->
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>	

<script type="text/javascript">
$( function() {
    $( document ).tooltip();
  } );
</script>

		<div class="content">
			<div class="white performers-details-titles">
			<h4><?php echo lang('Signup step 3 - Pricings ') ?></h4>
			<p><?php echo lang('Please fill this fields') ?></p>
		</div>
			<div id="pricing">
				<?php echo form_open_multipart(current_url(), 'class="register_performer"')?>
				<div class="m5-panel shadow  relativ">
				<div class="m5-signup-form w-form padding50" id="register_performer">
					<div class="relativ">
						<label><span class="m5-form-label"><?php echo sprintf(lang('True private chat %s/minute'), SETTINGS_SHOWN_CURRENCY); ?>:</span></label>
						<?php echo form_input('true_private_chips_price',set_value('true_private_chips_price', $true_private_chips_price), 'class="m5-input w-input"')?>
						<div class="price-info-input">
							<span class="icon info south"  title="<?php echo lang('This is the price per minute that a user will have to pay for a true private chat session. The difference between a true private chat and a private chat is that a true private chat session does not allow peeking users.')?>" style="vertical-align:middle;"></span>
							<span class="error" htmlfor="true_private_chips_price" generated="true"><?php echo form_error('true_private_chips_price')?form_error('true_private_chips_price'):($this->input->post()?NULL:sprintf(lang('Must be between %s as %s'),MIN_TRUE_PRIVATE_CHIPS_PRICE,MAX_TRUE_PRIVATE_CHIPS_PRICE))?></span>
						</div>
					</div>
					<div class="relativ">
						<label><span class="m5-form-label"><?php echo sprintf(lang('Private chat %s/minute'), SETTINGS_SHOWN_CURRENCY); ?>:</span></label>
						<?php echo form_input('private_chips_price',set_value('private_chips_price', $private_chips_price), 'class="m5-input w-input"')?>
						<div class="price-info-input">
							<span class="icon info south" title="<?php echo lang('This is the price per minute that a user will have to pay for a private chat sessions. The difference between a private chat and a true private chat is that a private chat session allows other users to \'peek\'.')?>" style="vertical-align:middle;"></span>
							<span class="error" htmlfor="private_chips_price" generated="true"><?php echo form_error('private_chips_price')?form_error('private_chips_price'):($this->input->post()?NULL:sprintf(lang('Must be between %s as %s'),MIN_PRIVATE_CHIPS_PRICE,MAX_PRIVATE_CHIPS_PRICE))?></span>
						</div>
					</div>
					<div class="relativ">
						<label><span class="m5-form-label"><?php echo sprintf(lang('Peek chat %s/minute'), SETTINGS_SHOWN_CURRENCY); ?>:</span></label>
						<?php echo form_input('peek_chips_price',set_value('peek_chips_price', $peek_chips_price), 'class="m5-input w-input"')?>
						<div class="price-info-input">
							<span class="icon info south" title="<?php echo lang('This is the price per minute that a user will have to pay in in order to be able to peek at a private chat session. Peeking is available for private chat sessions, but not true private ones.')?>"  style="vertical-align:middle;"></span>
							<span class="error" htmlfor="peek_chips_price" generated="true"><?php echo form_error('peek_chips_price')?form_error('peek_chips_price'):($this->input->post()?NULL:sprintf(lang('Must be between %s as %s'),MIN_PEEK_CHIPS_PRICE,MAX_PEEK_CHIPS_PRICE))?></span>
						</div>
					</div>
					<div class="relativ">
						<label><span class="m5-form-label"><?php echo sprintf(lang('Nude chat %s/minute'), SETTINGS_SHOWN_CURRENCY); ?>:</span></label>
						<?php echo form_input('nude_chips_price',set_value('nude_chips_price',$nude_chips_price), 'class="m5-input w-input"')?>
						<div class="price-info-input">
							<span class="icon info south" title="<?php echo lang('This is the price per minute that a user has to pay for a nude chat session. A nude chat session allows several users to talk to a performer and all of them have to pay this fee.')?>"  style="vertical-align:middle;"></span>
							<span class="error" htmlfor="nude_chips_price" generated="true"><?php echo form_error('nude_chips_price')?form_error('nude_chips_price'):($this->input->post()?NULL:sprintf(lang('Must be between %s as %s'),MIN_NUDE_CHIPS_PRICE,MAX_NUDE_CHIPS_PRICE))?></span>
						</div>
					</div>
					<div class="relativ">
						<label><span class="m5-form-label"><?php echo lang(' Paid photo gallery price') ?>:</span></label>
						<?php echo form_input('paid_photo_gallery_price',set_value('paid_photo_gallery_price',$paid_photo_gallery_price), 'class="m5-input w-input"')?>
						<div class="price-info-input">
							<span class="icon info south" title="<?php echo lang('This is the price that a user has to pay in order to gain access to your paid photo gallery. Once a user has paid this fee, they will have access to your gallery indefinitely.')?>"  style="vertical-align:middle;"></span>
							<span class="error" htmlfor="paid_photo_gallery_price" generated="true"><?php echo form_error('paid_photo_gallery_price')?form_error('paid_photo_gallery_price'):($this->input->post()?NULL:sprintf(lang('Must be between %s as %s'),MIN_PHOTOS_CHIPS_PRICE,MAX_PHOTOS_CHIPS_PRICE))?></span>
						</div>
					</div>					
					<div>
						<?php echo form_submit('submit', lang('Continue'), 'class="submit-button-2 w-button"')?>
					</div>
					</div>
					<div class="clear"></div>
				</div>
				<?php echo form_close()?>
			</div>

		<div class="dots-list">
				<div class="circle"></div>
				<div class="circle"></div>
				<div class="circle circle-select"></div>
				<div class="circle"></div>
		</div>
		</div>