	<div class="content">
		<div class="title">
			<?php echo lang('Performer\'s Banned Zones Page') ?>
		</div>
		<div id="profile">
			<div class="left" style="text-align: center;">
				<div class="dark_gray italic register_performer">
				<?php echo form_open('')?>
					<ul style="list-style-type:none; width: 280px; float:left; overflow-y: scroll; height: 500px;" >
						<?php if(form_error('countries[]')):?>
							<span class="error message" htmlfor="languages" generated="true" style="vertical-align:top"><?php echo form_error('countries[]')?></span>
						<?php endif?>					
						<p><?php echo lang('Countries') ?>:</p>
						<?php foreach ($countries as $key => $value):?>
							<li>
								<label style="text-align: left; padding: 0px; margin: 0px;">
									<?php echo form_checkbox('countries[]', $key,set_checkbox('countries',$key,(in_array($key,$banned_countries)?TRUE:FALSE)))?>
									<span class="bold"><?php echo $value?></span>
								</label>
							</li>
						<?php endforeach;?>
					</ul>				
					<ul style="list-style-type:none; width: 280px; float:left; overflow-y: scroll; height: 500px;">
						<?php if(form_error('states[]')):?>
							<span class="error message" htmlfor="states" generated="true" style="vertical-align:top"><?php echo form_error('states[]')?></span>
						<?php endif?>					
						<p><?php echo lang('States') ?>:</p>
						<?php foreach ($states as $key => $value):?>
							<li>
								<label  style="text-align: left; padding: 0px; margin: 0px;">
									<?php echo form_checkbox('states[]', $key,set_checkbox('states',$key,(in_array($key,$banned_states)?TRUE:FALSE)))?>
									<span class="bold"><?php echo $value?></span>
								</label>
							</li>
						<?php endforeach;?>
					</ul>
					<div class="clear"></div>	
					<div style="text-align: left;">
						<?php echo form_submit('submit', lang('Save'), 'class="red" style="width:206px;"')?>
					</div>			
					<?php echo form_close('')?>
				</div>
				<div class="clear"></div>
								
			</div>
			<div class="clear"></div>
		</div>
	</div>