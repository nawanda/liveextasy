<script type="text/javascript">
jQuery(function($){

	$.validator.setDefaults({
		validClass:"success",
		errorElement: "span",
		errorPlacement: function(error, element) {
			error.appendTo($(element).next('span').next('span'));
		}
	});
	
	$(".change_pass").validate({
		
		success: function(label) {
	    	label.addClass("valid");
	   },
		rules: {
			new_password: {
		   		required: true,
				minlength: 5
			},
			confirm_password: {
		   		required: true,
				minlength: 5,
				equalTo: "#new_password"

			}
		}, 
		messages: {
			new_password:					"<?php echo lang('Password must have at least 5 characters') ?>",
			confirm_password:				"<?php echo lang('Passwords do not match') ?>"
		}
	});
}); 
</script>	
		<div class="content">
			<div class="title">
				<?php echo lang('Account Summary - Settings') ?>
			</div>
			
			<div class="" id="profile">

				<?php echo form_open(current_url(), 'class="change_pass"')?>
				<div class="dark_gray italic">
					<div>
						<label><span class="bold"><?php echo lang('New Password') ?>:</span></label>
						<?php echo form_password(array('name' => 'new_password', 'id' => 'new_password'))?>
						<span class="error message" htmlfor="new_password" generated="true"><?php echo form_error('new_password')?></span>
					</div>
					<div>
						<label><span class="bold"><?php echo lang('Confirm Password') ?>:</span></label>
						<?php echo form_password('confirm_password')?>
						<span class="error message" htmlfor="confirm_password" generated="true"><?php echo form_error('confirm_password')?></span>
					</div>
					<div>
						<?php echo form_submit('submit', lang('Save'), 'class="red" style=width:222px;')?>
					</div>
				</div>
				<?php echo form_close() ?>

			<div class="clear"></div>
		</div>
	</div>