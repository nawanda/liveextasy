<div class="content">
	<div class="title">
		<?php echo lang('Performer\'s Categories Page') ?>
	</div>
	<div id="profile">
			<div class="dark_gray italic">
				<?php echo form_open() ?>
				<label><span class=" bold"><?php echo lang('Categories') ?></span></label>
				<?php if (sizeof($categories['main_categories']) > 0): ?>
					<ul style="list-style-type:none; display:inline-block;margin-top: 0px; ">
						<?php foreach ($categories['main_categories'] as $main_category): ?>
							<li>
								<label  style="text-align: left; padding: 0px; margin: 0px;">
									<?php echo form_checkbox('categories[' . $main_category->id . ']', $main_category->id, set_checkbox('categories', $main_category->id, (in_array($main_category->id, $performer_categories) ? TRUE : FALSE))) ?>
									<span class=" bold"><?php echo lang($main_category->name) ?></span>
								</label>
							</li>
							<?php if (sizeof($categories['sub_categories']) > 0 && isset($categories['sub_categories'][$main_category->id]) && sizeof($categories['sub_categories'][$main_category->id]) > 0): ?>
								<li>
									<ul style="list-style-type:none; ">
										<?php foreach ($categories['sub_categories'][$main_category->id] as $sub_category): ?>
											<li>
												<label  style="text-align: left; padding: 0px; margin: 0px;">
													<?php echo form_checkbox('categories[' . $sub_category->id . ']', $sub_category->id, set_checkbox('categories', $sub_category->id, (in_array($sub_category->id, $performer_categories) ? TRUE : FALSE))) ?>
													<span class="bold"><?php echo lang($sub_category->name) ?></span>
												</label>
											</li>
										<?php endforeach ?>
									</ul>
								</li>
							<?php endif ?>
						<?php endforeach ?>
					</ul>
				<?php endif ?>						
				<div style="text-align: center;">
					<?php echo form_submit('submit', lang('Save'), 'class="red" style="width:206px;"') ?>
				</div>			
				<?php echo form_close('') ?>
			</div>

		<div class="clear"></div>
	</div>
</div>