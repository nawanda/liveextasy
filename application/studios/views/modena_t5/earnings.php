	<script type="text/javascript">
		jQuery(function($){
			$('#payment_form').submit(function(){
				var value = $('#paymentDates :selected').val();
				if(value == 0) {
					window.location = 'earnings';
				} else {
					var split_dates = value.split('~');
					window.location = '<?php echo base_url()?>earnings/' + split_dates[0] + '/' + split_dates[1];
				}
				return false;
			});
		});
	</script>
	<link rel="stylesheet" type="text/css" href="<?php echo assets_url()?>css/table.css" media="screen" />
		<div class="content m5-panel-gray shadowv2">
			<div class="white performers-details-titles">
				<h4><?php echo lang('Account Summary - Earnings') ?></h4>
				<p><?php echo lang('Here is what performers earn') ?></p>
			</div>
						
			<div class="earn-inputs" style="margin: 20px; margin-bottom: 0px;">
				<?php echo form_open('earnings', array('method' => 'GET','id'=>'payment_form')) ?>
					<?php echo form_dropdown('paymentDates', $options, set_value('paymentDates') , 'class="time_period_sele select-input" id="paymentDates"') ?>
						<button class="w-button viewearnings m-t-2" style="" onclick="javascript:viewearnings();"><?php echo lang('View Earnings') ?></button>
				<?php echo form_close(); ?>		
			</div>
		
			<h4 class="white data-earn" style="margin-right: 20px;"><?php echo date('d M Y', strtotime($start_date)) . ' - ' . date('d M Y', strtotime($stop_date)); ?></h4>

			<div class="studio-earning relativ" id="earnings">
				<!-- <div class="fl_l">
					<div class="">
						<?php echo form_open('earnings', array('method' => 'GET','id'=>'payment_form')) ?>
							<?php echo form_dropdown('paymentDates', $options, set_value('paymentDates') , 'class="w-input m5-inputv2 time_period_sele" id="paymentDates"') ?>
							<div class="button_container">
								<button class="w-button viewearnings m-t-2" style="" onclick="javascript:viewearnings();"><?php echo lang('View Earnings') ?></button>
							</div>
						<?php echo form_close(); ?>
					</div>
				</div> -->
				<div class="fl_r">

					<table class="data display datatable table-studio">
						<thead>
							<tr>
								<th style="width: 25%; white-space: nowrap;"><?php echo lang('Performer') ?></th>
								<th style="width: 20%; white-space: nowrap;"><?php echo lang('Performer earnings') ?></th>
								<th style="width: 20%; white-space: nowrap;"><?php echo lang('Studio earnings') ?></th>
								<th style="width: 20%; white-space: nowrap;"><?php echo lang('Details') ?></th>
							</tr>
						</thead>
						<tbody>
						<?php if( sizeof($watchers) == 0 ):?>
							<tr style="text-align: center;">
								<td colspan="4"><?php echo lang('You have no earnings for this period.')?></td>
							</tr>
						<?php else:?>
							<?php $i = 0?>
							<?php foreach($watchers as $row): ?>
								<tr class="<?php echo ($i % 2==0)?'even':'odd'?>">
									<td><?php echo $row->username ?></td>
									<td><?php echo print_amount_by_currency($row->performer_chips) ?></td>
									<td><?php echo print_amount_by_currency($row->studio_chips) ?></td>
									<td><a href="<?php echo site_url('earnings-detail/' . $row->performer_id.'/'.$start_date.'/'.$stop_date)?>" target="_blank"><img src="<?php echo assets_url()?>images/icons/right_arrow.png" alt="" /></a></td>
								</tr>
								<?php $i++?>
							<?php endforeach;?>
						<?php endif?>
						</tbody>
					</table>
					<?php echo $pagination?>
				</div>
			</div>

			<div class="clear"></div>
		</div>
