<div class="content">
	<h3 class="account-page white">My Account page</h3>
		<p>Check here to see your balance and account information.</p>
	<div id="summary" class="m5-panel-gray shadowv2">
		<span class="bold">
			<?php $credits_red = '<span class="white">' . print_amount_by_currency($this->user->credits) . '</span>' ?>

			<div class="s-margin">
				<?php echo sprintf(lang('Your account has %s'), $credits_red)?>
				<?php if( SETTINGS_CURRENCY_TYPE )://daca nu e in bani reali afisez suma si in real money?>
					<?php $equivalent = '<span class="white">' . print_amount_by_currency($this->user->credits,TRUE) . '</span>'?>
					<?php echo sprintf(lang('the equivalent of %s.'), $equivalent)?>
				<?php endif?>
			</div>
		</span>
		<?php if ($this->user->release <= convert_chips_to_value($this->user->credits, TRUE)): ?>
			<span class="s-margin"><?=lang('You have been marked for payment.') ?></span>
		<?php else: ?>
			<?php $need_more = '<span class="white">' . print_amount_by_currency(convert_value_to_chips(abs($this->user->release - convert_chips_to_value($this->user->credits)))) . '</span>' ?>
			<span class="s-margin"><?=sprintf(lang('You need %s more and we\'ll send you the payment!'), $need_more) ?></span>
		<?php endif; ?>
		<div class="clear"></div>

		<div class="fl_l s-margin" style="width: 100%;">
			<div class="fl_l bold"><?=lang('Username')?>:</div><div class="fl_l white"><?=$this->user->username ?></div>
			<br>
			<div class="fl_l bold"><?=lang('Register Date')?>:&nbsp; </div><div class="fl_l white"><?=date("Y-m-d", $this->user->register_date)?></div>
			<br>
			<div class="fl_l bold"><?=lang('Your percentage')?>:&nbsp; </div><div class="fl_l white"><?=$this->user->percentage?>%</div>
			<br>
			<div class="fl_l bold"><?=lang('Nr. of your performers')?>:&nbsp; </div><div class="fl_l"><a class="white" href="<?=site_url('performers') ?>"><?=$number_of_performers ?></a></div>
			<br>
			<div class="fl_l bold"><?=lang('Contract Status')?>:&nbsp; </div><div class="fl_l"><a class="white" href="<?=site_url('contracts') ?>"><?=$this->user->contract_status ?></a></div>
		</div>
<br>
		<div class="performers-status">
			<div class="fl_l s-margin m5-panel-gray shadowv2 info-panel" style="margin-top: 10px; padding: 15px;">
				<span class="bold white"><b><?=lang('Performer status') ?></b></span><br/>
				<span><span class="icon approved"></span><?=sprintf(lang('%s Performers'), $approved_performers) ?></span><br/>
				<span><span class="icon pending"></span><?=sprintf(lang('%s Performers'), $pending_performers) ?></span><br/>
				<span><span class="icon rejected"></span><?=sprintf(lang('%s Performers'), $rejected_performers) ?></span><br/>
			</div>
			<div class="fl_l s-margin m5-panel-gray shadowv2" style="margin-top: 10px; padding: 15px;">
				<span class="bold white"><b><?=lang('Performer contract status') ?></b></span><br/>
				<span><span class="icon approved"></span><?=sprintf(lang('%s Performers'), $contract_approved_performers) ?></span><br/>
				<span><span class="icon pending"></span><?=sprintf(lang('%s Performers'), $contract_pending_performers) ?></span><br/>
				<span><span class="icon rejected"></span><?=sprintf(lang('%s Performers'), $contract_rejected_performers) ?></span><br/>
			</div>
			<div class="fl_l s-margin m5-panel-gray shadowv2" style="margin-top: 10px; padding: 15px;">
				<span class="bold white"><b><?=lang('Performer photo id status') ?></b></span><br/>
				<span><span class="icon approved"></span><?=sprintf(lang('%s Performers'), $photo_id_approved_performers) ?></span><br/>
				<span><span class="icon pending"></span><?=sprintf(lang('%s Performers'), $photo_id_pending_performers) ?></span><br/>
				<span><span class="icon rejected"></span><?=sprintf(lang('%s Performers'), $photo_id_rejected_performers) ?></span><br/>
			</div>
		</div>
		<div class="clear"></div>
	</div>

	<br/>
	<br/>
	<script type="text/javascript" src="<?php echo assets_url()?>js/jquery.strtotime.min.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		<script type="text/javascript">
			$(function() {


				$( ".datepickerStart" ).datepicker({ maxDate: '<?php echo date('d-m-Y') ?>',  dateFormat: 'dd-mm-yy'});
				$( ".datepickerEnd" ).datepicker({ maxDate: '<?php echo date('d-m-Y') ?>',  dateFormat: 'dd-mm-yy'});


			});

			function show_chart(id){

				$('#statistics .displayed').hide();
				$('#chart_'+id).addClass('displayed');
				$('#chart_'+id).show();
			}
		</script>
		<div class="m5-panel-gray shadowv2 relativ">
			<div class="title-statistics">
				<h4 class="white"><?=$this->user->username ?>'s statistics</h4>
				<p>Here is your detailed information on earnings for your sessions.</p>
			</div>
				<div class="f-right white" style="padding-bottom: 10px;">
					<?php echo form_open(current_url())?>
					<div style="display:inline-block; "><?php echo lang('Start date')?><br/><?php echo form_input("start_date",  $start_date, 'class="datepickerStart  m5-input w-input " readonly="readonly"  style="width:140px;"')?></div>
					<div style="display:inline-block;"><?php echo lang('End date')?><br/><?php echo form_input("end_date", $end_date, ' class="datepickerEnd  m5-input w-input "  readonly="readonly" style="width:140px;"')?></div>
					<div style="display: inline-block; vertical-align: middle; color: #000; padding: 3px 0;"><input type="submit" value="Refresh" class="submit-button w-button" style="margin-top: -3px;" /></div>
					<?php echo form_close()?>
				</div>

				<div id="statistics_buttons" class="f-left" style="width:100%">
						<button class="black bold" id="tatals-stats" onclick="show_chart('totals');"><?php echo lang('Totals')?></button>
						<button class="black bold" id="view-stats" onclick="show_chart('watchers');"><?php echo lang('Viewers')?></button>
						<button class="black bold"id="earn-stats" onclick="show_chart('earnings');"><?php echo lang('Earnings')?></button>
				</div>

	<div class="clear"></div>
	<div id="studio_statistics">

		<script type="text/javascript" src="<?=assets_url() ?>js/highcharts/highcharts.src.js"></script>
		<script type="text/javascript" src="<?=assets_url() ?>js/highcharts/modules/exporting.js"></script>
		<script type="text/javascript" src="<?=assets_url() ?>js/highcharts/themes/gray.js"></script>
		<?php if ($chart_totals != ''): ?>
			<script type="text/javascript">

				var chart;
				$(document).ready(function() {

					// define the options

					chart = new Highcharts.Chart({


						chart: {
							renderTo: 'chart_totals',
							marginBottom:50,
							height:450,
							zoomType: 'x'

						},

						title: {
							text: "<?=$this->user->username . lang('\'s statistics') ?>"
						},

						subtitle: {
							text: null
						},

						xAxis: {
							type: 'datetime',
							dateTimeLabelFormats: { // don't display the dummy year
								month: '%e. %b',
								year: '%b'
							},
							maxZoom: 28 * 24 * 3600000, // fourteen days
							tickWidth: 10,
							gridLineWidth: 1,
							labels: {
							}
						},

						yAxis: [{ // left y axis
								title: {
									text: null
								},
								labels: {
									formatter: function() {
										return Highcharts.numberFormat(this.value, 0);
									}
								},
								showFirstLabel: false
							}],

						legend: {
							align: 'right',
							verticalAlign: 'top',
							y: 20,
							x: 5,
							floating: false,
							width: 180,
							style:{
								display: 'block'
							},
							//margin:100,
							borderWidth: 0
						},


						tooltip: {
							shared:true,
							formatter: function() {
								var tooltip = '';
								tooltip += '<b>'+Highcharts.dateFormat('%e %b. %Y', this.x)+'<b><br/>';
								for(var i in this.points){
									if(this.points[i].series.name == 'Total earnings'){
										y = this.y;
										unit = 'chips';
									}else if(this.points[i].series.name == 'Total sessions'){
										y = this.y;
										unit = 'sessions';
									}else{
										y = time(this.y * 100000);
										unit = '';
									}

									if(this.points[i].series.name == 'Total sessions duration'){
										tooltip += '<span><b style="color:'+this.points[i].series.color+'">'+ this.points[i].series.name +'</b>: '+ time(this.points[i].y * 100000) +' '+ unit+ ' </span><br/>';
									}else{
										tooltip += '<span><b style="color:'+this.points[i].series.color+'">'+ this.points[i].series.name +'</b>: '+this.points[i].y +' '+ unit+ '</span><br/>';
									}
								}


								return tooltip;
							}
						},
						plotOptions: {
							series: {
								cursor: 'pointer',
								point: {
									events: {
										click: function() {
											if(this.series.name == 'Total earnings'){
												y = this.y;
												unit = 'chips';
											}else if(this.series.name == 'Total sessions'){
												y = this.y;
												unit = 'sessions';
											}else{
												y = time(this.y * 10000);
												unit = '';
											}
											hs.htmlExpand(null, {
												pageOrigin: {
													x: this.pageX,
													y: this.pageY
												},
												headingText: this.series.name,
												maincontentText: Highcharts.dateFormat('%e %b. %Y', this.x) +':<br/> '+
													y +' '+unit,
												width: 200
											});
										}
									}
								},
								marker: {
									enabled: false,
									states: {
										hover: {
											enabled: true,
											radius: 5
										}
									}
								}
							}
						},

						series: [<?=$chart_totals ?>]
					});
				});
			</script>
			<div id="chart_totals" class="displayed" style="width:925px;">
			</div>
		<?php else: ?>
			<div id="chart_totals" class="displayed" style="width:925px; text-align: center;">
				<?=lang('Not available') ?>
			</div>
		<?php endif ?>
		<?php if ($chart_earnings != ''): ?>
			<script type="text/javascript">

				var chart;
				$(document).ready(function() {

					// define the options

					chart = new Highcharts.Chart({


						chart: {
							renderTo: 'chart_earnings',
							marginBottom:50,
							height:450,
							zoomType: 'x'
						},

						title: {
							text: "<?=$this->user->username . lang('\'s earnings statistics') ?>"
						},

						subtitle: {
							text: null
						},

						xAxis: {
							type: 'datetime',
							dateTimeLabelFormats: { // don't display the dummy year
								month: '%e. %b',
								year: '%b'
							},
							maxZoom: 28 * 24 * 3600000, // fourteen days
							tickWidth: 10,
							gridLineWidth: 1,
							labels: {
							}
						},

						yAxis: [{ // left y axis
								title: {
									text: 'chips'
								},
								labels: {
									formatter: function() {
										return Highcharts.numberFormat(this.value, 0);
									}
								},
								showFirstLabel: false
							}],

						legend: {
							align: 'right',
							verticalAlign: 'top',
							y: 20,
							x: 5,
							floating: false,
							width: 180,
							style:{
								display: 'block'
							},
							//margin:100,
							borderWidth: 0
						},


						tooltip: {
							shared:true,
							formatter: function() {
								var tooltip = '';
								tooltip += '<b>'+Highcharts.dateFormat('%e %b. %Y', this.x)+'<b><br/>';
								for(var i in this.points){

									if(this.points[i].series.name == 'Total sessions duration'){
										tooltip += '<span><b style="color:'+this.points[i].series.color+'">'+ this.points[i].series.name +'</b>: '+ time(this.points[i].y * 10000) +' chips </span><br/>';
									}else{
										tooltip += '<span><b style="color:'+this.points[i].series.color+'">'+ this.points[i].series.name +'</b>: '+this.points[i].y +' chips </span> <br/>';
									}
								}


								return tooltip;
							}
						},
						plotOptions: {
							series: {
								cursor: 'pointer',
								point: {
									events: {
										click: function() {
											hs.htmlExpand(null, {
												pageOrigin: {
													x: this.pageX,
													y: this.pageY
												},
												headingText: this.series.name,
												maincontentText: Highcharts.dateFormat('%e %b. %Y', this.x) +':<br/> '+
													this.y +' chips',
												width: 200
											});
										}
									}
								},
								marker: {
									enabled: false,
									states: {
										hover: {
											enabled: true,
											radius: 5
										}
									}
								}
							}
						},

						series: [<?=$chart_earnings ?>]
					});



				});

			</script>
			<div id="chart_earnings"  style="display:none;width:925px;" ></div>
		<?php else: ?>
			<div id="chart_earnings"   style="display:none; width:925px; text-align: center;">
				<?=lang('Not available') ?>
			</div>
		<?php endif ?>
		<?php if ($chart_watchers != ''): ?>
			<script type="text/javascript">

				var chart;
				$(document).ready(function() {

					// define the options

					chart = new Highcharts.Chart({


						chart: {
							renderTo: 'chart_watchers',
							marginBottom:50,
							height:450,
							zoomType: 'x'
						},

						title: {
							text: "<?=$this->user->username . lang('\'s watchers statistics') ?>"
						},

						subtitle: {
							text: null
						},

						xAxis: {
							type: 'datetime',
							dateTimeLabelFormats: { // don't display the dummy year
								month: '%e. %b',
								year: '%b'
							},
							maxZoom: 28 * 24 * 3600000, // fourteen days
							tickWidth: 10,
							gridLineWidth: 1,
							labels: {
							}
						},

						yAxis: [{ // left y axis
								title: {
									text: 'watchers'
								},
								labels: {
									formatter: function() {
										return Highcharts.numberFormat(this.value, 0);
									}
								},
								showFirstLabel: false
							}],

						legend: {
							align: 'right',
							verticalAlign: 'top',
							y: 20,
							x: 5,
							floating: false,
							width: 180,
							style:{
								display: 'block'
							},
							//margin:100,
							borderWidth: 0
						},



						tooltip: {
							shared:true,
							formatter: function() {
								var tooltip = '';
								tooltip += '<b>'+Highcharts.dateFormat('%e %b. %Y', this.x)+'<b><br/>';
								for(var i in this.points){
									if(this.points[i].series.name == 'Gifts'){
										unit = 'gifts';
									}else{
										unit = 'watchers';
									}
									if(this.points[i].series.name == 'Total sessions duration'){
										tooltip += '<span><b style="color:'+this.points[i].series.color+'">'+ this.points[i].series.name +'</b>: '+ time(this.points[i].y * 10000) +' '+ unit +' </span><br/>';
									}else{
										tooltip += '<span><b style="color:'+this.points[i].series.color+'">'+ this.points[i].series.name +'</b>: '+this.points[i].y +' '+ unit +'</span> <br/>';
									}
								}


								return tooltip;
							}
						},
						plotOptions: {
							series: {
								cursor: 'pointer',
								point: {
									events: {
										click: function() {
											if(this.series.name == 'Gifts'){
												unit = 'gifts';
											}else{
												unit = 'watchers';
											}
											hs.htmlExpand(null, {
												pageOrigin: {
													x: this.pageX,
													y: this.pageY
												},
												headingText: this.series.name,
												maincontentText: Highcharts.dateFormat('%e %b. %Y', this.x)+' :<br/> '+
													this.y +' '+unit,
												width: 200
											});
										}
									}
								},
								marker: {
									enabled: false,
									states: {
										hover: {
											enabled: true,
											radius: 5
										}
									}
								}
							}
						},

						series: [<?=$chart_watchers ?>]
					});



				});

			</script>
			<div id="chart_watchers" style="display:none;width:925px;"></div>
		<?php else: ?>
			<div id="chart_watchers"   style="display:none; width:925px; text-align: center;">
				<?=lang('Not available') ?>
			</div>
		<?php endif ?>
		<!-- Additional files for the Highslide popup effect -->
		<script type="text/javascript" src="<?=assets_url() ?>js/highcharts/highslide-full.min.js"></script>
		<script type="text/javascript" src="<?=assets_url() ?>js/highcharts/highslide.config.js" charset="utf-8"></script>
		<link rel="stylesheet" type="text/css" href="<?=assets_url() ?>css/highslide.css" />
	</div>
</div>
</div>
