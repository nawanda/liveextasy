<footer id="footer" class="footer">
	<span class="strip bl"></span>
	<span class="strip br"></span>

	<div class="fl_1">
		<div class="links">
			<a href="<?php echo main_url(PREFORMERS_URL)?>"><?php echo lang('Performers <span class="fg_demi">Area</span>')?></a><span class="bara-dreapta">|</span>
			<a href="<?php echo main_url(PREFORMERS_URL . '/register')?>" id="become_performer"><?php echo lang('Performers <span class="fg_demi">wanted</span>')?></a><span class="bara-dreapta">|</span>
			<a href="<?php echo main_url(STUDIOS_URL)?>"><?php echo lang('Studios <span class="fg_demi">Area</span>')?></a><span class="bara-dreapta">|</span>
			<a href="<?php echo main_url(AFFILIATES_URL)?>"><?php echo lang('Affiliates <span class="fg_demi">Area</span>')?></a><span class="bara-dreapta">|</span>
			<a href="<?php echo main_url('documents/tos')?>"><?php echo lang('Terms & conditions')?></a><span class="bara-dreapta">|</span>
			<a href="<?php echo main_url('documents/policy')?>"><?php echo lang('Privacy policy')?></a><span class="bara-dreapta">|</span>
			<a href="<?php echo main_url('documents/2257')?>"><?php echo lang('18 U.S.C 2257')?></a><span class="bara-dreapta">|</span>
			<a href="<?php echo main_url('contact')?>"><?php echo lang('Contact us')?></a>
		</div>
		<div class="links2">
			<p>This site contains sexually explicit material. Enter this site ONLY if You are over the age of 18!
18 U.S.C 2257 Record-Keeping Requirements Compliance Statement</p>
		</div>
	</div>
	<div class="fl_2">
		<div class="copyright">
			<?php echo sprintf(lang('&copy; 2017 %s  |  All rights reserved.'), SETTINGS_SITE_TITLE)?>
		</div>
		<div id="langs">
			<?php
			$langs_av = $this->config->item('lang_avail');
			if(count($langs_av) > 1):
				foreach($langs_av as $row => $language):
			?>
				<a href="<?php echo main_url('language/'.$row)?>" title="<?php echo sprintf(lang('Change language to %s'),$language)?>"><img src="<?php echo assets_url()?>images/flags/<?php echo strtoupper($row)?>.png" alt="<?php echo $language?>" /></a>&nbsp;
			<?php
				endforeach;
			endif;
			?>

		</div>
	</div>

</footer>

<?php if($this->agent->is_browser('Internet Explorer')):?>
<script type="text/javascript">
	jQuery(function($){
		$('button').click(function(){
			if($(this).parent().is("a")){
				document.location.href = $(this).parent().attr('href');
			}
		});
	});
</script>
<?php endif?>


<?php
if(is_file(BASEPATH.'../assets/txt/analytics.txt')){
	echo file_get_contents(BASEPATH.'../assets/txt/analytics.txt');
}
?>
