<div class="w-tab-menu center performer-menu">
	<div class="m5-tab-link-simple  <?=($this->uri->segment(1) == 'settings' && $this->uri->segment(2) == '') ? 'selected' : NULL ?> w-inline-block w-tab-linkv2" onclick="document.location.href='<?php echo site_url('settings')?>';">
		<a href="<?php echo site_url('settings')?>"><?php echo lang('Personal details') ?></a>
	</div>
	<div class="m5-tab-link-simple <?=($this->uri->segment(2) == 'payment') ? 'selected' : NULL ?> w-inline-block w-tab-linkv2" onclick="document.location.href='<?php echo site_url('settings/payment')?>';">
		<a href="<?php echo site_url('settings/payment')?>"><?php echo lang('Payment details') ?></a>
	</div>
	<div class="m5-tab-link-simple <?=($this->uri->segment(2) == 'password') ? 'selected' : NULL ?> w-inline-block w-tab-linkv2" onclick="document.location.href='<?php echo site_url('settings/password')?>';">
		<a href="<?php echo site_url('settings/password')?>"><?php echo lang('Change password') ?></a>
	</div>
	<div class="m5-tab-link-simple  <?=($this->uri->segment(2) == 'percentage') ? 'selected' : NULL ?> w-inline-block w-tab-linkv2" onclick="document.location.href='<?php echo site_url('settings/percentage')?>';">
		<a href="<?php echo site_url('settings/percentage')?>"><?php echo lang('Change percentage') ?></a>
	</div>
	<div class="m5-tab-link-simple  <?=($this->uri->segment(1) == 'contracts') ? 'selected' : NULL ?> w-inline-block w-tab-linkv2" onclick="document.location.href='<?php echo site_url('contracts')?>';">
		<a href="<?php echo site_url('contracts')?>"><?php echo lang('Contracts') ?></a>
	</div>
</div>
<div class="m5-tab-content-actualv2 w-tab-content shadowv3">
	<?php
		if(isset($page)){
			$this->load->view($page);
		}
	?>
</div>
<div class="clear"></div>