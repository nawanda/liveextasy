
<?php if ($this->user->id > 0){ ?>	

<div class="w-col w-col-6 paddingzero">
  <ul class="m5-secondary-nav w-clearfix">
		<li class="m5-sec-link">
			<a class="m5-link <?=(!$this->uri->segment(1)) ? 'bold white' : NULL?>"  href="<?php echo base_url()?>">
				<?php echo lang('My Account')?>
			</a>
		</li>
		<li class="m5-sec-link">
			<a class="m5-link <?=($this->uri->segment(1) == 'settings') ? 'bold white' : NULL?>"  href="<?php echo site_url('settings')?>">
				<?php echo lang('My Settings')?>
			</a>
		</li>
		<li class="m5-sec-link">
			<a class="m5-link <?=($this->uri->segment(1) == 'performers') ? 'bold white' : NULL?>"  href="<?php echo site_url('performers')?>">
				<?php echo lang('Performers')?>
			</a>
		</li>
		<li class="m5-sec-link">
			<a class="m5-link <?=($this->uri->segment(1) == 'earnings') ? 'bold white' : NULL?>"  href="<?php echo site_url('earnings')?>">
				<?php echo lang('Earnings')?>
			</a>
		</li>
		<li class="m5-sec-link">
			<a class="m5-link <?=($this->uri->segment(1) == 'payments') ? 'bold white' : NULL?>"  href="<?php echo site_url('payments')?>">
				<?php echo lang('Payments')?>
			</a>
		</li>
	</ul>
</div>
<div class="w-col w-col-6">
	<?php if(isset($this->performer->id)):?>
	<div class="performer-menu">
		<ul class="m5-secondary-nav w-clearfix">
			<li class="m5-sec-link">
				<a href="<?=site_url('performer')?>" class="white <?=(!$this->uri->segment(2)) ? 'bold white' : NULL?>"><?=lang('My Account')?></a>
			</li>
			<li class="m5-sec-link">
				<a href="<?=site_url('performer/settings')?>" class="white <?=($this->uri->segment(2) == 'settings') ? 'bold white' : NULL?>" ><?=lang('My Settings')?></a>
			</li>
			<li class="m5-sec-link">
				<a href="<?=site_url('performer/my_payments')?>" class="white <?=($this->uri->segment(2) == 'my_payments') ? 'bold white' : NULL?>"><?=lang('My Payments')?></a>
			</li>
			<li class="m5-sec-link">
				<a href="<?=site_url('performer/messenger')?>" class="white <?=($this->uri->segment(1) == 'messenger') ? 'bold white' : NULL?>"><?=lang('Messenger')?></a>
			</li>
		</ul>
	</div>
	<?php endif ?>
</div>
<?php }else{ ?>	
<div class="w-col w-col-6">
  <ul class="m5-secondary-nav w-clearfix">
		<li class="m5-sec-link">
			<a class="m5-link"  href="<?php echo base_url().'../'?>">
				<?php echo lang('Online Models')?>
			</a>
		</li>
		<li class="m5-sec-link">
			<a class="m5-link"  href="<?php echo site_url().'../'.'performers'?>">
				<?php echo lang('Performers')?>
			</a>
		</li>
		<li class="m5-sec-link">
			<a class="m5-link"  href="<?php echo site_url().'../'.'videos'?>">
				<?php echo lang('Videos')?>
			</a>
		</li>
		<li class="m5-sec-link">
			<a class="m5-link"  href="<?php echo site_url().'../'.'favorites'?>">
				<?php echo lang('Favorites')?>
			</a>
		</li>
	</ul>
</div>
<div class="w-col w-col-6">
	<div class="form-block w-clearfix w-form">
		<form class="m5-search-form w-clearfix" id="email-form" name="email-form" action="<?php echo site_url().'../performers';?>">
			<input class="m5-search-input w-input" data-name="Field 2" id="search" maxlength="256" name="search" placeholder="Nickname" type="text" value="<?php if(isset($_GET['search'])) echo $_GET['search']; ?>">
			<input class="submit-button w-button" data-wait="Please wait..." type="submit" value="Submit"></form>
	</div>
</div>

<?php } ?>