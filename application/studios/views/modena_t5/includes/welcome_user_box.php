<div class="m5-main-nav w-nav-menu" role="navigation">
			<?php if ($this->user->id > 0): ?>
	<div class="box-nav boxlogin">

		<div class="login-navbar">

			<span class="welcome-txt"><?php echo lang('Welcome back') ?> &nbsp;
				<span><?php echo $this->user->username ?></span>
			</span>
			<div class="logout f-right">
				<a href="<?php echo site_url('logout') ?>">
					<i class="fa fa-sign-out" aria-hidden="true"></i>
					<span><?php echo lang('Logout') ?></span>
				</a>
			</div>

		</div>	
	</div>	
		<?php else: ?>

		<div class="logout-navbar">

			<?php echo sprintf(lang('<a href="%s" class="m5-emphasize-link m5-nav-link w-nav-link">Register</a> <a class="m5-nav-link w-nav-link" href="'.site_url('login').'" >Login</a>'), site_url('register')) ?>

		</div>
		
		<?php endif; ?>

</div>
