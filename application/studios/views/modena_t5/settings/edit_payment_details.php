	<div class="content">
		<div class="title">
			<?php echo lang('Edit Payment Details')?>
		</div>	
		<?php echo form_open('settings/edit-payment-details' , 'class="set_payment_details"')?>
			<div class="gray italic payment_details"  id="studio_settings">
			<?php 
			$fields = unserialize($payment_details->fields);
			$perfomer_account = unserialize($this->user->account);
			foreach ($fields as $field): ?>
				<div>
					<label><span class="m5-form-labelv2" id="performerTexAlign"><?php echo lang($field)?>:</span></label>
					<?php echo form_input(str_replace(' ' ,'_' , $field), set_value(str_replace(' ' ,'_' , $field), $perfomer_account[$field]),'class="m5-inputv2 w-input"')?>
					<span class="error" htmlfor="<?php echo str_replace(' ' ,'_' , $field) ?>" generated="true"><?php echo form_error(str_replace(' ' ,'_' , $field))?></span>
				</div>
			<?php endforeach ?>
			<div>
				<label><span class="m5-form-labelv2"><?php echo lang('Release Amount')?>:</span></label>
				<?php echo form_input('rls_amount', set_value('rls_amount', $this->user->release),'class="m5-inputv2 w-input"')?>
				<span class="error message" htmlfor="rls_amount" generated="true"><?php echo (form_error('rls_amount'))?form_error('rls_amount'):sprintf(lang('Minim %s'),print_amount_by_currency($payment_details->minim_amount,TRUE,TRUE))?></span>					
			</div>	
			<br/>
			<?php echo form_submit('submit',lang('Save'), 'class="submit-button-2 w-button"  ')?>
			</div>
		<?php echo form_close()?>
		<br/>
		<div class="clear"></div>
		
	</div>