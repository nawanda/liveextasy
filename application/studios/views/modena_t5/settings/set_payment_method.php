	<div class="content" style="margin-left:50px;">
		<div class="red_h_sep"></div>
		<div class="title">
			<?php echo lang('Set Payment Method')?>
		</div>
		<div class="italic current_payment"><span class="current_payment_mess"><?php echo lang('Current Payment Method') ?>:</span>
		<?php if ($current_method): ?>
			<div><label><?php echo lang('Name')?></label><?php echo ' - ' . $current_method->name ?></div>
			<div><label><?php echo lang('Currency')?></label><?php echo ' - ' . $current_method->currency ?></div>
			<div><label><?php echo lang('Minimum amount')?></label><?php echo ' - ' . $current_method->minim_amount ?></div>
			<?php $fields = unserialize($this->user->account); ?>
			<?php if( ! empty($fields)): ?>
			<?php foreach ($fields as $key => $value): ?>
				<div><label><?php echo $key ?></label><?php echo ' - ' . $value ?></div>
			<?php endforeach ?>
				<div><label><?php echo lang('Release amount') ?></label><?php echo ' - ' . print_amount_by_currency($this->user->release,TRUE,TRUE) ?></div>
			<?php endif ?>
			<div class="edit_payment"><a href="<?php echo site_url('settings/edit-payment-details')?>"><?php echo lang('Edit payment details')?></a></div>
		<?php else: ?>
			<span>- <?php echo lang('Not Set') ?></span>
		<?php endif?>
		</div>
		
		<div class="select_payment"><?php echo lang('Change payment method:')?></div>
		<?php if( ! empty($methods)): ?>
			<?php foreach ($methods as $method): ?>
				<div class="payment_method">
					<a style="color:#e30707; font:italic 15px 'century gothic';" href="<?php echo site_url('settings/set-payment-details/' . $method->id) ?>"><?php echo $method->name ?></a>
				</div>
			<?php endforeach ?>
		<?php else: ?>
			<div><?php echo lang('No payment methods available') ?>.</div>
		<?php endif ?>
		<div class="clear"></div>
		
	</div>