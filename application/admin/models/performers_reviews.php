<?php
class Performers_reviews extends MY_Model{
	
	protected $table = 'performers_reviews';
		
	public function __construct() {
		$this->set_table($this->table);
	}	
	
	/**
	 * Get performer reviews
	 * @param unknown_type $performer_id
	 * @param unknown_type $count
	 * @param unknown_type $offset
	 * @param unknown_type $limit
	 */
	function get_reviews_by_performer_id($performer_id, $count = FALSE,$offset = FALSE,$limit = FALSE){
		if( $count ){
			return $this->db->select('count(id) as nr')->where('performer_id',$performer_id)->get($this->table)->row()->nr;
		}
		
		$this->db->select('performers_reviews.add_date,message,rating,users.id as user_id,performers_reviews.id as id,users.username')
					->join('users','users.id = performers_reviews.user_id','inner')
					->where('performer_id',$performer_id)
					->order_by('performers_reviews.id','desc')
					->limit($limit)
					->offset($offset);
					
		return $this->db->get($this->table)->result();
		
	}
}