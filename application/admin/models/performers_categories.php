<?php
class Performers_categories extends MY_Model{
	
	private $table = 'performers_categories';
	
	public function __construct() {
		$this->set_table($this->table);
	}
	
	/**
	 * AVAILABLE METHODS:
	 * 
	 *		get_all($filters = FALSE, $count = FALSE, $order = FALSE, $offset = FALSE, $limit = FALSE)
	 *		get_by_id($id)
	 *		get_rand($many = 1)
	 *		save($data)
	 *		delete($id)
	 */
	
	
	#############################################################################################
	######################################### GET ###############################################
	#############################################################################################
	
	// -----------------------------------------------------------------------------------------
	/**
	 * Returneaza categoriile dupa $performer_id
	* @param $performer_id
	*/
	function get_multiple_by_performer_id($performer_id){
		return $this->db->where('performer_id',$performer_id)->get($this->table)->result();
	}
	
	
	// -----------------------------------------------------------------------------------------
	/**
	 * Verifica daca exista cel putin un performer activ in categoria respectiva
	 * @param int $category_id
	 * @returns object
	 */
	function check_for_active_performers($category_id){
		return $this->db->select('performers.id')
						->join('performers','performers.id = performers_categories.performer_id','inner')
						->where('performers.status','approved')
						->where('performers.contract_status','approved')
						->where('performers.photo_id_status','approved')
						->where('category_id',$category_id)
						->limit(1)
						->get($this->table)->row();
	}	
	
	#############################################################################################
	######################################### ADD ###############################################
	#############################################################################################	
	// -----------------------------------------------------------------------------------------
	/**
	 * Adauga categorii in performers_categories dupa user id
	 * @param $user_id
	 * @param $language
	 * @author VladG
	 */
	function add($user_id, $category) {
		$data = array(
				'category_id' => $category ,
				'performer_id' => $user_id
		);
	
		return $this->db->insert('performers_categories', $data);
	}
	
	
	#############################################################################################
	######################################### DELETE ############################################
	#############################################################################################
	
	// -----------------------------------------------------------------------------------------
	/**
	* Sterge categorii in performers_categories dupa user id
	* @param $user_id
	* @param $category
	* @author VladG
	*/
	function remove($user_id, $category){
	return $this->db->where('category_id', $category)->where('performer_id', $user_id)->delete('performers_categories');
	}	
}