

     <script type="text/javascript">
            // For version detection, set to min. required Flash Player version, or 0 (or 0.0.0), for no version detection. 
            var swfVersionStr = "0.0.0";
            // To use express install, set to playerProductInstall.swf, otherwise the empty string. 
            var xiSwfUrlStr = "playerProductInstall.swf";
            var flashvars = {};
            flashvars.themeFile="<?php echo assets_url()?>swf/assets/files/theme.xml" 
            flashvars.languageFile="<?php echo main_url()?>fms/translation"     
            flashvars.configFile="<?php echo assets_url()?>swf/assets/files/config.xml" 
            flashvars.smileysFile="<?php echo assets_url()?>swf/assets/files/smileys.xml"
            flashvars.censureFile="<?php echo main_url()?>censure.xml"
            flashvars.sitePath="<?php echo main_url()?>"
            
            flashvars.rtmp="<?php echo $params['rtmp']?>"
            flashvars.pasword="<?php echo $params['pasword']?>"
            flashvars.userName="<?php echo $params['userName']?>"
            flashvars.nickColor="0xFF0010"
            flashvars.performerId = "<?php echo $params['performerId']?>"
            flashvars.uniqId="<?php echo $params['uniqId']?>"
            
            flashvars.userId="<?php echo $params['userId']?>";         
     
            flashvars.redirectLink="<?php echo $params['redirectLink']?>";
            flashvars.performerNick="<?php echo $params['performerNick']?>";
            flashvars.performerNickColor="<?php echo $params['performerNickColor']?>";
            flashvars.noChat="false";

            var params = {};
            params.quality = "high";
            params.scale="exactfit";
            params.allowscriptaccess = "always";
            params.allowfullscreen = "true";
            params.wmode = "direct";
            var attributes = {};
            attributes.id = "ModeratorApp";
            attributes.name = "ModeratorApp";
            attributes.align = "middle";
            swfobject.embedSWF(
                "<?php echo main_url()?>assets/swf/ModeratorApp.swf", "flashContent", 
                "900", "600", 
                swfVersionStr, xiSwfUrlStr, 
                flashvars, params, attributes);
            // JavaScript enabled so display the flashContent div in case it is not replaced with a swf object.
            swfobject.createCSS("#flashContent", "display:block;text-align:left;");
        </script>

        <!-- SWFObject's dynamic embed method replaces this alternative HTML content with Flash content when enough 
             JavaScript and Flash plug-in support is available. The div is initially hidden so that it doesn't show
             when JavaScript is disabled.
        -->
        <div id="flashContent">
            <p>
                To view this page ensure that Adobe Flash Player version 
                0.0.0 or greater is installed. 
            </p>
            <script type="text/javascript"> 
                var pageHost = ((document.location.protocol == "https:") ? "https://" : "http://"); 
                document.write("<a href='http://www.adobe.com/go/getflashplayer'><img src='" 
                                + pageHost + "www.adobe.com/images/shared/download_buttons/get_flash_player.gif' alt='Get Adobe Flash player' /></a>" ); 
            </script> 
        </div>
        
        <noscript>
            <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="900" height="600" id="<?php echo main_url()?>assets/swf/ModeratorApp">
                <param name="movie" value="ModeratorApp.swf" />
                <param name="quality" value="high" />
                <param name="bgcolor" value="#ffffff" />
                <param name="allowScriptAccess" value="always" />
                <param name="allowFullScreen" value="true" />
                <param name="sAlign" value="tl" />
                <param name="scale" value="exactfit" />
                <param name="themeFile" value="<?php echo assets_url()?>swf/assets/files/theme.xml"/>
                <param name="languageFile" value="<?php echo main_url()?>fms/translation"/>
                <param name="configFile" value="<?php echo assets_url()?>swf/assets/files/config.xml"/>
                <param name="smileysFile" value="<?php echo assets_url()?>swf/assets/files/smileys.xml"/>
                <param name="censureFile" value="<?php echo main_url()?>censure.xml"/>
                <param name="sitePath" value="<?php echo main_url()?>"/>
                



                <param name="rtmp" value="<?php echo $params['rtmp']?>" />
                <param name="pasword" value ="<?php echo $params['pasword']?>"  />
                <param name="userName" value ="<?php echo $params['userName']?>"  />
                <param name="nickColor" value="0x123FF0"  />
                <param name="performerId" value = "<?php echo $params['performerId']?>"  />
                <param name="uniqId" value="<?php echo $params['uniqId']?>" />     

                <param name="userId" value="<?php echo $params['userId']?>"/>          
                <param name="performerNick" value="<?php echo $params['performerNick']?>"/> 
                <param name="performerNickColor" value="<?php echo $params['performerNickColor']?>"/> 
                <param name="noChat" value="false"/> 
                

                <!--[if !IE]>-->
                <object type="application/x-shockwave-flash" data="<?php echo main_url()?>assets/swf/ModeratorApp.swf" width="900" height="600">
                    <param name="quality" value="high" />
                    <param name="bgcolor" value="#ffffff" />
                    <param name="allowScriptAccess" value="always" />
                    <param name="allowFullScreen" value="true" />
                    <param name="sAlign" value="tl" />
                    <param name="scale" value="exactfit" />
                    <param name="themeFile" value="<?php echo assets_url()?>swf/assets/files/theme.xml"/>
                    <param name="languageFile" value="<?php echo main_url()?>fms/translation"/>
                    <param name="configFile" value="<?php echo assets_url()?>swf/assets/files/config.xml"/>
                    <param name="smileysFile" value="<?php echo assets_url()?>swf/assets/files/smileys.xml"/>
                    <param name="censureFile" value="<?php echo main_url()?>censure.xml"/>
                    <param name="sitePath" value="<?php echo main_url()?>"/>
                    
                    <param name="rtmp" value="<?php echo $params['rtmp']?>" />
                    <param name="pasword" value ="<?php echo $params['pasword']?>"  />
                    <param name="userName" value ="<?php echo $params['userName']?>"  />
                    <param name="nickColor" value="0x123FF0"  />
                    <param name="performerId" value = "<?php echo $params['performerId']?>"  />
                    <param name="uniqId" value="<?php echo $params['uniqId']?>" />     

                    <param name="userId" value="<?php echo $params['userId']?>"/>          
                    <param name="performerNick" value="<?php echo $params['performerNick']?>"/> 
                    <param name="performerNickColor" value="<?php echo $params['performerNickColor']?>"/> 
                    <param name="noChat" value="false"/>           
                    
                <!--<![endif]-->
                <!--[if gte IE 6]>-->
                    <p> 
                        Either scripts and active content are not permitted to run or Adobe Flash Player version
                        0.0.0 or greater is not installed.
                    </p>
                <!--<![endif]-->
                    <a href="http://www.adobe.com/go/getflashplayer">
                        <img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash Player" />
                    </a>
                <!--[if !IE]>-->
                </object>
                <!--<![endif]-->
            </object>
        </noscript>     