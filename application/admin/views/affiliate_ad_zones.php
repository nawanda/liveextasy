<script type="text/javascript">
$(document).ready(function(){

	
	$('.sort').click(function(){
		var url = '<?php echo current_url()?>?<?php echo $filters?>&';
		document.location.href= url + 'orderby[' + $(this).attr('id')  + ']=' + $(this).attr('order'); 
	});
		
	$('#reset_filters').click(function(){
		document.location.href='<?php echo site_url('affiliates/ads/'.$affiliate->username)?>';
	})
});
</script>
<div class="container">
	<div class="conthead">
		<?php $this->load->view('includes/affiliate_buttons')?>
	</div>
	<div id="observer">0</div>	
	<div class="contentbox">
		<?php if(sizeof($ad_zones) == 0 ):?>
			<div style="text-align:center; width:100%; " class="clear"><h3><?php echo lang('Ads not found.')?></h3></div>
		<?php else:?>
			<table width="100%" border="0">
				<thead>
					<tr>
						<?php $this->load->view('table_sorting',array('name'=>lang('ID'),				'key'=>'id') )?>					
						<?php $this->load->view('table_sorting',array('name'=>lang('name'),				'key'=>'name') )?>					
						<?php $this->load->view('table_sorting',array('name'=>lang('Created date'),		'key'=>'add_date') )?>					
						<?php $this->load->view('table_sorting',array('name'=>lang('type'),				'key'=>'type') )?>					
						<?php $this->load->view('table_sorting',array('name'=>lang('views'),			'key'=>'views') )?>					
						<?php $this->load->view('table_sorting',array('name'=>lang('hits'),				'key'=>'hits') )?>					
						<?php $this->load->view('table_sorting',array('name'=>lang('registers'),		'key'=>'registers') )?>					
						<?php $this->load->view('table_sorting',array('name'=>lang('earnings'),			'key'=>'earnings') )?>					
					</tr>
				</thead>
				<tbody>
				<?php $i = 1;?>
				<?php foreach ($ad_zones as $ad_zone):?>
					<tr <?php echo ($i%2 == 0) ? 'class="alt"' : null?>>
						<td><?php echo $ad_zone->id?></td>
						<td><?php echo $ad_zone->name?></td>
						<td><?php echo date('d M Y H:i' ,$ad_zone->add_date);?></td>
						<td><?php echo substr($ad_zone->type, 0, -2)?></td>
						<td><?php echo $ad_zone->views?></td>
						<td><?php echo $ad_zone->hits?></td>
						<td><?php echo $ad_zone->registers?></td>
						<td><?php echo $ad_zone->earnings?></td>						
					</tr>
					<?php $i++?>
				<?php endforeach?>
				</tbody>
			</table>
		<?php endif?>			
		<br />
		<div class="extrabottom">
			<?php echo $pagination?>
			<div class="bulkactions">
			</div>
		</div>
	</div>
</div>