<script type="text/javascript">
$(document).ready(function(){
	$('#reset_filters').click(function(){
		document.location.href='<?php echo current_url()?>';
	});

	$('.sort').click(function(){
		var url = '<?php echo current_url()?>?<?php echo $filters?>&';
		document.location.href= url + 'orderby[' + $(this).attr('id')  + ']=' + $(this).attr('order'); 
	});
		
	$(".payments_info").fancybox({
		'transitionIn' : 'none',
		'transitionOut' : 'none'
	}); 
});
</script>

<div class="container">
	<div class="conthead">
		<?php $this->load->view('includes/edit_buttons')?>
	</div>
	<div id="observer">0</div> <!-- Daca se schimba in 1 atunci inseamna ca s-au facut modificari in iframe si se da reload la pagina -->	
	<div class="contentbox">		
		<?php if(sizeof($payments) == 0):?>
			<div style="text-align:center; width:100%; " class="clear"><h3><?php echo lang('No payments found.')?></h3></div>		
		<?php else:?>
		<table width="100%" border="0">
			<thead>
				<tr>
					<?php $this->load->view('table_sorting',array('name'=>lang('ID'),				'key'=>'id'	) )?>
					<?php $this->load->view('table_sorting',array('name'=>lang('Paid date'),		'key'=>'paid_date'	) )?>
					<?php $this->load->view('table_sorting',array('name'=>lang('Amount chips'),		'key'=>'amount_chips') )?>																				
					<?php if($performer->studio_id <= 0):?>
						<th style="width: 120px; text-align: left;"><?php echo lang('Payment info') ?></th>
					<?php endif?>
					<th><?php echo lang('Status')?></th>
					<th><?php echo lang('Details')?></th>
				</tr>
			</thead>
			<tbody>
			<?php $i = 1;?>
			<?php foreach ($payments as $payment):?>				
				<tr <?php echo ($i%2 == 0) ? 'class="alt"' : null?>>
					<td><?php echo $payment->id?></td>
					<td><?php echo date('d M Y H:i' ,$payment->paid_date);?></td>
					<td><?php echo print_amount_by_currency($payment->amount_chips,TRUE);?></td>
					<?php if( ! $performer->studio_id ):?>
						<td>
							<a href="#payment_info_<?php echo $i?>" class="payments_info"><?php echo $payment->payment_name?></a>
							<div class="payments_info_content" id="payment_info_<?php echo $i?>" style="margin:20px;">
								<h3><?php echo sprintf(lang('Payments info for <strong>%s</strong> account.'), $performer->username)?></h3>
								<?php 
									$fields = unserialize($payment->payment_fields_data);
									if(is_array($fields)):?>
										<span style="display:inline-block; width:auto; font-weight: bold;"><?php echo $payment->payment_name?></span><br/>
										<?php foreach($fields as $field => $value):?>
											<span style="display:inline-block; width:70px; text-align:right;"><?php echo ucfirst($field) ?>:</span>
											<span style="display:inline-block; width:auto; font-weight: bold;"><?php echo $value ?></span><br/>
										<?php endforeach?>
								<?php endif?>
							</div>
						</td>
					<?php endif?>
					<td><?php echo $payment->status;?></td>
					<td>
						<a href="<?php echo site_url('to_pay/payment_details/' . $payment->id)?>" target="_blank"><img src="<?php echo assets_url()?>images/icons/right_arrow.png" alt="" /></a>										
					</td>
				</tr>
				<?php ++$i;?>
			<?php endforeach?>
			</tbody>
		</table>
		<br />
		<?php endif?>
		<div class="extrabottom">
			<?php echo $pagination?>
			<div class="bulkactions">
			</div>
		</div>
	</div>
</div>