<script type="text/javascript">
$(document).ready(function(){
	
	$('#reset_filters').click(function(){
		document.location.href='<?php echo site_url('affiliates/payments/'.$affiliate->username)?>';
	});
	
	$('.sort').click(function(){
		var url = '<?php echo current_url()?>?<?php echo $filters?>&';
		document.location.href= url + 'orderby[' + $(this).attr('id')  + ']=' + $(this).attr('order'); 
	});
			
	$(".payments_info").fancybox({
			'transitionIn' : 'none',
			'transitionOut' : 'none'
	}); 
});
</script>
<div class="container">
	<div class="conthead">
		<?php $this->load->view('includes/affiliate_buttons')?>
	</div>
	<div id="observer">0</div>	
	<div class="contentbox">		
		<?php if(sizeof($users) == 0):?>
			<div style="text-align:center; width:100%; " class="clear"><h3><?php echo lang('No users found.')?></h3></div>		
		<?php else:?>
			<table width="100%" border="0">
				<thead>
					<tr>
						<?php $this->load->view('table_sorting',array('name'=>lang('ID'),				'key'=>'id') )?>						
						<th><?php echo lang('Country')?></th>				
						<?php $this->load->view('table_sorting',array('name'=>lang('Username'),			'key'=>'username') )?>					
						<?php $this->load->view('table_sorting',array('name'=>lang('Email'),			'key'=>'email') )?>					
						<?php $this->load->view('table_sorting',array('name'=>lang('Credits'),			'key'=>'credits') )?>					
					</tr>
				</thead>
				<tbody>
				<?php $i = 1;?>
				<?php foreach ($users as $user) :?>
					<tr <?php echo ($i%2 == 0) ? 'class="alt"' : null?>>
						<td><?php echo $user->users_id?></td>
						<td><img src="<?php echo assets_url('admin/images/flags/' . $user->users_detail_country_code . '.png') ?>" /></td>
						<td><a href="<?php echo site_url()?>users/account/<?php echo $user->users_username?>"><?php echo $user->users_username?></a></td>
						<td><?php echo $user->users_email?></td>
						<td><?php echo print_amount_by_currency($user->users_credits)?></td>
					</tr>
					<?php ++$i?>
				<?php endforeach?>
				</tbody>
			</table>
		<?php endif?>
		<br />
		<div class="extrabottom">
			<?php echo $pagination?>
			<div class="bulkactions">
			</div>
		</div>
	</div>
</div>