<script type="text/javascript">
$(document).ready(function(){
	$('#reset_filters').click(function(){
		document.location.href='<?php echo current_url()?>';
	});

	$('.sort').click(function(){
		var url = '<?php echo current_url()?>?<?php echo $filters?>&';
		document.location.href= url + 'orderby[' + $(this).attr('id')  + ']=' + $(this).attr('order'); 
	});	
});
</script>
<div class="container">
	<div class="conthead">
		<?php $this->load->view('includes/studio_buttons')?>
	</div>
	<div id="observer">0</div> <!-- Daca se schimba in 1 atunci inseamna ca s-au facut modificari in iframe si se da reload la pagina -->				
	<div class="contentbox">
		<?php if(sizeof($performers) == 0):?>
			<div style="text-align:center; width:100%; " class="clear"><h3><?php echo lang('There are no performers.')?></h3></div>
		<?php else:?>				
			<table width="100%" border="0">
				<thead>
					<tr>
						<?php $this->load->view('table_sorting',array('name'=>lang('ID'),				'key'=>'id') )?>					
						<?php $this->load->view('table_sorting',array('name'=>lang('Country Code'),		'key'=>'country_code') )?>					
						<?php $this->load->view('table_sorting',array('name'=>lang('Nickname'),			'key'=>'nickname') )?>					
						<?php $this->load->view('table_sorting',array('name'=>lang('Email'),			'key'=>'email') )?>					
						<?php $this->load->view('table_sorting',array('name'=>lang('First name'),		'key'=>'first_name') )?>					
						<?php $this->load->view('table_sorting',array('name'=>lang('Last name'),		'key'=>'last_name') )?>					
						<?php $this->load->view('table_sorting',array('name'=>lang('Status'),			'key'=>'status') )?>					
						<?php $this->load->view('table_sorting',array('name'=>lang('Register Date'),	'key'=>'register_date') )?>					
					</tr>
				</thead>
				<tbody>
				<?php $i = 1;?>
				<?php foreach ($performers as $performer):?>
					<tr <?php echo ($i%2 == 0) ? 'class="alt"' : null?>>
						<td><?php echo $performer->id?></td>
						<td><img src="<?php echo assets_url('admin/images/' . (($performer->country_code != '')? 'flags/'.$performer->country_code : 'icons/na') . '.png') ?>" /></td>
						<td><a href="<?php echo base_url()?>performers/account/<?php echo $performer->username?>"><?php echo $performer->username?></a></td>
						<td><?php echo $performer->email?></td>
						<td><?php echo $performer->first_name?></td>
						<td><?php echo $performer->last_name?></td>
						<td><?php echo $performer->status?></td>
						<td><?php echo date('d M Y H:i' ,$performer->register_date);?></td>
					</tr>
				<?php endforeach ?>
				</tbody>
			</table>
		<?php endif?>					
		<br />
		<div class="extrabottom">
			<?php echo $pagination?>
			<div class="bulkactions">
			</div>
		</div>
	</div>
</div>