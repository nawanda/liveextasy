	
<!-- calendar picker -->		
		
<script type="text/javascript" src="<?php echo assets_url()?>js/jquery.strtotime.min.js"></script>
<script type="text/javascript" src="<?php echo assets_url()?>js/jquery.ui.datepicker.js"></script>

<script type="text/javascript">
		$(function() {		
				$( ".datepickerStart" ).datepicker({  maxDate: '<?php echo date('d-m-Y')?>',  dateFormat: 'dd-mm-yy'});
				$( ".datepickerEnd" ).datepicker({ maxDate: '<?php echo date('d-m-Y')?>',  dateFormat: 'dd-mm-yy'});
		});
		
</script>

<!-- calendar picker -->


<script type="text/javascript">
$(document).ready(function(){
	$('#reset_filters').click(function(){
		document.location.href='<?php echo current_url()?>';
	});

	$('.sort').click(function(){
		var url = '<?php echo current_url()?>?<?php echo $filters?>&';
		document.location.href= url + 'orderby[' + $(this).attr('id')  + ']=' + $(this).attr('order'); 
	});
});
</script>


<script>
	
$(document).ready(function(){
	$('.datepickerStart').change(function(){
		$('.start_date_hidden').val($('.datepickerStart').val());
	});
	
	$('.datepickerEnd').change(function(){
		$('.end_date_hidden').val($('.datepickerEnd').val());
	});

});

	
</script>


<div class="container">
				<div class="conthead">
					<?php $this->load->view('includes/edit_buttons')?>
				</div>
				
				<div id="date_picker_content">
				
					<?php echo form_open('') ?>
				
						<input type="hidden" name="calendar_enabled" value="enabled" />
				
						<label style="display:inline-block; float : left ; margin-right: 15px; "><?php echo lang('Start date')?><br/><?php echo form_input("start_date",  $calendar_start_date, 'class="datepickerStart" readonly="readonly"')?></label>
						<label style="display:inline-block; float : left ; margin-right: 15px;"><?php echo lang('End date')?><br/><?php echo form_input("end_date", $calendar_end_date, ' class="datepickerEnd"  readonly="readonly"')?></label>
						<input style="display:inline-block; float : left ;  margin-top : 10px;" type="submit" class="btn" value="Apply Dates"  />
							
					<?php  echo form_close(); ?>
				
				
					
						<?php echo form_open('performers/export_xls') ?>
					
							<input class="start_date_hidden" type="hidden" name="start_date_hidden" value="<?php echo $calendar_start_date; ?>" />
							<input class="end_date_hidden" type="hidden" name="end_date_hidden" value="<?php echo $calendar_end_date; ?>" />
							
							<input type="hidden" name="performer_id_hidden" value="<?php echo $performer_id ?>" />
							
							<input style="margin-top : 10px; margin-left : 10px;" type="submit" class="btn" value="Export to Excel" style="margin-top:0px;" />
								
						<?php  echo form_close(); ?>
					
				</div>	

				<div id="observer">0</div> <!-- Daca se schimba in 1 atunci inseamna ca s-au facut modificari in iframe si se da reload la pagina -->
				
				<div class="contentbox">
					<div class="bulkactions" style="margin-bottom: 10px;">
						<form action="" id="filters" name="filters" method="GET">
						
						<!--
								<p style="float:left; margin-right:10px;">
									&nbsp;<?php echo lang('Period')?><br/>
									<?php echo form_dropdown('filters[period]', $payments, $this->input->_fetch_from_array($this->input->get('filters'),'period',TRUE) , 'style="min-width:155px; margin:0px;"')?>																								
								</p>
						-->
						
							<p style="float:left; margin-right:10px;">
								&nbsp;<?php echo lang('Chat type')?><br/>
								<?php echo form_dropdown('filters[type]',$this->types, $this->input->_fetch_from_array($this->input->get('filters'),'type',TRUE) , 'style="min-width:155px; margin:0px;"')?>																
							</p>
							<p style="float:left; margin-right:10px;">
								&nbsp;<br/>
								<input type="submit" class="btn" value="Apply Filter" style="margin-top:0px;" />
								<?php echo (strlen($filters) > 0) ? '<input type="button" id="reset_filters" class="btn" value="Reset Filters" style="margin-top:0px;" />' : null?>																
							</p>
						</form>
					</div>					
					<?php if(sizeof($sessions) == 0) :?>
						<div style="text-align:center; width:100%; " class="clear"><h3><?php echo lang('Sessions not found.')?></h3></div>
					<?php else:?>
						<table width="100%" border="0">
							<thead>
								<tr>
									<?php $this->load->view('table_sorting',array('name'=>lang('ID'),				'key'=>'id'	) )?>
									<?php $this->load->view('table_sorting',array('name'=>lang('type'),				'key'=>'type') )?>
									<?php $this->load->view('table_sorting',array('name'=>lang('Start date'),		'key'=>'start_date'	) )?>
									<?php $this->load->view('table_sorting',array('name'=>lang('End date'),			'key'=>'end_date') )?>
									<?php $this->load->view('table_sorting',array('name'=>lang('Duration'),			'key'=>'duration') )?>
									<?php $this->load->view('table_sorting',array('name'=>lang('IP'),				'key'=>'ip'	) )?>
									<?php $this->load->view('table_sorting',array('name'=>lang('Fee per minute'),	'key'=>'fee_per_minute'	) )?>
									<?php $this->load->view('table_sorting',array('name'=>lang('User paid chips'),	'key'=>'user_paid_chips') )?>
									<th><?php echo lang('Site chips')?></th>
									<th><?php echo lang('Studio chips')?></th>
									<th><?php echo lang('Performer chips')?></th>
									<?php $this->load->view('table_sorting',array('name'=>lang('User id'),			'key'=>'user_id') )?>
									<th><?php echo lang('Studio id')?></th>
									<th><?php echo lang('Performer video id')?></th>
								</tr>
							</thead>
							<tbody>
							<?php $i = 1;?>
							<?php foreach ($sessions as $session):?>
								<tr <?php echo ($i%2 == 0) ? 'class="alt"' : null?>>
									<td><?php echo $session->id?></td>
									<td><?php echo $session->type?></td>
									<td><?php echo date('d M Y H:i' ,$session->start_date);?></td>
									<td><?php echo date('d M Y H:i' ,$session->end_date);?></td>
									<td><?php echo sec2hms($session->duration);?></td>
									<td><?php echo long2ip($session->ip);?></td>
									<td><?php echo print_amount_by_currency($session->fee_per_minute);?></td>
									<td><?php echo print_amount_by_currency($session->user_paid_chips);?></td>
									<td><?php echo print_amount_by_currency($session->site_chips);?></td>
									<td><?php echo print_amount_by_currency($session->studio_chips);?></td>
									<td><?php echo print_amount_by_currency($session->performer_chips);?></td>
									<td><?php echo $session->user_id;?></td>
									<td><?php echo $session->studio_id;?></td>
									<td><?php echo $session->performer_video_id;?></td>
								</tr>
								<?php ++$i; ?>
							<?php endforeach?>
						</tbody>
					</table>
					<?php endif?>
					<br />

					<div class="extrabottom">
						<?php echo $pagination?>
						<div class="bulkactions">
						</div>
					</div>
				</div>

			</div>