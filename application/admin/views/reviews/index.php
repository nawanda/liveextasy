<div class="container">
	<script src="<?php echo assets_url() ?>js/jquery.ui.stars.min.js"></script>
	<script type="text/javascript">
	$(document).ready(function(){
		$(".stars").stars({
			cancelShow: false,
			disabled: true,
			split: 4,
			callback: function(ui, type, value) {
				$.ajax({
					url: "<?php echo site_url('performers/rate') ?>",
					type: "post",
					data: {
						performer_id: <?php echo $performer->id ?>,
						rating:       value
					}
				});
			}
		});
	})
	</script>
	<div class="conthead">
		<?php $this->load->view('includes/edit_buttons')?>
	</div>
	<div id="observer">0</div> <!-- Daca se schimba in 1 atunci inseamna ca s-au facut modificari in iframe si se da reload la pagina -->				
	<div class="contentbox">
		<?php if(sizeof($performer_reviews) == 0):?>
			<div style="text-align:center; width:100%; " class="clear"><h3><?php echo lang('No reviews.')?></h3></div>
		<?php else:?>				
			<table width="100%" border="0">
				<thead>
					<tr>
						<th style="width:80px"><?php echo lang('User ID')?></th>
						<th style="width:100px"><?php echo lang('Username')?></th>
						<th style="width:350px"><?php echo lang('Message')?></th>
						<th style="width:80px"><?php echo lang('Rating')?></th>
						<th style="width:120px"><?php echo lang('Date')?></th>
						<th style="text-align:center;width:100px"><?php echo lang('Actions')?></th>
					</tr>
				</thead>
				<tbody>
				<?php $i = 1;?>
				<?php foreach ($performer_reviews as $review):?>
					<tr <?php echo ($i%2 == 0) ? 'class="alt"' : null?>>
						<td><?php echo $review->user_id?></td>
						<td><?php echo $review->username?></td>
						<td><?php echo $review->message?></td>
						<td>
							<span class="stars">	
								<?php $rating = round($review->rating * 4);
								for ($i = 0; $i < 21; $i++):?>
									<?php if ($i == $rating):?>
										<input name="rating" type="radio" class="star" value="<?php echo $i/4?>" checked="checked"/>
									<?php else:?>
										<input name="rating" type="radio" class="star" value="<?php echo $i/4?>"/>
									<?php endif?>
								<?php endfor?>
							</span>						
						</td>											
						<td><?php echo date('d M Y H:i' ,$review->add_date);?></td>
						<td style="text-align: center;">
							<a href="<?php echo site_url()?>reviews/edit/<?php echo $performer->username ?>/<?php echo $review->id ?>"  title=""><img src="<?php echo assets_url('admin/images/icons/icon_edit.png') ?>"></a>
							<a href="<?php echo site_url()?>reviews/delete/<?php echo $performer->username ?>/<?php echo $review->id ?>" onclick="if(confirm('<?php echo lang('Are you sure you want to delete this review?')?>')) { return true;} else {return false;}" title=""><img src="<?php echo assets_url('admin/images/icons/icon_unapprove.png') ?>"></a>
						</td>																
					</tr>
				<?php endforeach ?>
				</tbody>
			</table>
		<?php endif?>					
		<br />
		<div class="extrabottom">
			<?php echo $pagination?>
			<div class="bulkactions">
			</div>
		</div>
	</div>
</div>