<script type="text/javascript" src="<?php echo assets_url()?>js/jquery.validate.js"></script>

            
<script type="text/javascript">
jQuery(function($){
    $('.add_payment').submit(function(){
        $('.add_payment').validate();
    });

    $.validator.setDefaults({
        validClass:"success_adm",
        errorClass: "error_adm",
        errorPlacement: function(error, element) {
            return true;
        }
    });
    
    var validator = $(".add_payment").validate({
        rules: {
            review: {
                required: true,
                maxlength:255
            }
        }, 
        messages: {
            review:   "<?php echo lang('This field is required!')?>",
            
        },
        
        submitHandler: function(form) {
            form.submit();
        },
        debug: true
    });
    
    
}); 
</script>   
            
            <div class="container">
                <div class="conthead">
                    <?php $this->load->view('includes/edit_buttons')?>
                </div>
                <div class="contentbox">
                    <div class="center_contentbox">
                        
                        <h2><?php echo lang('Edit Payment Method')?></h2>
                        
                        <?php echo form_open('reviews/edit/'.$performer->username.'/'.$review->id, 'class="add_payment"') ?>

                            <!-- NAME -->
                            <div class="inputboxes">
                                <label for="message"><?php echo lang('Review')?>: </label>
                                <?php echo form_textarea('message', set_value('message', $review->message), 'id="message" class="inputbox" tabindex="1" type="text"')?>
                            </div>
                            
                      
                        
                                <input class="btn" type="button" onclick="$('.add_payment').submit();" tabindex="3" value="<?php echo lang('Save')?>" /> 
                          <?php echo form_close()?>
                    </div>
                </div>
            </div>
