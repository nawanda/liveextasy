<style>
	.contentbox ul{
		padding:5px 40px;
	}
</style>
<div class="container">
	<div class="conthead">
	<?php $this->load->view('includes/edit_buttons')?>
	</div>
	<div class="contentbox">
		<div class="center_contentbox">			
			<h2><?php echo lang('Profile information')?></h2>			
			<?php echo form_open('performers/categories/'. $performer->username)?>
				<?php if(sizeof($categories['main_categories']) > 0):?>
					<ul style="list-style-type:none; display:inline-block;margin-top:0px; ">
					<?php foreach($categories['main_categories'] as $main_category):?>
						<li>
							<?php echo form_checkbox('categories[]', $main_category->id,set_checkbox('categories',$main_category->id,(in_array($main_category->id,$performer_categories)?TRUE:FALSE)))?>
							<span class="gray italic bold"><?php echo lang($main_category->name) ?></span>
						</li>
						<?php if(sizeof($categories['sub_categories']) > 0 && isset($categories['sub_categories'][$main_category->id]) && sizeof($categories['sub_categories'][$main_category->id]) > 0):?>
							<li>
								<ul style="list-style-type:none; ">
									<?php foreach($categories['sub_categories'][$main_category->id] as $sub_category):?>
										<li>
											<?php echo  form_checkbox('categories[]', $sub_category->id,set_checkbox('categories',$sub_category->id,(in_array($sub_category->id,$performer_categories)?TRUE:FALSE)))?>
											<span class="gray italic bold"><?php echo lang($sub_category->name) ?></span>
										</li>
									<?php endforeach?>
								</ul>
							</li>
						<?php endif?>
					<?php endforeach?>
					</ul>
				<?php endif?>	
				<br /> <br />
				<input class="btn" type="submit" tabindex="3" value="<?php echo lang('Update categories')?>" />
			<?php echo form_close()?>	
		</div>
	</div>
</div>
