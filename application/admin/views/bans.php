<script type="text/javascript">
	$(document).ready(function(){
		$('#reset_filters').click(function(){
			document.location.href='<?php echo site_url('bans')?>';
		});

		$('.sort').click(function(){
			var url = '<?php echo site_url('bans')?>?<?php echo $filters?>&';
			document.location.href= url + 'orderby[' + $(this).attr('id')  + ']=' + $(this).attr('order'); 
		});
		$(function() {		
				$( ".date" ).datepicker({ dateFormat: 'dd-mm-yy'});
		});
	});
	</script>
			<div class="container">
				<div class="conthead">
					<h2><?php echo lang ('Banned Users') ?></h2>
				</div>

				<div class="contentbox">
					<div class="bulkactions" style="margin-bottom: 10px;">
						<?php echo form_open('bans','method="GET"')?>
							<p style="float:left; margin-right:10px;">
								&nbsp;<?php echo lang('User username')?><br/>
								<?php echo form_input('filters[user_name]', $this->input->_fetch_from_array($this->input->get('filters'),'user_name',TRUE), 'class="inputbox" style="min-width:150px;"')?>
							</p>
							<p style="float:left; margin-right:10px;">
								&nbsp;<?php echo lang('Performer nickname')?><br/>
								<?php echo form_input('filters[performer_nickname]', $this->input->_fetch_from_array($this->input->get('filters'),'performer_nickname',TRUE), 'class="inputbox" style="min-width:150px;"')?>					
							</p>
							<p style="float:left; margin-right:10px;">
								&nbsp;<?php echo lang('Ban expire date')?><br/>
								<?php echo form_input('filters[ban_expire_date]', $this->input->_fetch_from_array($this->input->get('filters'),'ban_expire_date',TRUE), 'class="inputbox date" style="min-width:150px;"')?>
							</p>				
							<p style="float:left; margin-right:10px;">
								&nbsp;<?php echo lang('User ip')?><br/>
								<?php echo form_input('filters[ip]', $this->input->_fetch_from_array($this->input->get('ip'),'first_name',TRUE), 'class="inputbox" style="min-width:150px;"')?>					
							</p>								
										
							<p style="float:left; margin-right:10px;">
								&nbsp;<br/>
								<input type="submit" class="btn" value="Apply Filter" style="margin-top:0px;" />
								<?php echo (strlen($filters) > 0) ? '<input type="button" id="reset_filters" class="btn" value="Reset Filters" style="margin-top:0px;" />' : null?>
							</p>
						</form>
					</div>						
					<table width="100%" border="0">
						<thead>
							<tr>
								<?php $this->load->view('table_sorting',array('name'=>lang('User'),				'key'=>'user_id',			'style'=>'width:100px') )?>
								<?php $this->load->view('table_sorting',array('name'=>'Performer',				'key'=>'performer_id',		'style'=>'width:100px') )?>
								<?php $this->load->view('table_sorting',array('name'=>lang('Ban date'),			'key'=>'ban_date',			'style'=>'width:100px'))?>
								<?php $this->load->view('table_sorting',array('name'=>lang('Expire ban'),		'key'=>'ban_expire_date',	'style'=>'width:100px'))?>
								<?php $this->load->view('table_sorting',array('name'=>lang('Ban time remaining'),'key'=>'ban_expire_date',	'style'=>'width:100px'))?>
								<?php $this->load->view('table_sorting',array('name'=>lang('Session duration'),	'key'=>'duration',			'style'=>'width:100px'))?>
								<?php $this->load->view('table_sorting',array('name'=>lang('User paid'),		'key'=>'user_paid_chips',	'style'=>'width:100px'))?>
								<?php $this->load->view('table_sorting',array('name'=>lang('Performer earning'),'key'=>'performer_chips',	'style'=>'width:100px'))?>
								<?php $this->load->view('table_sorting',array('name'=>lang('User ip'),			'key'=>'ip',				'style'=>'width:60px'))?>
								<?php $this->load->view('table_sorting',array('name'=>lang('Session type'),		'key'=>'type',				'style'=>'width:60px'))?>
								<th style="width: 60px; text-align: center;"><?php echo lang('Unban')?></th>
							</tr>
						</thead>
						<tbody>
					<?php if( sizeof($bans) == 0) : ?>
						<tr>
							<td colspan="11"><div style="text-align:center; width:100%; " class="clear"><h3><?php echo lang('There are no entries')?></h3></div></td>
						</tr>
					<?php else :
						$i = 1;
						foreach ($bans as $ban):?>
						
							<tr <?php echo ($i%2 == 0) ? 'class="alt"' : null?>>
								<td><a href="<?php echo site_url('users/account/'. $ban->user_name)?>"><?php echo $ban->user_name?></a></td>
								<td><a href="<?php echo site_url('performers/account/'.$ban->user_name)?>"><?php echo $ban->performer_nickname?></a></td>
								<td><?php echo date('d M Y H:i:s', $ban->ban_date)?></td>
								<td><?php echo date('d M Y H:i:s', $ban->ban_expire_date)?></td>
								<td><?php echo sec2hms($ban->ban_expire_date - $ban->ban_date)?></td>
								<td><?php echo sec2hms($ban->duration)?></td>
								<td><?php echo $ban->user_paid_chips?></td>
								<td><?php echo $ban->performer_chips?></td>
								<td><?php echo long2ip($ban->ip)?></td>
								<td><?php echo $ban->type?></td>
								<td style="text-align: center;">
									<a href="<?php echo site_url('bans')?>/unban/<?php echo $ban->unique_id?>" onclick="if(confirm('<?php echo lang('Are you sure you want to unban this user?')?>')) { return true;} else {return false;}" title=""><img src="<?php echo assets_url('admin/images/icons/icon_cross.png') ?>"></a>
								</td>
							</tr>
							<?php $i++?>
						<?php endforeach?>
					<?php endif?>
						</tbody>
					</table>
					<br />
					<div class="extrabottom">
						<?php echo $pagination?>
						<div class="bulkactions">
						</div>
					</div>
				</div>
			</div>