<?php
class Bans_controller extends MY_Admin{
	
	/*
	 * Constructor
	 */
	function __construct() {
		parent::__construct();
		$this->load->model('watchers');
	}

	// -----------------------------------------------------------------------------------------------------	
	/**
	 * Listare Banuri
	 * @return unknown_type
	 */
	function index(){			
		$filters	= purify_filters($this->input->get('filters'),'watchers');
		$order		= purify_orders($this->input->get('orderby'),'watchers');
		$data['filters']	= array2url($filters,'filters');
		$data['order_by']	= $this->input->get('orderby');

		$this->load->library('admin_pagination');
		
		$config['base_url']     = site_url('bans/page/');
		$config['uri_segment'] 	= 3;
		$config['total_rows']   = $this->watchers->get_banned_users(TRUE);
		$config['per_page']		= 20;
		$this->admin_pagination->initialize($config);
		$data['pagination']     = $this->admin_pagination->create_links();

		$data['bans']		= $this->watchers->get_banned_users(FALSE, $filters, implode_order($order), $this->uri->segment(3), $config['per_page']);
		
		$data['page'] = 'bans';
		$data['breadcrumb'][lang('Banned Users')]	= 'current';
		$data['page_head_title']		= lang('Banned Users'); 
		
		$this->load->view('template', $data);
	}
	
	
	// -----------------------------------------------------------------------------------------------------	
	/**
	 * Removes ban from wattchers
	 * @param unknown_type $unique_id
	 */
	function unban($unique_id = FALSE){
                restrict_on_demo();
		if( ! $unique_id ){		
			$this->session->set_flashdata('msg',	array('success'=>FALSE,	'message'=>lang('Invalid session ID!')));
			redirect('bans');
		}
		
		$session = $this->watchers->get_one_old_by_unique_id($unique_id);		
		if( ! $session ){
			$this->session->set_flashdata('msg',	array('success'=>FALSE,	'message'=>lang('Invalid session ID!')));
			redirect('bans');				
		}
		
                if(is_demo()){
                    $this->session->set_flashdata('msg',	array('success'=>FALSE,	'message'=>lang('Demo Mode!')));
                    redirect('bans');
                }
                
		if( ! $this->watchers->unban_session_by_unique_id($unique_id) ) {
			$this->session->set_flashdata('msg',	array('success'=>FALSE,	'message'=>lang('Database error!')));
			redirect('bans');				
		}
				
		$this->session->set_flashdata('msg',	array('success'=>TRUE,	'message'=>lang('Ban has been removed!')));
		redirect('bans');
	}
	
}