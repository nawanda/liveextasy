<?php
class Reviews_controller extends MY_Admin{
	
	/**
	 * Constructor
	 */
	function __construct(){
		parent::__construct();
		$this->load->model('performers_reviews');	
		$this->load->model('performers');	
	}
	
	
	/**
	 * Reviews List
	 * @param unknown_type $username
	 */
	function index($username = FALSE){
		if( ! $username ){
			redirect('performers');
		}
	
		
		$performer = $this->performers->get_all(array('username'=>$username));
		if( sizeof($performer) == 0 ){
			redirect('performers');
		}
	
		$performer = $performer[0];
	
		$this->load->library('admin_pagination');
		$config['base_url']     = site_url('reviews/'.$username);
		$config['uri_segment'] 	= 3;
		$config['total_rows']   = $this->performers_reviews->get_reviews_by_performer_id($performer->id, TRUE);
		$config['per_page']		= 20;
		$this->admin_pagination->initialize($config);
	
		$data['performer_reviews'] = $this->performers_reviews->get_reviews_by_performer_id($performer->id,FALSE,(int)$this->uri->segment(3), $config['per_page']);
		$data['pagination']     				= $this->admin_pagination->create_links();
	
		
		$data['breadcrumb']['Performers'] = base_url().'performers';	
		$data['page'] = 'reviews/index';	
		$data['page_head_title'] = $performer->username.'\'s '. lang('reviews');
		$data['breadcrumb'][$performer->username.'\'s ' . lang('reviews')] = 'current';
			
		$data['performer'] = $performer;
		$this->load->view('template', $data);	
	}
	
	
	/**
	 * Edit review
	 * @param int $review_id
	 */
	function edit($username, $review_id = FALSE){
		if( ! $review_id ){
			$this->session->set_flashdata('msg', array('type' => 'error', 'message' => lang('Invalid review id!')));
			redirect('reviews/'.$username);				
		}

		
		$review = $this->performers_reviews->get_by_id($review_id);		
		if( ! $review ){
			$this->session->set_flashdata('msg', array('type' => 'error', 'message' => lang('Review doesnt exist!')));
			redirect('reviews/'.$username);			
		}

		$performer = $this->performers->get_all(array('username'=>$username));
		if( sizeof($performer) == 0 ){
			$this->session->set_flashdata('msg', array('type' => 'error', 'message' => lang('Performer doesnt exist!')));
			redirect('reviews/'.$username);		
		}
	
		$performer = $performer[0];
		
		$this->load->library('form_validation');
		$this->form_validation->set_rules('message',	'message',	'trim|required|purify');

		if( $this->form_validation->run() === FALSE ){

			$data['breadcrumb']['Performers'] = base_url().'performers';	
			$data['page'] = 'reviews/edit';	
			$data['page_head_title'][$username.'\'s '. lang('reviews')] =  base_url().'reviews/'.$username;
			$data['breadcrumb'][lang('edit')] = 'current';
			$data['performer'] = $performer;
			$data['review'] = $review;
			$data['page'] = 'reviews/edit';
			$this->load->view('template', $data);
		} else {
			restrict_on_demo();
			$message = $this->input->post('message');
			$rows['id'] =  $review->id;
			$rows['message'] =  $message;
			if($this->performers_reviews->save($rows)){
				$this->session->set_flashdata('msg', array('type' => 'success', 'message' => lang('Review was saved!')));
				redirect('reviews/'.$username);		
			}

			$this->session->set_flashdata('msg', array('type' => 'error', 'message' => lang('Review was not saved! Please try again!')));
			redirect('reviews/'.$username);		

		}
	}
	
	/**
	 * Delete one review by id
	 * @param int $review_id
	 */
	function delete($username, $review_id = FALSE){
                restrict_on_demo();
		if( ! $review_id ){
			$this->session->set_flashdata('msg', array('type' => 'error', 'message' => lang('Invalid review id!')));
			redirect('reviews/'.$username);				
		}
		
		$review = $this->performers_reviews->get_by_id($review_id);
		if( ! $review ){
			$this->session->set_flashdata('msg', array('type' => 'error', 'message' => lang('Review doesnt exist!')));
			redirect('reviews/'.$username);			
		}
		
		if( $this->performers_reviews->delete($review_id) ){
			$this->session->set_flashdata('msg', array('type' => 'success', 'message' => lang('Review was deleted!')));
			redirect('reviews/'.$username);		
		}

		$this->session->set_flashdata('msg', array('type' => 'error', 'message' => lang('Review was not deleted! Please try again!')));
			redirect('reviews/'.$username);	
	}
}