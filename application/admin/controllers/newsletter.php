<?php
/**
 * @property Performers $performers
 * @property Status $status
 * @property System_log $system_log
 */
class Newsletter_controller extends MY_Admin {
	
	function __construct() {
		parent::__construct();
		$this->load->model('newsletter_cron');
		$this->load->model('affiliates');
		$this->load->model('performers');
		$this->load->model('studios');
		$this->load->model('users');
		$this->load->library('form_validation');
		$this->load->helper('generic_helper');
	}
	
	function index() {
		$data['account_types'] = array('all' => lang('All'),'affiliates'=>lang('Affiliates'), 'performers'=>lang('Performers'), 'studios'=>lang('Studios'), 'users'=>lang('Users'));
		$data['account_status'] = array('all'=>lang('All'));
		$avaiable_status = $this->users->get_enums_field('status');
		
		$data['account_status'] = array_merge($data['account_status'], $avaiable_status);
		
		$this->form_validation->set_rules('subject', lang('Email Subject'), 'trim|required|purify');
		$this->form_validation->set_rules('body', lang('Email body'), 'required');
		
		if($this->form_validation->run() == FALSE){			
			$data['page'] = 'newsletter';
			$data['breadcrumb'][lang('Newsletter')]	= 'current';
			$data['page_head_title']				= lang('Newsletter'); 

			$this->load->view('template', $data);
			return;
		}
                
                restrict_on_demo();
		
		$account_type	= $this->input->post('account_type');
		$account_status = $this->input->post('account_status');
		$subject		= $this->input->post('subject');
		$body			= $this->input->post('body');
		
                $subject = mysql_real_escape_string($subject);
                $body    = mysql_real_escape_string($body);
                
		if(!isset($data['account_types'][$account_type])){
			$this->session->set_flashdata('msg', array('type' => 'error', 'message' => lang('Invalid acount type!')));
			redirect('newsletter');
		}
		
		if(!isset($data['account_status'][$account_status])){
			$this->session->set_flashdata('msg', array('type' => 'error', 'message' => lang('Invalid acount status!')));
			redirect('newsletter');
		}
		
		$where_clause = "";
		$join         = "";
		if($account_status != 'all'){
			$where_clause = 'WHERE status IN (\''.$data['account_status'][$account_status].'\')';
		}else{
			$where_clause = 'WHERE status NOT IN (\'rejected\')';
		}

        if($account_type == 'users'){
            $where_clause .= ' AND newsletter = 1';
			$join          = 'INNER JOIN users_detail on users_detail.user_id = users.id';
        }

		$query  = 'INSERT INTO newsletter_cron (recipient_email, email_subject, email_body, add_date, sent)'."\n";
		if($account_type == 'all'){
			$query .= 'SELECT email, "'.$subject.'", "'.$body.'", '.time().', 0 FROM users'."\n";
			$query .= 'INNER JOIN users_detail on users_detail.user_id = users.id'."\n";
			$query .= $where_clause.' AND newsletter = 1'."\n";
			
			$query .= 'UNION ALL'."\n";
			$query .= 'SELECT email, "'.$subject.'", "'.$body.'", '.time().', 0 FROM performers'."\n";
			$query .= $where_clause."\n";
			
			$query .= 'UNION ALL'."\n";
			$query .= 'SELECT email, "'.$subject.'", "'.$body.'", '.time().', 0 FROM studios'."\n";
			$query .= $where_clause."\n";
			
			$query .= 'UNION ALL'."\n";
			$query .= 'SELECT email, "'.$subject.'", "'.$body.'", '.time().', 0 FROM affiliates'."\n";
			$query .= $where_clause."\n";
		}else{
			$query .= 'SELECT email, "'.$subject.'", "'.$body.'", '.time().', 0 FROM '.$account_type."\n";
			$query .= $join."\n";
			$query .= $where_clause."\n";
		}
		
		$this->db->query($query);
		$i = $this->db->affected_rows();
		
		$this->session->set_flashdata('msg', array('type' => 'success', 'message' => sprintf(lang('Newsletter was saved and will be sent to %s accounts!'),$i)));
		redirect('newsletter');	
	}	
}