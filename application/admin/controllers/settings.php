<?php
class Settings_controller extends MY_Admin {
	
	function __construct() {
		parent::__construct();
		$this->load->model('settings');
		$this->load->library('form_validation');
		$this->load->library('Load_settings');
	}
	
	
	
		// index
	function index() {
		
		
		$settings_files = array(
			'./application/admin/config/settings.php',
			'./application/affiliates/config/settings.php',
			'./application/performers/config/settings.php',
			'./application/studios/config/settings.php',
			'./application/main/config/settings.php'
		);

		
		
		// If settings files is writable
		foreach($settings_files as $file){
			if(!is_file($file)){
				$data['disabledForm'] = true;
				$data['err_msg']		= sprintf(lang('File %s not exist! Please create it and set CHMOD 777 rights!'), $file);
				break;
			}
			if(!is_writable($file)) {
				$data['disabledForm'] = true;
				$data['err_msg']		= sprintf(lang('File %s is not writable! Please set CHMOD 777 rights!'), $file);
				break;
			}
		}
		

		if($this->input->post('submit')) {
                        restrict_on_demo();
			if($this->config->item('demo_mode') === true) {
				$this->session->set_flashdata('msg', array('type' => 'error', 'message' => lang('Action restricted by demo mode!')));				
				redirect(current_url());
			}
			
			foreach ($_POST as $name => $value) {
				$setting = $this->settings->get_by_name($name);

				if(is_object($setting)) {
					
					$row['id'] = $setting->id;

					if($setting->type == 'boolean'){
						$value = (bool)$value;
					}elseif($setting->type == 'integer'){
						$value = (int)$value;
					}

					$row['value'] = $value;
					$this->settings->save($row);
					
				}
			}
			
			//get the new settings to write in files
			$new_settings = $this->settings->get_all();
			
			if($this->load_settings->load($new_settings, $settings_files)){
				$this->session->set_flashdata('msg', array('type' => 'success', 'message' => lang('Settings updated!')));
			}else{
				$this->session->set_flashdata('msg', array('type' => 'error', 'message' => lang('Failed to save settings!')));
			}

			redirect(current_url());
			
		} else {
			$data['themes']								= prepare_themes_dropdown();
			$data['currency']							= array('chips' => 'chips', 'euro' => 'euro', 'dollar' => 'dollar');
			
			$data['title'] 								= SETTINGS_SITE_TITLE;
			$data['breadcrumb'][lang('Settings')]		= 'current';
			$data['page_head_title']					= lang('Settings'); 
			$data['page']		= 'settings';
			$data['settings']	= $this->settings->get_all();
			$this->load->view('template', $data);
		}
	}
}