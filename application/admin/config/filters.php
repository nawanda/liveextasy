<?php
//accepted filters for admin
$config['filters']['users'] = array (
		'id' 			=> 'id',
		'username' 		=> 'username',
		'email' 		=> 'email',
		'country_code'	=> 'country_code',
		'status'		=> 'status',
		'gateway' 		=> 'gateway',
		'affiliate_id'	=> 'affiliate_id',
		'credits'		=> 'credits'
);
$config['filters']['performers'] = array (
		'id' 			=> 'id',
		'username' 		=> 'username',
		'nickname' 		=> 'nickname',
		'country_code'	=> 'country_code',
		'email' 		=> 'email',
		'first_name' 	=> 'first_name',
		'last_name' 	=> 'last_name',
		'status'		=> 'status',
		'contract_status'	=> 'contract_status',
		'photo_id_status'	=> 'photo_id_status',
		'is_online'			=> 'is_online',
		'is_online_hd'		=> 'is_online_hd',
		'is_online_type'	=> 'is_online_type',
		'is_in_private'		=> 'is_in_private',
		'enable_peek_mode'	=> 'enable_peek_mode',
		'studio_id'			=> 'studio_id',
		'credits'			=> 'credits'
);
$config['filters']['studios'] = array (
		'id' 			=> 'id',
		'username' 		=> 'username',
		'email' 		=> 'email',
		'country_code'	=> 'country_code',
		'first_name' 	=> 'first_name',
		'last_name' 	=> 'last_name',
		'status'		=> 'status',
		'contract_status' => 'contract_status',
		'percentage'	=> 'percentage',
		'credits'		=> 'credits'
);
$config['filters']['affiliates'] = array (
		'id' 			=> 'id',
		'username' 		=> 'username',
		'email' 		=> 'email',
		'country_code'	=> 'country_code',
		'first_name' 	=> 'first_name',
		'last_name' 	=> 'last_name',
		'register_ip'	=> 'register_ip',
		'register_date' => 'register_date',
		'status'		=> 'status',
		'credits'		=> 'credits'
);
$config['filters']['system_logs'] = array(
		'date'			=> 'date',
		'actor'			=> 'actor',
		'actor_id'		=> 'actor_id',
		'action_on'		=> 'action_on',
		'action_on_id'	=> 'action_on_id',
		'ip'			=> 'ip',
		'action'		=> 'action'
);
$config['filters']['categories'] = array(
		'id' 			=> 'id',
		'name' 			=> 'name',
		'link' 			=> 'link',
		'parent_id'		=> 'parent_id',		
);
$config['filters']['admins'] = array(
		'id' 			=> 'id',
		'username' 		=> 'username',
);

$config['filters']['to_pay'] = array(
		'status'		=> 'status'
);

$config['filters']['performer_sessions'] = array(
		'type'				=> 'type',
		'period'			=> 'period',
		'user_paid_chips'	=> 'user_paid_chips',
		'start_date'		=> 'start_date',
		'end_date'			=> 'end_date',
		'duration'			=> 'duration',
		'user_id'			=> 'user_id',
		'fee_per_minute'	=> 'fee_per_minute',
		'ip'				=> 'ip',
		'id'				=> 'id'
);

$config['filters']['studio_performers'] = array(
		'id' 			=> 'id',
		'nickname' 		=> 'nickname',
		'country_code'	=> 'country_code',
		'email' 		=> 'email',
		'first_name' 	=> 'first_name',
		'last_name' 	=> 'last_name',
		'status'		=> 'status',
		'register_date' => 'register_date'
);

$config['filters']['affiliate_payments'] = array(
		'id'			=> 'id',
		'paid_date'		=> 'paid_date',		
		'status'		=> 'status',		
		'comments'		=> 'comments',		
		'amount_chips'	=> 'amount_chips'
);

$config['filters']['affiliates_ads'] = array(
		'id'			=> 'id',
		'name'			=> 'name',
		'add_date'		=> 'add_date',
		'type'			=> 'type',
		'views'			=> 'views',
		'hits'			=> 'hits',
		'registers'		=> 'registers',
		'earnings'		=> 'earnings'
);

$config['filters']['affiliates_signups'] = array(
		'id'			=> 'id',
		'username'		=> 'username',
		'email'			=> 'email',
		'credits'		=> 'credits'
);

$config['filters']['studio_payments'] = array(
		'id'			=> 'id',
		'paid_date'		=> 'paid_date',		
		'amount_chips'	=> 'amount_chips'
);

$config['filters']['performer_payments'] = array(
		'id'			=> 'id',
		'paid_date'		=> 'paid_date',
		'amount_chips'	=> 'amount_chips'
);

$config['filters']['watchers'] = array(
		'user_id'			=> 'user_id',
		'user_name'			=> 'user_name',
		'performer_nickname'=> 'performer_nickname',
		'perormer_id'		=> 'performer_id',
		'ban_date'			=> 'ban_date',
		'ban_expire_date'	=> 'ban_expire_date',
		'duration'			=> 'duration',
		'user_paid_chips'	=> 'user_paid_chips',
		'performer_chips'	=> 'performer_chips',
		'ip'				=> 'ip',
		'type'				=> 'type',
);

