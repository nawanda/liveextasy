<?php $messages = $this->session->flashdata('msg');
if( (isset($messages) && $messages) ):
	if($messages['message']!=""):
		// TODO check issue with notifs
		// print_r($messages);
		if( ! $messages['success'] ):// ERRORS?>
			<div class="status error-alert" >
				<?php if(isset($messages) && $messages):?>
					<p><?php echo lang($messages['message'])?></p>
				<?php endif?>
			</div>
		<?php else://success?>
			<div class="status success-alert">
				<p><?php echo $messages['message']?></p>
			</div>
		<?php endif?>
	<?php endif?>
<?php endif?>
<div class="clear"></div>
