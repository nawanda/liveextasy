<div class="w-tab-menu center performer-menu">
	<div class="m5-tab-link-simple <?=($this->uri->segment(2) == 'password') ? 'selected' : NULL ?> w-inline-block w-tab-linkv2" onclick="document.location.href='<?php echo site_url('settings/password')?>';">
		<a href="<?php echo site_url('settings/password')?>"><?php echo lang('Password') ?></a>
	</div>
	<div class="m5-tab-link-simple <?=($this->uri->segment(2) == 'profile') ? 'selected' : NULL ?> w-inline-block w-tab-linkv2" onclick="document.location.href='<?php echo site_url('settings/profile')?>';">
		<a href="<?php echo site_url('settings/profile')?>"><?php echo lang('Profile') ?></a>
	</div>
	<div class="m5-tab-link-simple <?=($this->uri->segment(2) == 'personal-details') ? 'selected' : NULL ?> w-inline-block w-tab-linkv2" onclick="document.location.href='<?php echo site_url('settings/personal-details')?>';">
		<a href="<?php echo site_url('settings/personal-details')?>"><?php echo lang('Details') ?></a>
	</div>
	<?php if( ! $this->user->studio_id ):?>
		<div class="m5-tab-link-simple <?=($this->uri->segment(2) == 'payment') ? 'selected' : NULL ?> w-inline-block w-tab-linkv2" onclick="document.location.href='<?php echo site_url('settings/payment')?>';">
			<a href="<?php echo site_url('settings/payment')?>"><?php echo lang('Payment') ?></a>
		</div>
	<?php endif?>
	<div class="m5-tab-link-simple <?=($this->uri->segment(2) == 'pricing') ? 'selected' : NULL ?> w-inline-block w-tab-linkv2" onclick="document.location.href='<?php echo site_url('settings/pricing')?>';">
		<a href="<?php echo site_url('settings/pricing')?>"><?php echo lang('Pricing') ?></a>
	</div>
	<div class="m5-tab-link-simple <?=($this->uri->segment(2) == 'banned_zones') ? 'selected' : NULL ?> w-inline-block w-tab-linkv2" onclick="document.location.href='<?php echo site_url('settings/banned_zones')?>';">
		<a href="<?php echo site_url('settings/banned_zones')?>"><?php echo lang('Banned zones') ?></a>
	</div>
	<div class="m5-tab-link-simple <?=($this->uri->segment(2) == 'categories') ? 'selected' : NULL ?> w-inline-block w-tab-linkv2" onclick="document.location.href='<?php echo site_url('settings/categories')?>';">
		<a href="<?php echo site_url('settings/categories')?>"><?php echo lang('Categories') ?></a>
	</div>
	<div class="m5-tab-link-simple <?=($this->uri->segment(2) == 'schedule') ? 'selected' : NULL ?> w-inline-block w-tab-linkv2" onclick="document.location.href='<?php echo site_url('settings/schedule')?>';">
		<a href="<?php echo site_url('settings/schedule')?>"><?php echo lang('Schedule') ?></a>
	</div>
	<div class="m5-tab-link-simple <?=($this->uri->segment(1) == 'photos') ? 'selected' : NULL ?> w-inline-block w-tab-linkv2" onclick="document.location.href='<?php echo site_url('photos')?>';">
		<a href="<?php echo site_url('photos')?>"><?php echo lang('Photos') ?></a>
	</div>
	<div class="m5-tab-link-simple <?=($this->uri->segment(1) == 'videos') ? 'selected' : NULL ?> w-inline-block w-tab-linkv2" onclick="document.location.href='<?php echo site_url('videos')?>';">
		<a href="<?php echo site_url('videos')?>"><?php echo lang('Videos') ?></a>
	</div>
	<div class="m5-tab-link-simple <?=($this->uri->segment(1) == 'contracts') ? 'selected' : NULL ?> w-inline-block w-tab-linkv2" onclick="document.location.href='<?php echo site_url('contracts')?>';">
		<a href="<?php echo site_url('contracts')?>"><?php echo lang('Contracts') ?></a>
	</div>
	<div class="m5-tab-link-simple <?=($this->uri->segment(1) == 'photo_id') ? 'selected' : NULL ?> w-inline-block w-tab-linkv2" onclick="document.location.href='<?php echo site_url('photo_id')?>';">
		<a href="<?php echo site_url('photo_id')?>"><?php echo lang('Photo ID') ?></a>
	</div>
</div>
<div class="m5-tab-content-actualv2 w-tab-content shadowv3">
	<?php
		if(isset($page)){
			$this->load->view($page);
		}
	?>
</div>
<div class="clear"></div>
