
<div id="left_menu" class="performer-menu">
	<div class="m5-tab-link-simple <?=($this->uri->segment(1) == 'my_earnings') ? 'selected' : NULL ?>  w-inline-block w-tab-linkv2" onclick="document.location.href='<?php echo site_url('my_earnings')?>';">
		<a href="<?php echo site_url('my_earnings')?>"><?php echo lang('Earnigns') ?></a>
	</div>
	<div class="m5-tab-link-simple  <?=($this->uri->segment(1) == 'my_payments') ? 'selected' : NULL ?>  w-inline-block w-tab-linkv2" onclick="document.location.href='<?php echo site_url('my_payments')?>';">
		<a href="<?php echo site_url('my_payments')?>"><?php echo lang('Payments') ?></a>
	</div>
</div>
<div class="fl_r m5-tab-content-actualv2 w-tab-content shadowv3">
	<?php
		if(isset($page)){
			$this->load->view($page);
		}
	?>
</div>
<div class="clear"></div>
