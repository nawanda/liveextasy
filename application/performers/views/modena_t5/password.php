<script type="text/javascript">
jQuery(function($){

	$.validator.setDefaults({
		validClass:"success",
		errorElement: "span",
		errorPlacement: function(error, element) {
			error.appendTo($(element).next('span').next('span'));
		}
	});

	$(".change_pass").validate({

		success: function(label) {
	    	label.addClass("valid");
	   },
		rules: {
		    old_password: {
	   			required: true,
				minlength: 5
			},
			new_password: {
		   		required: true,
				minlength: 5
			},
			confirm_password: {
		   		required: true,
				minlength: 5,
				equalTo: "#new_password"

			}
		},
		messages: {
			old_password: 					"<?php echo lang('Password must have at least 5 characters') ?>",
			new_password:					"<?php echo lang('Password must have at least 5 characters') ?>",
			confirm_password:				"<?php echo lang('Passwords do not match') ?>"
		}
	});
});
</script>
		<div class="content" id="profile">
			<div class="white performers-details-titles">
				<h4><?php echo lang('Update your Password') ?></h4>
				<p><?php echo lang('You can choose to change your password here.') ?></p>
			</div>

			<div class="m5-signup-form w-form">

				<?php echo form_open(current_url(), 'class="change_pass"')?>
					<div>
						<label><span class="m5-form-label"><?php echo lang('Old Password') ?>:</span></label>
						<?php echo form_password(array('name' => 'old_password', 'class' => 'm5-input w-input'))?>
						<span class="error" htmlfor="old_password" generated="true"><?php echo form_error('old_password')?></span>
					</div>
					<div>
						<label><span class="m5-form-label"><?php echo lang('New Password') ?>:</span></label>
						<?php echo form_password(array('name' => 'new_password', 'id' => 'new_password', 'class' => 'm5-input w-input'))?>
						<span class="error" htmlfor="new_password" generated="true"><?php echo form_error('new_password')?></span>
					</div>
					<div>
						<label><span class="m5-form-label"><?php echo lang('Confirm Password') ?>:</span></label>
						<?php echo form_password(array('name' => 'confirm_password', 'class' => 'm5-input w-input'))?>
						<span class="error" htmlfor="confirm_password" generated="true"><?php echo form_error('confirm_password')?></span>
					</div>
					<div>
						<?php echo form_submit('submit', lang('Save Changes'), 'class="submit-button-2 w-button"')?>
					</div>
				<?php echo form_close() ?>

			<div class="clear"></div>
		</div>
	</div>
