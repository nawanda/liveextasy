<div class="go-online">
  <div class="content">
    <div class="go-online-container">
      <h3 class="account-page white"><?php echo lang('Go Online') ?></h3>
      <p><?php echo lang('Ready. Set. Go! Choose which platform you want to go online with.') ?></p>
    </div>
    <?php
      $height = '640';
    if($this->agent->is_browser('Safari')){
      $height = '640';
    }
    ?>

    <div class="left-online-section m5-panel-gray shadowv2">
      <div class="second-title-go-online">
        <h4 class="center white">Web Based Encoder</h4>
        <p class="center">go online using our web-based encoder.</p>
        <div class="border-black"></div>
      </div>

      <div class="content-txt">
        <span class="white"><?php echo lang('Web based encoder advantages')?></span>:
        <div>
          &raquo;&nbsp;<?php echo lang('Does not require 3rd party apps or plug-ins')?><br/>
          &raquo;&nbsp;<?php echo lang('Automated bandwidth selection based on speed test')?><br/>
          &raquo;&nbsp;<?php echo lang('Pre-set Low, Medium and High quality settings')?><br/>
          &raquo;&nbsp;<?php echo lang('Low resource usage ')?><br/>
        </div>
      </div>
      <?php echo form_button('Go online',lang('Go Online Now'),'class="submit-button-2 w-button" onclick="window.open(\''. site_url('live') . '\',\'\',\'menubar=no,width=900,height='.$height.',toolbar=no\')"')?>
    </div>
  </div>
  <div class="content right-online-section m5-panel-gray shadowv2">
    <div class="second-title-go-online">
      <h4 class="center white">FMLE/WireCast (Recommended)</h4>
      <p class="center">go online using fmle or wirecast service.</p>
      <div class="border-black"></div>
    </div>
    <div class="content-txt">
      <span class="white"><?php echo lang('FMLE encoder advantages')?></span>:
      <div style="margin-left:10px;">
        &raquo;&nbsp;<?php echo lang('H.264/MP3 video/audio encoding technology')?><br/>
        &raquo;&nbsp;<?php echo lang('Customizable individual quality settings')?><br/>
        &raquo;&nbsp;<?php echo lang('The result is HD video and audio')?><br/>
        &raquo;&nbsp;<?php echo lang('Less bandwidth usage')?><br/>
      </div>
    </div>
          <?php echo form_button('Go online',lang('Go Online Now'),'class="submit-button-2 w-button" onclick="window.open(\''. site_url('live?fmle=1') . '\',\'\',\'menubar=no,width=900,height='.$height.',toolbar=no\')"')?>
      <br/>
    </div>
    <div class="clear"></div>
    <div class="go-online-list m5-panel-gray shadowv2">
      <div class="content-txt">
        <span class="white"><?php echo lang('1. What are they?')?></span>
        <div><?php echo lang('These are 3rd party applications that have to be installed on your Windows or MAC computer. They are video and audio encoders that compress the video from your camera and stream it to ModenaCam server with a higher quality than the web based application.')?></div>
        <span class="white"><?php echo lang('2. Who is using it?')?></span>
        <div><?php echo lang('All professionals from the entertainment industry are using it.')?></div>
        <span class="white"><?php echo lang('3. How much does it cost?')?></span>
        <div><?php echo lang('Adobe\'s FMLE is free software, but WireCast is a commercial application.')?></div>
        <span class="white"><?php echo lang('4. Flash Media Live Encoder contains spyware or adware since it\'s free?')?></span>
        <div><?php echo lang('No, the application is clean and safe. It is built by Adobe. <a class="white" href="https://offers.adobe.com/en/na/leap/landings/fmle3.html">Official home page.</a>')?></div>
        <span class="white"><?php echo lang('5. Do I need to run both? Which one should I choose?')?></span>
        <div><?php echo lang('They\'re both complete and powerful solutions, you can choose whichever you want. You don\'t need to use them both, basically they\'re doing the same thing.')?></div>
        <span class="white"><?php echo lang('Start now!')?></span>
        <span class="white"><?php echo lang('Step1: Installation of Adobe Live Encoder or WireCast')?></span>
        <div><?php echo lang('Download the Adobe application here. Install it and run it. MAC users, please visit Adobe\'s site for the latest installation kit. WireCast users, please consult the official site in order to get a license.')?></div>

        <span class="white"><?php echo lang('Step 2: Configuration')?></span>
        <div><?php echo sprintf(lang('Download the XML configuration file <a class="white" href="%s">here</a>, run Flash Media Live Encoder, go to file > Open profile and select the recently downloaded configuration file. Select the video and audio device you\'re going to use and press the Start button. You can adjust the video settings in order to adapt the stream to your hardware and Internet connection.'),site_url('fmle?url=' . $fms->fms .'&stream=cam1&fms_id='. $fms->fms_id))?></div>
      </div>
    </div>
  </div>
  <div class="clear"></div>
</div>
