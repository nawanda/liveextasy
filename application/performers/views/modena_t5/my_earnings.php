<script type="text/javascript" src="<?php echo assets_url()?>js/jquery.strtotime.min.js"></script>
<script type="text/javascript" src="<?php echo assets_url()?>js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
jQuery(function($){

	$( ".datepicker" ).datepicker({ dateFormat: 'dd-mm-yy' });
	$('.action').click(function(){
		document.location = '/my_earnings/' + $.strtotime($('input[name=start_date]').val()) + '/' + $.strtotime($('input[name=end_date]').val());
	});
});

jQuery(function($){
	$('#payment_form').submit(function(){
		var value = $('#paymentDates :selected').val();
		if(value == 0) {
			window.location = 'my_earnings';
		} else {
			var split_dates = value.split('~');
			window.location = '<?php echo site_url('my_earnings')?>/' + split_dates[0] + '/' + split_dates[1];
		}
		return false;
	});
});
</script>
<script type="text/javascript">
function toTimestamp(strDate){
	var datum = Date.parse(strDate);
	return datum/1000;
}
</script>
<link rel="stylesheet" type="text/css" href="<?php echo assets_url()?>css/table.css" media="screen" />
	<div class="content earnings">
		<div class="white performers-details-titles">
			<h4><?php echo lang('Earnings')?></h4>
		</div>
		<div class="earn-inputs">
			<?php echo form_open('payments', array('method' => 'GET','id'=>'payment_form', 'class'=>'payment_form')) ?>
				<?php echo form_dropdown('paymentDates', $options, set_value('paymentDates') , 'class="time_period_sele select-input" id="paymentDates"') ?>
				&nbsp;&nbsp;&nbsp;<button class="w-button viewearnings m-t-2" onclick="javascript:viewearnings();"><?php echo lang('View Earnings') ?></button>
			<?php echo form_close()?>
		</div>
		<h4 class="payment_interval white"><?php echo date('d M Y', strtotime($start_date)) . ' - ' . date('d M Y', strtotime($stop_date)); ?></h4>
		<div>
			<table class="data display datatable">
				<thead>
					<tr>
						<th style="width: 20%; white-space: nowrap;"><?php echo lang('Start date') ?></th>
						<th style="width: 20%; white-space: nowrap;"><?php echo lang('End date') ?></th>
						<th style="width: 20%; white-space: nowrap;"><?php echo lang('User') ?></th>
						<th style="width: 20%; white-space: nowrap;"><?php echo lang('Type') ?></th>
						<th style="width: 20%; white-space: nowrap;"><?php echo lang('Length') ?></th>
						<th style="width: 20%; white-space: nowrap;"><?php echo lang('Earnings') ?></th>
					</tr>
				</thead>
				<tbody>
					<?php if(sizeof($watchers) == 0): ?>
						<tr>
							<td colspan="6" style="text-align: center;"><?php echo lang('You have no earnings') ?></td>
						</tr>
					<?php else:?>
						<?php $i = 0?>
						<?php foreach($watchers as $watcher ):?>
							<tr class="<?php echo ($i % 2==0)?'even':'odd'?>">
								<td><?php echo date('Y-m-d H:i:s',$watcher->start_date)?></td>
								<td><?php echo date('Y-m-d H:i:s',$watcher->end_date)?></td>
								<td><?php echo $watcher->username ?></td>
								<td><?php echo lang($watcher->type)?></td>
								<td><?php echo ($watcher->duration)?sec2hms($watcher->duration):'N/A'?></td>
								<td><?php echo print_amount_by_currency($watcher->performer_chips)?></td>
							</tr>
							<?php $i++?>
						<?php endforeach;?>
					<?php endif;?>
				</tbody>
			</table>
			<?php echo $pagination?>
		</div>
		<div class="clear"></div>
	</div>
