<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title><?php echo lang('Live broadcasting - ').SETTINGS_SITE_TITLE ?></title>
	<script type="text/javascript" src="<?php echo assets_url()?>swf/assets/js/swfobject.js"></script>
        <script src="<?php echo assets_url()?>mobile/stream/js/socket.io/socket.io.js"></script>
            <script src="<?php echo assets_url()?>mobile/stream/js/moment.min.js"></script>
</head>
<body style="margin:0px;background-color:#000">
	<script type="text/javascript">
		function downloadXml(){
			window.open('<?php echo site_url('fmle')?>?url=<?php echo $fms->fms?>&stream=cam1&fms_id=<?php echo $fms->fms_id?>','Download');
		}
                
                var socket =  io.connect('<?=SOCKET_URL.':'.SOCKET_PORT?>');
                socket.on('connect', function() {
                    socket.emit('register', {
                        uniqueId : "<?php echo mt_rand()?>",
                        username : "<?php echo $performer->username?>",
                        pasword : "<?php echo $performer->password ?>",
                        performerId : "<?php echo $performer->id?>",
                        userId : "<?php echo $performer->id?>",
                        sessionType : "",
                        ip: "<?php echo $this->input->ip_address()?>",
                        truePrivate: false,
                        userType: "performer"
                    });
                });

                socket.on('message', function(data, ackServerCallback) {
                    console.log(data);
                    
                    //Check if tip
                    if(data.hasOwnProperty("amount") && data.hasOwnProperty("username")) {
                        postTip(data.username, data.amount);
                        return;
                    }
                    
                    //Check if request
                    if(data.hasOwnProperty("chatType")) {
                        inviteToChat(data);
                    }
                    
                    if(data.hasOwnProperty("type") && data.type == "TakeABreak") {
                        dressMode();
                    }
                    //Message
                    if(!data.fromFlash) {
                        thisMovie("PerformerApp").postChatLine(
                            "0x" + data.color, data.username, data.message, "");
                    }
                });

                function postTip($username, $amount){
                    thisMovie("PerformerApp").postTip($username, $amount);
                }

                function inviteToChat(data){
                    console.log("DEBUG: " + JSON.stringify(data, replacer));
                    thisMovie("PerformerApp").invite( data.credits, data.username, data.chatType, data.id);
                }

               	function dressMode(){
                    thisMovie("PerformerApp").dressMode();
                }

                function thisMovie(movieName)
                {
                    if (navigator.appName.indexOf("Microsoft") != -1)
                    {
                        return window[movieName];
                    }
                    else
                    {
                        return document[movieName];
                    }
                }

                function receivedMessage(color, nick, message, avatar){
                    var jsonObject = {
                        '@class': 'eu.agilio.actions.Message',
                        message: message,
                        username: nick,
                        color: color,
                        fromFlash: true
                    };
                    socket.json.send(jsonObject);
                }

                function acceptedInvite(inviteId){
                    var jsonObject = {
                        '@class': 'eu.agilio.actions.InviteResponse',
                        inviteId: inviteId,
                        status: 'allow',
                    };
                    socket.json.send(jsonObject);
                }
                
                function denyInvite(inviteId){ 
                    var jsonObject = {
                        '@class': 'eu.agilio.actions.InviteResponse',
                        inviteId: inviteId,
                        status: 'deny',
                    };
                    socket.json.send(jsonObject);
                }
                
                function replacer(key, value) {
                    if (typeof value === 'number' && !isFinite(value)) {
                        return String(value);
                    }
                    return value;
                }

            </script>

	<script type="text/javascript">
            // For version detection, set to min. required Flash Player version, or 0 (or 0.0.0), for no version detection. 
            var swfVersionStr = "0.0.0";
            // To use express install, set to playerProductInstall.swf, otherwise the empty string. 
            var xiSwfUrlStr = "playerProductInstall.swf";
            var flashvars = {};
            flashvars.skin = "modern"
            flashvars.assetDir = "<?php echo assets_url()?>swf/assets"
            flashvars.themeFile="<?php echo assets_url()?>swf/assets/files/modern/theme.xml" 
            flashvars.languageFile="<?php echo main_url()?>fms/translation/<?php echo $this->user->id ?>"     
            flashvars.configFile="<?php echo assets_url()?>swf/assets/files/config.xml" 
            flashvars.smileysFile="<?php echo assets_url()?>swf/assets/files/smileys.xml"
            flashvars.mp3ListFile="<?php echo site_url()?>fms/mp3"
            flashvars.censureFile="<?php echo main_url()?>censure.xml"
            flashvars.sitePath="<?php echo main_url()?>"
            
            flashvars.rtmp="<?php echo $fms->fms?>"
            flashvars.pasword="<?php echo $performer->password?>"
            flashvars.userName="<?php echo $performer->nickname?>"
            flashvars.nickColor="0xFF0010"
            flashvars.userId = "<?php echo $performer->id?>"
            flashvars.performerId = "<?php echo $performer->id?>"
            flashvars.recPrivateShow="false"
            flashvars.fmsId="<?php echo $fms->fms_id?>"
            flashvars.uniqId="<?php echo $unique_id?>"
            flashvars.defaultChatType = "1"
            
            flashvars.logoImage = "<?=assets_url().'images/watermark.png'?>"  
            flashvars.logoBottom = "10"  
            flashvars.logoRight= "10"
            //flashvars.logoTop = "10"  
            //flashvars.logoLeft= "10"
            
            flashvars.translateLink = "<?php echo main_url()?>fms/googletranslate"
            <?php if(defined('GOOGLE_TRANSLATION_ENABLE') && GOOGLE_TRANSLATION_ENABLE && GOOGLE_API_KEY != 'null'): ?>
            flashvars.useTranslation = "true"
            <?php endif ?>
            
            <?php if($fmle):?>
                flashvars.fmleUrl="<?php echo base64_encode($fms->fms. $this->user->id .'?userId=' . $this->user->id . '&uniqId=' . $unique_id . '&pasword=' . $this->user->password . '&performerId=' . $this->user->id . '&username=' . $this->user->username . '&fmsId='.$fms->fms_id)?>"
                flashvars.fmleStream="cam1"
            <?php endif ?>
            
            var params = {};
            params.quality = "high";
            params.scale="exactfit";
            params.allowscriptaccess = "always";
            params.allowfullscreen = "true";
            params.wmode = "direct";
            var attributes = {};
            attributes.id = "PerformerApp";
            attributes.name = "PerformerApp";
            attributes.align = "middle";
            swfobject.embedSWF(
                "<?php echo main_url()?>assets/swf/PerformerApp.swf", "flashContent", 
                "900", "640", 
                swfVersionStr, xiSwfUrlStr, 
                flashvars, params, attributes);
            // JavaScript enabled so display the flashContent div in case it is not replaced with a swf object.
            swfobject.createCSS("#flashContent", "display:block;text-align:left;");
        </script>


	<div id="flashContent">
			<p>
			    To view this page ensure that Adobe Flash Player version 
                0.0.0 or greater is installed. 
            </p>
            <script type="text/javascript"> 
                var pageHost = ((document.location.protocol == "https:") ? "https://" : "http://"); 
                document.write("<a href='http://www.adobe.com/go/getflashplayer'><img src='" 
                                + pageHost + "www.adobe.com/images/shared/download_buttons/get_flash_player.gif' alt='Get Adobe Flash player' /></a>" ); 
            </script> 
	</div>


    
        <noscript>
            <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="900" height="640" id="PerformerApp">
                <param name="movie" value="<?php echo main_url()?>assets/swf/PerformerApp.swf" />
                <param name="quality" value="high" />
                <param name="bgcolor" value="#ffffff" />
                <param name="allowScriptAccess" value="always" />
                <param name="allowFullScreen" value="true" />
                <param name="sAlign" value="tl" />
                <param name="scale" value="exactfit" />
                <param name="themeFile" value="<?php echo assets_url()?>swf/assets/files/theme.xml"/>
                <param name="languageFile" value="<?php echo main_url()?>fms/translation/<?php echo $this->user->id ?>"/>
                <param name="configFile" value="<?php echo assets_url()?>swf/assets/files/config.xml"/>
                <param name="smileysFile" value="<?php echo assets_url()?>swf/assets/files/smileys.xml"/>
                <param name="mp3ListFile" value="<?php echo site_url()?>fms/mp3"/>
                <param name="censureFile" value="<?php echo main_url()?>censure.xml"/>
                <param name="sitePath" value="<?php echo main_url()?>"/>
                
                <param name="rtmp" value="<?php echo $fms->fms?>" />
           		<param name="pasword" value ="<?php echo $performer->password?>"  />
	            <param name="userName" value ="<?php echo $performer->nickname?>"  />
	            <param name="nickColor" value="0xFF0010"  />
	            <param name="userId" value = "<?php echo $performer->id?>"  />
	            <param name="performerId" value = "<?php echo $performer->id?>"  />
	            <param name="recPrivateShow" value="false"  />
	            <param name="fmsId" value="<?php echo $fms->fms_id?>"  />    
	            <param name="uniqId" value="<?php echo $unique_id?>" />      
	            
                <?php if($fmle):?>
    	            <param name="fmleUrl" value="<?php echo base64_encode($fms->fms. $this->user->id .'?userId=' . $this->user->id . '&uniqId=' . $unique_id . '&pasword=' . $this->user->password . '&performerId=' . $this->user->id . '&username=' . $this->user->username . '&fmsId='.$fms->fms_id)?>" />    
                    <param name="fmleStream" value="cam1" />
                <?php endif?>
                
                <!--[if !IE]>-->
                <object type="application/x-shockwave-flash" data="<?php echo main_url()?>assets/swf/PerformerApp.swf" width="900" height="640">
                    <param name="quality" value="high" />
                    <param name="bgcolor" value="#ffffff" />
                    <param name="allowScriptAccess" value="always" />
                    <param name="allowFullScreen" value="true" />
                    <param name="sAlign" value="tl" />
                    <param name="scale" value="exactfit" />
                    <param name="themeFile" value="<?php echo assets_url()?>swf/assets/files/theme.xml"/>
                    <param name="languageFile" value="<?php echo main_url()?>fms/translation/<?php echo $this->user->id ?>"/>
                    <param name="configFile" value="<?php echo assets_url()?>swf/assets/files/config.xml"/>
                    <param name="smileysFile" value="<?php echo assets_url()?>swf/assets/files/smileys.xml"/>
                    <param name="mp3ListFile" value="<?php echo site_url()?>fms/mp3"/>
                    <param name="censureFile" value="<?php echo main_url()?>censure.xml"/>
                    <param name="sitePath" value="<?php echo main_url()?>"/>
                    
                    <param name="rtmp" value="<?php echo $fms->fms?>" />
                            <param name="pasword" value ="<?php echo $performer->password?>"  />
		            <param name="userName" value ="<?php echo $performer->nickname?>"  />
		            <param name="nickColor" value="0xFF0010"  />
		            <param name="userId" value = "<?php echo $performer->id?>"  />
		            <param name="performerId" value = "<?php echo $performer->id?>"  />
		            <param name="recPrivateShow" value="false"  />
		            <param name="fmsId" value="<?php echo $fms->fms_id?>"  />    
		            <param name="uniqId" value="<?php echo $unique_id?>" /> 
		            
                    <?php if($fmle):?>
    		            <param name="fmleUrl" value="<?php echo base64_encode($fms->fms. $this->user->id .'?userId=' . $this->user->id . '&uniqId=' . $unique_id . '&pasword=' . $this->user->password . '&performerId=' . $this->user->id . '&username=' . $this->user->username . '&fmsId='.$fms->fms_id)?>" />   
    		            <param name="fmleStream" value="cam1" />
                    <?php endif ?>
		            
                <!--<![endif]-->
                <!--[if gte IE 6]>-->
                    <p> 
                        Either scripts and active content are not permitted to run or Adobe Flash Player version
                        0.0.0 or greater is not installed.
                    </p>
                <!--<![endif]-->
                    <a href="http://www.adobe.com/go/getflashplayer">
                        <img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash Player" />
                    </a>
                <!--[if !IE]>-->
                </object>
                <!--<![endif]-->
            </object>
        </noscript>     


</body>
</html>