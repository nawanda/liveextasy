
<?php $this->load->view('includes/head')?>
	<body class="m5-body">
		<div class="m5-main-header">
			<div class="w-container">
				<div class="navbar w-nav" data-animation="default" data-collapse="medium" data-duration="400">
					<div class="w-container">

						<?php $this->load->view('includes/logo_box')?>
						<?php $this->load->view('includes/welcome_user_box')?>

					</div>
				</div>
			</div>
		</div>
		<div class="m5-secondary-header">
			<div class="w-container">
      			<div class="row w-row">

					<?php $this->load->view('includes/menu');?>

				</div>
			</div>
		</div>
		<div class="m5-videos-container w-container">

			<?php $this->load->view('includes/errors');?>
					
					<?php
					if (isset($_sidebar) && $_sidebar) {
						$this->load->view('includes/_sidebar');
					} elseif (isset($page) && !(isset($_payments_sidebar) && $_payments_sidebar)) {
						$this->load->view($page);
					}

					if (isset($_payments_sidebar) && $_payments_sidebar) {
						$this->load->view('includes/_my_payments_sidebar');
					}
					?>

		</div>

			<?php $this->load->view('includes/footer')?>

	</body>
	</html>
