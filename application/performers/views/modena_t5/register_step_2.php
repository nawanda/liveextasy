<script type="text/javascript">
jQuery.validator.setDefaults({
	validClass:"success",
	errorElement: "span",
	errorPlacement: function(error, element) {
		error.appendTo($(element).next('span').next('span'));
	}
});

jQuery(function($){

	$(".register_performer").validate({
		success: function(label) {
	    	label.addClass("valid")
	   },
		rules: {
			payment_method: {
				required: true
			}
		},
		messages: {
			payment_method: 			"<?php echo lang('Please select your payment method') ?>"

		}
	});

	if( $('#payment_method').val() > 0 ){
		$('#payment_method_'+$('#payment_method').val()+ " input").each(function(key,element){
			$(element).rules("add",{
				required:true,
				messages:{
					required: "<?php echo lang('This field is required')?>"
				}
			});

		});
	}

	$('#payment_method').change(function(){
		$('.methods input').each(function(key,element){
			$(element).rules("remove");
		});
		$('#payment_method_'+$('#payment_method').val()+ " input").each(function(key,element){

			$(element).rules("add",{
				required:true,
				messages:{
					required: "<?php echo lang('This field is required')?>"
				}
			});

		});

		$('.methods:visible').slideUp();
		$('#payment_method_'+$('#payment_method').val()).slideDown();
	});

	$('#payment_method_'+$('#payment_method').val()).slideDown();
});
</script>
<div class="content">
	<div class="white performers-details-titles">
		<h4><?php echo lang('Signup step 2 - Payment') ?></h4>
		<p><?php echo lang('Please fill this fields') ?></p>
	</div>
	<?php echo form_open('register', 'class="register_performer"')?>
	<div class="m5-panel shadow relativ" id="register_performer">
		<div class="m5-signup-form w-form">
			<div>
				<label><span class="m5-form-label"><?php echo lang('Payment Method') ?>:</span></label>
				<?php echo form_dropdown('payment_method', $payment_methods, set_value('payment_method'),'id="payment_method" class="m5-input w-input"')?>
				<span class="error" htmlfor="payment_method" generated="true"><?php echo form_error('payment_method')?></span>
			</div>
			<div id="selected_payment_fields" style="margin-left:0px; display:block;">
				<?php foreach($this->payment_method_list as $payment_method):?>
					<?php $fields = unserialize($payment_method->fields)?>
					<div id="payment_method_<?php echo $payment_method->id?>" class="methods" <?php echo  ($selected_method != $payment_method->id)?' style="display:none"':NULL?>>
						<?php foreach($fields as $field):?>
							<?php $field_name = strtolower(str_replace(' ', '_', $field)) . '_'.$payment_method->id?>
							<div>
								<label><span class="m5-form-label"><?php echo lang($field)?>:</span></label>
								<?php echo form_input($field_name,set_value($field_name),'class="m5-input w-input"', '');?>
								<span generated="true" htmlfor="<?php echo $field_name?>" class="error"><?php echo form_error($field_name)?></span>
							</div>
						<?php endforeach?>
						<div>
							<label><span class="m5-form-label"><?php echo lang('Release amount')?>:</span></label>
							<?php echo form_input('rls_amount' . '_' . $payment_method->id,set_value('rls_amount'. '_' . $payment_method->id),'class="m5-input w-input"')?>
							<span generated="true" htmlfor="rls_amount_<?php echo $payment_method->id?>" class="error"><?php echo (form_error('rls_amount'. '_' . $payment_method->id))?form_error('rls_amount'. '_' . $payment_method->id):sprintf(lang('Min. %s %s'),$payment_method->minim_amount,SETTINGS_REAL_CURRENCY_NAME)?></span>
						</div>
					</div>
				<?php endforeach?>
			</div>
			<div class="clear"></div>
			<div>
				<?php echo form_submit('submit', lang('Continue'), 'class="submit-button-2 w-button"')?>
			</div>
			<div class="clear"></div>
		</div>
	</div>
	<div class="dots-list">
			<div class="circle"></div>
			<div class="circle circle-select"></div>
			<div class="circle"></div>
			<div class="circle"></div>
			<div class="circle"></div>
	</div>
</div>
