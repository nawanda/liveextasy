	<div class="content">
		<div class="white performers-details-titles">
			<h4><?php echo lang('Performer\'s Profile Page') ?></h4>
			<p><?php echo lang('You can edit your performer page here.') ?></p>
		</div>
		<div id="performer_profile">
					<?php echo form_open('')?>
						<div class="register_performer m5-signup-form w-form" >
						<div>
							<label><span class="m5-form-labelv2" id="performerTexAlign"><?php echo lang('Description') ?>:</span></label>
							<?php echo form_textarea('description',set_value('description', (isset($performer->description)) ? $performer->description : null), 'rows="10" cols="90" style="overflow:hidden;" class="m5-inputv2 w-input"')?>
							<span class="error" htmlfor="description" generated="true"><?php echo form_error('description')?></span>
						</div>
						<div>
							<label><span class="m5-form-labelv2" id="performerTexAlign"><?php echo lang('What turns me on') ?>:</span></label>
							<?php echo form_textarea('what_turns_me_on',set_value('what_turns_me_on', (isset($performer->what_turns_me_on)) ? $performer->what_turns_me_on : null), 'rows="10" cols="90" style="overflow:hidden;" class="m5-inputv2 w-input"')?>
							<span class="error" htmlfor="what_turns_me_on" generated="true"><?php echo form_error('what_turns_me_on')?></span>
						</div>
						<div>
							<label><span class="m5-form-labelv2" id="performerTexAlign"><?php echo lang('What turns me off') ?>:</span></label>
							<?php echo form_textarea('what_turns_me_off',set_value('what_turns_me_off', (isset($performer->what_turns_me_off)) ? $performer->what_turns_me_off : null), 'rows="10" cols="90" style="overflow:hidden;" class="m5-inputv2 w-input"')?>
							<span class="error" htmlfor="what_turns_me_off" generated="true"><?php echo form_error('what_turns_me_off')?></span>
						</div>
						<div>
							<label><span class="m5-form-labelv2" id="performerTexAlign"><?php echo lang('Start chat text') ?>:</span></label>
				              <?php echo form_textarea('start_chat_text',set_value('start_chat_text', (isset($performer->start_chat_text)) ? $performer->start_chat_text : null), 'rows="10" cols="90" style="overflow:hidden;" class="m5-inputv2 w-input"')?>
				              <span class="error" htmlfor="start_chat_text" generated="true"><?php echo form_error('start_chat_text')?></span>
				        </div>
				        <div>
							<label><span class="m5-form-labelv2" ><?php echo lang('Birthday') ?>:</span></label>
							<?php echo form_dropdown('day',$days,set_value('day', (isset($performer->birthday)) ?  date('d',$performer->birthday) : null), 'class="m5-inputv2"')?>
							<?php echo form_dropdown('month',$months,set_value('month', (isset($performer->birthday)) ? date('m',$performer->birthday) : null ), 'class="m5-inputv2"')?>
							<?php echo form_dropdown('year',$years,set_value('year', (isset($performer->birthday)) ? date('Y',$performer->birthday) : null ), 'class="m5-inputv2"')?>
							<span class="error" htmlfor="birthday" generated="true"><?php echo form_error('year')?></span>
						</div>
						<div>
							<label><span class="m5-form-labelv2"><?php echo lang('Gender') ?>:</span></label>
							<?php echo form_dropdown('gender',$gender,set_value('gender', (isset($performer->gender)) ? $performer->gender : null),'class="m5-inputv2"',NULL,TRUE)?>
							<span class="error" htmlfor="gender" generated="true"><?php echo form_error('gender')?></span>
						</div>
						<div>
							<label><span class="m5-form-labelv2"><?php echo lang('Ethnicity') ?>:</span></label>
							<?php echo form_dropdown('ethnicity',$ethnicity,set_value('ethnicity', (isset($performer->ethnicity)) ? $performer->ethnicity : null),'class="m5-inputv2"',NULL,TRUE)?>
							<span class="error" htmlfor="ethnicity" generated="true"><?php echo form_error('ethnicity')?></span>
						</div>
						<div>
							<label><span class="m5-form-labelv2"><?php echo lang('Sexual preference') ?>:</span></label>
							<?php echo form_dropdown('sexual_prefference',$sexual_prefference,set_value('sexual_prefference', (isset($performer->sexual_prefference)) ? $performer->sexual_prefference : null),'class="m5-inputv2"',NULL,TRUE)?>
							<span class="error" htmlfor="sexual_prefference" generated="true"><?php echo form_error('sexual_prefference')?></span>
						</div>
						<div>
							<label><span class="m5-form-labelv2"><?php echo lang('Height') ?>:</span></label>
							<?php echo form_dropdown('height',$height,set_value('height', (isset($performer->height)) ? $performer->height : null),'class="m5-inputv2"',NULL,TRUE)?>
							<span class="error" htmlfor="height" generated="true"><?php echo form_error('height')?></span>
						</div>
						<div>
							<label><span class="m5-form-labelv2"><?php echo lang('Weight') ?>:</span></label>
							<?php echo form_dropdown('weight',$weight,set_value('weight', (isset($performer->weight)) ? $performer->weight : null),'class="m5-inputv2"',NULL,TRUE)?>
							<span class="error" htmlfor="weight" generated="true"><?php echo form_error('weight')?></span>
						</div>
						<div>
							<label><span class="m5-form-labelv2"><?php echo lang('Hair color') ?>:</span></label>
							<?php echo form_dropdown('hair_color',$hair_color,set_value('hair_color', (isset($performer->hair_color)) ? $performer->hair_color : null),'class="m5-inputv2"',NULL,TRUE)?>
							<span class="error" htmlfor="hair_color" generated="true"><?php echo form_error('hair_color')?></span>
						</div>
						<div>
							<label><span class="m5-form-labelv2"><?php echo lang('Hair length') ?>:</span></label>
							<?php echo form_dropdown('hair_length',$hair_length,set_value('hair_length', (isset($performer->hair_length)) ? $performer->hair_length : null ),'class="m5-inputv2"',NULL,TRUE)?>
							<span class="error" htmlfor="hair_length" generated="true"><?php echo form_error('hair_length')?></span>
						</div>
						<div>
							<label><span class="m5-form-labelv2"><?php echo lang('Eye color') ?>:</span></label>
							<?php echo form_dropdown('eye_color',$eye_color,set_value('eye_color', (isset($performer->eye_color)) ? $performer->eye_color : null),'class="m5-inputv2"',NULL,TRUE)?>
							<span class="error" htmlfor="eye_color" generated="true"><?php echo form_error('eye_color')?></span>
						</div>
						<div>
							<label><span class="m5-form-labelv2"><?php echo lang('Cup size') ?>:</span></label>
							<?php echo form_dropdown('cup_size',$cup_size,set_value('cup_size',(isset($performer->cup_size)) ? $performer->cup_size : null),'class="m5-inputv2"',NULL,TRUE)?>
							<span class="error" htmlfor="cup_size" generated="true"><?php echo form_error('cup_size')?></span>
						</div>
						<div>
							<label><span class="m5-form-labelv2"><?php echo lang('Build') ?>:</span></label>
							<?php echo form_dropdown('build',$build,set_value('build', (isset($performer->build)) ? $performer->build : null),'class="m5-inputv2"',NULL,TRUE)?>
							<span class="error" htmlfor="build" generated="true"><?php echo form_error('build')?></span>
						</div>
						<div>
							<?php echo form_submit('submit', lang('Save'), 'class="submit-button-2 w-button"')?>
						</div>
					<?php echo form_close()?>
			</div>
			<div class="clear"></div>
		</div>
	</div>
