<script type="text/javascript">
    function confirm_delete() {
        if (confirm("<?php echo lang('Are you sure you want to delete this photo?')?>")) {
            window.location.replace('<?php echo site_url('photos/delete/'.$photo->photo_id)?>');
        }
    }
</script>
<div class="content edit-photos">
  <div class="white performers-details-titles">
    <h4><?php echo lang('Edit Photo') ?></h4>
  </div>
	<div id="photo">
		<?php echo form_open(current_url())?>
		<div class="m5-signup-form w-form">
			<div>
				<label><span class="m5-form-labe"><?php echo lang('Photo title') ?>:</span></label>
				<?php echo form_input('title', $photo->title, 'class="m5-inputv2 w-input"')?>
				<span class="error" htmlfor="title" generated="true" ><?php echo form_error('title')?></span>
			</div>
			<div>
				<label><span class="m5-form-labe"><?php echo lang('Gallery') ?>:</span></label>
				<?php echo form_dropdown('is_paid',$is_paid,set_value('is_paid',$photo->is_paid), 'class="m5-inputv2 w-input"')?>
				<span class="error" htmlfor="title" generated="true" ><?php echo form_error('is_paid')?></span>
			</div>
			<div>
				<?php echo form_submit('submit', lang('Save'),'class="submit-button-2 w-button"')?>
				<?php echo form_button(array('id' => 'delete-bttn', 'class'=>'delete-button', 'content' => 'Delete', 'onclick' => 'confirm_delete()'))?>
			</div>
			<?php echo form_close()?>
		</div>
	</div>
</div>
