<script type="text/javascript">
    function confirm_delete(id) {
        if (confirm("<?php echo lang('Are you sure you want to delete this photo?')?>")) {
            window.location.replace('<?php echo site_url('photos/delete')?>/'+id);
        }
    }

	function set_avatar(elem, photo_name, photo_id){
		$.ajax({
            url: "<?php echo site_url('photos/set_avatar')?>",
            type: 'post',
            dataType: "json",
            data: {'photo_name': photo_name, 'photo_id':photo_id, 'ci_csrf_token': '<?php echo $this->security->_csrf_hash?>'},
            success: function(response) {
				if(response.success){
					$(elem).find('img').attr('src', '<?php echo assets_url()?>images/icons/approved.png');
					$(elem).attr('title', '<?php echo lang('Avatar was set!')?>');
				}else{
					$(elem).find('img').attr('src', '<?php echo assets_url()?>images/icons/rejected.png');
					$(elem).attr('title', response.message);
				}
            }
        });
	}

	$(document).ready(function(){

		$('.photo').hover(
			function(){
				$(this).find('.buttons').css('display', 'block');
			},
			function(){
				$(this).find('.buttons').css('display', 'none');
			}
		)
	});


</script>
<style media="screen">
  #photos h2{
  	color: #fff;
  }
	.photo{
		float: left;
		margin: 10px;
		color: #fff;
		min-height: 155px;
	}
	.photo p, .photo span{
		text-align: center;
	}
	.photo .m5-perf-image{
		margin-bottom: 5px;
	}
</style>
	<div class="content">
    <div class="white performers-details-titles">
			<h4><?php echo lang('My Photos') ?></h4>
			<p><?php echo lang('You can edit your photos here.') ?></p>
		</div>
		<a href="<?php echo site_url('photos/add')?>" style="float:right" class="decoration-none"><?php echo form_button('add', lang('Add Photo'),'class="submit-button-2 w-button"')?></a>
        <div class="clear"></div>
		<div id="photos" style="width: 100%; margin:0px auto; text-align: center;">
			<h4 class="white" ><?php echo lang('Free photos')?></h4>
            <?php foreach($photos as $photo):?>
                <div class="photo">
                    <a class="thumb" data-lightbox="roadtrip" href="<?php echo main_url('uploads/performers/' . $this->user->id . '/' . $photo->name_on_disk)?>" rel="free_gallery"><img src="<?php echo main_url('/uploads/performers/' . $this->user->id . '/small/' . $photo->name_on_disk)?>" /></a>
                    <br>
                    <span><?php echo $photo->title?></span>
                    <span><a class="white" href="<?php echo site_url('photos/edit/'.$photo->photo_id)?>">Edit</a></span>
                </div>
            <?php endforeach?>
            <?php if(sizeof($paid_photos) > 0):?>
            	<div style="clear:both"></div>
				      <h4 class="white" ><?php echo lang('Paid photos')?></h4>
	            <?php foreach($paid_photos as $photo):?>
      					<div class="photo">
        				    <a href="<?php echo main_url('photo/' . $photo->photo_id)?>" rel="paid_gallery">
        				        <img src="<?php echo main_url('photo/thumb/' . $photo->photo_id)?>"/>
        						 </a>
                     <br>
        				     <span><?php echo $photo->title?></span>
      			    </div>
	            <?php endforeach?>
			<?php endif?>
		</div>
        <div class="clear"></div>
	</div>
