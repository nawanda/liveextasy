	<div class="content">
		<div class="white performers-details-titles">
	    <h4><?php echo lang('Edit Video') ?></h4>
	  </div>
		<script type="text/javascript">
			jQuery(function($){
				if($('#type').val() == 1){
					$('#price').show();
				} else {
					$('#price').hide();
				}

				$('#type').change(function(){
					if($('#type').val() == 1){
						$('#price').show();
					} else {
						$('#price').hide();
					}
				});
			});
		</script>
		<div id="photo">
            <?php echo form_open()?>
			<div class="m5-signup-form w-form">
				<div>
					<label><span class="white"><?php echo lang('Description') ?></span></label>
					<?php echo form_input('description', $video->description)?>
					<span class="error" htmlfor="title" generated="true" ><?php echo form_error('description')?></span>
				</div>
				<div>
					<label><span class="white"><?php  echo lang('Type') ?></span></label>
					<?php echo form_dropdown('type',$types,set_value('type',$video->is_paid),'id="type"')?>
					<span class="error" htmlfor="title" generated="true" ><?php echo form_error('type')?></span>
				</div>
				<div style="display:none" id="price">
					<label><span class="white"><?php  echo lang('Price') ?></span></label>
					<?php echo form_input('price',set_value('price',$video->price))?>
					<span class="error" htmlfor="title" generated="true" ><?php echo form_error('price')?></span>
				</div>
				<div>
					<?php echo form_submit('submit', lang('Save'),'class="submit-button-2 w-button"')?>
				</div>
                <?php echo form_close()?>
            </div>
        </div>
    </div>
