<div id="main_nav">

	<ul>
		<li><a href="<?=site_url('')?>" class="white <?=(!$this->uri->segment(1)) ? 'selected' : NULL?>"><?=lang('My Account')?></a></li>
		<li><a href="<?=site_url('broadcast')?>" class="white <?=($this->uri->segment(1) == 'broadcast') ? 'selected' : NULL?>" ><?=lang('Go Online')?></a></li>
		<li><a href="<?=site_url('settings')?>" class="white <?=($this->uri->segment(1) == 'settings') ? 'selected' : NULL?>"><?=lang('My Settings')?></a></li>
		<li><a href="<?=site_url('my_payments')?>" class="white <?=($this->uri->segment(1) == 'my_payments') ? 'selected' : NULL?>"><?=lang('My Payments')?></a></li>
		<li><a href="<?=site_url('messenger')?>" class="white <?=($this->uri->segment(1) == 'messenger') ? 'selected' : NULL?>"><?=lang('Messenger')?></a></li>
	</ul>
</div>
