<div id="left_menu">
	<div class="menu_item <?=($this->uri->segment(1) == 'my_earnings') ? 'selected' : NULL ?>" onclick="document.location.href='<?php echo site_url('my_earnings')?>';">
		<span class="icon"></span>
		<a href="<?php echo site_url('my_earnings')?>"><?php echo lang('Earnigns') ?></a>
	</div>
	<div class="menu_item <?=($this->uri->segment(1) == 'my_payments') ? 'selected' : NULL ?>" onclick="document.location.href='<?php echo site_url('my_payments')?>';">
		<span class="icon"></span>
		<a href="<?php echo site_url('my_payments')?>"><?php echo lang('Payments') ?></a>
	</div>
</div>
<div class="fl_r">
	<?php
		if(isset($page)){
			$this->load->view($page);
		}
	?>
</div>
<div class="clear"></div>