	<div class="content">
		<div class="title">
			<?php echo lang('Performer\'s Pricing Page')?>
		</div>
		<div id="profile">
			<?php echo form_open()?>
			<div class="dark_gray italic">
				<div>
					<label><span class="bold"><?php echo sprintf(lang('True private chat %s/minute'), SETTINGS_SHOWN_CURRENCY); ?></span></label>
					<?php echo form_input('true_private_chips_price',set_value('true_private_chips_price', $true_private_chips_price))?>
					<span class="error" htmlfor="private_chips_price" generated="true"><?php echo form_error('true_private_chips_price')?form_error('true_private_chips_price'):($this->input->post()?NULL:sprintf(lang('Must be between %s as %s'),MIN_TRUE_PRIVATE_CHIPS_PRICE,MAX_TRUE_PRIVATE_CHIPS_PRICE))?></span>
				</div>						
				<div>
					<label><span class="bold"><?php echo sprintf(lang('Private chat %s/minute'), SETTINGS_SHOWN_CURRENCY); ?></span></label>
					<?php echo form_input('private_chips_price',set_value('private_chips_price', $private_chips_price))?>
					<span class="error" htmlfor="private_chips_price" generated="true"><?php echo form_error('private_chips_price')?form_error('private_chips_price'):($this->input->post()?NULL:sprintf(lang('Must be between %s as %s'),MIN_PRIVATE_CHIPS_PRICE,MAX_PRIVATE_CHIPS_PRICE))?></span>
				</div>
				<div>
					<label><span class="bold"><?php echo sprintf(lang('Peek chat %s/minute'), SETTINGS_SHOWN_CURRENCY); ?></span></label>
					<?php echo form_input('peek_chips_price',set_value('peek_chips_price', $peek_chips_price))?>
					<span class="error" htmlfor="peek_chips_price" generated="true"><?php echo form_error('peek_chips_price')?form_error('peek_chips_price'):($this->input->post()?NULL:sprintf(lang('Must be between %s as %s'),MIN_PEEK_CHIPS_PRICE,MAX_PEEK_CHIPS_PRICE))?></span>
				</div>						
				<div>
					<label><span class="bold"><?php echo sprintf(lang('Nude chat %s/minute'), SETTINGS_SHOWN_CURRENCY); ?></span></label>
					<?php echo form_input('nude_chips_price',set_value('nude_chips_price',$nude_chips_price))?>
					<span class="error" htmlfor="nude_chips_price" generated="true"><?php echo form_error('nude_chips_price')?form_error('nude_chips_price'):($this->input->post()?NULL:sprintf(lang('Must be between %s as %s'),MIN_NUDE_CHIPS_PRICE,MAX_NUDE_CHIPS_PRICE))?></span>
				</div>
				<div>
					<label><span class="bold"><?php echo lang('Paid photo gallery price') ?></span></label>
					<?php echo form_input('paid_photo_gallery_price',set_value('paid_photo_gallery_price',$paid_photo_gallery_price))?>
					<span class="error" htmlfor="paid_photo_gallery_price" generated="true"><?php echo form_error('paid_photo_gallery_price')?form_error('paid_photo_gallery_price'):($this->input->post()?NULL:sprintf(lang('Must be between %s as %s'),MIN_PHOTOS_CHIPS_PRICE,MAX_PHOTOS_CHIPS_PRICE))?></span>
				</div>						
				<div>
					<?php echo form_submit('submit', lang('Save'), 'class="red"')?>
				</div>			
			<?php echo form_close()?>
			</div>
		</div>
		<div class="clear"></div>
	</div>