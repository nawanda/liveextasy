<script type="text/javascript">
var contract = <?php echo $contract?'true':'false'?>;
var photo_id = <?php echo $photo_id?'true':'false'?>;


jQuery(function($){

	$.validator.setDefaults({
		validClass:"success",
		errorElement: "span",
		errorPlacement: function(error, element) {
			error.appendTo($(element).next('span').next('span'));
		}
	});
		
		
	$.validator.addMethod("hasContract", function() {		
		return true;
	}, "<?php echo lang('Please upload your contract!') ?>!");

	$.validator.addMethod("hasPhoto", function(value,element) {		
		if( $('#photos_id .qq-upload-list').children('li:visible').length > 0 && $('#photos_id .qq-upload-list').children('li:visible').length < 4){
			return true;
		} else {
			return false;
		}
	}, "<?php echo lang('Please upload your photo ID!') ?>!");	
	
	$.validator.addMethod("uniqueUserName", function(value,element) {
		return true;
	}, "<?php echo lang('Not Available') ?>!");
	
	$(".register_performer").validate({		 				
		success: function(label) {
	    	label.addClass("valid")
	   },
		rules: {
			username: {
				required: true,
				minlength: 3,
				maxlength: 25,
				uniqueUserName: true
			},
			nickname: {
				required: true,
				minlength: 3,
				maxlength: 25
			},
			password:{
				required: true,
				minlength: 5
			},
			email: {
				required: true,
				email: true
			},
			firstname: {
				required: true,
				minlength: 3,
				maxlength: 90
			},
			lastname: {
				required: true,
				minlength: 3,
				maxlength: 90
			},
			address: {
				required: true,
				minlength: 3,
				maxlength: 100
			},
			city: {
				required: true,
				minlength: 3,
				maxlength: 60
			},
			zip: {
				required: true,
				minlength: 3,
				maxlength: 10
			},
			state: {
				required: true,
				minlength: 3
			},
			country: {
				required: true
			},
			photo_id:{
				hasPhoto: true
			},
			phone: {
				required: true,
				minlength: 3
			},			
			tos: {
				required: true,
				minlength: 1,
				maxlength: 3
			}
		}, 
		messages: {
			name: 						"<?php echo lang('Please enter your name') ?>",
			nickname: 					"<?php echo lang('Please enter a nickname') ?>",
			email: 						"<?php echo lang('Please enter a valid email address') ?>",
			password: 					"<?php echo lang('Password must have at least 3 characters') ?>",
			firstname: 					"<?php echo lang('Please enter a valid first name') ?>",
			lastname: 					"<?php echo lang('Please enter a valid last name') ?>",
			address: 					"<?php echo lang('Please enter a valid address') ?>",
			city: 						"<?php echo lang('Please enter a valid city') ?>",
			zip: 						"<?php echo lang('Please enter a valid zip') ?>",
			state: 						"<?php echo lang('Please enter a valid state') ?>",
			country: 					"<?php echo lang('Please enter a valid country') ?>",
			photo_id:					"<?php echo lang('Please upload your photo ID') ?>",
			phone: 						"<?php echo lang('Please enter a valid phone') ?>",
			tos: 						"<?php echo lang('Please agree with the terms and conditions') ?>"
		}
	});
});
</script>
<div class="content">
	<div class="title">
		<?php echo lang('Signup step 1 - Personal Information') ?>
	</div>
	<?php echo form_open_multipart('register', 'class="register_performer"') ?>
	<div class="dark_gray italic" id="register_performer">
		<div>
			<label><span class="bold"><?php echo lang('Username') ?>:</span></label>
			<?php echo form_input('username',set_value('username'))?>
			<span class="error" htmlfor="username" generated="true" ><?php echo form_error('username')?></span>
		</div>
		<div>
			<label><span class="bold"><?php echo lang('Nickname') ?>:</span></label>
			<?php echo form_input('nickname',set_value('nickname'))?>
			<span class="error" htmlfor="nickname" generated="true"><?php echo form_error('nickname')?></span>
		</div>
		<div>
			<label><span class="bold"><?php echo lang('Password') ?>:</span></label>
			<?php echo form_password('password',set_value('password'))?>
			<span class="error" htmlfor="password" generated="true"><?php echo form_error('password')?></span>
		</div>
		<div>
			<label><span class="bold"><?php echo lang('Email') ?>:</span></label>
			<?php echo form_input('email',set_value('email'))?>
			<span class="error" htmlfor="email" generated="true"><?php echo form_error('email')?></span>
		</div>
		<div>
			<label><span class="bold"><?php echo lang('First name') ?>:</span></label>
			<?php echo form_input('firstname',set_value('firstname'))?>
			<span class="error" htmlfor="firstname" generated="true"><?php echo form_error('firstname')?></span>
		</div>
		<div>
			<label><span class="bold"><?php echo lang('Last name') ?>:</span></label>
			<?php echo form_input('lastname',set_value('lastname'))?>
			<span class="error" htmlfor="lastname" generated="true"><?php echo form_error('lastname')?></span>
		</div>
		<div>
			<label><span class="bold"><?php echo lang('Address') ?>:</span></label>
			<?php echo form_input('address',set_value('address'))?>
			<span class="error" htmlfor="address" generated="true"><?php echo form_error('address')?></span>
		</div>
		<div>
			<label><span class="bold"><?php echo lang('City') ?>:</span></label>
			<?php echo form_input('city',set_value('city'))?>
			<span class="error" htmlfor="city" generated="true"><?php echo form_error('city')?></span>
		</div>
		<div>
			<label><span class="bold"><?php echo lang('Zip') ?>:</span></label>
			<?php echo form_input('zip',set_value('zip'))?>
			<span class="error" htmlfor="zip" generated="true"><?php echo form_error('zip')?></span>
		</div>
		
		<script type="text/javascript">
			$(function(){
				if($('#country').val() == 'US'){
					$('#state').show();
				}			
				
				$('#country').change(function(){
						if($('#country').val() == 'US'){
							$('#state').slideDown();
							$('input[name=state]').val('');
						} else {
							$('#state').slideUp();
							$('input[name=state]').val('state');						
						}
				});
			});
		</script>				
		<div>
			<label><span class="bold"><?php echo lang('Country') ?>:</span></label>
			<?php echo form_dropdown('country', $countries, set_value('country'),'id="country"')?>
			<span class="error" htmlfor="country" generated="true"><?php echo form_error('country')?></span>
		</div>
		<div id="state" style="display:none">
			<label><span class="bold"><?php echo lang('State') ?>:</span></label>
			<?php echo form_input('state',set_value('state'))?>
			<span class="error" htmlfor="state" generated="true"><?php echo form_error('state')?></span>
		</div>				
		<div>
			<label><span class="bold"><?php echo lang('Phone') ?>:</span></label>
			<?php echo form_input('phone',set_value('phone'))?>
			<span class="error " htmlfor="phone" generated="true"><?php echo form_error('phone')?></span>
		</div>
            <script src="<?php echo assets_url()?>js/fileuploader.js" type="text/javascript"></script>
            <link href="<?php echo assets_url()?>css/fileuploader.css" rel="stylesheet" type="text/css">			   							
	    	    
		<div id="photos_id">
				<?php echo form_hidden('photo_id',1)?>
				<label  style="vertical-align: top;"><span class="bold"><?php echo lang('Photo ID') ?>:</span></label>
				<a title="<?php echo lang('You can upload jpg/png/pdf/zip files')?>"><span id="upload-photo_id" style="display: inline-block; width:300px;" class="uploader"></span></a>
				<br/>						
				<span style="display: inline-block; width:300px;margin-left: 202px;" id="upload-photo_id" class="uploader">
					<?php if($photo_id):?>								
						<div class="qq-uploader">
							<?php foreach($photo_id as $key => $value):?>
								<ul class="qq-upload-list">
									<li class="qq-upload-success" key="<?php echo $key?>">
										<span class="qq-upload-file-loader" style="width: 100%;"></span>
										<span class="qq-upload-file"><?php echo $value['name']?></span>
										<span class="qq-upload-size" style="display: inline;"><?php echo $value['size']?></span>
										<span class="qq-upload-failed-text"></span>
										<span class="icon qq-upload-delete" onclick="delete_file(this)"></span>
									</li>
								</ul>
							<?php endforeach?>							
						</div>
					<?php endif?>
				</span>
				<span class="error" htmlfor="photo_id" generated="true" style="left: 586px; position: absolute; top: 0;"><?php echo form_error('photo_id')?></span>
		    
		</div>				   							
	    <script type="text/javascript">
	    	jQuery(function($){
	    		createUploader_Photo();
	    	});
	        function createUploader_Photo(){				         
	            var uploader = new qq.FileUploader({
	                element: document.getElementById('upload-photo_id'),
	                action: '<?php echo main_url('upload')?>',
		                params: {
		                    type: 'photo_id'
		                },					                
                        name: 'userfile[]',
                        multiple: true,					                
		                allowedExtensions: [], 
		                onComplete: function(id, fileName, responseJSON){ 
							if(responseJSON.success){
								photo_id = true;
								$('#photos_id .qq-upload-list li').eq(id).attr('key', responseJSON.key);
							} 
			            },
		                onSubmit: function(id, fileName){
							 $('#photos_id .qq-upload-button-error').html('');
							 
							 var nr_of_files = $('#photos_id .qq-upload-list').children('li:visible').length;
							 
							 if(nr_of_files > 1){
								 $('#photos_id .qq-upload-list').append('<li style="display:none"></li>');
								 $('#photos_id .qq-upload-button-error').html("<?php echo lang('You are allowed to upload max 2 files!') ?>");
							 return false;
						 }
					},
	                onProgress: function(id, fileName, loaded, total){
						$('#photos_id .qq-upload-list li').eq(id).find('.qq-upload-file-loader').css('width', Math.round(loaded / total * 100) + '%');
					},
	                onCancel: function(id, fileName){
						//alert("cancel");
		            },	
	                
					showMessage: function( message, id){ 
							 if(id == undefined){
								 $('#photos_id .qq-upload-button-error').html(message);
							 }else{
								$('#photos_id .qq-upload-list li').eq(id).find('.qq-upload-size').html(message);
							 }									  
						},
	                			                					                
	                debug: false
	            });           
	        }
			   
			function delete_file(item){						
				var key = $(item).parent('li').attr('key');
				var item_file = $(item).parent('li');
				var type =  $(item).parent().parent().parent().parent().parent().children('.uploader').attr('id');
				if(item_file.attr('class') == ' qq-upload-fail'){
					item_file.css('display', 'none');
					 $('.qq-upload-button-error').html('');
				}else{
					$.ajax({
						type: "POST",
						url: "<?php echo main_url('upload/delete_file')?>",
						data: "type="+type+"&file_key="+key+"&ci_csrf_token=<?php echo $this->security->_csrf_hash?>",
						
						// If successfully request
						success: function(msg){
							if(msg == 'deleted') {												
								item_file.css('display', 'none');
								$('.qq-upload-button-error').html('');
							} 
						},

						// If failed request
						error: function(){
							alert("<?php echo lang('Error please try again') ?>");
						}
					});
					// End ajax												
				}																
			}					
	    </script>				    
<div id="contracts">
        <?php echo form_hidden('contract',1)?>
        <label style="vertical-align: top;">
            <span class="bold" style="position:relative;top:-7px">
                <?php echo lang('Download contract')?>:
            </span>
        </label>
    <a href="<?php echo main_url('uploads/stuff/sample_contract.pdf')?>" target="_blank">
                
        <span class="icon download_red"  title="<?php echo lang('Download contract')?>" ></span> </a>
    </div>	    
    <div >&nbsp;</div>
    <div class="sigPad">
        <label for="name" class="bold dark_gray">Print your name</label>
        <input type="text" name="name" id="contract_owner_name" />
        <input type="hidden" name="contract_signature" id="contract_signature"  />
        <input type="hidden" name="contract_signature_base64" id="contract_signature_base64"  />
        <input type="hidden" name="contract_typed" id="contract_typed_base64" />
        <input type="hidden" name="contract_signature_typed" id="contract_signature_typed" />
        <div></div>
        <label class="typeItDesc" class="bold dark_gray">Review your signature</label>
        <label class="drawItDesc" class="bold dark_gray">Or draw your signature</label>
        
        <div style="margin: 3px 0;width: 212px;display: inline-block;">
            <ul class="sigNav">
                <li class="typeIt"><a href="#type-it" class="current">Type It</a></li>
                <li class="drawIt"><a href="#draw-it" >Draw It</a></li>
                <li class="clearButton"><a href="#clear">Clear</a></li>
            </ul>

            <div class="sig sigWrapper">
                <div class="typed"></div>
                
                <canvas class="pad" width="212" height="55"></canvas>
            </div>
        </div>
    </div>
    <script src="<?= assets_url()?>js/jquery.signaturepad.js"></script>
    <script>
    $(document).ready(function() {
      var $signature = $('.sigPad').signaturePad({
                            name: '#contract_owner_name',
                            output: '#contract_signature',
                            onDrawEnd: function(){ update_signature();},
                        });
       function update_signature() {
           $('#contract_signature_base64').val($signature.getSignatureImage());
       }
       
       $('input.red').click(function(e){
           e.preventDefault();
           $signature.clearCanvas();
           var context = $('canvas.pad').get(0).getContext("2d");
           context.font = 'normal 1.875em "Journal"';
           context.fillStyle = '#145394';
           context.fillText($('#contract_owner_name').val(), 5, 34);
           
           $('#contract_typed_base64').val($('canvas.pad').get(0).toDataURL("image/png")); 
           $('#contract_signature_typed').val($('.sigNav li a.current').parent().attr('class'));
           
           $(this).unbind('click').click();
       });
      
    });
  </script>
		<div>
			<label></label>
			<?php echo form_checkbox('tos', 'tos',set_value('tos'))?><span class="bold" id="tos"><a href="<?php echo main_url('documents/tos')?>" target="_blank"><?php echo lang('I agree the Terms of Service')?></a></span>
			<span class="error" htmlfor="tos" generated="true"><?php echo form_error('tos')?></span>
		</div>
		<div>
			<?php echo form_submit('submit', lang('Continue'), 'class="red"')?>
		</div>
		
		
		<div class="clear"></div>
	</div>
</div>