	<div class="content">
		<div class="title">
			<?php echo lang('Edit Video') ?>
		</div>
		<script type="text/javascript">
			jQuery(function($){
				if($('#type').val() == 1){
					$('#price').show();
				} else {
					$('#price').hide();						
				}

				$('#type').change(function(){
					if($('#type').val() == 1){
						$('#price').show();
					} else {
						$('#price').hide();						
					}						
				});
			});
		</script>
		<div id="photo">
            <?php echo form_open()?>
			<div class="dark_gray italic">
				<div>
					<label><span class="bold"><?php echo lang('Description') ?></span></label>
					<?php echo form_input('description', $video->description)?>
					<span class="error" htmlfor="title" generated="true" ><?php echo form_error('description')?></span>
				</div>
				<div>
					<label><span class="bold"><?php  echo lang('Type') ?></span></label>
					<?php echo form_dropdown('type',$types,set_value('type',$video->is_paid),'id="type"')?>
					<span class="error" htmlfor="title" generated="true" ><?php echo form_error('type')?></span>
				</div>
				<div style="display:none" id="price">
					<label><span class="bold"><?php  echo lang('Price') ?></span></label>
					<?php echo form_input('price',set_value('price',$video->price))?>
					<span class="error" htmlfor="title" generated="true" ><?php echo form_error('price')?></span>
				</div>
				<div>
					<?php echo form_submit('submit', lang('Save'),'class="red"')?>
				</div>
                <?php echo form_close()?>
            </div>
        </div>
    </div>