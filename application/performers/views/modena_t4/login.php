<div class="content">
	<div id="login">
		<div class="login_box">
			<div class="title">
				<?php echo lang('Performer Area')?>
			</div>	
			<div class="dark_gray">
				<?php echo form_open('')?>
					<div class="b_sep_line">
						<span class="italic"><?php echo lang('Your name') ?>*:</span><br/>
						<?php echo form_input('username', set_value('username'), 'style="width:275px;" onfocus="if(this.value == \''.lang('username').'\') { this.value = \'\' }" onblur="if(this.value.length == 0) { this.value = \''.lang('username').'\' }"')?>
						<span class="italic"><?php echo form_error('username')?></span>
					</div>
					<div class="b_sep_line">
						<span class="italic"><?php echo lang('Your password') ?>*:</span><br/>
						<?php echo form_password('password', set_value('password'), 'style="width:275px;" onfocus="if(this.value == \''.lang('password').'\') { this.value = \'\' }" onblur="if(this.value.length == 0) { this.value = \''.lang('password').'\' }"')?>
						<span class="italic"><?php echo form_error('password')?></span>
					</div>

					<div style="margin-top:8px; text-align: center;">
						<button class="red"  type="submit" style="width:285px;" ><?php echo lang('Login as performer') ?></button><br/>
						<br/>
						<a href="<?php echo site_url('forgot_password')?>" class="red f13 forgot_password" ><?php echo lang('Forgot Password?') ?></a>
						&nbsp;&nbsp;|&nbsp;&nbsp;
						<a href="<?php echo site_url('register')?>" class="red f13" ><?php echo lang('Register') ?></a>
					</div>
				<?php echo form_close('')?>
			</div>
		</div>
	</div>

	<div class="clear"></div>
</div>