	<div class="content">
		<div class="title">
			<?php echo lang('Performer\'s Banned Locations Page') ?>
		</div>
		<div id="profile">		
			<div class="dark_gray italic">
			<?php echo form_open('')?>
				<ul style="list-style-type:none; width: 270px; float:left; overflow-y: scroll; height: 500px;" >
					<?php if(form_error('countries[]')):?>
						<span class="error" htmlfor="languages" generated="true" style="vertical-align:top"><?php echo form_error('countries[]')?></span>
					<?php endif?>					
					<p><?php echo lang('Countries') ?>:</p>
					<?php foreach ($countries as $key => $value):?>
						<li>
							<?php echo form_checkbox('countries[]', $key,set_checkbox('countries', $key,(in_array($key,$banned_countries)?TRUE:FALSE)))?>
							<span class="bold"><?php echo $value?></span>
						</li>
					<?php endforeach;?>
				</ul>				
				<ul style="list-style-type:none; width: 270px; float:left;overflow-y: scroll; height: 500px;" >
					<?php if(form_error('states[]')):?>
						<span class="error" htmlfor="states" generated="true" style="vertical-align:top"><?php echo form_error('states[]')?></span>
					<?php endif?>										
					<p><?php echo lang('States') ?>:</p>
					<?php foreach ($states as $key => $value):?>
						<li>
							<?php echo form_checkbox('states[]', $key,set_checkbox('states', $key,(in_array($key,$banned_states)?TRUE:FALSE)))?>
							<span class="bold"><?php echo $value?></span>
						</li>
					<?php endforeach;?>
				</ul>
				<div class="clear"></div>	
				<div>
					<?php echo form_submit('submit', lang('Save'), 'class="red"')?>
				</div>			
				<?php echo form_close('')?>
			</div>
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
	</div>
