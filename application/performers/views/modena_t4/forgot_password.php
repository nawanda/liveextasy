<div >
	<div style="padding:20px;">
			<div class="title">
				<?PHP echo lang('Forgot password?')?>
			</div>				
		<?php echo form_open('')?>
			<div>
				<span class="italic dark_gray"><?php echo lang('User name')?>*:</span><br/>
				<?php echo form_input('username', set_value('username'), 'style="width:275px;"')?>
				<span class="italic red"><?php echo form_error('username', ' ', ' ')?></span>
			</div>
		<br/>
			<div>
				<span class="italic dark_gray"><?php echo lang('Email address')?>*:</span><br/>
				<?php echo form_input('email', set_value('email'), 'style="width:275px;"')?>
				<span class="italic red"><?php echo form_error('email', ' ', ' ')?></span>
			</div>

			<div style="margin-top:8px; text-align: center;">
				<button class="red"  type="submit" style="width:130px; display: inline-block;"> <?php echo lang('Submit')?> </button>
			</div>
		<?php echo form_close('')?>
	</div>
</div>