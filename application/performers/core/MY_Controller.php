<?php

/**
 * General controller
 * @author Andrei
 *
 */
class MY_Controller extends CI_Controller {

    public $user;

    /**
     * Construieste obiectul $user
     * @return unknown_type
     */
    function __construct() {
        parent::__construct();
        $this->user = $this->access->get_account('performer');

        if (SETTINGS_DEBUG) {
            $this->output->enable_profiler();
        }
    }

}

/**
 * Controllerul pentru register performer
 * @author Andrei
 *
 */
class MY_Registration extends CI_Controller {

    public $register_user;
    public $step;
    public $user;

    /**
     * Constructorul
     * @return unknown_type
     */
    function __construct() {
        parent::__construct();

        $this->user = $this->access->get_account('performer');

        //nu las performerii inregistrati sa isi faca alt cont
        $this->access->restrict('logged_out');

        $register = $this->session->userdata('register');

        //nu are in sessiune nimic
        if (!$register) {
            $this->step = 'step1';
        } else {

            $this->load->model('performers');

            $performer = $this->performers->get_one_by_id($register['performer_id']);

            //performerul a fost sters/rejectat -> trimitem performeru la step1 back
            if (!$performer || $performer->status == 'rejected') {
                $this->session->unset_userdata('register');
                redirect('register');
            }

            $this->register_user = $performer;

            $this->step = 'step' . $register['step'];
        }
    }

}

/**
 * Users controller
 * @author Andrei
 *
 */
class MY_Performer extends MY_Controller {

    /**
     * Restrictioneaza accessul catre controllerul de users
     * @return null
     */
    function __construct() {
        parent::__construct();
        $this->access->restrict('performers');
        $this->load->model('messages');
        $this->user->unread_messages = $this->messages->get_all_received_by_user_id($this->user->id, $this->user->type, FALSE, FALSE, TRUE, TRUE);
    }

}

/**
 * Affiliate controller
 * @author	AgLiAn
 *
 */
class MY_Affiliate extends MY_Controller {

    /**
     * Restrictioneaza accessul catre controllerul de affiliates
     */
    function __construct() {
        parent::__construct();
        $this->access->restrict('affiliates');
    }

}

class MY_FMS_Free extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

}

/**
 * Controllerul de FMS
 * @author Andrei
 *
 */
class MY_FMS extends MY_FMS_Free {

    function __construct() {
        parent::__construct();
        $hash = $this->input->post('hash');
        if (defined('FMS_SECRET_HASH') && $hash !== FMS_SECRET_HASH) {
            write_request(APPPATH . 'logs/fms/denys.txt', 'status=deny&log=bad_secret_hash', true, true);
        }
    }
}